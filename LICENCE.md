# Licence

Creative common BY-NC-SA 
https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode

Authors are open to discuss for commercial usage 

# Contributors

Oscar Tellez oskarts@gmail.com
Samuel Vercraene samuel.vercraene@insa-lyon.fr

# Librairies used

## ALNS Framework

All files starting with 
"The Original Code is the ALNS_Framework library." 
are originaly based on the ALNS framework : 
https://code.google.com/archive/p/alns-framework/
The main contributor of this framework is Renaud Masson. 
The original framework is published with licence 
GNU Lesser GPL : http://www.gnu.org/licenses/lgpl.html
We have a fair agreament to remove the Copyright and 
change the licence

## JSON for Modern C++
    __ _____ _____ _____
 __|  |   __|     |   | |  JSON for Modern C++
|  |  |__   |  |  | | | |  version 2.1.1
|_____|_____|_____|_|___|  https://github.com/nlohmann/json

Licensed under the MIT License <http://opensource.org/licenses/MIT>.
Copyright (c) 2013-2017 Niels Lohmann <http://nlohmann.me>.