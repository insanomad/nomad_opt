

#include <boost/filesystem.hpp>
#include "unitTests/Test.h"


using namespace std;

// for classic darp instances set on bitSet and for QuAndBard set it off.

void benchmark(string name, string path_output, int timeLimit, string file_instance,string file_distances,string file_times,string file_vehicles,int nb_reconfigurations,int inst_type, int nb_replica, int nb_iters, int nb_iters_scp);

int main(int argc, char* argv[])
{
	if(argc>1)
	{
		cout<<argc<<endl;
		string file_instance =argv[1];
		string file_distances =argv[2];
		string file_times =argv[3];
		string file_vehicles =argv[4];
		int inst_type = atoi(argv[5]); // 0 Cordeau;  3 Gihp 2 Qu&Bard
		int nb_instance= atoi(argv[6]);
		int nb_replica = atoi(argv[7]); // 1
		int nb_reconfigurations = atoi(argv[8]);   // 0,1,...,1000
		cst::QuAndBard=atoi(argv[9]);
		cst::BitSet= atoi(argv[10]);
		cst::maxNbReconfigurations=atoi(argv[11]);
		bool unused=atoi(argv[12]);
		cst::SortVehicle=atoi(argv[13]);
		cst::SCP=atoi(argv[14]);
		unused=atoi(argv[15]);
		int nb_iters=atoi(argv[16]);
		int nb_iters_scp=atoi(argv[17]);
		int timeLimit=atoi(argv[18]);
		string path_output=argv[19];
		cst::ASCP2=atoi(argv[20]);
		unused=atoi(argv[21]);
		cst::allOperators=atoi(argv[22]);
		cst::ALNS=atoi(argv[23]);


		cst::costRideTimes=cst::QuAndBard;
		cst::writeFilesSol=true;
		cst::pathInst=file_instance;
		cst::pathVehicle=file_vehicles;

		string name=(nb_instance<10?"0"+to_string(nb_instance):to_string(nb_instance));//+"_"+to_string(nb_replica)+"_"+to_string(nb_reconfigurations)+"]";

		benchmark(name, path_output, timeLimit, file_instance,file_distances,file_times,file_vehicles,nb_reconfigurations,inst_type, nb_replica, nb_iters, nb_iters_scp);
	}
	else Test t;
	return 0;
}

void benchmark(string name, string path_output, int timeLimit, string file_instance,string file_distances,string file_times,string file_vehicles,int nb_reconfigurations,int inst_type, int nb_replica, int nb_iters, int nb_iters_scp) {
	srand(nb_replica);

	cst::checkAll=false;
	cst::verbosite=false;

	if(inst_type==0){ //cordeau
		cst::QuAndBard=false;
		cst::maxNbReconfigurations=false;
		cst::costRideTimes=false;
	}else if(inst_type==2){//qu and bard
		cst::QuAndBard=true;
		cst::maxNbReconfigurations=true;
		cst::costRideTimes=true;
	}else if(inst_type==3){//gihp
		cst::QuAndBard=false;
		cst::maxNbReconfigurations=true;
		cst::costRideTimes=false;
	}else{
		cout<<"[E] invalid instance type: "<< inst_type <<" IN "<<__FILE__<<__LINE__<< endl;
		abort();
	}

	boost::filesystem::create_directories(path_output);

	darpMod Darp(name,file_instance, file_distances, file_times, file_vehicles,path_output, nb_reconfigurations, inst_type, nb_replica);
	Darp.getAlnsParameters()->setMaxNbIterations(nb_iters);
	Darp.getAlnsParameters()->setMaxNbIterationSetCovering(nb_iters_scp);
	Darp.getAlnsParameters()->setMaxRunningTime((double)timeLimit);
	Darp.solveDARP();

}
