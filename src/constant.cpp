/*
 * constant.cpp
 *
 *  Created on: Jan 16, 2017
 *      Author: sam
 */


#include "constant.h"
#include <sstream>
#include <fstream>
#include <iostream>

#include <cstdlib>


// for classic darp instances set bitSet and QuAndBard to false
bool cst::genInstanceFile=false;
bool cst::QuAndBard=false;
bool cst::costRideTimes=QuAndBard;
bool cst::BitSet= true;
bool cst::maxNbReconfigurations=true;

bool cst::SortVehicle=true;
cst::ClusterPossibilities cst::Clustering=cst::single;

bool cst::writeFilesSol=false;

#ifndef CPLEXNOTINSTLED
	bool cst::SCP=true;/**< use the Set Covering resolution. */
#else
	bool cst::SCP=false;
#endif

bool cst::ASCP2=true;  // dynamic frecuency
bool cst::allOperators=true;

//Temporal
bool cst::ALNS= false;


#ifdef DEBUG
	bool cst::checkAll= true;  /**< Activates a solution checker for every iteration*/
	bool cst::verbosite = true;
	bool cst::checkerAbort=true; /**< abort if the checker returns false */
	bool cst::checkFinal=true; /**< check the final solution */
#else
	bool cst::checkAll= false;
	bool cst::verbosite = false;
	bool cst::checkerAbort=false;
	bool cst::checkFinal=true;
#endif

//used in the toString method
std::string cst::pathInst="";  // this variable is updated in the main
std::string cst::pathVehicle="";  // this variable is updated in the main

std::string cst::toString(std::string path) {
	std::stringstream s;

	s<<" Constant FIle: \n genInstanceFile\t"<<genInstanceFile;   /**< This option becomes useful when you wanna generate new instances */
	s<<"\n pathInst\t"<<pathInst;    /**< Activates a solution checher for every iteration*/
	s<<"\n pathVehicle\t"<<pathVehicle;    /**< Activates a solution checher for every iteration*/

	s<<"\n checkAll\t"<<checkAll;    /**< Activates a solution checher for every iteration*/
	s<<"\n QuAndBard\t"<<QuAndBard;   /**< Configure the matheuristic for Qu And Bard Instances */
	s<<"\n costRideTimes\t"<<costRideTimes;  		/**< computes the sum ride time and cost */
	s<<"\n maxNbReconfigurations\t"<<maxNbReconfigurations;  /**< Activates a constraint limiting the number of reconfigurations allowed in route */
	s<<"\n BitSet\t"<<BitSet;                 /**< True: uses bit set for capacity testing. False: an exaustive test*/
	s<<"\n SCP\t"<<SCP;					/**< use the Set Covering resolution. */
	s<<"\n SortVehicle\t"<<SortVehicle;			/**< sort the vehicle list for the capacity test */
	s<<"\n Clustering\t"<<Clustering;
	s<<"\n RSCP\t"<<ASCP2;
	s<<"\n verbosite\t"<<verbosite;
	s<<"\n checkerAbort\t"<<checkerAbort;			/**< abort if the checker return false */
	s<<"\n checkFinal\t"<<checkFinal;				/**< check the final solution */
	s<<"\n writeFilesSol\t"<<writeFilesSol;			/**< write solutions in output files */

	s<<"\n displaySol\t"<<displaySol;  /**< prints out routes and cost in console */
	s<<"\n fullStats\t"<<fullStats;  /**< This option collect information aboutoperators and solutions in every iteration, the it save it in ouput file*/

	s<<"\n recordtorecord\t"<<recordtorecord;  /**< acceptance criteria */
	s<<"\n ALNS\t"<<ALNS;  /**<  Adaptive Large Neighborhood search */

	s<<"\n SRSCP\t"<<SRSCP; /**<  remove duplicats computing marginal cost*/

	s<<"\n clusterRemoval\t"<<clusterRemoval;

	s<<"\n explainInsert\t"<<explainInsert;	//TODO REMOVE ALL EXPLAIN		/**< use the explaination of an non feasible insertion test to optimize the tests */
	s<<"\n allOperator\t"<<allOperators;

	path +=".const";
	std::ofstream fichier(path.c_str(), std::ios::out | std::ios::trunc);
	fichier << s.str();
	fichier.close();

	return s.str();
}


std::ostream& operator<<(std::ostream& out, const cst& f){
	out<<"************* Parameters Algo \n";
	out << f.toString("");
	return out;
}

