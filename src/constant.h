/*
 * constant.h
 *
 *  Created on: Jul 1, 2016
 *      Author: sam
 */


//#define CPLEXNOTINSTLED

#ifndef CONSTANT_H_
#define CONSTANT_H_

#include <cmath>
#include "iostream"

//#include <assert.h>

inline void _assert2(const char* expression, const char* file, int line)
{
	fprintf(stderr, "Assertion '%s' failed, file '%s' line '%d'.", expression, file, line);
	abort();
}
#define assert2(EXPRESSION) ((EXPRESSION) ? (void)0 : _assert2(#EXPRESSION, __FILE__, __LINE__))



class cst{
public:
	enum ClusterPossibilities {
		single,  /**< no cluster*/
		exact,  /**< MIP resolution */
		kMedoid,   /**< KMedoid resolution*/
	};


	//non constant
	static bool genInstanceFile;   /**< This option becomes useful when you wanna generate new instances */
	static bool QuAndBard;   /**< Configure the matheuristic for Qu And Bard Instances */
	static bool costRideTimes;  		/**< computes the sum ride time and cost */
	static bool maxNbReconfigurations;  /**< Activates a constraint limiting the number of reconfigurations allowed in route */
	static bool BitSet;                 /**< True: uses bit set for capacity testing. False: an exaustive test*/

	static bool SortVehicle;			/**< sort the vehicle list for the capacity test */
	static ClusterPossibilities Clustering;
	static bool ASCP2;					/**< it adaptatively adjust the execusion frecuency of the scp */

	static bool SCP; /**< use the Set Covering resolution. */
	static bool writeFilesSol;			/**< write solutions in output files */

	static bool ALNS;  /**<  Adaptive Large Neighborhood search */

	//Temporal
	static bool allOperators;

	static std::string pathInst;   /**<  useful to identify the instance used,  once it is solved*/
	static std::string pathVehicle;   /**<  useful to identify the instance used,  once it is solved*/


	static bool checkAll;  /**< Activates a solution checher for every iteration*/
	static bool verbosite;  /**< printout in console current results*/
	static bool checkerAbort; /**< abort if the checker return false */
	static bool checkFinal; /**< check the final solution */

	//constants
	const static bool tightenTW=true; 				/**< agir sur l'instance pour reduire les timeWindows. */
	const static bool checkInstance=true;  /**< verifies instance coherence and tringular inequality*/
	const static bool displaySol= true;  /**< prints out routes and cost in console */
	const static bool fullStats= true;  /**< This option collect information aboutoperators and solutions in every iteration, the it save it in ouput file*/

	const static bool recordtorecord= true;  /**< acceptance criteria */

	const static bool SRSCP= true; /**<  remove duplicats computing marginal cost*/
	const static bool clusterRemoval=true;
	const static bool explainInsert=false;	//TODO REMOVE ALL EXPLAIN		/**< use the explaination of an non feasible insertion test to optimize the tests */

	static double round(double x)
	{
		double f, xi, xf;
		xf = modf(x,&xi);
		f = floor(xf*100+0.5)/100.0;
		return xi + f;
	}

	static std::string toString(std::string path);

};

#endif /* CONSTANT_H_ */
