#include "../unitTests/Test.h"


void Test::quAndBardReadSolution() {
	cst::QuAndBard=true;
	cst::costRideTimes=true;
	cst::maxNbReconfigurations=false;
	cst::BitSet=true;
	cst::checkerAbort=false;
	string name="11_B201"; //"11_B201";
	double objective=0;
	if(cst::SCP)
		objective=41.9411; //"11_B201";
	else
		objective=44.078; //"11_B201";

	stringstream file_inst;
	file_inst<<"./instance/QuAndBard/" <<name<<".txt";
	string file_vehicles ="./instance/QuAndBard/vehicle.txt";
	string path_output="./output/unitTest/solutions/";
	boost::filesystem::create_directories(path_output);
	darpMod Darp2(name,file_inst.str(), "", "", file_vehicles, path_output,0, 2, 0);
	stringstream file_instance_sol;
	file_instance_sol<<"./instance/QuAndBard/results/S"<<name<<".txt";
	Insert_QuAndBard insQuAndBard(file_instance_sol.str());
	double cost=(Darp2.computeOnlyInitSol(dynamic_cast<ARepairOperator*>(&insQuAndBard),path_output+name));
	Darp2.getSol()->writeSolution(path_output+name+".csv");
	//cout<<scientific<<setprecision(10)<<cst::round(cost)<<endl;
	double pres=abs(cst::round(cost)-objective);
	assert2(pres<0.01);
	cout<<name<<" ok"<<endl;
	cout<<"test solution QuAndBard ok"<<endl;
	cst::QuAndBard=false;
	cst::costRideTimes=false;
	cst::maxNbReconfigurations=true;
	cst::BitSet=true;
}



void Test::quAndBardCompute() {
	srand(3);  //short term better results with solIni 2 regret
	cst::QuAndBard=true;
	cst::maxNbReconfigurations=true;
	cst::BitSet=true;
	cst::costRideTimes=true;
	cst::writeFilesSol=true;
	cst::SortVehicle=false;
	cst::checkerAbort=false; // because tringular inequality is not respected in this test!
	string name="11_B201";

	double objective;
	if(cst::SCP)
		objective=41.9411;
	else
		objective=41.9411; // outdate..

	string file_instance = "./instance/QuAndBard/"+name+".txt";
	char const* file_distances ="";
	char const* file_times ="";
	char const* file_vehicles ="./instance/QuAndBard/vehicle.txt";
	string path_output="./output/unitTest/";
	boost::filesystem::create_directories(path_output);
	int nb_reconfigurations = 0; //0
	int inst_type = 2; //  2 Qu_Bard
	int nb_replica = 0; //0
	darpMod Darp(name,file_instance, file_distances, file_times, file_vehicles, path_output, nb_reconfigurations, inst_type, nb_replica);
	Darp.getAlnsParameters()->setMaxNbIterations(2000);// with 1000 achieves a good result of 44.8886;
	Darp.getAlnsParameters()->setMaxNbIterationSetCovering(1000);
	//Darp.getAlnsParameters()->setMaxRunningTime(3);
	Darp.solveDARP();
	string path=path_output+"s"+Darp.getName()+".csv";

	ifstream myfile(path);
	string line;
	double data;
	if (myfile.is_open()){
		myfile >> data;	myfile.close();
		std::cout<< data <<" vs "<< objective <<std::endl;
		//assert(abs(data-objective)<0.01);
		cout<<"test Qu&Bard  ok"<<endl;
	}else{
		cout<<"it can't open file "<<path<<__FILE__<<__LINE__<<endl;
		//abort();
	}
	cst::QuAndBard=false;
	cst::costRideTimes=false;
	cst::maxNbReconfigurations=false;
}


