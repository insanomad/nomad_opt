#include <boost/filesystem.hpp>
#include "../unitTests/Test.h"

void Test::testGIHP(){

	cst::verbosite=true;
	//createGihpInstances(); //ATTENTION NE PAS ECRASER LES INSTANCES ACTUELLES
	cst::checkAll= false;  /**< Activates a solution checker for every iteration*/
	cst::checkerAbort=true; /**< abort if the checker returns false */
	cst::checkFinal=true; /**< check the final solution */
	cst::writeFilesSol=true;
	//	cst::writeFilesSol=false;
	//	cst::checkFinal=true;
	//timeMatrix();
	//ClusteringFuntionsTest();
	//cst::allOperators=false;
	///checkGihpInstances("./instance/GIHP/newGIHP/T295-1");
	//orderSchedulingAndCapacity();
	//cst::SCP=false;  // the set covering is down for some test to obtain real compteurs du test de capacité et scheduling
	//cst::allOperators=true;
	//cst::ALNS=cst::allOperators;
	vector<int >reconfig={100};
	gihpTestNbReconfigurations(reconfig);
	//cout<<gihpComputeNew("0_100","./instance/GIHP/benchgihp/ins100mrtw/",10)<<endl;
	//gihpComputeNew("2_310,"./instance/GIHP/new_inst/creation/",0);
	//	gihpComputeNew("3_74","./instance/GIHP/new_inst/creation/");
}


void Test::gihpTestNbReconfigurations(vector<int >& reconfig) {
	//cst::allOperators=true;
	//cst::SCP=true;
	vector<string >l={"M11_160"};//{"0_64","1_103", "2_31", "3_74","4_104","5_48", "6_49", "7_19","8_61","9_22"}; //0_7","2_80","4_151","6_82","8_17","full","1_36","3_17","5_21","7_84","9_80"};
	vector<vector<double> > perf(l.size(),vector<double>(reconfig.size(),-1));
	vector<vector<int> > nbRoutes(l.size(),vector<int>(reconfig.size(),0));
	vector<vector<int> > nbRec(l.size(),vector<int>(reconfig.size(),0));

	for(size_t i = 0; i < l.size(); ++i)
	{
		for (size_t j = 0; j< reconfig.size();++j)
		{
			perf[i][j]=gihpComputeNew(l[i],"./instance/GIHP/GIHPTimeDist/instances/",reconfig[j], &nbRoutes[i][j],&nbRec[i][j]);
			cout<<endl<<"****************************************"<<endl;
		}
	}
	for (size_t i = 0; i < perf.size(); ++i) {
		cout << l[i];
		for (size_t j=0; j < reconfig.size(); ++j) {
			if(perf[i][j]!=-1)
				cout << "\t" << perf[i][j] <<"("<<((perf[i][0] - perf[i][j])/perf[i][0])*100<<"%)"<<"["<<nbRoutes[i][j]<<","<<nbRec[i][j]<<"]";
		}
		cout << endl;
	}
}



double Test::gihpComputeNew(std::string name, std::string path_in, int nb_reconf, int* nbRoutes, int* nbRec) {
	srand(0);
	cst::QuAndBard=false;
	cst::BitSet=true;
	cst::maxNbReconfigurations=true;
	cst::costRideTimes=false;
	string file_nodes = path_in+name+".node";
	string file_distances =path_in+name+".dist";
	string file_times =path_in+name+".time";
	string file_vehicles =path_in+"hdarpc.vehi";
	string path_output="./output/GIHP/";
	boost::filesystem::create_directories(path_output);
	int inst_type = 3;
	int nb_replica = 0;
	darpMod Darp(name,file_nodes, file_distances, file_times, file_vehicles, path_output, nb_reconf, inst_type, nb_replica);
	Darp.getAlnsParameters()->setMaxNbIterations(50000);
	Darp.getAlnsParameters()->setMaxNbIterationSetCovering(1000);

	double res=Darp.solveDARP();
	*nbRoutes=Darp.getSol()->compNbToursUsed();
	size_t routesrec=0;
	*nbRec=Darp.getSol()->computeNbReconfigurations(&routesrec);
	return res;
}


void Test::createGihpInstances() {

	cst::BitSet=false;
	//cst::tightenTW=false;
	cst::checkAll=true;
	cst::checkFinal=true;
	cst::checkerAbort=false;
	string ins_name="fullBase";
	string path_input="./instance/GIHP/GIHPTimeDist/";
	boost::filesystem::create_directories(path_input);

	string file_nodes = path_input+ins_name+".node";
	string file_distances = path_input+ins_name+".dist";
	string file_times =path_input+ins_name+".time";
	string file_vehicles =path_input+ins_name+".vehi";
	cout<<"Inst: "<<ins_name<<endl;
	Instance instance(file_nodes, file_distances, file_times, file_vehicles, 0, 3);
	instance.forceTriangularInequality();

	//50kmh pour les distances:
//	for (size_t i = 0; i < instance.distance_ij.size(); ++i) {
//		for (size_t j = 0; j < instance.distance_ij.size(); ++j) {
//			instance.distance_ij[i][j]=(unsigned long)(((double)instance.time_ij[i][j])*13.89);
//		}
//	}
	instance.setServiceTime();
	instance.setMaxRT();
	instance.randomizeTWdel();
	instance.randomizeTWpic();
	//instance.arrangeTWpic();

	cst::checkerAbort=true;

	NeighbourShaw neigh(&instance);
	//NeighbourMinRoute neigh(&instance);
	neigh.init();

	ClusterEstablishment clust(&instance,&neigh);
//#ifndef CPLEXNOTINSTLED
//	ClusterMIPCplex clust(&instance,&neigh);
//#else
//	ClusterMIPCoin clust(&instance,&neigh);
//#endif

	string path_output=path_input+"clEtabLast/";
	boost::filesystem::create_directories(path_output);
	//FULL
	vector<string > clusterName;
	string name_clust=path_output+"full";
	clusterName.push_back(name_clust);

	clust.setSingleCluster();
	instance.write_instance_gihp(name_clust, clust.getVvGroups()[0]);
	//checkGihpInstances(path_output+"full");

	//CLUSTER = 10
	// 60-80 x 8 / 120-160 x 4 / 240-320 x 2
	clust.createClusters(2,240,320);
//	std::cout<<clust<<std::endl;
//	clust.clearGroups();
//	clust.kmedoid_swap(10);  //not optimal!!
//	std::cout<<clust<<std::endl;

	int i=12;
	size_t j=0;
	vector<string> nametw = {"T","M", "L"};
	for(vector<size_t> group : clust.getVvGroups())
	{
		instance.modifyDeliveryTW(15*60);
		name_clust=path_output + nametw[0] +(i<10?"0"+to_string(i):to_string(i))+"_"+to_string(group.size());
		instance.depot_arrival[0]->cap=(long)(group.size()/3);
		instance.depot_depart[0]->cap=(long)(group.size()/3);
		instance.write_instance_gihp(name_clust, group);
		clusterName.push_back(name_clust);
		for(j=1; j< nametw.size();++j)
		{
			instance.modifyDeliveryTW(15*(j+1)*60);
			name_clust=path_output+nametw[j]+(i<10?"0"+to_string(i):to_string(i))+"_"+to_string(group.size());
			instance.write_instance_gihp(name_clust, group);
			clusterName.push_back(name_clust);
		}
		++i;
	}

	//CHECK
	for(auto& s:clusterName){
		checkGihpInstances(s);
	}
	cout<<"done"<<endl;
}


void Test::ClusteringFuntionsTest(){
	srand(0);
	cst::QuAndBard=false;
	cst::BitSet=true;
	cst::maxNbReconfigurations=true;
	cst::costRideTimes=false;

	//cst::displaySol=true;
	string  path_in="./instance/GIHP/new_inst/mRnTW/";
	string name="4_73";
	string file_nodes = path_in+name+".node";
	string file_distances =path_in+name+".dist";
	string file_times =path_in+name+".time";
	string file_vehicles =path_in+name+".vehi";
	string path_output="./output/GIHP/";
	boost::filesystem::create_directories(path_output);
	Instance instance(file_nodes, file_distances, file_times, file_vehicles, 0, 3);

	//NeighbourMinRoute neigh(&instance);
	NeighbourShaw neigh(&instance);

	neigh.init();  // dont make inside constructor. otherwise it is not gonna compile :(
#ifndef CPLEXNOTINSTLED
	ClusterMIPCplex clust(&instance,&neigh);
#else
	ClusterMIPCoin clust(&instance,&neigh);
#endif
	clust.createClusters(5,10,15);
	std::cout<<clust<<std::endl;
}
void Test::gihpCompute() {
	srand(0);
	cst::QuAndBard=false;
	cst::BitSet=true;
	cst::maxNbReconfigurations=true;
	cst::costRideTimes=false;
	cst::writeFilesSol=true;
	string name="M00_80";
	double objective;

	if(cst::SCP)
		objective=2017.16;
	else
		objective=2017.16;

	string file_nodes = "./instance/GIHP/GIHPTimeDist/instances/"+name+".node";
	string file_distances ="./instance/GIHP/GIHPTimeDist/instances/"+name+".dist";
	string file_times ="./instance/GIHP/GIHPTimeDist/instances/"+name+".time";
	string file_vehicles ="./instance/GIHP/GIHPTimeDist/instances/"+name+".vehi";
	string path_output="./output/unitTest/";
	int nb_reconfigurations = 0; //0
	int inst_type = 3; //  3 Gihp new
	int nb_replica = 0; //0
	darpMod Darp(name,file_nodes, file_distances, file_times, file_vehicles,path_output, nb_reconfigurations, inst_type, nb_replica);
	Darp.getAlnsParameters()->setMaxNbIterations(30);
	Darp.getAlnsParameters()->setMaxNbIterationSetCovering(30);
	Darp.solveDARP();
	string path=path_output+"s"+Darp.getName()+".csv";
	ifstream myfile(path);
	string line;
	double data;
	if (myfile.is_open()){
		myfile >> data;
		myfile.close();
		cout<<data<<" vs "<<objective<<endl;
		assert(abs(data-objective)<0.01);
		cout<<"test gihp inst "<<name<<"ok "<<endl;
	}else{
		cout<<"it can't open file "<<name<<__FILE__<<__LINE__<<endl;
		abort();
	}

}

void Test::timeMatrixTest(){
	srand(0);
	cst::QuAndBard=false;
	cst::BitSet=true;
	cst::maxNbReconfigurations=true;
	cst::costRideTimes=false;
	cst::writeFilesSol=true;
	string name="full"; //c17
	string file_nodes = "./instance/GIHP/"+name+".node";
	string file_distances ="./instance/GIHP/"+name+".dist";
	string file_times ="./instance/GIHP/full.time";
	string file_vehicles ="./instance/GIHP/multi.vehi";
	string path_output="./output/unitTest/";
	int nb_reconfigurations = 0; //0
	int inst_type = 3; //  3 Gihp new
	int nb_replica = 0; //0
	darpMod Darp(name,file_nodes, file_distances, file_times, file_vehicles,path_output, nb_reconfigurations, inst_type, nb_replica);
	Darp.getInstance()->readTimesGIHPtest("./instance/GIHP/full2.time");

}

void Test::gihpReadWrite(std::string name) {
	cst::genInstanceFile=true;
	cst::maxNbReconfigurations=true;
	cst::costRideTimes=false;

	string file_nodes = "./instance/GIHP/10_classic_cluster/"+name+".node";
	string file_distances ="./instance/GIHP/10_classic_cluster/"+name+".dist";
	string file_times ="./instance/GIHP/10_classic_cluster/"+name+".time";
	string file_vehicles ="./instance/GIHP/10_classic_cluster/"+name+".vehi";
	string path_output="./output/unitTest/readwrite/";

	boost::filesystem::create_directories(path_output);

	int nb_reconfigurations = 1; //0
	int inst_type = 3; //  3 Gihp new
	int nb_replica = 0; //0
	darpMod Darp(name,file_nodes, file_distances, file_times, file_vehicles, path_output, nb_reconfigurations, inst_type, nb_replica);

	std::vector<size_t> depots_arrive;
	std::vector<size_t> nodesCluster;
	std::vector<size_t> reqInCluster;
	for(auto& d : Darp.getInstance()->getDataNodes()){
		nodesCluster.push_back(d->id);
		if(d->getType()==0) reqInCluster.push_back((int)d->getId());
	}

	for(auto& d : Darp.getInstance()->getDepotArrival()){
		depots_arrive.push_back(d->id);
	}

	file_nodes = path_output+name+".node";
	file_distances =path_output+name+".dist";
	file_times =path_output+name+".time";
	file_vehicles =path_output+name+".vehi";

	string p1_inst = Darp.getInstance()->write_nodes_gihp(file_nodes, depots_arrive, reqInCluster).str(); //TODO passer dir+name en arguement
	string p1_dist =Darp.getInstance()->write_distance_gihp(file_distances, nodesCluster).str();
	string p1_times =Darp.getInstance()->write_times_gihp(file_times, nodesCluster).str();
	string p1_vehicles =Darp.getInstance()->write_vehicles_gihp(file_vehicles).str();

	nb_reconfigurations = 1; //0
	inst_type = 3; //  3 Gihp new
	nb_replica = 0; //0
	darpMod Darp2(name,file_nodes, file_distances, file_times, file_vehicles, path_output, nb_reconfigurations, inst_type, nb_replica);

	std::vector<size_t> depots_arrive2;
	std::vector<size_t> nodesCluster2;
	std::vector<int> reqInCluster2;
	for(auto& d : Darp2.getInstance()->getDataNodes()){
		nodesCluster2.push_back(d->id);
		if(d->getType()==0) reqInCluster2.push_back((int)d->getId());
	}
	for(auto& d : Darp2.getInstance()->getDepotArrival()){
		depots_arrive2.push_back(d->id);
	}

	string p2_inst = Darp.getInstance()->write_nodes_gihp(file_nodes, depots_arrive, reqInCluster).str(); //TODO passer dir+name en arguement
	string p2_dist =Darp.getInstance()->write_distance_gihp(file_distances, nodesCluster).str();
	string p2_times =Darp.getInstance()->write_times_gihp(file_times, nodesCluster).str();
	string p2_vehicles =Darp.getInstance()->write_vehicles_gihp(file_vehicles).str();
	assert(p1_inst!=p2_dist );
	assert(p1_inst==p2_inst );
	assert(p1_dist==p2_dist );
	assert(p1_times==p2_times );
	assert(p1_vehicles==p2_vehicles );
	cout<<"test read and write gihp ok"<<endl;

	cst::QuAndBard=false;
	cst::genInstanceFile=false;
	cst::maxNbReconfigurations=true;
	cst::costRideTimes=false;
}

void Test::checkGihpInstances(string name) {
	cst::BitSet=true;
	cst::checkAll=true;
	string file_nodes = name+".node";
	string file_distances = name+".dist";
	string file_times =name+".time";
	string file_vehicles ="./instance/GIHP/newGIHP/multi.vehi";
	cout<<"Inst: "<<name<<endl;
	Instance instance(file_nodes, file_distances, file_times, file_vehicles, 0, 3);
	Checker ch(&instance);
	ch.instCheck();
}
