/*
 * test.h
 *
 *  Created on: Jan 16, 2017
 *      Author: sam
 */

#ifndef TEST_H_
#define TEST_H_
#include <boost/filesystem.hpp>

#include "iostream"
#include "../constant.h"
#include "../darp/darpMod.h"
#include "../alns/SetCovering.h"
#include "../darp/clustering/ClusterMIPCoin.h"
#include "../darp/instanceManager/readSolCordeau.h"


class Test {
public:
	Test();

	virtual ~Test(){};
	void timeMatrixTest();
	void fullTest();
	void clustering();
	void ClusteringFuntionsTest();
	void genInstanceGIHP(int nbClusters);

	/**
	 * compute a known instance of cordeau
	 * compute a known instance of qu and bard
	 */
	void cordeauCompute();
	void quAndBardCompute();

	/**
	 * n is instance number
	 */
	void runCordeau(int n,int nb_replica);

	/**
	 * desactive le set covering
	 */
	void scpTest();

	/**
	 * lire et calculer les instances / solutions et recalculer les performances (scheduling inclu) de la solution
	 */
	void cordeauReadSolution();
	void quAndBardReadSolution();

	/**
	 * capacity testing : bitSet vs classic
	 */
	void bitSetVsClassic();

	/**
	 * Test insert operators: k-regret k=1,2,3,4.
	 */
	void testKregret();

	/**
	 * compare execution time with and without explanations of non feasibility
	 */
	void benchScheduling();

	/**************************** GIHP *****************************/

	/**
	 * test the resolution of a GIHP instance
	 */
	void gihpCompute();
	/**
	 * Computes a set of instances with different values of nb reconfigurations allowed
	 */
	void gihpTestNbReconfigurations(vector<int >& reconfig);

	/**
	 * Computes a single gihp "name" instance.
	 */
	double gihpComputeNew(std::string name, std::string path_in, int nb_reconf, int* nbRoutes, int* nbRec);

	/**
	 * read and write GIHP instances (instance type=3)
	 */
	void gihpReadWrite(std::string name);


	/**
	 * manage the instance of GIHP
	 */
	void createGihpInstances();
	void checkGihpInstances(std::string name);


	/**
	 * tests for the GIHP managed from Test_gihp.cpp
	 */
	void testGIHP();

};

#endif /* TEST_H_ */
