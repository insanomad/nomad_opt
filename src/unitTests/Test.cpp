
#include "../unitTests/Test.h"

#include <sstream>
//#include <stdlib.h>
#include "../darp/operators/operators.h"

#include <iomanip> // more precision in decimals
#include <ctime>


/*
 * test.cpp
 *
 *  Created on: Jan 16, 2017
 *      Author: sam
 */

using namespace std;

Test::Test(){
	//cst::writeFilesSol=true;
//	cst::verbosite = true;
//	cst::writeFilesSol=true;
//	cst::checkFinal=true;
	//cst::checkAll=true;
	//quAndBardCompute();
	//quAndBardReadSolution();

	//cordeauCompute();
	//fullTest();

	//quAndBardCompute();
	//quAndBardCompute();
	//runCordeau(2, 0) ;
	//testGIHP();

	//fullTest();
	//clustering();
	//quAndBardReadSolution();
	//benchScheduling();
	//cordeauReadSolution();
	//gihpReadWrite("c10"); // {"c10","c17"}
	//testKregret();
	//cordeauCompute();
	//gihpCompute();

	//bitSetVsClassic();
	//quAndBardCompute();
	//tighteningTest();


	//cst::toString("");  // gen file with current algo configuration

}


void Test::fullTest(){
	//bitSetVsClassic();
	//testKregret();
	//quAndBardReadSolution();
	cst::checkAll=true;
	cst::verbosite = false;
	gihpCompute();
	cordeauReadSolution();
	cordeauCompute();
	quAndBardCompute();

	//clustering();
	cout<<"all test ok"<<endl;
}

void Test::benchScheduling(){
	clock_t begin = clock();
	fullTest();
	double time1= double(clock() - begin) / CLOCKS_PER_SEC;
	cout << "Total Execution Time FALSE = " <<time1<<endl;
}

void Test::scpTest(){
	cst::SCP=false;
	cordeauCompute();
	gihpCompute();
	quAndBardCompute();
	cst::SCP=true;
}


//with gihp Instances
void Test::bitSetVsClassic() {
	cst::QuAndBard=false;
	cst::costRideTimes=false;
	cst::maxNbReconfigurations=true;
	cst::BitSet=true;
	cst::writeFilesSol=true;
	string name="c17";
	double objective1=0;
	string file_nodes = "./instance/GIHP/10_classic_cluster/"+name+".node";
	string file_distances ="./instance/GIHP/10_classic_cluster/"+name+".dist";
	string file_times ="./instance/GIHP/10_classic_cluster/"+name+".time";
	string file_vehicles ="./instance/GIHP/10_classic_cluster/"+name+".vehi";
	string path_output="./output/unitTest/";
	boost::filesystem::create_directories(path_output);
	int nb_reconfigurations = 1; //0
	int inst_type = 3; //  3 Gihp new
	int nb_replica = 0; //0
	srand(0);
	darpMod Darp(name,file_nodes, file_distances, file_times, file_vehicles, path_output,  nb_reconfigurations, inst_type, nb_replica);
	Darp.getAlnsParameters()->setMaxNbIterations(389);
	Darp.getAlnsParameters()->setMaxNbIterationSetCovering(1000);
	Darp.solveDARP();
	string path=path_output+"s_"+name+"-0_R"+to_string(nb_reconfigurations)+".csv";
	ifstream myfile(path);
	if (myfile.is_open()){
		myfile >> objective1;
		myfile.close();
	}else{
		cout<<"it can't open file "<<path<<__FILE__<<__LINE__<<endl;
		abort();
	}

	cst::BitSet=false;
	double objective2=0;
	srand(0);
	darpMod Darp2(name,file_nodes, file_distances, file_times, file_vehicles, path_output, nb_reconfigurations, inst_type, nb_replica);
	Darp2.getAlnsParameters()->setMaxNbIterations(389);
	Darp2.getAlnsParameters()->setMaxNbIterationSetCovering(1000);
	Darp2.solveDARP();
	string path2=path_output+"s_"+name+"-0_R"+to_string(nb_reconfigurations)+".csv";
	ifstream myfile2(path2);
	if (myfile2.is_open()){
		myfile2 >> objective2;
		myfile2.close();
	}else{
		cout<<"it can't open file "<<path<<__FILE__<<__LINE__<<endl;
		abort();
	}
	cout<<objective1<<" "<<objective2<<endl;
	assert2(objective1==objective2);
	cout<<"test bitset ok"<<endl;
	cst::QuAndBard=false;
	cst::costRideTimes=false;
	cst::maxNbReconfigurations=true;
	cst::BitSet=true;
}

/**
 * T
 */
void Test::testKregret() {
	cst::QuAndBard=false;
	cst::maxNbReconfigurations=false;
	cst::BitSet=false;
	srand(0);
	double k1reg=231.292;
	double k2reg=212.241;
	double k3reg=204.005;
	double k4reg=221.37962;

	string path_output="./output/unitTest/testKregret/";
	boost::filesystem::create_directories(path_output);

	darpMod Darp("pr01","./instance/ICordeau/pr01", "", "", "",path_output, 0, 0, 0);
	stringstream file_instance_sol;

	file_instance_sol<<"./instance/Cordeau/resultsCordeau/pr01.res";
	string name=to_string(1)+"_regret";
	Insert_KRegret Kregret(name, 1 ,Darp.getInstance(), false);
	double cost=Darp.computeOnlyInitSol(dynamic_cast<ARepairOperator*>(&Kregret), path_output+name);
	assert2(abs(cost-k1reg)<0.01);

	name=to_string(2)+"_regret";
	Insert_KRegret Kregret2(name, 2 ,Darp.getInstance(), false);
	cost=(Darp.computeOnlyInitSol(dynamic_cast<ARepairOperator*>(&Kregret2),path_output+name));
	assert2(abs(cost-k2reg)<0.01);

	name=to_string(3)+"_regret";
	Insert_KRegret Kregret3(name, 3 ,Darp.getInstance(), false);
	cost=(Darp.computeOnlyInitSol(dynamic_cast<ARepairOperator*>(&Kregret3),path_output+name));
	assert2(abs(cost-k3reg)<0.01);

	Insert_KRegret Kregret4(to_string(4)+"_regret", 4 ,Darp.getInstance(), false);
	cost=(Darp.computeOnlyInitSol(dynamic_cast<ARepairOperator*>(&Kregret4),path_output+name));
	//cout<<scientific<<setprecision(10)<<cost<<" "<<k4reg<<endl;
	assert2(abs(cost-k4reg)<0.01);

	cout<<"test kregret operators ok"<<endl;
	cst::QuAndBard=false;
	cst::maxNbReconfigurations=false;
	cst::BitSet=true;
}
// it will be useful when generatin gihp instances
void Test::clustering(){
	srand(0);
	cst::QuAndBard=false;
	cst::maxNbReconfigurations=false;
	cst::BitSet=false;
	cst::SCP=true;
	cst::Clustering=cst::exact;
	cst::writeFilesSol=true;
	string name="01";
	int nbClusters=2;
	double objective;
	if(cst::SCP)
		objective=207.189; // test with 2 clusters on instance 01 cordeau
	else
		objective=207.189;
	string file_instance = "./instance/ICordeau/pr"+ name;
	char const* file_distances ="";
	char const* file_vehicles ="";
	char const* file_times ="";
	string path_output="./output/unitTest/";
	boost::filesystem::create_directories(path_output);
	int inst_type = 0; // 0 Cordeau;
	int nb_replica = 0; //0
	int nb_reconfigurations = 0; //0
	darpMod Darp(name,file_instance, file_distances, file_times, file_vehicles,path_output, nb_reconfigurations, inst_type, nb_replica);
	Darp.setNbClusters(nbClusters);
	Darp.getAlnsParameters()->setMaxNbIterations(200);
	Darp.getAlnsParameters()->setMaxNbIterationSetCovering(50);
	Darp.getInstance()->getDepotDepart()[0]->cap*=2;
	Darp.solveDARP();
	string path=path_output+"s_"+name+"-0_R"+to_string(nb_reconfigurations)+"_"+to_string(nbClusters)+"|all.csv";
	ifstream myfile(path);
	double data=0;
	if (myfile.is_open()){
		myfile >> data;	myfile.close();
		cout<<data<<" "<<objective<<endl;
		assert(abs(data-objective)<0.01);
		cout<<"test cordeau inst 1 ok"<<endl;
	}else{
		cout<<"it can't open file "	<<path<<__FILE__<<__LINE__<<endl;
		abort();
	}
	cst::maxNbReconfigurations=true;
	cst::BitSet=true;
	cst::writeFilesSol=false;
}

