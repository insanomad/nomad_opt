#include "../unitTests/Test.h"

void Test::cordeauReadSolution() {
	cst::QuAndBard=false;
	cst::maxNbReconfigurations=false;
	cst::BitSet=false;
	string path_output="./output/unitTest/";
	boost::filesystem::create_directories(path_output);

	for(int n=1; n<21;++n){
		stringstream name; name<<"pr"<<(n<10?"0":"")<<to_string(n);
		darpMod Darp(name.str(),"./instance/ICordeau/"+name.str(), "", "", "","./output/unitTest/", 0, 0, 0);
		stringstream file_instance_sol;
		file_instance_sol<<"./instance/ICordeau/results/resultsCordeau/pr"<<(n<10?"0":"")<<to_string(n)<<".res";
		Insert_Cordeau insCordeau(file_instance_sol.str());
		double cost=Darp.computeOnlyInitSol(dynamic_cast<ARepairOperator*>(&insCordeau),path_output+name.str());
		//cout<<scientific<<setprecision(10)<<cst::round(cost)<<insCordeau.getCost()<<endl;
		double pres=abs(cst::round(cost)-insCordeau.getCost());
		assert2(pres<0.01);
		cout<<n<<" ok"<<endl;
	}
	cout<<"test solution Cordeau ok"<<endl;
	cst::QuAndBard=false;
	cst::maxNbReconfigurations=true;
	cst::BitSet=true;
}


void Test::runCordeau(int n, int nb_replica) {
	srand(nb_replica);
	cst::QuAndBard=false;
	cst::maxNbReconfigurations=false;
	cst::BitSet=false;
	cst::SCP=true;
	string name=(n<10?"0"+to_string(n):to_string(n));
	string file_instance = "./instance/ICordeau/pr"+ name;
	char const* file_distances ="";
	char const* file_vehicles ="";
	char const* file_times ="";
	string path_output="./output/unitTest/";
	boost::filesystem::create_directories(path_output);

	int inst_type = 0; // 0 Cordeau;
	//int nb_replica = 0; //0
	int nb_reconfigurations = 0; //0
	darpMod Darp(name,file_instance, file_distances, file_times, file_vehicles,path_output, nb_reconfigurations, inst_type, nb_replica);
	Darp.getAlnsParameters()->setMaxNbIterations(500);
	Darp.getAlnsParameters()->setMaxNbIterationSetCovering(1000);
	Darp.getAlnsParameters()->setMaxRunningTime(30000);
	Darp.solveDARP();
	cst::maxNbReconfigurations=true;
	cst::BitSet=true;
}



void Test::cordeauCompute() {
	srand(2);
	cst::QuAndBard=false;
	cst::maxNbReconfigurations=false;
	cst::BitSet=false;
	cst::SCP=true;
	cst::writeFilesSol=true;
	string name="01";
	double objective=190.02;
	string file_instance = "./instance/ICordeau/pr"+ name;

	char const* file_distances ="";
	char const* file_vehicles ="";
	char const* file_times ="";
	string path_output="./output/unitTest/";
	boost::filesystem::create_directories(path_output);

	int inst_type = 0; // 0 Cordeau;
	int nb_replica = 0; //0
	int nb_reconfigurations = 0; //0
	darpMod Darp(name,file_instance, file_distances, file_times, file_vehicles,path_output, nb_reconfigurations, inst_type, nb_replica);
	Darp.getAlnsParameters()->setMaxNbIterations(162);
	Darp.getAlnsParameters()->setMaxNbIterationSetCovering(162);
	Darp.solveDARP();
	string path=path_output+"s"+Darp.getName()+".csv";
	ifstream myfile(path);
	double data=0;
	if (myfile.is_open()) {
		myfile >> data;	myfile.close();
		cout<<data<<" "<<objective<<endl;
		assert(abs(data-objective)<0.01);
		cout<<"test cordeau inst 1 ok"<<endl;
	}else {
		cout<<"it can't open file "	<<path<<__FILE__<<__LINE__<<endl;
		abort();
	}
	cst::maxNbReconfigurations=true;
	cst::BitSet=true;
}
