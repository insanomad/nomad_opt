/* ALNS_Framework - a framework to develop ALNS based solvers
 *
 * Copyright (C) 2012 Renaud Masson
 *
 * This library is free software; you can redistribute it and/or
 * modify it either under the terms of the GNU Lesser General Public
 * License version 3 as published by the Free Software Foundation
 * (the "LGPL"). If you do not alter this notice, a recipient may use
 * your version of this file under the LGPL.
 *
 * You should have received a copy of the LGPL along with this library
 * in the file COPYING-LGPL-3; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Suite 500, Boston, MA 02110-1335, USA
 *
 * This software is distributed on an "AS IS" basis, WITHOUT WARRANTY
 * OF ANY KIND, either express or implied. See the LGPL for
 * the specific language governing rights and limitations.
 *
 * The Original Code is the ALNS_Framework library.
 *
 *
 * Contributor(s):
 *	Renaud Masson
 */
#ifndef STATISTICS_H_
#define STATISTICS_H_

#include <vector>
#include <time.h>

class Statistics {
public:
	//! Constructor.
	Statistics(std::string _path);

	//! Destructor.
	virtual ~Statistics();

	//! This method adds an entry to the data
	void addEntry(double timeStamp,
				  size_t iteration,
				  std::string destroyName,
				  std::string recreateName,
				  double newCost,
				  double currentCost,
				  double bestCost,
				  int cumKS,
				  bool getAcceptedAsCurrentSolution,
				  bool getNewBestSolution,
				  bool getAlreadyKnownSolution,
				  size_t nbNewBestSolSCP,
				  size_t nbNewCurrSolSCP,
				  size_t PoolSize,
				  size_t nbOptSolSCP);

	void addOperatorEntry(std::vector<double>* weight,
						  std::vector<size_t>* calls);

	void addOperatorsNames(std::vector<std::string>* names){operatorNames = names;};

	//! This method generate the file containing statistics about the metaheurstic.
	void generateStatsFile(std::string path, std::string pathOp);

	//!this files hold general information for different instances
	void generalTimes( std::string name ,std::string dir, double times, double bestSol, size_t iter, size_t numtours, double timeForBesSolution, size_t NbNewBestSolScp, size_t NbNewCurrSolScp, size_t NbOptSolScp, size_t NbReconfigurations, double fleetUtilization, size_t NbRoutesRec);

	//! this is the start
	void setStart(){start = clock();};

private:
	std::vector<double> timeStamps;
	std::vector<size_t> iterations;
	std::vector<std::string> destroyNames;
	std::vector<std::string> recreateNames;
	std::vector<double> newCosts;
	std::vector<double> currentCosts;
	std::vector<double> bestCosts;
	std::vector<int> cumulativeKnownSolutions;
	std::vector<std::vector<double>* > weights;
	std::vector<std::vector<size_t>* > nbCalls;
	std::vector<double> timeStampsOperators;
	std::vector<std::string>* operatorNames=nullptr;
	clock_t start;
	std::vector<double> temperatures;
	std::vector<bool> VAcceptedAsCurrentSolution;
	std::vector<bool> VNewBestSolution;
	std::vector<bool> VAlreadyKnownSolution;
	std::vector<size_t> cnbNewBestSCP;
	std::vector<size_t> cnbNewCurrSCP;
	std::vector<size_t> cnbsizePool;
	std::vector<size_t> cnbOptSolSCP;

	std::string path;

};

#endif /* STATISTICS_H_ */
