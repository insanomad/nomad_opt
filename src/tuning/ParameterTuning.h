/* ALNS-framework - a solver for temporal constraint problems
 *
 * Copyright (C) 2013 Renaud Masson
 *
 * This library is free software; you can redistribute it and/or
 * modify it either under the terms of the GNU Lesser General Public
 * License version 3 as published by the Free Software Foundation
 * (the "LGPL"). If you do not alter this notice, a recipient may use
 * your version of this file under the LGPL.
 *
 * You should have received a copy of the LGPL along with this library
 * in the file COPYING-LGPL-3; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Suite 500, Boston, MA 02110-1335, USA
 *
 * This software is distributed on an "AS IS" basis, WITHOUT WARRANTY
 * OF ANY KIND, either express or implied. See the LGPL for
 * the specific language governing rights and limitations.
 *
 * The Original Code is the ALNS-framework library.
 *
 * File name:
 *  ParameterTuning.h
 * Contributor(s):
 *	Renaud Masson
 */

#ifndef PARAMETERTUNING_H_
#define PARAMETERTUNING_H_

#include <iostream>
#include <map>
#include <vector>
#include <limits>

class ParameterTuning {
public:
	//! Constructor.
	//! \param temperature for the simulated annealing.
	//! \param alpha rate for sa.
	//! \param runTime the maximum run time .
	ParameterTuning(int temperature, double alpha, long long runTime, size_t iter=-1);

	virtual ~ParameterTuning();

	int temp;
	double alpha;
	long long runtime;
	size_t itermax;

};

#endif /* PARAMETERTUNING_H_ */
