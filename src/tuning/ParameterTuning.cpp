/* ALNS-framework - a solver for temporal constraint problems
 *
 * Copyright (C) 2013 Renaud Masson
 *
 * This library is free software; you can redistribute it and/or
 * modify it either under the terms of the GNU Lesser General Public
 * License version 3 as published by the Free Software Foundation
 * (the "LGPL"). If you do not alter this notice, a recipient may use
 * your version of this file under the LGPL.
 *
 * You should have received a copy of the LGPL along with this library
 * in the file COPYING-LGPL-3; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Suite 500, Boston, MA 02110-1335, USA
 *
 * This software is distributed on an "AS IS" basis, WITHOUT WARRANTY
 * OF ANY KIND, either express or implied. See the LGPL for
 * the specific language governing rights and limitations.
 *
 * The Original Code is the ALNS-framework library.
 *
 * File name:
 *  ParameterTuning.cpp
 * Contributor(s):
 *	Renaud Masson
 */

#define _GLIBCXX_USE_NANOSLEEP

#include <ctime>
#include "ParameterTuning.h"
#include "../ALNS_inc.h"
#include <sstream>

using namespace std;



ParameterTuning::ParameterTuning(int temperature, double _alpha,long long runTime,size_t itera){
	temp=temperature;
	alpha=_alpha;
	runtime=runTime;
	itermax=itera;
}

/*
ParameterTuning::ParameterTuning(Instance& ins, double Alpha, long long runTime ){
	temp=ins.nb_ride;
	alpha=Alpha;
	runtime=runTime;
}
*/

ParameterTuning::~ParameterTuning() {
}

