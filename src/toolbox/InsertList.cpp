#include "InsertList.h"
#include "iostream"

/*
 * InsertList.cpp
 *
 *  Created on: Jul 2, 2016
 *      Author: sam
 */

using namespace std;

InsertList::InsertList(const int & _siz){
	siz=_siz;
	for (int i = 1; i < siz; ++i) // L'ordre est important !
		for (int j = i+1; j <= siz; ++j){
			std::pair<int,int> p(i,j);
			l.push_back(p);
		}
}

InsertList::~InsertList() {
}

bool InsertList::setNext(RequestInfo & r){
	if(l.empty())
		return false;
	r.i=l.front().first;
	r.j=l.front().second;
	l.erase(l.begin());
	return true;
}

void InsertList::exploit(const NonFeasibExplain& nfe){ //,const RequestInfo & req) {
	switch (nfe.ex) {
		case capacityLimit:{
			std::list<std::pair<int,int>>::iterator it = l.begin();
			while (it != l.end()){
				if ( (*it).first<nfe.pos && (*it).second>nfe.pos) {
					//removed.push_back(std::pair<int,int>((*it).first,(*it).second));
					l.erase(it++);
				}
				else ++it;
			}
			//cout<<nfe<<endl;
			//cout<<*this<<endl;
			break;}
		case pickupAloneNotPossible:{
			std::list<std::pair<int,int>>::iterator it = l.begin();
			while (it != l.end()){
				if ( (*it).first == nfe.pos) {
					//removed.push_back(std::pair<int,int>((*it).first,(*it).second));
					l.erase(it++);
				}
				else ++it;
			}
			break;}
		case deliveryAloneNotPossible:{
			std::list<std::pair<int,int>>::iterator it = l.begin();
			while (it != l.end()){
				if ( (*it).second == nfe.pos) {
					//removed.push_back(std::pair<int,int>((*it).first,(*it).second));
					l.erase(it++);
				}
				else ++it;
			}
			break;}
		case pickupAfterNotPossible:{
			std::list<std::pair<int,int>>::iterator it = l.begin();
			while (it != l.end()){
				if ( (*it).first >= nfe.pos) {
					//removed.push_back(std::pair<int,int>((*it).first,(*it).second));
					l.erase(it++);
				}
				else ++it;
			}
			break;}
		case pickupBeforeNotPossible:{
			std::list<std::pair<int,int>>::iterator it = l.begin();
			while (it != l.end()){
				if ( (*it).first <= nfe.pos) {
					//removed.push_back(std::pair<int,int>((*it).first,(*it).second));
					l.erase(it++);
				}
				else ++it;
			}
			break;}
		case deliveryAfterNotPossible:{
			std::list<std::pair<int,int>>::iterator it = l.begin();
			while (it != l.end()){
				if ( (*it).second >= nfe.pos) {
					//removed.push_back(std::pair<int,int>((*it).first,(*it).second));
					l.erase(it++);
				}
				else ++it;
			}
			break;}
		case deliveryBeforeNotPossible:{
			std::list<std::pair<int,int>>::iterator it = l.begin();
			while (it != l.end()){
				if ( (*it).second <= nfe.pos) {
					//removed.push_back(std::pair<int,int>((*it).first,(*it).second));
					l.erase(it++);
				}
				else ++it;
			}
			break;}
		default: break;
	}
}

void InsertList::convert2Matrix(std::vector<std::vector<bool> > & v) const {
	v.resize(siz);
	for (int i = 0; i < siz; ++i) {
		v[i].resize(siz,false);
	}
	std::list<std::pair<int,int>>::const_iterator i = l.begin();
	while (i != l.end()){
		v[(*i).first][(*i).second]=true;
		++i;
	}
}

std::ostream& operator<<(std::ostream& out, const InsertList& f){
	std::vector<std::vector<bool> > v;
	f.convert2Matrix(v);
	for (int i = 0; i < f.siz; ++i) {
		for (int j = 0; j < f.siz; ++j) {
			out<<v[i][j]<<" ";
		}
		out<<"\n";
	}
	return out;
}

