/*
 * TimeEvaluation.h
 *
 *  Created on: Aug 28, 2016
 *      Author: oscar
 */

#ifndef SRC_TOOLBOX_TIMEEVALUATION_H_
#define SRC_TOOLBOX_TIMEEVALUATION_H_

#include "../darp/structure/NodeSol.h"
#include "../darp/instanceManager/Instance.h"
#include "../constant.h"

class TimeEvaluation {

public:

	TimeEvaluation(Instance* _inst);
	virtual ~TimeEvaluation();

	bool performScheduling(std::vector<NodeSol*> vnode);
	void updateParameters();

	const std::vector<long>& getFts() const {
		return FTS;
	}

	const std::vector<long>& getT() const {
		return T;
	}

	long getDuration(const long & distance=0) const{
		long duration=0;
		if(cst::QuAndBard) duration= distance*2; // car speed is 0.5 miles/min
		else duration=getT()[node.size()-1] - getT()[0];
		return duration;
	}

	long getSumridetimes() const {
		return sumridetimes;
	}

	/**
	 * Computes an estimation of the route variable cost.
	 * This method is useful to calculate the cost of miniroutes in neightbourhood class
	 */
	long computeCostEstimation();

	/**
	 * given an id, it returns the position in route
	 */
	int getNodePosition(int id);


	void setNodes(std::vector<NodeSol*> vnode){
		node=vnode;
	}
	bool presedenceCheck();

private:
	/**
	 * test the skeduling
	 * optimize the tour duration
	 * optimize the ride time
	 * @return
	 */
	bool scheduling();


	bool nestedDelivery(int i, int pos_delivery);

	void setBeginningOfService(int pos=1);

	void computeSumRideTimes();

	void printT();

	bool isDelivery(int pos) const{
		return (type(pos)==1);
	}

	int idSym(const int & i) const{
		return (int) node[i]->node->idsym;
	}

	bool isPickup(int pos) const{
		return (type(pos)==0);
	}

	int id(const int & i) const{
		return (int) node[i]->node->id;
	}

	const long & s(const int & i) const{
		return node[i]->node->stime;
	}

	const int & L(const int & i,const int & u) const{
		return node[i]->load[u];
	}
	const int & l(const int & i,const int & u) const{
		return node[i]->node->rload[u];
	}

	const long & a(const int & i) const{
		return node[i]->node->tw_start;
	}

	const long & b(const int & i) const{
		return node[i]->node->tw_end;
	}


	const long & maxRT(const int & i) const{
		return node[i]->node->maxRT;
	}

	int type(const int & i) const{
		return (int) node[i]->node->type;
	}

	int posSym(const int & i) const{
		return (int) *node[i]->posSym;
	}

	void setPos(const int & i) const{
		node[i]->position=i;
	}

	long  Tnext(const int & i) const{
		return inst->time_ij[node[i]->node->id][node[i+1]->node->id];
	}

	long  Dnext(const int & i) const{
		return inst->distance_ij[node[i]->node->id][node[i+1]->node->id];
	}
	long & ti(const int & i) const{
		return node[i]->time;
	}


	long & fts(const int & i) const{
		return node[i]->FTS;
	}

	long & bts(const int & i) const{
		return node[i]->time;
	}


	/**
	 * affect the duration of segments in the tour
	 */
	//void setTij();

	/**
	 * compute the service time
	 * @return true if feasible
	 */
	//bool setT();

	/********************Debugging methods***************************/
	bool twtest(int type);
	bool testTimes();

	//**********************Parameters
	std::vector< long > T;  // Passing time
	std::vector< long > FTS;  // Forward time Slack
	std::vector<NodeSol*> node;
	Instance* inst;
	int dim;
	long sumridetimes=0;
	long sumridetimes2=0;  // temporal..REMOVE
	long sumridetimes3=0;  // temporal..REMOVE
	std::vector<int> sol;    // temporal..REMOVE
	int cont=0;
};

#endif /* SRC_TOOLBOX_TIMEEVALUATION_H_ */
