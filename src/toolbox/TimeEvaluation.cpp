/*
 * TimeEvaluation.cpp
 *
 *  Created on: Aug 28, 2016
 *      Author: oscar
 */

#include <iostream>

#include "TimeEvaluation.h"
#include "../constant.h"

using namespace std;


TimeEvaluation::TimeEvaluation(Instance* _inst){
	inst=_inst;
	dim=node.size();
	sumridetimes=0;
	// temporal to remove
	cont=-1;
}

TimeEvaluation::~TimeEvaluation() {

}
bool TimeEvaluation::performScheduling(std::vector<NodeSol*> vnode){
	node.clear();
	node=vnode;
	dim=node.size();
	T.resize(dim,0);  // Departure time
	FTS.resize(dim,0);  // Forward Time Slack
	++cont;
	return scheduling();
}

void TimeEvaluation::updateParameters(){
	for (size_t i=0; i <node.size()-1; ++i) {
		node[i]->setTime(T[i]);
		node[i]->setTnext(Tnext(i));
	}
	size_t n=node.size()-1;
	node[n]->setTime(T[n]);
	node[n]->setTnext(0);
	T.clear();
}

bool TimeEvaluation::scheduling() {



	long delta=0, pos=0;
	long W=0; // waiting time used in Forward time slack

	setPos(0);
	T[0]=a(0);
	FTS[0]= b(0) -T[0];

	//earliest start
	for (int i = 1; i < dim; ++i)
	{
		T[i] = max(T[i-1]+Tnext(i-1) + s(i-1), a(i));
		if(T[i] > b(i))
			return false;
		W = W + max((long)0, a(i) -(T[i-1]+Tnext(i-1)+s(i-1)));
		FTS[0]= min(FTS[0], W + max((long)0,b(i) - T[i]));

		setPos(i);  //used neighbourhood class
	}

	// optimize route duration
	T[0]=T[0]+FTS[0];
	setBeginningOfService();

	if((T[dim-1]-T[0])>maxRT(0))
		return false;  // route duration feasibility

	// Second pass: Fix Max Ride time violations  (Tang et all)
	for (int i = dim-2; i > 0; --i)
	{
		if (isPickup(i))
		{
			//pos=getNodePosition(idSym(i));  // Can be optimized with vrevers, chech in eval insertion is updated aswell
			pos=posSym(i);

			if(cst::QuAndBard) delta=T[pos] -T[i] - maxRT(i);
			else delta=T[pos] -(T[i] +s(i)) - maxRT(i);

			if(delta>0){

				T[i]=T[i]+delta;

				if (T[i] > b(i))
					return false;

				for (int k = i+1; k < dim; ++k) {

					auto Tnew=T[k-1]+Tnext(k-1)+s(k-1);
					T[k] = max(T[k],Tnew);

					if(T[k] > b(k))
						return false;
					//W =  T[k]- (T[k-1]+inst->vtime[nj->id][nk->id]+nj->stime);
					//F[k-1]=min(W , (long) nj->tw_end - T[k-1]);
				}
				if((T[pos]- (T[i]+s(i)) - maxRT(i)) > 0)
					return false;

			}
		}
	}
	//twtest(0);
	//printT();
	if(cst::costRideTimes) computeSumRideTimes(); // make this method inside scheduling
	return true;

}


long TimeEvaluation::computeCostEstimation(){
	long t_time=T[dim-1]-T[0];
	long t_dist=0;
	for(size_t i =0;i< T.size()-1;++i){
		t_dist+=Dnext(i);
	}
	return 3*t_time+ t_dist;  // estimation acording to gihp
}

void TimeEvaluation::computeSumRideTimes(){
	sumridetimes=0;

	if(cst::QuAndBard)
	{
		vector<vector<int>> Load(T.size());
		Load[0].resize(3);
		for(size_t i =1;i< T.size();++i){
			Load[i].resize(3);
			for (size_t u = 0; u < inst->getNbUserType(); ++u) {
				Load[i][u]=Load[i-1][u]+l(i,u);
			}
			sumridetimes+=(Load[i-1][0])*(T[i]-T[i-1]);
		}
	}
	else
	{
		for(size_t i =0;i< T.size();++i)
		{
			if(isPickup(i)){
				sumridetimes+=T[posSym(i)]-T[i];
			}
		}
	}
}
// This method can be removed once we include a revers  reference to pos in each node
int TimeEvaluation::getNodePosition(int idn){
	for (size_t i = 0; i < T.size(); ++i) {
		int idx=id(i);
		if (idn==idx){
			return i;
		}
	}
	return -1;
}
bool TimeEvaluation::nestedDelivery(int i, int pos_delivery){
	bool nested=false;
	int pos_pickup=getNodePosition(idSym(pos_delivery));
	if(pos_pickup > i) nested=true;
	return nested;
}

void TimeEvaluation::setBeginningOfService( int pos)
{
	for (int i = pos ; i < (int) T.size(); ++i) {
		T[i] = max(T[i-1]+Tnext(i-1) + s(i-1), a(i));
	}
}

void TimeEvaluation::printT(){
	for (size_t i = 0; i < T.size(); ++i) {
		cout<<T[i]<<" ";
	}
	cout<<endl;
}
bool TimeEvaluation::presedenceCheck(){
	if(!isPickup(0)) return false;
	for(int i=0; i < node.size(); ++i){
		int ps=getNodePosition(idSym(i));
		if(isPickup(i) && ps<i) return false;
	}
	return true;
}


/********************** TESTING METHODS *********************************/

// testing method differs from cheacke in considering vector T.
bool TimeEvaluation::twtest(int type){
	NodeSol *nj,*njp;
	bool ok=true;
	if (type==0){
		for (size_t j = 0; j < node.size(); ++j) {
			nj=node[j];
			if(T[j] > nj->node->tw_end || T[j] < nj->node->tw_start) {
				cout << "fenetre de temps in node ["<<j<<"]"<<endl;
				ok=false;
			}
		}
		for (size_t j = 0; j < node.size(); ++j) {
			nj=node[j];
			if (nj->node->type==0) {
				int posym=getNodePosition(nj->node->idsym);
				long maxrt=T[getNodePosition(nj->node->idsym)]-T[j]-nj->node->stime;
				if (maxrt > nj->node->getMaxRt()) {
					ok=false;
					cout<<"max ride time " <<maxrt<<" R: "<<j<<","<<posym<<endl;
				}
			}
		}
		for (size_t j = 0; j < node.size()-1; ++j) {
			nj=node[j];
			njp=node[j+1];
			if(T[j] + inst->time_ij[nj->node->id][njp->node->id] > T[j+1]) {
				ok=0;
				cout << "durée de trajet non respecté pour node["<<j<<"->"<<j+1<<"]"<<endl;
			}
		}
	}else{
		for (size_t j = 1; j < node.size()-1; ++j) {
			nj=node[j];
			if(nj->time  > nj->node->tw_end || nj->time < nj->node->tw_start) {
				cout << "fenetre de temps node["<<j<<"]"<<endl;
				ok=false;
			}
		}
	}
	return ok;
}

bool TimeEvaluation::testTimes(){
	NodeSol *nj,*njp;
	bool test=1;
	nj=node[node.size()-1];
	if(nj->time > nj->node->tw_end || nj->time < nj->node->tw_start){
		test=0;
		cout << "fenetre de temps non respecté pour le_node["<< node.size()<<"]"<<endl;
	}
	//les autres
	for (size_t j = 0; j < node.size()-1; ++j) {
		nj=node[j];
		njp=node[j+1];
		if(nj->time > nj->node->tw_end || nj->time < nj->node->tw_start) {
			test=0;
			cout << "fenetre de temps non respecté pour node["<<j<<"]"<<endl;
		}
		if(nj->time + inst->time_ij[nj->node->id][njp->node->id] > njp->time) {
			test=0;
			cout << "durée de trajet non respecté pour node["<<j<<"->"<<j+1<<"]"<<endl;
		}
	}
	//maximum riding time constraint
	for (size_t j = 0; j < node.size(); ++j) {
		nj=node[j];
		if (nj->node->type==0) {
			int posym=-1;
			for (int k = 0; k < node.size(); ++k) {
				if (node[k]->node->id==nj->node->idsym) {
					posym=k;
				}
			}
			long maxrt=node[posym]->time - nj->time - nj->node->stime;
			if (maxrt > nj->node->getMaxRt()) {
				test=false;
				cout<<"violation max ride time " <<maxrt<<" Req : ("<<j<<","<<posym<<")"<<endl;
			}
		}
	}
	return true;
}
