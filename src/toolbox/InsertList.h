/*
 * InsertList.h
 *
 *  Created on: Jul 2, 2016
 *      Author: sam
 */

#ifndef DARP_TOOLS_INSERTLIST_H_
#define DARP_TOOLS_INSERTLIST_H_

#include <ostream>
#include <list>
#include <vector>
#include "../darp/structure/RequestInfo.h"
#include "NonFeasibExplain.h"

//#include <tuple>

class InsertList {
public:

	/**
	 * initialize the list l
	 * @param _siz
	 */
	InsertList(const int & _siz);

	virtual ~InsertList();

	int siz;
	std::list<std::pair<int,int>> l;
	//std::list<std::pair<int,int>> removed;

	/**
	 * set the value of first element of the list and remove it
	 * @param n
	 * @return false if the list is empty
	 */
	bool setNext(RequestInfo & r);

	/**
	 * exploit the information in the explaination of non feasibility
	 * @param nfe
	 */
	void exploit(const NonFeasibExplain & nfe); //,const RequestInfo & req);


	/**
	 * convert to a matrix to simplify the display
	 */
	void convert2Matrix(std::vector<std::vector<bool> > & v) const ;

};


/**
 * affichage avec cout <<
 */
std::ostream& operator<<(std::ostream&, const InsertList&);



#endif /* DARP_TOOLS_INSERTLIST_H_ */
