#include "NonFeasibExplain.h"

/*
 * FeasibData.cpp
 *
 *  Created on: Jul 1, 2016
 *      Author: sam
 */


std::ostream& operator<<(std::ostream& out, const NonFeasibExplain& f){
	out<< f.toStr() << " "<<f.pos;
	return out;
}
