/*
 * CapacityEvaluation.cpp
 *
 *  Created on: Sep 3, 2016
 *      Author: oscar
 */

#include "CapacityEvaluation.h"
#include "../constant.h"
#include <bitset>

CapacityEvaluation::CapacityEvaluation(Instance* _inst) {
	inst=_inst;
	distance=0;
}

CapacityEvaluation::~CapacityEvaluation() {
}

// !Returns best vehicle if feasible or nullptr if none vehicle can do it
VehicleType* CapacityEvaluation::performCapacityTest(std::vector<NodeSol*> & vnode, std::vector<std::pair<double ,VehicleType*>> lvehicles, long _distance) {
	nodes=&vnode;
	vehicles=lvehicles;
	distance=_distance;
	std::vector<int> dominantNodes=getDominantNodes();
	VehicleType* minCostVehicle=nullptr;
	double minCost=INT_MAX, cost=0;
	for(auto& vehicle: vehicles){
		if(isFesibleVehicle(vehicle.second,dominantNodes)){
			cost = vehicle.second->getFixedCost()+vehicle.second->getDistanceCost()*(double)distance;
			if(cost < minCost){ //selecting the min cost vehicle
				minCostVehicle=vehicle.second;
				vehicle.first=cost;
				minCost=cost;
			}
			if(cst::SortVehicle)
				return minCostVehicle;
		}
	}
	return minCostVehicle;
}

bool CapacityEvaluation::isFesibleVehicle(VehicleType* vt, std::vector<int>& dominantNodes) {
	if(cst::BitSet) {
		return singleVehicleCapacityBitSet(vt, dominantNodes);
	}else{
		return singleVehicleCapacityTest(vt);
	}
}

std::vector<int> CapacityEvaluation::getDominantNodes(){
	l.resize(inst->nb_user_type,0); // s
	L.resize(size(),l); 							 // load profile														// compute new demands in the route
	std::vector<int> dominantNodes;  				// start in the origin
	for (size_t i = 1; i < size(); ++i) {
		for (size_t u = 0; u < inst->getNbUserType(); ++u) {
			L[i][u]=L[i-1][u]+niSol(i)->node->rload[u];
		}
		if(cst::QuAndBard){
			L[i][2] += niSol(i)->node->rload[0]+2*niSol(i)->node->rload[1];
		}
		if(isDelivery(i) && isPickup(i-1)) {
			dominantNodes.push_back(i-1);
		}
	}
	return dominantNodes;
}




// !Returns true if vehicle is feasible else false
bool  CapacityEvaluation::singleVehicleCapacityTest(VehicleType* vehicle){
	setConfigurations.clear();
	setConfigurations.resize(size(),0);  // stores the id of a given configuraion.
	bool isFeasible=false;
	for(size_t i=0; i< size(); ++i ){
		for (auto& configuration : vehicle->getConfiguration()) {
			isFeasible=true;
			for (size_t u = 0; u < inst->getNbUserType() && isFeasible; ++u) {
				if(L[i][u] > configuration->getCapacity()[u]) {
					isFeasible=false;
				}
			}
			if(isFeasible) {
				setConfigurations[i].set(configuration->getId());
				//break;
			}
		}
		if(setConfigurations[i].none()) {
			return false;
		}
		//std::cout<<fconfig[node_pos].to_string()<<std::endl;
	}
	if(cst::maxNbReconfigurations){
		nbReconfigurations=computeNbReconfigurations(setConfigurations);
		//if(nbReconfigurations>0) std::cout<<"nbr > 0!!!!!!!!!!!!!"<<nbReconfigurations<<std::endl;
		if(nbReconfigurations > inst->getMaxNbReconfigurations()) return false;
	}
	return isFeasible;
}

int CapacityEvaluation::computeNbReconfigurations(std::vector<std::bitset<64> >& configurations){
	auto cont=0;
	idconfiguration.clear();
	idconfiguration.resize(size(),-1);
	std::vector<std::bitset<64> > s(configurations.size(),0);
	s[0].flip(); // set to 1
	auto last=configurations.size();
	for(size_t i=1; i< last; ++i){
		s[i]=configurations[i]&s[i-1];
		if(s[i]==0){
			cont++;
			s[i]=configurations[i];
			//idconfiguration[i]=getConfigurationId(s[i]); //TODO Remove calcul idconfiguration
		}
	}
	//idconfiguration[last-1]=getConfigurationId(s[last-1]);
	return cont;
}


int CapacityEvaluation::getConfigurationId(std::bitset<64> b){
	int idx = 0;
	while (idx < (int)b.size() && !b[idx]) {
		++idx;
	}
	return idx;
}

bool CapacityEvaluation::singleVehicleCapacityBitSet(VehicleType* vehicle, std::vector<int>& dominantNodes){
	std::bitset<64> s;
	std::bitset<64> e;
	e.flip();
	int r=0;
	for(int i : dominantNodes ){
		s=vehicle->getBitSet(L[i]);
		if(s==0){  // capacity test
			return false;
		}
		e = e & s; // max nb_reconfigurations
		if(e==0){
			r++;
			if(r > inst->getMaxNbReconfigurations() && cst::maxNbReconfigurations)
				return false;
			e=s;
		}
	}
	nbReconfigurations=r;  //important
	return true;
}


void CapacityEvaluation::updateLoadParameters() {
	for (size_t i = 0; i < size(); ++i) {
		niSol(i)->load=L[i];
	}
	L.clear();
}

void CapacityEvaluation::setIdConfigParameters(std::vector<NodeSol*>& vnode, VehicleType * vt, int* nbRec){
	nodes=&vnode;
	getDominantNodes();
	nbReconfigurations=0;
	idconfiguration.clear();
	idconfiguration.resize(size(),-1);
	std::bitset<64> s,e,ep;
	e.flip();
	for(int i =0 ; i<size() ; ++i){
		s=vt->getBitSet(L[i]);
		ep=e;
		e = ep & s; // max nb_reconfigurations
		if(e==0){
			for (int j = i-1; j >= 0; --j) {
				if(idconfiguration[j]==-1)
					idconfiguration[j]=getConfigurationId(ep);
				else break;
			}
			e=s;
			nbReconfigurations++;
		}
	}

	for (int j = static_cast<int>(size())-1; j >= 0; --j) {
		if(idconfiguration[j]==-1)
			idconfiguration[j]=getConfigurationId(ep);
		else break;
	}

	e=s;
	for (size_t i = 0; i < size(); ++i) {
		niSol(i)->setIdConfig(idconfiguration[i]);
	}
	L.clear();
	*nbRec=nbReconfigurations;  //usefull pour l'affichage
}


void CapacityEvaluation::updateVehicleListWithFeasibleVehicles(
		std::vector<NodeSol*> & vnode,
		std::vector<std::pair<double, VehicleType*> >& vehicles) {
	nodes=&vnode;
	std::vector<int> dominantNodes=getDominantNodes();
	for(std::vector<std::pair<double, VehicleType*>>::iterator itr = vehicles.begin();
			itr != vehicles.end();){
		std::pair<double, VehicleType*> vehicle =*itr;
		if (isFesibleVehicle(vehicle.second,dominantNodes))
			++itr;
		else
			itr=vehicles.erase(itr);
	}
}

void CapacityEvaluation::printLoadProfile(){
	for (size_t i = 0; i < size(); ++i) {
		std::cout<<niSol(i)->id<<"\t"<<niSol(i)->position;
		for (size_t u = 0; u < inst->getNbUserType(); ++u) {
			std::cout<<"\t"<<L[i][u];
		}
		std::cout<<std::endl;
	}
	std::cout<<"-------"<<std::endl;
}

