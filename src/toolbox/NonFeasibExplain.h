/*
 * FeasibData.h
 *
 *  Created on: Jul 1, 2016
 *      Author: sam
 */

#ifndef DARP_TOOLS_NONFEASIBEXMPLAIN_H_
#define DARP_TOOLS_NONFEASIBEXMPLAIN_H_

#include <ostream>
#include "string"

//	enum FeasibData {
//		none,
//		timeWindowLimit,
//		maxRideTimeLimit,
//		capacityLimit,
//	};


enum Explain{
	none,
	scheduling,
	capacityLimit,
	pickupAloneNotPossible,
	deliveryAloneNotPossible,
	pickupAfterNotPossible,
	pickupBeforeNotPossible,
	deliveryBeforeNotPossible,
	deliveryAfterNotPossible
};

class NonFeasibExplain {
public:
	NonFeasibExplain(){
		ex=none;
		pos=-1;
	}

	virtual ~NonFeasibExplain(){}


	void operator=(const Explain & e){
		ex=e;
	}

	const bool operator==(const Explain & e) const {
		return ex==e;
	}


	void setExplain(const Explain & e, const int & i){
		if(this!=nullptr){
			ex=e;
			pos=i;
		}
	}

	std::string toStr()const {
	    switch(ex) {
	        case none:   return  "  0";
	        case scheduling:   return  "tim";
	        case capacityLimit:   return  "cap";
	        case pickupAloneNotPossible:   return  "Pic";
	        case deliveryAloneNotPossible:   return  "Del";
	        case pickupAfterNotPossible : return "PiA";
	        case pickupBeforeNotPossible : return "PiB";
	        case deliveryAfterNotPossible : return "DeA";
	        case deliveryBeforeNotPossible : return "DeB";
	    }
	    return  "-";
	}

//	void setTimeWindow(){
//		ex=timeWindows;
//	}
//

//private:
	Explain ex;
	int pos;


};


/**
 * affichage avec cout <<
 */
std::ostream& operator<<(std::ostream&, const NonFeasibExplain&);



#endif /* DARP_TOOLS_NONFEASIBEXPLAIN_H_ */
