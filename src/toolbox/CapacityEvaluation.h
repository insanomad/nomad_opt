/*
 * CapacityEvalutaion.h
 *
 *  Created on: Sep 3, 2016
 *      Author: oscar
 */

#ifndef SRC_TOOLBOX_CAPACITYEVALUATION_H_
#define SRC_TOOLBOX_CAPACITYEVALUATION_H_
#include "../darp/instanceManager/Instance.h"
#include <list>
#include <bitset>

#include "../darp/structure/NodeSol.h"
#include "NonFeasibExplain.h"

class CapacityEvaluation {
public:
	CapacityEvaluation(Instance* _inst);
	virtual ~CapacityEvaluation();
	/**
	 *
	 * @param vnode the list of visited nodes in the route
	 * @param lvehicles the list of vehicle candidate
	 * @param dist the total distance of the route (to compute the total cost of the route)
	 * @return the cheapest feasible vehicle or NULL
	 */
	VehicleType* performCapacityTest(std::vector<NodeSol*> & vnode, std::vector<std::pair<double ,VehicleType*> > lvehicles, long dist);

	/**
	 * update the load in loadInSol of the route
	 */
	void updateLoadParameters();

	/**
	 * set the idConfig of the route
	 * @param vt vehicle type chosen for the route
	 */
	void setIdConfigParameters(std::vector<NodeSol*>& vnode, VehicleType * vt, int* nbRec);

	/**
	 * constitute the list of fesible vehicle
	 * @param vehicles list of vehicle to test and filter
	 */
	void updateVehicleListWithFeasibleVehicles(std::vector<NodeSol*> & vnode, std::vector<std::pair<double ,VehicleType*> >& vehicles) ;

	/**
	 * test the feasibility of a vehicle on the route
	 * @param vt the vehicle type to test
	 * @param dominantNodes the set of dominant nodes (usefull only in bitSet mode)
	 * @return true if the vehicle is feasible
	 */
	bool isFesibleVehicle(VehicleType * vt,std::vector<int>& dominantNodes);

	/**
	 * get the list of dominant nodes
	 * @return the list of dominant nodes
	 */
	std::vector<int> getDominantNodes();

	/**
	 * Prints in console the current load profile L
	 */
	void printLoadProfile();

	const std::vector<std::pair<double ,VehicleType*> >& getVehicles() const { return vehicles;}

	bool isPickup(int pos) const{
		return (type(pos)==0);
	}

	bool isDelivery(int pos) const{
		return (type(pos)==1);
	}


	int type(const int & i) const{
		return (int)(*nodes)[i]->node->type;
	}

	NodeSol * niSol(size_t i) const{
		return (* nodes)[i];
	}

	const size_t size() const {
		return (* nodes).size();
	}

	void setNodes(std::vector<NodeSol*>& nodes) {
		this->nodes = &nodes;
	}

	int getNbReconfigurations() const {
		return nbReconfigurations;
	}



private:
	bool singleVehicleCapacityTest(VehicleType* vehicle);

	/**
	 * test the fesibility of a given vehicle
	 * @param vehicle
	 * @param dominantNodes
	 * @return true if vehicle is feasible
	 */
	bool singleVehicleCapacityBitSet(VehicleType* vehicle, std::vector<int>& dominantNodes);

	//void computeDistance();
	int computeNbReconfigurations(std::vector<std::bitset<64> >& cload);

	int getConfigurationId(std::bitset<64> b);

	//Parameters
	Instance * inst=nullptr;
	long distance=0;
	int nbReconfigurations=0;

	//auto nbUserType=0;
	std::vector<NodeSol*> * nodes=nullptr;
	std::vector<std::pair<double ,VehicleType*> > vehicles; //TODO sam:je ne vois pas l'interet de cette structure de donnée, une simple liste de vehicule devrait suffire non ?

	//route profile using ints
	std::vector< std::vector<int> > L;  // Load
	std::vector<int> l;  // vector needed to initialize L each iteration.
	std::vector<int> idconfiguration;
	std::vector<std::bitset<64> > setConfigurations; //idconfigurations

};

#endif /* SRC_TOOLBOX_CAPACITYEVALUATION_H_ */

