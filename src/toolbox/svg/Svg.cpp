#include "../../toolbox/svg/Svg.h"

/*
 * Svg.cpp
 *
 *  Created on: Jun 1, 2016
 *      Author: sam
 */


#include "../../toolbox/svg/simple_svg_1.0.0.h"



//using namespace svg;
using namespace std;



void modeChoice(Tour * t, svg::Document& doc, int mode, int y0, int ecart, vector<int> & rgb, double fa) {
	long x0=t->v_node[0]->node->tw_start-30;

	for (size_t i = 0; i < t->v_node.size(); ++i) {
		Node *n =  t->v_node[i]->node;
		size_t sympos = *(t->v_node[i]->posSym);
		svg::Color color(rgb[i*3],rgb[i*3+1],rgb[i*3+2]);
		if (n->type==1) {
			color.assign(rgb[sympos*3],rgb[sympos*3+1],rgb[sympos*3+2]);
		}
		//name
		double yDec=y0+i*ecart;
		doc << svg::Text(svg::Point(fa*5, (yDec+7)*fa), to_string(t->v_node[i]->node->id), color, svg::Font(12*fa, "Verdana"));
		//TW
		doc << svg::Line(svg::Point((n->tw_start-x0)*fa,yDec*fa), svg::Point((n->tw_end-x0)*fa, yDec*fa), svg::Stroke(1*fa, color));
		doc << svg::Text(svg::Point((n->tw_start-x0)*fa, (yDec+8)*fa), "a"+to_string(i)+"="+to_string(n->tw_start), color, svg::Font(8*fa, "Verdana"));
		doc << svg::Text(svg::Point((n->tw_end-x0)*fa,   yDec*fa), "b"+to_string(i)+"="+to_string(n->tw_end  ), color, svg::Font(8*fa, "Verdana"));
		//time
		long time=t->v_node[i]->time;
		if(mode==1) time=t->v_node[i]->tTot;
		else if(mode==2) time=t->v_node[i]->tTard;
		doc << svg::Line(svg::Point((time-x0)*fa, (yDec-5)*fa), svg::Point((time-x0)*fa, (yDec+2)*fa), svg::Stroke(1*fa, color));
		doc << svg::Text(svg::Point((time-x0)*fa, (yDec-5)*fa), "t"+to_string(i)+"="+to_string(time), color, svg::Font(8*fa, "Verdana"));
		yDec+=10;
		//transport
		if(i< t->v_node.size()-1){
			long tij=t->v_node[i]->tnext;
			doc << svg::Line(svg::Point((time-x0)*fa,     yDec*fa), svg::Point((time-x0+tij)*fa, yDec*fa), svg::Stroke(4*fa, color));
			doc << svg::Text(svg::Point((time-x0+tij)*fa, (yDec+2)*fa), "traj"+to_string(i)+"="+to_string(tij), color, svg::Font(8*fa, "Verdana"));
		}
		//maximum ride time
		if(n->type==0 || n->type==2 ){
			long maxRT=t->v_node[i]->node->getMaxRt();
			doc << svg::Line(svg::Point((time-x0)*fa, (yDec+3)*fa), svg::Point((time-x0+maxRT)*fa, (yDec+3)*fa), svg::Stroke(0.5*fa, color));
			doc << svg::Text(svg::Point((time-x0+maxRT)*fa, (yDec+3)*fa), "maxRT"+to_string(i)+"="+to_string(maxRT), color, svg::Font(8*fa, "Verdana"));
		}
		else if (n->type==1 || n->type==3) {
			doc << svg::Line(svg::Point((time-x0)*fa, (y0+13+sympos*ecart)*fa), svg::Point((time-x0)*fa, (yDec-7)*fa), svg::Stroke(1*fa, color));
		}
		if(mode==0){
			doc << svg::Line(svg::Point((time-x0)*fa, (yDec-2)*fa), svg::Point((time-x0)*fa, (yDec+11)*fa), svg::Stroke(1*fa, color));
			long F=t->v_node[i]->FTS;
			doc << svg::Line(svg::Point((time-x0)*fa, (yDec+5)*fa), svg::Point((time-x0+F)*fa, (yDec+5)*fa), svg::Stroke(0.5*fa, color));
			doc << svg::Text(svg::Point((time-x0+F)*fa, (yDec+11)*fa), "F"+to_string(i)+"="+to_string(F), color, svg::Font(8*fa, "Verdana"));
			long B=t->v_node[i]->BTS;
			doc << svg::Line(svg::Point((time-x0)*fa, (yDec+5)*fa), svg::Point((time-x0-B)*fa, (yDec+5)*fa), svg::Stroke(0.5*fa, color));
			doc << svg::Text(svg::Point((time-x0-B-25)*fa, (yDec+11)*fa), "B"+to_string(i)+"="+to_string(B), color, svg::Font(8*fa, "Verdana"));
		}
	}
}




Svg::Svg(Tour * t , std::string file) {
	std::vector<int> rgb;
	int r,g,b;
	for (size_t i = 0; i < t->v_node.size()*3; ++i) {
		do{
			r=rand()%255;
			g=rand()%255;
			b=rand()%255;
		}while(abs(r-b)+abs(b-g)+abs(g-r)<200 || (r>150 && g>150 && b>150));
		rgb.push_back(r);
		rgb.push_back(g);
		rgb.push_back(b);
	}
	int ecart=50;
	int largeur=t->v_node[t->v_node.size()-1]->node->tw_end-t->v_node[0]->node->tw_start;
	svg::Dimensions dimensions(((double)largeur+60)*fact, (t->v_node.size()*ecart//*3
			+40)*fact);
	svg::Document doc(file, svg::Layout(dimensions, svg::Layout::TopLeft));
    modeChoice(t,doc,0,10,ecart,rgb,fact);
    //modeChoice(t,doc,1,10+t->v_node.size()*ecart,ecart,rgb);
    //modeChoice(t,doc,2,10+t->v_node.size()*ecart*2,ecart,rgb);
    doc.save();
}

Svg::~Svg() {
}

