/*
 * RouteForSetCovering.h
 *
 *  Created on: Jun 13, 2016
 *      Author: oscar
 */

#ifndef SRC_ALNS_ROUTEFORSETCOVERING_H_
#define SRC_ALNS_ROUTEFORSETCOVERING_H_

#include<vector>
#include "../darp/structure/Tour.h"

using namespace std;

class RouteForSetCovering {
public:
	RouteForSetCovering();
	RouteForSetCovering(Tour* t, vector<int>* key);
	virtual ~RouteForSetCovering();
	vector<int> route;
	vector<int>* key=nullptr;
	double cost;
	VehicleType* vehicle;
	int nbSelected;

};

#endif /* SRC_ALNS_ROUTEFORSETCOVERING_H_ */
