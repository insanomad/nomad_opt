
/* ALNS_Framework - a framework to develop ALNS based solvers
 *
 * Copyright (C) 2012 Renaud Masson
 *
 * This library is free software; you can redistribute it and/or
 * modify it either under the terms of the GNU Lesser General Public
 * License version 3 as published by the Free Software Foundation
 * (the "LGPL"). If you do not alter this notice, a recipient may use
 * your version of this file under the LGPL.
 *
 * You should have received a copy of the LGPL along with this library
 * in the file COPYING-LGPL-3; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Suite 500, Boston, MA 02110-1335, USA
 *
 * This software is distributed on an "AS IS" basis, WITHOUT WARRANTY
 * OF ANY KIND, either express or implied. See the LGPL for
 * the specific language governing rights and limitations.
 *
 * The Original Code is the ALNS_Framework library.
 *
 *
 * Contributor(s):
 *	Renaud Masson
 */

#ifndef ADESTROYOPERATOR_H_
#define ADESTROYOPERATOR_H_

#include "AOperator.h"
#include "cmath"

class ISolution;

/*!
 * \class ADestroyOperator.
 * \brief This is an abstract class used to represent Destroy Operators.
 *
 * Any destroy operator should inherit from this class and implement the
 * destroySolution function.
 */
#include <limits.h>

class ADestroyOperator : public AOperator {

public:
	//! The minimum destroy size used. doit etre >0 !!!
	double minimunDestroyFrac;
	//! The maximum destroy size used.
	double maximumDestroyFrac;

public:
	//! Constructor.
	//! \param mini_percent the minimum percentage destroy size.
	//! \param maxi_percent the maximum percentage destroy size.
	//! \param s the name of the destroy operator.
	ADestroyOperator(std::string s, double mini_percent=0, double maxi_percent=UINT_MAX) : AOperator(s)
	{
		minimunDestroyFrac = mini_percent;
		maximumDestroyFrac = maxi_percent;
	}

	//! Destructor.
	virtual ~ADestroyOperator(){};

	//! This function is the one called to destroy a solution.
	//! \param sol the solution to be destroyed.
	virtual void destroySolution(ISolution& sol)=0;

	double getMaximumDestroyFrac() const {
		return maximumDestroyFrac;
	}

	void setMaximumDestroyFrac(double maximumDestroy_percent) {
		this->maximumDestroyFrac = maximumDestroy_percent;
	}

	double getMinimunDestroyFrac() const {
		return minimunDestroyFrac;
	}

	void setMinimunDestroyFrac(double minimunDestroy_percent) {
		this->minimunDestroyFrac = minimunDestroy_percent;
	}

	int getNbRemoval(const int & nbRequest){
		int mini=(int) std::max(1.0,nbRequest*minimunDestroyFrac);
		int maxi=(int) std::max(1.0,nbRequest*maximumDestroyFrac);
		return ( mini+std::rand()%(1+std::min(maxi - mini, nbRequest)) );	 // max number of request to destroy
	}

};

#endif /* DESTROYOPERATOR_H_ */
