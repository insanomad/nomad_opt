/* ALNS_Framework - a framework to develop ALNS based solvers
 *
 * Copyright (C) 2012 Renaud Masson
 *
 * This library is free software; you can redistribute it and/or
 * modify it either under the terms of the GNU Lesser General Public
 * License version 3 as published by the Free Software Foundation
 * (the "LGPL"). If you do not alter this notice, a recipient may use
 * your version of this file under the LGPL.
 *
 * You should have received a copy of the LGPL along with this library
 * in the file COPYING-LGPL-3; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Suite 500, Boston, MA 02110-1335, USA
 *
 * This software is distributed on an "AS IS" basis, WITHOUT WARRANTY
 * OF ANY KIND, either express or implied. See the LGPL for
 * the specific language governing rights and limitations.
 *
 * The Original Code is the ALNS_Framework library.
 *
 *
 * Contributor(s):
 *	Renaud Masson
 */


#ifndef ALNS_H_
#define ALNS_H_

/*!
 * \class ALNS.
 * \brief This class contains the ALNS logic.
 *
 * This class contains the logic of the ALNS (Adaptive Large Neighborhood Search).
 * The general idea of the ALNS is to iteratively destroy then repair a solution
 * to improve its quality. Non improving solution may be accepted as the new current
 * solution according to some acceptance criteria.
 * If you are interested about the general functioning of this method please refer to:
 * S. Ropke & D. Pisinger. An Adaptive Large Neighborhood Search Heuristic for Pickup
 * and Delivery Problem with Time Windows. Transportation Science, 40 (2006) 455-472.
 */

#include <cstring>
#include <math.h>
#include <time.h>
#include <set>
#include <vector>


#include "SetCovering.h"
#include "../darp/checker/Checker.h"
#include "../darp/operators/DesHistoryRemoval.h"
#include "ALNS_Iteration_Status.h"

class ISolution;
class ALNS_Parameters;
class AOperatorManager;
class ARepairOperator;
class ADestroyOperator;
class Statistics;




class ALNS {

private:
	//! The max number of reconfiguraions performed among all routes.
	double fleetUtilization;
	//! The max number of reconfiguraions performed among all routes.
	size_t nbReconfigurations;
	//! Number of routes using reconfigurations.
	size_t nbRoutesRec;
	//! The current number of iterations.
	size_t nbIterations;
	//! Keeps the time when the best solution is found
	clock_t TimeToReachBestSol;

	//! counters sa
	int count_iter=0;
	int count_succes=1; // start in 1 to avoid a temperature decrease in 1st iteration

	//! The parameters to be used.
	ALNS_Parameters* param;
	//! Current time
	double time=0.0;
	double timeLimitScp=0.0;
	size_t baseNbIterScp=0.0;
	bool prevSolOptimal=0;
	double timeLimit=0.0;

	//! difference between newsolution and currentsolution
	double sumDeltaCost=0;

	//! Solution objects.
	ISolution* currentSolution;
	ISolution* bestsolution;
	ISolution* newSolution;

	// Repair operator
	ARepairOperator* repair;

	/**
	 * Historical remove operator. Stats on this operator are updated evevery iteration.
	 */
	HistoryRemoval* history;

	// Destroy Operator
	ADestroyOperator* destroy;

	// Checker for feasibility
	Checker* checkNewSol;

	// Manager of the set covering problem
	SetCovering* SCP;

	//! Manager of the operators.
	AOperatorManager* opManager;

	//! The number of iterations since last weight recomputation.
	size_t nbIterationsWC;

	//! The current number of iterations without improvement.
	size_t nbIterWithoutImprove;

	//! The number of iterations without improvement of the current solution.
	size_t nbIterWithoutImproveCurrent;

	//! The number of iterations without acceptance of a transition.
	size_t nbIterationsWithoutTransition;

	//! The number of iterations since the last call to a local search
	//! operator.
	size_t nbIterationsWithoutLocalSearch;

	//! The number of times SCP finds new best solutions
	size_t nbNewBestSolSCP;

	//! The number of times SCP finds new current solutions (when its better than the previous one)
	size_t nbNewCurrSolSCP;

	//! The number of times SCP reaches optimality within the time limit
	size_t nbOptSolSCP;

	//! The time the optimization process started.
	clock_t startingTime;

	/**
	 * Acceptance probability in record to recod
	 */
	double acceptanceRecorToRecord;

	//! A lower bound on the optimum solution cost.
	double lowerBound;

	//! A set containing the hash keys of the encountred solutions.
	std::set<long long> knownKeys;

	//! An object to compute some statistics about the solving process.
	Statistics* stats;

	//! Name of output file
	std::string name;

	//! instance
	Instance* instance;

	//! An object representing the status of the last iteration.
	ALNS_Iteration_Status status;


public:
	//! Constructor.
	//! \param instanceName the name of the instance.
	//! \param initialSolution the starting solution that is going to be optimized.
	//! is accepted as the current solution.
	//! \param parameters the set of parameters to be use by the ALNS.
	//! \param opMan an operator manager.
	//! \param statistics object to record statistics.
	//! \param inst instance of the problem
	ALNS(std::string instanceName,
		 ISolution& initialSolution,
		 ALNS_Parameters& parameters,
		 AOperatorManager& opMan,
		 Statistics& statistics,
		 Instance* inst);

	//! Destructor.
	virtual ~ALNS();

	//! This method launch the solving process.
	//! \return true if a feasible solution is found,
	//! false otherwise.
	bool solve();

	//! This method seeks if a solution is already known,
	//! if not it is added to the set of known solutions.
	//! \param sol the solution to be checked.
	//! \return true if the solution was unknown, false otherwise.
	bool checkAgainstKnownSolution(ISolution& sol);

	//! This method perform one iteration of the ALNS solving
	//! process.
	void performOneIteration();

	//! This method check whether or not the stopping criteria is met.
	bool isStoppingCriterionMet();

	//! Determine whether or not the new solution is better than the
	//! best known solution.
	bool isNewBest(ISolution* newSol);

	//! Solve set covering problem
	void adaptiveLNS();
	void solveSetCoveringProblem(ISolution& currentSol, ISolution& bestSolution,  bool warmStart);

	//! Determine whether we should update or current known solution.
	void recordToRecord(ISolution* newSol);


	//! \return the number of known solutions.
	size_t getNumberKnownSolutions(){return knownKeys.size();};

	//! Determine whether or not the new solution should be accepted
	//! as the current solution.
	bool transitionCurrentSolution(ISolution* newSol);

	//compare hash keys of two solution
	bool nonEqual( ISolution* curSol, ISolution* newSol );

	//!add statistics entry
	void addStatEntry(std::string opdestroy, std::string oprepair);

	const std::set<long long>& getKnownKeys() const {
		return knownKeys;
	}

	const std::string& getName() const {
		return name;
	}

	size_t getNbIterations() const {
		return nbIterations;
	}

	size_t getNbNewBestSolScp() const {
		return nbNewBestSolSCP;
	}

	size_t getNbNewCurrSolScp() const {
		return nbNewCurrSolSCP;
	}

	size_t getNbOptSolScp() const {
		return nbOptSolSCP;
	}

	double getTimeLimit() const {
		return timeLimit;
	}

	clock_t getTimeToReachBestSol() const {
		return TimeToReachBestSol;
	}

	size_t getNbReconfigurations() const {
		return nbReconfigurations;
	}

	double getFleetUtilization() const {
		return fleetUtilization;
	}

	size_t getNbRoutesRec() const {
		return nbRoutesRec;
	}
};

#endif /* ALNS_H_ */
