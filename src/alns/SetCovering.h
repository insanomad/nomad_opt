/*
 * SetCovering.h
 *
 *  Created on: Jun 12, 2016
 *      Author: oscar
 */

#ifndef SRC_ALNS_SETCOVERING_H_
#define SRC_ALNS_SETCOVERING_H_
#include <map>
#include "RouteForSetCovering.h"
#include "../darp/solutionManager/DARPSolution.h"

using namespace std;


class SetCovering {

public:
	SetCovering();
	virtual ~SetCovering();

	//This method call the mip solver to perform the set covering
	// currentSo
	// warmStart
	// BestSolution used to build an initial solution and to set an upperBound
	// Time limit of resolution
	bool performSetCovering(ISolution& currenSol, ISolution& bestSol, size_t* NbCurrSol, size_t* nbOptSol, bool warmStart, bool* isOptimal , double timeLimit);
	//Add tours one by one from a given solution
	void addTours(DARPSolution* s);
	// deletes all the objects from the pool
	void cleanPoolTours();
	void cleanElite();
	// Determines the size of the pool for the scp for the next iteration
	void poolManagement(double clock_time, double timeLimit, int spoolsize, bool* isOptimal, size_t* nbOptSol);
	// init the scp solution considering that not all request are solved in this solution.
	void initSCPcluster(DARPSolution& setCoveringSol,DARPSolution& bestSol);

	//getters and setters
	size_t getPoolSize ()const {
		return poolTours.size();
	}

private:
	// Add dominant tours into the pool
	void addTour(Tour* t);
	void addToursElite(DARPSolution* Sol);
	void getTourIdentifier(std::vector<int>& hashkey, Tour* tour);

	//std::vector<std::pair<RouteForSetCovering*,std::vector<int> > > routes;
	std::map< std::vector<int>, RouteForSetCovering* > poolTours; // map with all routes so far
	std::map< std::vector<int>, RouteForSetCovering* > elite;  // routes from the BKS
	double countSCP; //dont change to int! othewise we need casting in other clases
	int maxNbColumns;
	int NbColumns;
	int poolIncrement;
	int maxNbColumnsSolvedOpt;
	double p=1;
	bool prevSolOptimal;
};


#endif /* SRC_ALNS_SETCOVERING_H_ */
