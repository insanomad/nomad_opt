/*
 * RouteForSetCovering.cpp
 *
 *  Created on: Jun 13, 2016
 *      Author: oscar
 */

#include "RouteForSetCovering.h"
#include <algorithm>
#include <vector>

using namespace std;

RouteForSetCovering::RouteForSetCovering(){
	cost=0;
	nbSelected=0;
	vehicle=nullptr;
}
/*transform a tour into a id key and tour sequence */
RouteForSetCovering::RouteForSetCovering(Tour* t, vector<int>* hashkey) {
	int nnodes=t->v_node.size();
	route.push_back(t->v_node[0]->node->id);
	for (int i = 1; i < (nnodes-1) ; ++i) {
		route.push_back(t->v_node[i]->id);//id node
	}
	route.push_back(t->v_node[nnodes-1]->node->id);
	vehicle=t->getVehicle();
	cost=t->getCost();
	key=hashkey;
	nbSelected=0;
}

RouteForSetCovering::~RouteForSetCovering() {
}
