/*
 * SetCovering.cpp
 *
 *  Created on: Jun 12, 2016
 *      Author: oscar
 */

//#define CPLEXINSTLED

#include "SetCovering.h"
//#include "MIPCoinOR.h"
#include "MIPCplex.h"
#include "ARepairOperator.h"
#include "../darp/operators/insRestoreSolution.h"
#include "../darp/checker/Checker.h"
#include "ISolution.h"
#include "../constant.h"

#include <algorithm>


SetCovering::SetCovering(){
	p=6;
	countSCP=0;
	maxNbColumns=50000;
	NbColumns=maxNbColumns;
	maxNbColumnsSolvedOpt=0;
	poolIncrement=300;
	prevSolOptimal=0;
};

SetCovering::~SetCovering() {
	cleanElite();
	cleanPoolTours();
}


// Perform the set covering among the available tours
//! true it founds a new best solution
//! false it couldn't improve the current solution or the solution no
bool SetCovering::performSetCovering(ISolution& currentSol, ISolution& bestSolution, size_t* nbCurrSol, size_t* nbOptSol, bool warmStart, bool* isOptimal, double timeLimit) {

#ifndef CPLEXNOTINSTLED

	countSCP++;

	DARPSolution* currSol=&dynamic_cast<DARPSolution&>(currentSol);
	DARPSolution* bestSol=&dynamic_cast<DARPSolution&>(bestSolution);

	if (warmStart) addToursElite(bestSol);
	MIPCplex PL(*bestSol, *currSol);

	// Create SCP solution
	DARPSolution* setCoveringSol= new DARPSolution("SCS",currSol->getInstance());
	if(cst::Clustering!=cst::single) initSCPcluster(*setCoveringSol,*bestSol);

	std::map<std::vector<int>,RouteForSetCovering*> selected_routes;

	auto start=clock();
	bool isFeasible=PL.mipSetCovering(*setCoveringSol,poolTours ,elite, warmStart,timeLimit,isOptimal);
	auto end=clock();
	double clock_time=static_cast<double>(end-start)/CLOCKS_PER_SEC;

	if (isFeasible) {
		// Comparison with current solution
		if ((long long)setCoveringSol->getPenalizedObjectiveValue() < (long long)currSol->getPenalizedObjectiveValue()) {
			if(cst::checkAll) { Checker check(setCoveringSol); if(!check.solCheck()) abort(); }
			currSol->replicateSolution(setCoveringSol);
			++(*nbCurrSol);
			if(cst::checkAll) { Checker check2(currSol);if(!check2.solCheck()) abort();}
		}
		poolManagement(clock_time, timeLimit, (int)selected_routes.size(), isOptimal, nbOptSol);
	}else{
		cout<<"The SCP solution is not feasible!"<<end;
	}
	delete setCoveringSol;
	return isFeasible;

#else
	return false;
#endif
}

void SetCovering::poolManagement(double clock_time, double timeLimit, int spoolsize, bool* isOptimal, size_t* nbOptSol){
	double slack_time=3.0-clock_time;
	cout<<clock_time<<" TL: "<<timeLimit<<" Pool:"<<spoolsize<<" Opt."<<(*isOptimal)<<" NbOpt."<<(*nbOptSol)<<endl;
	if (!(*isOptimal)){ // is not opitmal
		cleanPoolTours();
		cout<<"cleaning"<<endl;
	}else{
		++(*nbOptSol);
	}
}

void SetCovering::initSCPcluster(DARPSolution& setCoveringSol, DARPSolution& bestSol){
	setCoveringSol.init("SCS",bestSol.getRequestInSolution());
	for(size_t i=0; i<setCoveringSol.getTours().size(); ++i){
		auto t = setCoveringSol.getTours()[i];
		bool find=false;
		for(auto& tbest : bestSol.getTours()){
			if(t->v_node[0]->id==tbest->v_node[0]->id){
				find=true;
				break;
			}
		}
		if(!find){
			setCoveringSol.getTours().erase(setCoveringSol.getTours().begin()+i);
			--i;
		}
	}
	for(size_t i=0;i<setCoveringSol.getTours().size(); ++i)
		setCoveringSol.getTours()[i]->id=i;
}


void SetCovering::addTours(DARPSolution* Sol) {
	for (Tour* t : Sol->tours) {
		addTour(t);
	}
}

// Add nonDominant tours
void SetCovering::addTour(Tour* t) {
	vector<int> hash;
	getTourIdentifier(hash,t);
	auto it = poolTours.find(hash);
	if(it != poolTours.end()){
		if ((*it).second->cost > t->getCost()) {// Dominance rule
			RouteForSetCovering* RouteSC =new RouteForSetCovering(t,&hash);
			RouteSC->nbSelected+=(*it).second->nbSelected;
			delete (*it).second;
			(*it).second=RouteSC;
		}
	}else{
		RouteForSetCovering* newRoute =new RouteForSetCovering(t,&hash);
		poolTours[hash]=newRoute;
	}
}

void SetCovering::addToursElite(DARPSolution* Sol) {
	cleanElite();
	for (Tour* t : Sol->tours) {
		vector<int> hash;
		getTourIdentifier(hash,t);
		RouteForSetCovering* newRoute =new RouteForSetCovering(t,&hash);
		elite[hash]=newRoute;
	}
}
/* This method generates an identifier of the tour */
void SetCovering::getTourIdentifier(vector<int>& hashkey, Tour* tour){
	hashkey.push_back((int)(*tour->v_node.begin())->node->id);  // deport depart
	for(auto& node : tour->v_node){
		if (node->node->type==0) { //pickup
			hashkey.push_back(node->id);
		}
	}
	hashkey.push_back((int)(*(--tour->v_node.end()))->node->id); // depot arrive
	sort(hashkey.begin(),hashkey.end());
}


void SetCovering::cleanPoolTours() {
	for (auto& m: poolTours) {
		delete m.second;
	}
	poolTours.clear();
}

void SetCovering::cleanElite() {
	for (auto& m: elite) {
		delete m.second;
	}
	elite.clear();
}
