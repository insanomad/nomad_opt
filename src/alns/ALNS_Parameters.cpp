/* ALNS_Framework - a framework to develop ALNS based solvers
 *
 * Copyright (C) 2012 Renaud Masson
 *
 * This library is free software; you can redistribute it and/or
 * modify it either under the terms of the GNU Lesser General Public
 * License version 3 as published by the Free Software Foundation
 * (the "LGPL"). If you do not alter this notice, a recipient may use
 * your version of this file under the LGPL.
 *
 * You should have received a copy of the LGPL along with this library
 * in the file COPYING-LGPL-3; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Suite 500, Boston, MA 02110-1335, USA
 *
 * This software is distributed on an "AS IS" basis, WITHOUT WARRANTY
 * OF ANY KIND, either express or implied. See the LGPL for
 * the specific language governing rights and limitations.
 *
 * The Original Code is the ALNS_Framework library.
 *
 *
 * Contributor(s):
 *	Renaud Masson
 */

#include "ALNS_Parameters.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include <math.h>
//#include "../lib/tinyxml/tinyxml.h"

using namespace std;

//Constructor
ALNS_Parameters::ALNS_Parameters(string path_output, size_t temp)
{
	// Stopping parameters
	maxNbIterations = 50000;
	maxRunningTime = 30000; //300 seconds per cluster

	timeLimitSCP=3;
	maxNbIterationSetCovering=1000; //1000;
	frecRateSCP=0.8;

	displayLogFrequency = 1; //1 printout iterations in console

	probabilityAcceptance=0.05;//0.05 record-to-record criterion

	//! Simulated Annealing parameters
	warmupIterations=30;
	MaxNbIterSuccesSA=12;
	MaxNbIterFailureSA=100;
	temperature=(double) temp;
	endTemperature = 0.1;
	alpha=0.95;

	PATH=path_output;

	//follow parameters alns
	maxNbIterationsNoImp = 2000;
	timeSegmentsIt = 100; // recalculate scores alns, check selection operator is not commented for random case
	nbItBeforeReinit = 1000;
	sigma1 = 33;
	sigma2 = 20;
	sigma3 = 15;
	rho = 0.1;
	minimumWeight = 0.1;
	maximumWeight = 5;

	//unused
	acKind = SA;
	performLocalSearch = false;
	lock = false;
	stopCrit = ALL;
	noise = false;

	//TODO check initialization
	maxDestroyPerc=0;
	minDestroyPerc=0;
	probabilityOfNoise=0;
	reloadFrequency=0;
}

//Destructor
ALNS_Parameters::~ALNS_Parameters() {
}

std::string ALNS_Parameters::toString(std::string path) {
	std::stringstream s;
	s<<" Parameters: ";
	s<<"\n maxNbIterations\t"<<maxNbIterations;
	s<<"\n maxRunningTime \t"<<maxRunningTime; //300 seconds per cluster

	s<<"\n timeLimitSCP\t"<<timeLimitSCP;
	s<<"\n maxNbIterationSetCovering\t"<<maxNbIterationSetCovering; //1000;
	s<<"\n frecRateSCP\t"<<frecRateSCP;


	s<<"\n displayLogFrequency\t"<<displayLogFrequency; //1 printout iterations in console

	s<<"\n probabilityAcceptance\t"<<probabilityAcceptance;//0.05 record-to-record criterion

	s<<" \n Parameters: ";//alns parameters
	s<<"\n maxNbIterationsNoImp\t"<<maxNbIterationsNoImp;
	s<<"\n warmupIterations\t"<<warmupIterations;
	s<<"\n timeSegmentsIt\t"<<timeSegmentsIt; // recalculate scores alns, check selection operator is not commented for random case

	s<<"\n stopCrit \t"<<stopCrit;
	s<<"\n noise\t"<<noise;

	s<<"\n nbItBeforeReinit\t"<<nbItBeforeReinit;
	s<<"\n sigma1\t"<<sigma1;
	s<<"\n sigma2\t"<<sigma2;
	s<<"\n sigma3\t"<<sigma3;
	s<<"\n rho\t"<<rho;
	s<<"\n minimumWeight\t"<<minimumWeight;
	s<<"\n maximumWeight\t"<<maximumWeight;

	path +=".param";
	std::ofstream fichier(path.c_str(), std::ios::out | std::ios::trunc);
	fichier << s.str();
	fichier.close();


	return s.str();

}

//ALNS_Parameters::ALNS_Parameters(ALNS_Parameters& p)
//{
//	maxNbIterations = p.maxNbIterations;
//
//	maxRunningTime = p.maxRunningTime;
//
//	maxNbIterationsNoImp = p.maxNbIterationsNoImp;
//
//	stopCrit = p.stopCrit;
//	noise = p.noise;
//	timeSegmentsIt = p.timeSegmentsIt;
//	nbItBeforeReinit = p.nbItBeforeReinit;
//	sigma1 = p.sigma1;
//	sigma2 = p.sigma2;
//	sigma3 = p.sigma3;
//	rho = p.rho;
//	minimumWeight = p.minimumWeight;
//	maximumWeight = p.maximumWeight;
//	acKind = p.acKind;
//	displayLogFrequency = p.displayLogFrequency;
//	performLocalSearch = p.performLocalSearch;
//	minDestroyPerc = p.minDestroyPerc;
//	maxDestroyPerc = p.maxDestroyPerc;
//	lock = false;
//}
