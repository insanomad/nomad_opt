/*
 * MIPCplex.h
 *
 *  Created on: Jun 15, 2016
 *      Author: oscar
 */


#include "../constant.h"

#ifndef CPLEXNOTINSTLED


#ifndef SRC_ALNS_MIPCPLEX_H_
#define SRC_ALNS_MIPCPLEX_H_

#include "RouteForSetCovering.h"
#include "../darp/solutionManager/DARPSolution.h"
#include "../darp/operators/insRestoreSolution.h"

#include <vector>
#include <map>


/**
 * This class solve the set covering problem using cplex
 */
class MIPCplex {
public:
	MIPCplex(){bestSolution=nullptr;currentSolution=nullptr; restSol=nullptr;};

	/**
	 * Constructor
	 */
	MIPCplex(DARPSolution& bestSol, DARPSolution& currSol);
	virtual ~MIPCplex();

	//Perform the MIP resolution
	//! true  if a feasible solution was found within the time limit, false if not.
	bool mipSetCovering(DARPSolution& Sol, map<std::vector<int>,RouteForSetCovering*>& pool, std::map<std::vector<int>,RouteForSetCovering*>& elite, bool warmStart,double timeLimit,bool* isOptimal);

private:
	/**
	 * This method reacreates a complete DARPSolution from cplex output.
	 */
	void buildNewSolution(DARPSolution& Sol, const  std::vector<RouteForSetCovering*>& tours);
	/**
	 * As the SCP allows request duplicats, this methods removes thouse duplicats to get a consisten solution
	 */
	void removeDuplicats(const std::vector<RouteForSetCovering*>& tours, DARPSolution& Sol);
	/**
	 * To decide which request to remove in the remove duplicats phase, this methods probidi the marginal asociated cost or removing a request for a route.
	 */
	double computeMarginalCost(RouteForSetCovering& rscp, DARPSolution& Sol, int nidx);

	/**
	 * Debugging methods
	 */
	void detectDuplicats(vector<int>& t);
	void checkMipSolution(const std::vector<int>& idrequests,const vector<RouteForSetCovering*>& tours);

	/**
	 * Attributs
	 */
	DARPSolution* bestSolution;
	DARPSolution* currentSolution;
	Restore_Solution* restSol;
	int doublons=0;
};

#endif /* SRC_ALNS_MIPCPLEX_H_ */

#endif /* CPLEXNOTINSTLED */
