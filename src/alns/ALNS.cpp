/* ALNS_Framework - a framework to develop ALNS based solvers
 *
 * Copyright (C) 2012 Renaud Masson
 *
 * This library is free software; you can redistribute it and/or
 * modify it either under the terms of the GNU Lesser General Public
 * License version 3 as published by the Free Software Foundation
 * (the "LGPL"). If you do not alter this notice, a recipient may use
 * your version of this file under the LGPL.
 *
 * You should have received a copy of the LGPL along with this library
 * in the file COPYING-LGPL-3; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Suite 500, Boston, MA 02110-1335, USA
 *
 * This software is distributed on an "AS IS" basis, WITHOUT WARRANTY
 * OF ANY KIND, either express or implied. See the LGPL for
 * the specific language governing rights and limitations.
 *
 * The Original Code is the ALNS_Framework library.
 *
 *
 * Contributor(s):
 *	Renaud Masson
 */

#include <assert.h>
#include <float.h>
#include <iostream>
#include <stdlib.h>
#include <set>
#include <time.h>
#include <cmath>

#include "ALNS.h"
#include "../darp/solutionManager/DARPSolution.h"
#include "../darp/instanceManager/Instance.h"
#include "ALNS_Parameters.h"
#include "ISolution.h"
#include "AOperatorManager.h"
#include "ARepairOperator.h"
#include "ADestroyOperator.h"
#include "AOperator.h"
#include "../statistics/Statistics.h"
#include "../constant.h"

using namespace std;


ALNS::ALNS( string instanceName,
		ISolution& initialSolution,
		ALNS_Parameters& parameters,
		AOperatorManager& opMan,
		Statistics& statistics,
		Instance* inst){

	name = instanceName;
	instance=inst;
	bestsolution = &initialSolution; //.getCopy();
	stats=&statistics;
	param = &parameters;
	nbIterations = 0;
	opManager = &opMan;
	timeLimit=param->getMaxRunningTime()*(CLOCKS_PER_SEC);
	timeLimitScp=param->getTimeLimitSCP();

	//ASCP2
	baseNbIterScp=0;
	prevSolOptimal=false;

	nbNewBestSolSCP=0;
	nbNewCurrSolSCP=0;
	nbOptSolSCP=0;
	nbReconfigurations=0;
	fleetUtilization=0;
	nbRoutesRec=0;

	acceptanceRecorToRecord=1+param->getAcceptanceProbability();

	nbIterationsWC = 0;
	nbIterWithoutImprove = 0;
	//lsManager = &lsMan;
	opManager->setStatistics(&statistics);
	// We add the initial solution in the best solution manager.
	nbIterWithoutImproveCurrent = 0;
	nbIterationsWithoutTransition = 0;

	//non used
	lowerBound = -DBL_MAX;
	nbIterationsWithoutLocalSearch = 0;

	newSolution=nullptr;
	currentSolution=nullptr;
	SCP=nullptr;
	checkNewSol=nullptr;
	TimeToReachBestSol=0;
	startingTime=0;
	destroy=nullptr;
	repair=nullptr;
	history=nullptr;
}

ALNS::~ALNS(){
	delete currentSolution;
	delete newSolution;
	delete checkNewSol;
	delete SCP;
}


bool ALNS::solve()
{
	nbIterations=0;
	startingTime = clock();
	stats->setStart();
	currentSolution = bestsolution->getCopy();
	newSolution = bestsolution->getCopy();
	checkNewSol= new Checker(dynamic_cast<DARPSolution*>(newSolution));
	SCP=new SetCovering();
	if(cst::checkAll) checkNewSol->solCheck();
	history=&dynamic_cast<HistoryRemoval&>(opManager->getDestroyOperator("History_Removal"));

	while(!isStoppingCriterionMet()){
		performOneIteration();
		if(cst::SCP) solveSetCoveringProblem(*currentSolution,*bestsolution,true);
	}

	fleetUtilization=dynamic_cast<DARPSolution*>(bestsolution)->computeFleetUtilization();
	nbReconfigurations=dynamic_cast<DARPSolution*>(bestsolution)->computeNbReconfigurations(&nbRoutesRec);

	if(cst::displaySol) cout<<*dynamic_cast<DARPSolution*>(bestsolution)<<endl;

	cout<<"[ALNS]: BestSol "<<dynamic_cast<DARPSolution*>(bestsolution)->getObjectiveValue()<<" it. :"<<nbIterations<<" Time: "<<static_cast<double>(clock()-startingTime)/CLOCKS_PER_SEC<<" secs"<<" NbRec "<<nbReconfigurations<<" FU(%) "<<fleetUtilization*100<<endl;
	double percent=100.0*((double)param->getMaxNbIterationSetCovering()/(double)nbIterations);
	cout <<"SCP: NB.NewBest " << (double)nbNewBestSolSCP*percent <<"%"<<"  NB.NewCurr " << (double)nbNewCurrSolSCP*percent<<"%"<<"  NbOptSCP " << (double)nbOptSolSCP*percent<<"%"<< endl;

	return bestsolution->isFeasible();
}

void ALNS::performOneIteration()
{
	repair = &opManager->selectRepairOperator();
	destroy = &opManager->selectDestroyOperator();

	destroy->destroySolution(*newSolution);
	status.setAlreadyDestroyed(ALNS_Iteration_Status::TRUE);
	status.setAlreadyRepaired(ALNS_Iteration_Status::FALSE);
	repair->repairSolution(*newSolution);
	status.setAlreadyRepaired(ALNS_Iteration_Status::TRUE);

	if(cst::checkAll) checkNewSol->solCheck();
	if(cst::SCP) SCP->addTours(dynamic_cast<DARPSolution*>(newSolution));
	if(cst::fullStats) addStatEntry(destroy->getName(),repair->getName());

	if(checkAgainstKnownSolution(*newSolution))
		history->updateScores(*newSolution); //-> update scores for the historical removal operator

	isNewBest(newSolution); //-> Keep the best Solution

	recordToRecord(newSolution);

	nbIterations++;
	nbIterationsWC++;

	if(cst::ALNS) adaptiveLNS();

	// Printout in console
	if(cst::verbosite && (nbIterations % param->getLogFrequency() ==  0) ){ // Dynamic casting in to call getPenalizedObjectiveValue is necesary to get the real cost
		std::cout<<"[ALNS] it. " << nbIterations<<"  itnI. "<<nbIterWithoutImprove<<" CSol: "<< (currentSolution)->getObjectiveValue()<<" "<< dynamic_cast<DARPSolution*>(currentSolution)->compNbToursUsed() << " BSol :"<< (bestsolution)->getObjectiveValue()<< " "<< dynamic_cast<DARPSolution*>(bestsolution)->compNbToursUsed()<<std::endl;
	}
}

void ALNS::adaptiveLNS(){
	opManager->updateScores(*destroy,*repair,status);
	//  Re-start:  Test if it's useful otherwise delete
	if(nbIterWithoutImprove % param->getMaxNbIterationsNoImp() == 0){
		nbIterWithoutImprove = 0;
		newSolution->replicateSolution(bestsolution);
	}
	// [ALNS]Re-compute weights of operators: Test if it's useful for big instances otherwise delete
	if(nbIterationsWC % param->getTimeSegmentsIt() == 0) {
		opManager->recomputeWeights();
		nbIterationsWC = 0;
	}
}


void ALNS::solveSetCoveringProblem(ISolution& currentSolution, ISolution& bestsolution,  bool warmStart){
	if ((nbIterations-baseNbIterScp)%param->getMaxNbIterationSetCovering()==0 ){
		if(bestsolution.getSizeRequestBank()==0){
			bool isOptimal=false;
			SCP->performSetCovering(currentSolution,bestsolution, &nbNewCurrSolSCP, &nbOptSolSCP, warmStart, &isOptimal,timeLimitScp);
			if (isNewBest(&currentSolution)){
				nbNewBestSolSCP++;
				if(cst::verbosite) cout <<"SCP NEW BEST SOLUTION, COST : " << instance->D2Sd(currentSolution.getPenalizedObjectiveValue())<< endl;
				SCP->addTours(&dynamic_cast<DARPSolution&>(currentSolution));
				//prevSolOptimal=true; // TEST REMOVE!!!!
			}
			if(!isOptimal && cst::ASCP2 && !prevSolOptimal) {
				baseNbIterScp=nbIterations;
				param->setMaxNbIterationSetCovering((size_t)param->getMaxNbIterationSetCovering()*param->getFrecRateScp());
				//timeLimitScp*=param->getFrecRateScp();
				if(cst::verbosite) cout<<"Reducing..  Frec:"<<param->getMaxNbIterationSetCovering()<<" TimeL: "<< timeLimitScp<<endl;
			}
			if(isOptimal){
				prevSolOptimal=true;
			}
		}else cout<<"[SCP] no feasible solutions yet in the LNS :("<<endl;
	}
}

void ALNS::recordToRecord(ISolution* newSol) {
	// update best solution
	if((*newSol)<(*bestsolution)){
		nbIterWithoutImproveCurrent = 0;
		status.setImproveCurrentSolution(ALNS_Iteration_Status::TRUE);
	}else{
		nbIterWithoutImproveCurrent++;
		status.setImproveCurrentSolution(ALNS_Iteration_Status::FALSE);
	}
	status.setNbIterationWithoutImprovementCurrent(nbIterWithoutImproveCurrent);

	// update current solution
	if (newSol->getPenalizedObjectiveValue() < acceptanceRecorToRecord*bestsolution->getPenalizedObjectiveValue()){
		currentSolution->replicateSolution(newSol);
		status.setAcceptedAsCurrentSolution(ALNS_Iteration_Status::TRUE);
		nbIterationsWithoutTransition = 0;
	}else{
		newSol->replicateSolution(currentSolution);	//comming back to previos state
		status.setAcceptedAsCurrentSolution(ALNS_Iteration_Status::FALSE);
		nbIterationsWithoutTransition++;
	}
}


void ALNS::addStatEntry(string opdestroy, string oprepair){
	stats->addEntry(static_cast<double>(clock()-startingTime)/CLOCKS_PER_SEC,
			nbIterations,
			opdestroy,
			oprepair,
			newSolution->getObjectiveValue(),
			currentSolution->getObjectiveValue(),
			bestsolution->getObjectiveValue(),
			knownKeys.size(),
			status.getAcceptedAsCurrentSolution()==ALNS_Iteration_Status::TRUE,
			status.getNewBestSolution()==ALNS_Iteration_Status::TRUE,
			status.getAlreadyKnownSolution()==ALNS_Iteration_Status::TRUE,
			nbNewBestSolSCP,
			nbNewCurrSolSCP,
			SCP->getPoolSize(),
			nbOptSolSCP
	);
}

bool ALNS::isNewBest(ISolution* newSol ){
	// we can avoid this casting if we find a better hash key
	if (newSol->getPenalizedObjectiveValue() < bestsolution->getPenalizedObjectiveValue()){
		//cout<<newSol->getPenalizedObjectiveValue() <<" "<< bestsolution->getPenalizedObjectiveValue()<<endl;
		nbIterWithoutImprove = 1;
		status.setNewBestSolution(ALNS_Iteration_Status::TRUE);
		status.setNbIterationWithoutImprovement(nbIterWithoutImprove);
		status.setNbIterationWithoutImprovementSinceLastReload(0);
		count_succes++;
		bestsolution->replicateSolution(newSol);
		TimeToReachBestSol = clock();
		return true;
	}else {
		status.setNewBestSolution(ALNS_Iteration_Status::FALSE);
		nbIterWithoutImprove++;
		status.setNbIterationWithoutImprovement(nbIterWithoutImprove);
		status.setNbIterationWithoutImprovementSinceLastReload(status.getNbIterationWithoutImprovementSinceLastReload()+1);
		return false;
	}
}

bool ALNS::nonEqual( ISolution* newSol, ISolution* curSol){
	if (newSol->getHash()!= curSol->getHash()) {
		return true;
	} else {
		return false;
	}
}
// validate weather it should stop or not
bool ALNS::isStoppingCriterionMet()
{
	time=clock()-startingTime;
	//if( nbIterationsWC < param->maxNbIterationsNoImp  && (time < time_limit && param->temperature > param->endTemperature && nbIterations< param->maxNbIterations)){
	if( nbIterations< param->getMaxNbIterations() && time < timeLimit ){
		//if( time < time_limit ){
		return false;
	}
	return true;
}


bool ALNS::checkAgainstKnownSolution(ISolution& sol)
{
	bool notKnownSolution = false;
	long long keySol = sol.getHash();

	if(knownKeys.find(keySol) == knownKeys.end())
	{
		notKnownSolution = true;
		knownKeys.insert(keySol);
	}

	if(!notKnownSolution)
	{
		status.setAlreadyKnownSolution(ALNS_Iteration_Status::TRUE);
	}
	else
	{
		status.setAlreadyKnownSolution(ALNS_Iteration_Status::FALSE);
	}
	return notKnownSolution;
}



