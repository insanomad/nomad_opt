/*
 * MIPCplex.cpp
 *
 *  Created on: Jun 15, 2016
 *      Author: oscar
 */


#include "MIPCplex.h"

#ifndef CPLEXNOTINSTLED

#include <iostream>
#include <vector>
#include <ilcplex/ilocplex.h>
#include <algorithm>

#include "../darp/operators/insRestoreSolution.h"


MIPCplex::MIPCplex(DARPSolution& bestSol,DARPSolution& currSol) {
	bestSolution=&bestSol;
	currentSolution=&currSol;
	restSol=nullptr;
	doublons=0;
}

MIPCplex::~MIPCplex() {
}


bool MIPCplex::mipSetCovering(DARPSolution& Sol,std::map<std::vector<int>,RouteForSetCovering*>& pool, std::map<std::vector<int>,RouteForSetCovering*>& elite, bool warmStart,double timeLimit,bool* isOptimal ) {
	//cout<< Sol.getPenalizedObjectiveValue() <<" "<< pool.size() <<" "<< elite.size()<<" "<<warmStart<<" "<< timeLimit<<" "<<*isOptimal<<std::endl;
	vector<RouteForSetCovering*> selectedTours;
	std::vector<size_t> idrequests;
	if(cst::Clustering!=cst::single) idrequests=Sol.getRequestInSolution(); // todo check if this line is enough
	else{
		idrequests.resize(Sol.getRequestInSolution().size());
		std::iota (std::begin(idrequests), std::end(idrequests), 0); // Fill with 0, 1, 2,..., N
	}
	for(size_t i=0;i<Sol.getInstance()->depot_depart.size();++i){
		idrequests.push_back((int)Sol.getInstance()->depot_depart[i]->id);// adding ids depots
	}
	bool isFeasible=true;
	IloEnv env;
	try {
		IloInt  one=1;

		IloInt  nbTours;
		if (warmStart) nbTours=pool.size()+elite.size();
		else nbTours=pool.size();


		IloNumArray c(env, nbTours);  //cost
		IloBoolVarArray x(env, nbTours);  // decision variables
		IloModel model(env); // creating the modeling object

		//Constraint: every node should be present in the solution at least once
		//int nbRequests = Sol.getInstance()->nb_request + Sol.getInstance()->vp_depot_start.size();
		for(int cidx : idrequests) {
			IloExpr v(env);

			auto it=pool.begin();
			for(size_t tidx= 0 ; tidx < pool.size(); tidx++){
				auto res =  find((*it).first.begin(),(*it).first.end(), cidx);
				if ( res != (*it).first.end()) {
					v += x[tidx];
				}
				++it;
			}

			if (warmStart) {
				auto ite=elite.begin();
				for(auto tidx= (int)pool.size() ; tidx < nbTours; tidx++){
					auto res =  find(ite->first.begin(),ite->first.end(), cidx);
					if ( res != ite->first.end()) {
						v += x[tidx];
					}
					++ite;
				}
			}
			// constraint for depots
			auto node=Sol.getInstance()->dataNodes[cidx];
			if(node->type > 1){// if cidx its a depot
				IloInt  cap=min(node->cap,(long)bestSolution->getNbTours());
				model.add(v <= cap);
			}else{
				// constraints request
				model.add(v >= one);
			}
			v.end();

		}

		//init vector cost pool
		auto it=pool.begin();
		for(size_t tidx= 0 ; tidx < pool.size(); ++tidx){
			c[tidx]=(*it).second->cost;
			++it;
		}
		// vector cost for warm start solution
		if (warmStart) {
			auto ite=elite.begin();
			for(auto tidx= (int)pool.size() ; tidx < nbTours; tidx++){
				c[tidx]=(*ite).second->cost;
				++ite;
			}
		}

		IloCplex cplex(env);
		cplex.clear();
		cplex.extract(model);
		cplex.setOut(env.getNullStream()); // on/off output
		//cplex.setWarning(env.getNullStream()); // on/off warning streams

		if (warmStart) { // Create an initial solution with the elite tours
			IloNumVarArray startVar(env);
			IloNumArray startVal(env);
			for(auto tidx= (int)pool.size() ; tidx < nbTours; ++tidx){
				startVar.add(x[tidx]);
				startVal.add(IloTrue);

			}
			cplex.addMIPStart(startVar, startVal);
			startVal.end();
			startVar.end();
		}

		//Objective function
		IloExpr obj = IloScalProd(c, x);
		model.add(IloMinimize(env, obj));
		obj.end();

		cplex.setParam(IloCplex::MIPEmphasis, IloCplex::MIPEmphasisOptimality);
		cplex.setParam(IloCplex::TiLim, timeLimit);
		cplex.setParam(IloCplex::Threads, 1);
		cplex.setParam(IloCplex::CutUp, bestSolution->getPenalizedObjectiveValue()+1);  // NEVER change to getObjectiveValue() b/o that is the real value wich is smaill ..and make solution infeasible

		isFeasible=cplex.solve();
		//cplex.exportModel("scp.lp");   // writes the model into a text file
		//cplex.writeSolution("out");    // writes the solution in a html format

		*isOptimal= (cplex.getStatus()==IloAlgorithm::Status::Optimal);

		if (cplex.getStatus() == IloAlgorithm::Infeasible)
			env.out() << "No Solution found! this is acceptable only if there is't warm up" << endl;

		if(isFeasible){
			// Save selected tours and build a solution if(cst::verbosite )
			auto instance=bestSolution->getInstance();
			if(cst::verbosite) env.out() << "Solution status: " << cplex.getStatus() <<"\n";
			IloNum tolerance = cplex.getParam(IloCplex::Param::MIP::Tolerances::Integrality);
			if(cst::verbosite) env.out() << "Objecte: " << cplex.getObjValue()/instance->getMult() <<
					"  Prev.BestSol:" << bestSolution->getPenalizedObjectiveValue() /instance->getMult() <<
					"  SizePool " << nbTours<< endl;

			// Re-creating set-covering solution
			//cout<<cplex.getObjValue()<<" "<<currentSolution->getPenalizedObjectiveValue()<<endl;
			if(cplex.getObjValue() < currentSolution->getPenalizedObjectiveValue()){  // use allways penalized b/o getObjective value is another scale
				auto it=pool.begin();
				for(size_t tidx = 0; tidx < pool.size(); ++tidx) {
					if (abs(cplex.getValue(x[tidx])-one)<0.01) {
						RouteForSetCovering* newRoute =new RouteForSetCovering();
						*newRoute=*((*it).second);
						selectedTours.push_back(newRoute);
						(*it).second->nbSelected+=1;
					}
					++it;
				}
				if(warmStart){
					auto ite=elite.begin();
					for(int tidx = (int)pool.size(); tidx < nbTours; ++tidx) {
						if (abs(cplex.getValue(x[tidx])-one)<0.01) {
							RouteForSetCovering* newRoute =new RouteForSetCovering();
							*newRoute=*((*ite).second);  //necesary to aboid modifying the pool of routes
							selectedTours.push_back(newRoute);
							(*ite).second->nbSelected+=1;
						}
						++ite;
					}
				}

				//checkMipSolution(idrequests,selectedTours);
				buildNewSolution(Sol,selectedTours);
			}else{
				Sol.setTotalCost(cplex.getObjValue());
				const std::vector<size_t> Inserted;
				Sol.setPickNonInserted(Inserted);
			}
		}
	}catch(IloException& e) {
		cerr << " ERROR: " << e << endl;
	}
	catch(...) {
		cerr << " ERROR" << endl;
	}
	env.end();
	for(auto t : selectedTours) delete t;
	return isFeasible;
}
void MIPCplex::checkMipSolution(const std::vector<int>& idrequests,const vector<RouteForSetCovering*>& tours){
	bool in=true, ok=true;
	for(int i: idrequests){
		in=false;
		for(auto& r: tours){
			for(auto& n:r->route){
				if(i==n){
					in=true;
				}
			}
		}
		if(!in){
			cout<<"node "<<i<<" is not in the MIP solution!"<<endl;
			ok=false;
		}
	}

	if(tours.size() > bestSolution->getInst()->getMaxNbRoutes()){
		ok=false;
		cout<< "there are more tours than allowed in mip ouput!"<<endl;
	}
	if(!ok){
		abort();
	}
}

void MIPCplex::buildNewSolution(DARPSolution& Sol, const vector<RouteForSetCovering*>& tours){
	removeDuplicats(tours,Sol);
	restSol=new Restore_Solution("Set Covering Solution");
	ISolution& iSol=dynamic_cast<ISolution&>(Sol);
	restSol->rebuiltSolution(iSol,tours);
}

void MIPCplex::removeDuplicats(const vector<RouteForSetCovering*>& tours, DARPSolution& Sol){
	// Remove Duplicats
	int nb_request=(int)Sol.getInstance()->nb_request;
	vector<int> min_route(nb_request,-1); //pair <tour_id, node_pos>

	for(int tidx=0;tidx<(int)tours.size();++tidx)
	{
		for(int nidx = 1; nidx < (int) (tours[tidx]->route.size()-1);++nidx)
		{
			if( tours[tidx]->route[nidx] < nb_request ) // if pickup
			{
				if( min_route[tours[tidx]->route[nidx]]==-1 )// if non duplicated
				{
					min_route[tours[tidx]->route[nidx]]=tidx;
				}
				else
				{
					++doublons;
					if(cst::SRSCP)//comparer les couts marginaux et garder le min
					{
						double costNew=computeMarginalCost(*tours[tidx],Sol,nidx);

						int nidx2=0;
						for(int i=1 ; i< (int)tours[min_route[tours[tidx]->route[nidx]]]->route.size(); ++i){ // get pickup position in second route: no way to optimize this b/o positions it may changes
							if(tours[min_route[tours[tidx]->route[nidx]]]->route[i]==tours[tidx]->route[nidx]) {nidx2=i;break;}
						}
						cout<<"doublons!!!!! "<<doublons<<" node "<<tours[tidx]->route[nidx]<<" (tour "<<tidx<<" nidx "<<nidx<<")"<<" (tour "<<min_route[tours[tidx]->route[nidx]]<<" nidx "<<nidx2<<")"<<endl;

						double cost=computeMarginalCost(*tours[min_route[tours[tidx]->route[nidx]]], Sol ,nidx2);

						auto pickup=Sol.getVrevers()[tours[tidx]->route[nidx]];
						size_t id_delivery=pickup->node->idsym;
						if(costNew<cost)
						{
							tours[tidx]->route.erase(tours[tidx]->route.begin()+nidx);//remove pickup
							--nidx;
							for(size_t k=nidx ; k<tours[tidx]->route.size(); ++k){
								if(tours[tidx]->route[k]==id_delivery){
									tours[tidx]->route.erase(tours[tidx]->route.begin()+k); //remove delivery
									break;
								}
							}
						}
						else
						{
							int min_tidx=min_route[tours[tidx]->route[nidx]];
							int min_nidx=nidx2;
							cout<<"remove in tour "<<min_tidx<<" nidx "<<min_nidx<<endl;
							min_route[tours[tidx]->route[nidx]]=tidx; // mis à jour
							tours[min_tidx]->route.erase(tours[min_tidx]->route.begin()+min_nidx); // remove pickup
							for(size_t k=nidx2 ; k<tours[min_tidx]->route.size(); ++k){
								if(tours[min_tidx]->route[k]==id_delivery){
									tours[min_tidx]->route.erase(tours[min_tidx]->route.begin()+k); //remove delivery
									break;
								}
							}

						}
					}
					else
					{
						cout<<"doublons!!!!! "<<doublons<<" node "<<tours[tidx]->route[nidx]<<" (tour "<<tidx<<" nidx "<<nidx<<")"<<endl;

						auto pickup=Sol.getVrevers()[tours[tidx]->route[nidx]];
						size_t id_delivery=pickup->node->idsym;

						cout<<"before, node to be removed"<<nidx<<","<<id_delivery<<endl;
						for(size_t k:tours[tidx]->route) cout<<k<<" ";	cout<<endl;

						tours[tidx]->route.erase(tours[tidx]->route.begin()+nidx);
						--nidx;
						for(size_t k=nidx ; k<tours[tidx]->route.size(); ++k){
							if(tours[tidx]->route[k]==id_delivery){
								tours[tidx]->route.erase(tours[tidx]->route.begin()+k);
								break;
							}
						}
						for(auto k:tours[tidx]->route) cout<<k<<" ";	cout<<endl;
					}

				}
			}
		}
		//detectDuplicats(tours[tidx]->route);
		//	for(auto& n : tours[tidx]->route) cout<<n<<" ";
		//	cout<<endl;
	}
}

//Debuggin method
void MIPCplex::detectDuplicats(vector<int>& t){
	for(size_t i=1;i<t.size()-1;++i){
		bool in=false;
		for(size_t j=0;j<t.size();++j){
			if(t[i]==t[j]){
				if(in){
					cout<<"doublons in mip!!"<<__FILE__<<__LINE__<<endl;
					for(auto k:t) cout<<k;
					cout<<endl;
					abort();
				}
				in=true;
			}
		}
		if(!in){
			cout<<"missing node"<<i<<endl;
		}
	}
}

double MIPCplex::computeMarginalCost(RouteForSetCovering& rscp, DARPSolution& Sol, int nidx) {
	Tour* route1=restSol->makeRoute(Sol,rscp);
	int p_id=rscp.route[nidx];
	auto pickup=Sol.getVrevers()[p_id];
	int pos_d=*pickup->posSym;
	// remove after test is ok
	//	if(pos_d != route1->getPosition((int)pickup->node->idsym)){
	//		cout<<"problem to insert"<<endl;
	//		abort();
	//	}

	auto delivery=route1->v_node[pos_d];
	if(pickup->id>delivery->id){
		abort();
	}

	RequestInfo req1(pickup,delivery);
	req1.i=(int)nidx;
	req1.j=(int)pos_d;

	if(!route1->evalRemoval(req1)) {
		cout<<"EvalRemoval results in infeasibility! "<<__FILE__<<__LINE__<<endl;
		cout<<req1<<endl;
		abort();
	}

	delete route1;
	return req1.deltacost;
}

#endif


