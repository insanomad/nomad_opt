
/* ALNS_Framework - a framework to develop ALNS based solvers
 *
 * Copyright (C) 2012 Renaud Masson
 *
 * This library is free software; you can redistribute it and/or
 * modify it either under the terms of the GNU Lesser General Public
 * License version 3 as published by the Free Software Foundation
 * (the "LGPL"). If you do not alter this notice, a recipient may use
 * your version of this file under the LGPL.
 *
 * You should have received a copy of the LGPL along with this library
 * in the file COPYING-LGPL-3; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Suite 500, Boston, MA 02110-1335, USA
 *
 * This software is distributed on an "AS IS" basis, WITHOUT WARRANTY
 * OF ANY KIND, either express or implied. See the LGPL for
 * the specific language governing rights and limitations.
 *
 * The Original Code is the ALNS_Framework library.
 *
 *
 * Contributor(s):
 *	Renaud Masson
 */

#ifndef DARPSOLUTION_H_
#define DARPSOLUTION_H_

#include <vector>
//#include "../ALNS_inc.h"
#include "../../alns/ISolution.h"
#include "../structure/Tour.h"
#include "../structure/NodeSol.h"
#include "../instanceManager/Instance.h"


class DARPSolution: public ISolution {
public:
	//! Constructor
	DARPSolution(){Teval=nullptr; Ceval=nullptr;penaltyCost=0; totalCost=0;fleetUtilization=0; };
	DARPSolution(std::string n){name=n; Teval=nullptr; Ceval=nullptr; penaltyCost=0; totalCost=0;fleetUtilization=0; };

	/**
	 * init the solution and create all available routes
	 * @param n
	 * @param instan
	 */
	DARPSolution(std::string n, Instance * instan);

	void createTours();
	void init(std::string n, std::vector<size_t> cluster);
	//! Destructor.
	virtual ~DARPSolution();
	//! A getter for the value of the objective function.
	//! \return the value of the objective function of this solution.
	virtual double getObjectiveValue();
	//! \return a penalized version of the objective value if the solution
	//! is infeasible.
	virtual double getPenalizedObjectiveValue();
	//! A getter for the feasibility of the current solution.
	//! \return true if the solution is feasible, false otherwise.
	virtual bool isFeasible();
	//! A comparator.
	//! \return true if this solution is "better" than the solution it is compared to.
	virtual bool operator<(ISolution&);
	//! Compute the "distance" between solution.
	//! This feature can be used as part of the ALNS to favor the
	//! diversification process. If you do not plan to use this feature
	//! just implement a method returning 0.
	virtual int distance(ISolution&);
	//! Compute a hash key of the solution.
	virtual long long getHash();
	//! returns true if a given node id is inserted in the solution
	bool isInserted(size_t id);
	//! get the size of the request bank (nb request non inserted in the solution)
	virtual size_t getSizeRequestBank();
	//! It assigns configuration to every node in the solution according to their vehicle.
	double computeFleetUtilization();

	/*************************COPY****************************/
	//! This method create a copy of the solution.
	virtual ISolution* getCopy();
	//ISolution* deepcopy();
	void update(DARPSolution& s);
	virtual void replicateSolution(ISolution* s);
	void deepCopyVrevers(DARPSolution& s); //sauve la trace de s vers this
	void set_as_inserted(NodeSol * p);


	/****************************************************/

	//! Simple getter.
	Instance * getInstance() {return inst;};
	std::vector<Tour*>& getTours(){return tours;};


	void addTours(std::vector<Tour*>& newTours);

	std::vector<size_t>& getNonInserted(){
		return pickupNonInserted;
	};

	std::vector<NodeSol*>& getVrevers(){
		return v_revers;
	};

	int compNbToursUsed(){
		auto nbUsedTours=0;
		for(auto& tour: tours){
			if(tour->v_node.size()>2) ++nbUsedTours;
		}
		return nbUsedTours;
	};

	size_t getNbTours() const{
		return tours.size();
	};

	int computeNbReconfigurations(size_t* routeRec) const{
		int nbr=0;
		for(auto& t : tours){
			if(t->getNbReconfigurations()>0) ++(*routeRec);
			nbr=std::max(nbr, t->getNbReconfigurations());
		}
		return nbr;
	}
	/******************************** Add *******************************/


	/**
	 * add tour
	 * @param t pointeur vers le tour à ajouter
	 */
	int insertTour(Tour *t);


	/**
	 * calcule le cout total en sommant les couts des tours
	 */
	double recomputeCost();


	/******************************** Remove *******************************/

	/**
	 * remove a request
	 * une réoptimisation est effectuée
	 * @param idpickup id of the pickup node to remove
	 */
	void removeRequestById(int idpickup);


//	void reinitTours(assignConfigurationsToRouteNodes);

	/**
	 * remove a set of request and recompute the solution at the end of the removal
	 * @param request the set of request to remove
	 */
	void removeSetOfRequestById(std::vector<size_t>& request);

//	/**
//	(*node) * remove the tour in the POSITION v_id in the vector vp_tours
//	 * @param pos position of tour to remove in vector vp_tours
//	 */
//	void remove_tour(int pos);

	void setRequestInCluster(const std::vector<size_t>& allNode);

	void detectDuplicats(Tour * t);


	/******************************** Autres *******************************/

	/**
	 * write the solution in a csv file
	 * @param instance_csv
	 * @return
	 */
	std::string writeSolution(std::string instance_csv);

	/**
	 * wite the solution in a gpx file
	 * @param gpx_file
	 * @return
	 */
	std::string to_gpx(std::string gpx_file) const;

	const std::vector<size_t>& getRequestInSolution() const {
		return requestInSolution;
	}

	TimeEvaluation*& getTeval() {return Teval;	}

	CapacityEvaluation*& getCeval() {return Ceval;	}

	const std::vector<size_t>& getAllReqInClust() const {
		return requestInSolution;
	}

	void setAllReqInClust(const std::vector<size_t>& allReqInClust) {
		requestInSolution = allReqInClust;
	}

	void setCeval(CapacityEvaluation* ceval) {
		Ceval = ceval;
	}


	Instance* getInst() {
		return inst;
	}

	void setInstance(Instance* inst) {
		this->inst = inst;
	}

	const std::string& getName() const {
		return name;
	}

	void setName(const std::string& name) {
		this->name = name;
	}

	double getPenaltyCost() const {
		return penaltyCost;
	}

	void setPenaltyCost(double penaltyCost) {
		this->penaltyCost = penaltyCost;
	}

	const std::vector<size_t>& getPickNonInserted() const {
		return pickupNonInserted;
	}

	void setPickNonInserted(const std::vector<size_t>& pickNonInserted) {
		this->pickupNonInserted = pickNonInserted;
	}

	void setTeval(TimeEvaluation* teval) {
		Teval = teval;
	}
	/**
	 * This method returns the real cost of the solution
	 */
	double getTotalCost() const {
		if(cst::QuAndBard) return totalCost;
		else return inst->S2Dd(totalCost);
	}

	void setTotalCost(double totalCost) {
		this->totalCost = totalCost;
	}

	void setTours(const std::vector<Tour*>& tours) {
		this->tours = tours;
	}

	const std::vector<NodeSol*>& getRevers() const {
		return v_revers;
	}

	void setRevers(const std::vector<NodeSol*>& revers) {
		v_revers = revers;
	}

	int getNbDepots() const {
		return (int)depotDepart.size();
	}

	const std::vector<NodeSol*>& getDepotArrive() const {
		return depotArrive;
	}

	const std::vector<NodeSol*>& getDepotDepart() const {
		return depotDepart;
	}

	//decision variable
	std::vector<Tour*> tours;

private:
	std::string name;
	Instance *inst=nullptr;

	TimeEvaluation* Teval=nullptr;
	CapacityEvaluation* Ceval=nullptr;

	std::vector<size_t> pickupNonInserted;
	double totalCost;
	double penaltyCost; //usefull in getPenalizedObjectiveValue
	double fleetUtilization;

	//vector with all pickup and delivery nodes
	std::vector<NodeSol* > v_revers;
	// vector with all depot nodes 0..n/2 departure n/2+1...n arrival nodes
	//std::vector<Node_in_Sol* > depots;
	std::vector<NodeSol* > depotDepart;
	std::vector<NodeSol* > depotArrive;

	//useful for the checker when the solution comes from  a cluster
	std::vector<size_t> requestInSolution;

};




/******************************** Affichage ********************************/
/**
 * affichage avec cout<<
 */
std::ostream& operator<<(std::ostream&, DARPSolution&); //permet d'utiliser cout



#endif /* DARPSOLUTION_H_ */
