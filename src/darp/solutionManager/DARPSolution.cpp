
#include "DARPSolution.h"
#include "../../constant.h"

/* ALNS_Framework - a framework to develop ALNS based solvers
 *
 * Copyright (C) 2012 Renaud Masson
 *
 * This library is free software; you can redistribute it and/or
 * modify it either under the terms of the GNU Lesser General Public
 * License version 3 as published by the Free Software Foundation
 * (the "LGPL"). If you do not alter this notice, a recipient may use
 * your version of this file under the LGPL.
 *
 * You should have received a copy of the LGPL along with this library
 * in the file COPYING-LGPL-3; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Suite 500, Boston, MA 02110-1335, USA
 *
 * This software is distributed on an "AS IS" basis, WITHOUT WARRANTY
 * OF ANY KIND, either express or implied. See the LGPL for
 * the specific language governing rights and limitations.
 *
 * The Original Code is the ALNS_Framework library.
 *
 *
 * Contributor(s):
 *	Renaud Masson
 */
#include <iostream>
#include <vector>
#include <fstream>
#include <cmath>

using namespace std;


DARPSolution::DARPSolution(std::string _name, Instance* instan) {
	penaltyCost=(double)(1000*instan->getMult()); //useful in getPenalizedObjectiveValue
	name = _name;
	inst = instan;
	totalCost=0;
	fleetUtilization=0;
	//nodes
	v_revers.resize(inst->nb_request*2);
	for (size_t i = 0; i < inst->getNbRequest()*2; ++i)
		v_revers[i]=new NodeSol(inst->dataRequests[i]);
	for (size_t i = 0; i < inst->getNbRequest(); ++i)
		v_revers[i]->posSym=&v_revers[i]->position+inst->getNbRequest();
	Teval=new TimeEvaluation(inst);
	Ceval=new CapacityEvaluation(inst);
	createTours();

	requestInSolution.resize(instan->getNbRequest());
	for (int i = 0; i < (int)requestInSolution.size(); ++i){
		requestInSolution[i]=i;
	}
	init(_name, requestInSolution);
}

void DARPSolution::createTours() {
	//Creating all possible tours
	size_t idn=max(inst->nb_request*2,v_revers.size());
	auto narrive= inst->depot_arrival.begin();
	int vehicleid=0;
	for (auto& ndepart : inst->depot_depart)
	{
		for (int i=0; i< ndepart->getCap(); ++i)
		{
			NodeSol* depart=new NodeSol(idn,ndepart,2);
			depotDepart.push_back(depart);
			idn++;
			NodeSol* arrival=new NodeSol(idn, *narrive,3);
			depotArrive.push_back(arrival);
			auto vehicle=inst->vehicleTypes[vehicleid];
			Tour* tour=new Tour(inst, depart, arrival, vehicle, Teval,Ceval);
			insertTour(tour);
			depart->posSym=&arrival->position;
			arrival->posSym=&depart->position;
			v_revers.push_back(depart);
			v_revers.push_back(arrival);
			idn++;
		}
		if(cst::QuAndBard) ++vehicleid;  //vehicles wont change in routes
		++narrive;
	}
}

void DARPSolution::init(std::string n, std::vector<size_t> cluster) {
	name = n;
	pickupNonInserted.clear();
	pickupNonInserted.resize(cluster.size());
	for(size_t id = 0; id < cluster.size(); id++) {
		pickupNonInserted[id]=cluster[id];
	}
	requestInSolution.resize(cluster.size());
	requestInSolution=cluster;
}


DARPSolution::~DARPSolution() {
	for (auto& n :v_revers) {
		delete n;
	}
	delete Ceval;
	delete Teval;
	for (auto& t: tours) {
		delete t;
	}
}

void DARPSolution::setRequestInCluster(const std::vector<size_t>& cluster) {
	requestInSolution = cluster;
}

/**
 * it computes the real objective cost.
 * for Qu&Bard we have to remove the fix cost first before dividing by the multiplier setup in the instance
 */
double DARPSolution::getObjectiveValue() {
	if(cst::QuAndBard) {
		double cost=totalCost-0.01*compNbToursUsed();
		cost=inst->S2Dd(cost)+0.01*compNbToursUsed();
		return cost;
	}
	else return inst->S2Dd(totalCost);
}
/**
 * It returns the objective value penalized if a request is not in the solution
 * it is not the real value of the solution. for that use getObjectiveValue()
 */
double DARPSolution::getPenalizedObjectiveValue() {
	return (totalCost + (penaltyCost*(double)pickupNonInserted.size()));
}

bool DARPSolution::isFeasible() {
	return !pickupNonInserted.empty();
}

bool DARPSolution::operator<(ISolution& s) {
	if((isFeasible() && s.isFeasible()) || (!isFeasible() && !s.isFeasible())){
		return getObjectiveValue() < s.getObjectiveValue();
	}
	else{
		return isFeasible();
	}
}

size_t DARPSolution::getSizeRequestBank(){
	return pickupNonInserted.size();;
}


int DARPSolution::distance(ISolution&) {
	return 0;
}

/*************************** copy *********************************/

/**
 * fait l'update depuis s vers this*
 * attention les id des tours sont indépendants !!!
 * @param s
 */
void DARPSolution::replicateSolution(ISolution* sol){

	DARPSolution* s=dynamic_cast< DARPSolution* > (sol);
	requestInSolution=s->requestInSolution;
	deepCopyVrevers(*s);

	*(this->Teval)=*(s->Teval);
	*(this->Ceval)=*(s->Ceval);

	auto ptr_Teval=Teval; //vital!! never delete
	auto ptr_Ceval=Ceval; //vital!! never delete

	if(cst::Clustering!=cst::single) this->tours.resize(s->tours.size()); // dont delete vital in clustering
	for (size_t i=0; i < s->tours.size(); i++) {
		*(this->tours[i])=*(s->tours[i]);
		this->tours[i]->copyNodes(s->tours[i],v_revers);		// reconstitue les tournée a partir des Node_In_Sol de v_revers
		this->tours[i]->setTeval(ptr_Teval);
		this->tours[i]->setCeval(ptr_Ceval);
	}
	this->pickupNonInserted=s->pickupNonInserted;
	this->totalCost=s->totalCost;

}


/**
 * copie en profondeur v_revers
 * les parametres des Node_in_Sol sont copiés
 * initialise si besoin
 */
void DARPSolution::deepCopyVrevers(DARPSolution& s){

	//Deep Copy Vrevers
	if(cst::Clustering!=cst::single){ // DONOT REMOVE,NOR COMMENT!!!!
		for(int id: requestInSolution){  // deep copy nodes in this cluster
			*this->getVrevers()[id]=*s.getVrevers()[id];
			*this->getVrevers()[id+inst->getNbRequest()]=*s.getVrevers()[id+inst->getNbRequest()];
		}
		for(auto& t : s.getTours()){ // deep copy of depots from this cluster
			int idDepotIn= t->getNodes()[0]->id;
			int idDepotOut= t->getNodes()[t->getNodes().size()-1]->id;
			*this->getVrevers()[idDepotIn]=*s.getVrevers()[idDepotIn];
			*this->getVrevers()[idDepotOut]=*s.getVrevers()[idDepotOut];
		}
	}else{
		v_revers.resize(s.getVrevers().size());
		for(size_t i=0; i<s.getVrevers().size();++i){
			*this->getVrevers()[i]=*s.getVrevers()[i];
		}
	}

}

void DARPSolution::set_as_inserted(NodeSol * p){
	for(vector<size_t>::iterator it =  pickupNonInserted.begin(); it!=pickupNonInserted.end();it++){
		if(*it == p->node->id){
			it=pickupNonInserted.erase(it);
			break;
		}
	}
}

ISolution* DARPSolution::getCopy() {
	DARPSolution* s=new DARPSolution(name,inst);
	s->replicateSolution(dynamic_cast<ISolution*>(this));
	return dynamic_cast<ISolution*>(s);
}

/****************************************** NEW *********************************************************/

/******************************** Add *******************************/

int DARPSolution::insertTour(Tour *t){
	t->setId(tours.size());
	tours.push_back(t);
	return (int)t->getId();
}

void DARPSolution::addTours(std::vector<Tour*>& newTours){
	for(Tour* t: newTours){
		tours.push_back(t);
	}
};


/********************************* calculs finaux **********************/

double DARPSolution::recomputeCost() {
	totalCost=0;
	for (auto& t: tours) {
		totalCost+=t->getCost();
	}
	return totalCost;
}

/******************************** Remove methods *******************************/


// Remove request with the pickup id
void DARPSolution::removeRequestById(int  id_p){
	if (isInserted(id_p)) {
		auto pickup=v_revers[id_p];
		int id_sym= (int)inst->pickup[id_p]->idsym;
		auto delivery=v_revers[id_sym];
		int idTour= *(v_revers[id_p]->id_round);
		RequestInfo req(pickup,delivery,idTour);
		req.i= v_revers[id_p]->position;
		req.j= v_revers[id_sym]->position;
		Tour *t=tours[req.tourid];
		req.vehicle=t->getVehicle();
		if(t->removeRequest(req)){
			pickupNonInserted.push_back(id_p);
			recomputeCost();
		}else{
			cout<<"RemoveRequestById results in infeasibility!"<<__FILE__<<__LINE__<<endl;
			cout<<*this;
			abort();

		}
	}
}

void DARPSolution::removeSetOfRequestById(vector<size_t>& request){
	vector<vector<bool> > nodeInTourToRem(tours.size());
	for (size_t i = 0; i < tours.size(); ++i) {
		nodeInTourToRem[i].resize(tours[i]->v_node.size(),false);
	}
	for(size_t idP: request){
		if (isInserted(idP)){
			//recupérer le tour et les id des nodes
			int idT=*v_revers[idP]->id_round;
			int posD=*v_revers[idP]->posSym;
			int posP=v_revers[idP]->position;
			nodeInTourToRem[idT][posP]=true;
			nodeInTourToRem[idT][posD]=true;
			pickupNonInserted.push_back(idP);
		}
	}
	for (size_t i = 0; i < tours.size(); ++i) {
		bool toRecompute=false;
		Tour * t=tours[i];
		for (int j = t->v_node.size()-1; j >= 0; --j) { // Dont change j to size_t
			if(nodeInTourToRem[i][j]){
				t->v_node.erase(t->v_node.begin()+j);
				toRecompute=true;
			}
		}
		if(toRecompute){
			// remettre en cause le vehicule : nullptr
			if(!t->updateFeasibleTourParameters(nullptr)){ //remise en question du vehicule
				cout<<"tour infeasible wile removing multiple request at a time! "<<__FILE__<<__LINE__<<endl;
				abort();
			}
		}
	}
	recomputeCost();
}

void DARPSolution::detectDuplicats(Tour * t){
	for(size_t i=1;i<t->v_node.size()-1;++i){
		bool in=false;
		for(size_t j=0;j<t->v_node.size();++j){
			if(t->v_node[i]->id==t->v_node[j]->id){
				if(in){
					cout<<"doublons!! detected darpsol"<<__FILE__<<__LINE__<<endl;
					cout<<t->str_tour()<<endl;
					abort();
				}
				in=true;
			}
		}
	}
}

/******************************** Autres *******************************/


long long DARPSolution::getHash(){

	return (long long) this->getObjectiveValue() ;
}

bool DARPSolution::isInserted(size_t id){
	for (size_t idn : pickupNonInserted) {
		if(idn==id || v_revers[idn]->node->idsym==id){
			return false;
		}
	}
	return true;
}

double DARPSolution::computeFleetUtilization(){
	fleetUtilization=0;
	double maxUtilization=0;
	for(auto& t : tours){
		if(t->getNodes().size()>2){
			Ceval->setIdConfigParameters(t->getNodes(), t->getVehicle(), &t->nbReconfigurations);
			cout<<"Route "<<t->id<< " Nb reconfigurations "<< t->nbReconfigurations<<endl;
			fleetUtilization+=t->computeVehicleUtilization();
			++maxUtilization;
		}
	}
	fleetUtilization=fleetUtilization/maxUtilization;
	return fleetUtilization;
}


/******************************** Afficher *******************************/

std::ostream& operator<<(std::ostream& out, DARPSolution& f) {
	int nbRec=0;
	for (size_t i = 0; i < f.getTours().size(); ++i) {
		if(f.getTours()[i]->v_node.size()>2){
			out << *f.getTours()[i] << "\n";
			nbRec=max(nbRec,f.getTours()[i]->getNbReconfigurations());
		}
	}
	out << "Nb Reconfigurations:\t" <<nbRec<< "\n";
	out << "Total cost:\t" << f.getObjectiveValue()<< "\n";
	return out;
}

string DARPSolution::writeSolution(std::string instance_csv) {

	//std::string path = "../outputfiles/"+ instance_csv;
	stringstream s;
	//use always getObjective() to get the real value
	s<<this->getObjectiveValue()<<"\tcout_total\n";
	s<<tours.size()<<"#tournees\n";
	s<<"id_tour\tid_vehi\tcout_tour\tt_debut\tt_fin\tnodes\n";

	for (size_t i = 0; i < tours.size(); ++i) {
		s<<tours[i]->getId();
		s<<"\t"<<tours[i]->getVehicle()->getId();
		s<<"\t"<<inst->S2Dd(tours[i]->getCost());
		s<<"\t"<<inst->S2Dd(tours[i]->v_node[0]->time);
		s<<"\t"<<inst->S2Dd(tours[i]->v_node[tours[i]->v_node.size()-1]->time);
		for (size_t j = 0; j < tours[i]->v_node.size(); ++j) {
			s<<"\t"<<tours[i]->v_node[j]->id;
		}
		s<<"\n";
	}

	size_t totalwait=0;
	for (size_t i = 0; i < tours.size(); ++i) {
		if(tours[i]->v_node.size()>2){
			s<<tours[i]->str_tour();
		}
	}

	double fixedCost=0;
	double penalty=0;
	double costDuration=0;
	double costDistance=0;
	double costPenalty=0;
	double TotalDuration=0;
	double totalDistance=0;
	for (auto t: tours) {
		if(t->v_node.size()>2) fixedCost+=t->getVehicle()->getFixedCost();
		penalty+=(double)t->getSumRidetimes();
		costPenalty+=(double)t->getSumRidetimes()*this->inst->getPenaltyCost();
		TotalDuration+=(double)t->getDuration();
		totalDistance+=(double)t->getDistance();
		costDuration+=(double)t->getDuration()*t->getVehicle()->getTimeCost();
		costDistance+=(double)t->getDistance()*t->getVehicle()->getDistanceCost();
	}
	s<<"\n";
	s<<inst->S2Dd(totalwait)<<"\tT.Wait\n";
	s<<inst->S2Dd(TotalDuration)<<"\tT.Duration\n";
	s<<inst->S2Dd(totalDistance)<<"\tT.Distance\n";
	s<<inst->S2Dd(penalty)<<"\tT.SumRideTimes\n";
	s<<inst->S2Dd(costDuration)<<"\tC.Duration\n";
	s<<inst->S2Dd(costDistance)<<"\tC.Distance\n";
	s<<inst->S2Dd(costPenalty)<<"\tC.Penalty\n";
	s<<fixedCost<<"\tC.Fixed\n";
	s<< getObjectiveValue() <<"\tObjFunction\n";

	if(cst::QuAndBard)
	{
		name=instance_csv;
		size_t from=name.find("/s")+2;
		size_t to=name.find(".csv");
		name=name.substr(from,(to-from));
		ofstream fichier("./qbard.csv", ios::out | ios::app);  // ouverture en écriture avec effacement du fichier ouvert
		stringstream out;
		//out<<"T.Wait T.Distance T.Duration T.Penalty C.Distance C.Duration C.Penal C.ObjeFunction \n ";
		out<<name<<"\t";

		out<<inst->S2Dd(totalwait)<<"\t";
		out<<inst->S2Dd(totalDistance)<<"\t";
		out<<inst->S2Dd(TotalDuration)<<"\t";

		out<<inst->S2Dd(penalty)<<"\t";
		out<<inst->S2Dd(costDistance)<<"\t";

		out<<inst->S2Dd(costDuration)<<"\t";
		out<<inst->S2Dd(costPenalty)<<"\t";
		out<<fixedCost<<"\t";
		out<< getObjectiveValue() <<"\n";

		fichier << out.str();
		fichier.close();
	}

	ofstream fichier(instance_csv, ios::out | ios::trunc);  // ouverture en écriture avec effacement du fichier ouvert
	if(fichier){
		fichier << s.str();
		fichier.close();
	} else{
		cout <<"Impossible d'ouvrir le fichier "<< instance_csv <<" in "<<__FILE__<<__LINE__ << endl;
	}

	return s.str();
}

std::string DARPSolution::to_gpx(std::string gpx_file) const {
	stringstream out;
	out<<"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
	out<<"<gpx xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"http://www.topografix.com/GPX/1/0\" xsi:schemaLocation=\"http://www.topografix.com/GPX/1/0 http://www.topografix.com/GPX/1/0/gpx.xsd\">\n";
	for (int i = 0; i < (int)tours.size(); ++i) {
		if(tours[i]->v_node.size()>2){
			out<<"\t<rte>\n";
			out<<tours[i]->to_gpx();
			out<<"\t</rte>";
		}
	}
	out<<"</gpx>";
	ofstream fichier(gpx_file.c_str(), ios::out | ios::trunc);  // ouverture en écriture avec effacement du fichier ouvert
	if(fichier){
		fichier << out.str();
		fichier.close();
	}
	else cout << "Impossible d'ouvrir le fichier !" << endl;
	return out.str();
}


