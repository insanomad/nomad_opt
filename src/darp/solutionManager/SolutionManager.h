/*
 * SolutionManager.h
 *
 *  Created on: Feb 8, 2016
 *      Author: sam
 */

#ifndef DARP_SOLUTIONMANAGER_SOLUTIONMANAGER_H_
#define DARP_SOLUTIONMANAGER_SOLUTIONMANAGER_H_

#include "../instanceManager/Instance.h"
#include "../clustering/Cluster.h"
#include "DARPSolution.h"
#include "../ALNS_inc.h"
#include <ctime>

class SolutionManager {
public:
	SolutionManager();
	SolutionManager(std::string _name, Cluster * _c, Instance * _inst,
					DARPSolution * _sol, AOperatorManager * _opMan,
					ALNS_Parameters * _param, Statistics * _stat);

	virtual ~SolutionManager();

	/**
	 * @param nb : nombre de cluster
	 */
	void solve(int nb);

	/**
	 * solver cluster id
	 */
	void solveClusters(int nb);
	void solveSubProblem();

	/**
	 * reserve actual tours to the vector v_tour
	 * create all new routes if createNewTours==1
	 * set note to compute in v_requests
	 *
	 */
	void initSubProblem(size_t c, std::string name);

	/**
	 * agregate tours from v_tours
	 * set the id of the tours
	 */
	void restoreRoutes();


	//print solution
	void printExportBestSolution(std::string s, const std::vector<size_t>& reqInCluster);


	const Cluster* getClust() const {
		return clust;
	}

	void setClust(Cluster* clust) {
		this->clust = clust;
	}

	const Instance* getInst() const {
		return inst;
	}

	void setInst(Instance* inst) {
		this->inst = inst;
	}

	const std::string& getName() const {
		return name;
	}

	void setName(const std::string& name) {
		this->name = name;
	}

	ALNS_Parameters* getParam() const {
		return param;
	}

	DARPSolution* getSol() const {
		return sol;
	}

	void setSol(DARPSolution*& sol) {
		this->sol = sol;
	}

	Statistics* getStats() const {
		return stats;
	}

	const std::vector<Tour*>& getTours() const {
		return tours_general;
	}

	void setTours(const std::vector<Tour*>& tours) {
		tours_general = tours;
	}

private:
	std::string name;
	Cluster * clust=nullptr;
	Instance * inst=nullptr;
	DARPSolution * sol=nullptr;
	AOperatorManager * opMan=nullptr;
	Statistics * stats=nullptr;
	ALNS_Parameters * param=nullptr;
	Checker * checkSol=nullptr;
	std::vector<Tour * > tours_general;
	std::vector<size_t > v_requests;
	clock_t t0=clock();
};

#endif /* DARP_SOLUTIONMANAGER_SOLUTIONMANAGER_H_ */
