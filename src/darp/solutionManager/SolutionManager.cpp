/*
 * SolutionManager.cpp
 *
 *  Created on: Feb 8, 2016
 *      Author: sam
 */

#include "SolutionManager.h"
#include "sstream"
#include "../../statistics/Statistics.h"
#include "../checker/Checker.h"
#include "../../constant.h"
#include "../operators/InsInsertKRegret.h"


using namespace std;


SolutionManager::SolutionManager() {
	checkSol=nullptr;
}

SolutionManager::SolutionManager(std::string _name, Cluster * _c, Instance * _inst,
		DARPSolution * _sol, AOperatorManager * _opMan, ALNS_Parameters * _param,
		Statistics * _stats) {
	name=_name;
	clust= _c;
	inst=_inst;
	sol=_sol;
	opMan=_opMan;
	stats=_stats;
	param=_param;
	checkSol= new Checker(sol);
	t0=clock();
}

SolutionManager::~SolutionManager() {
	delete checkSol;
}

void SolutionManager::solve(int nb)
{
	t0=clock();
	std::stringstream file_path;
	if(cst::Clustering == cst::single || nb==1)
	{
		clust->setSingleCluster();
		file_path<<param->getPath()<<"s"<<name;
		opMan->getRepairOperator("4_regret").repairSolution(dynamic_cast<ISolution&>(*sol));
		if(cst::checkAll) checkSol->solCheck();
		ALNS alns("LNS", dynamic_cast<ISolution&>(*sol),*param, *opMan, *stats, inst);
		alns.solve();
		clock_t t1=clock();
		stats->generalTimes(name, "/GeneralResults.csv",(double)(t1-t0)/CLOCKS_PER_SEC, sol->getObjectiveValue(), alns.getNbIterations(), sol->compNbToursUsed(),(double)(alns.getTimeToReachBestSol()-t0)/CLOCKS_PER_SEC, alns.getNbNewBestSolScp(), alns.getNbNewCurrSolScp(), alns.getNbOptSolScp(), alns.getNbReconfigurations(), alns.getFleetUtilization(), alns.getNbRoutesRec());
	}
	else
	{
		if(cst::Clustering == cst::exact){
			clust->createClusters(nb,0,1000);
		}else if(cst::Clustering == cst::kMedoid){
			clust->kmedoid_swap(nb);  //not optimal
		}
		std::cout<<*clust;
		solveClusters(nb);
	}

	if(cst::writeFilesSol)	printExportBestSolution(file_path.str(), sol->getRequestInSolution());
	if(cst::checkFinal)		checkSol->solCheck();
	if(cst::displaySol) std::cout << *sol<<std::endl;
	if(cst::verbosite){ 	std::cout <<"\n***************************************\n*********** BEST SOLUTION ************\n***************************************\n"<<std::endl;
	std::cout << (sol->getObjectiveValue())<<std::endl;
	}

}

void SolutionManager::solveClusters(int nb){
	std::stringstream file_path;
	for (size_t i = 0; i < clust->getVvGroups().size(); ++i) {
		initSubProblem(i,name+"c");
		solveSubProblem();
		file_path.str("");
		file_path<<param->getPath()<<"s"<<name<<"_"<<i<<"|"<<nb;
		if(cst::writeFilesSol) printExportBestSolution(file_path.str(), sol->getRequestInSolution());
	}
	restoreRoutes();
	if(cst::checkFinal)
		checkSol->solCheck();
	file_path.str("");
	file_path<<param->getPath()<<"s"<<name<<"_"<<nb<<"|all";
	if(cst::writeFilesSol)	printExportBestSolution(file_path.str(), sol->getRequestInSolution()); // verifier toutes cest les nodes!!! TODO sam je ne comprends pas le commentaire
}

void SolutionManager::solveSubProblem(){
	opMan->getRepairOperator("4_regret").repairSolution(dynamic_cast<ISolution&>(*sol)); //->  Initial solution
	if(cst::checkAll) 	checkSol->solCheck();
	ALNS alns("LNS", dynamic_cast<ISolution&>(*sol),*param, *opMan, *stats, inst);
	alns.solve();
	clock_t t1=clock();
	stats->generalTimes(name, "/GeneralResults.csv",(double)(t1-t0)/CLOCKS_PER_SEC, sol->getObjectiveValue(), alns.getNbIterations(), sol->compNbToursUsed(),(double)(alns.getTimeToReachBestSol()-t0)/CLOCKS_PER_SEC, alns.getNbNewBestSolScp(), alns.getNbNewCurrSolScp(), alns.getNbOptSolScp(), alns.getNbReconfigurations(), alns.getFleetUtilization(),alns.getNbRoutesRec());
	if(cst::verbosite){
		std::cout <<"\n***************************************\n*********** BEST SOLUTION ************\n***************************************\n"<<std::endl;
		std::cout << (sol->getObjectiveValue())<<std::endl;
	}
}

void SolutionManager::initSubProblem(size_t c, string name) {
	for (vector<Tour *>::iterator it=sol->getTours().begin();
			it!=sol->getTours().end();){
		if((*it)->getNodes().size()>2){
			tours_general.push_back(*it);
			it = sol->getTours().erase(it);
		}
		else{
			++it;
		}
	}
	for(size_t t=0 ;t < sol->getTours().size(); ++t){
		sol->getTours()[t]->id=t;
	}
	sol->init(name,clust->getVvGroups()[c]);
	for (size_t i = 0; i < sol->getRequestInSolution().size(); ++i) {
		v_requests.push_back(sol->getRequestInSolution()[i]);
	}
	//init solution
	//sol->setRequestInCluster(clust->getVvGroups()[c]);
}

void SolutionManager::restoreRoutes() {
	sol->addTours(tours_general);
	for (size_t i = 0; i < sol->getTours().size(); ++i) {
		sol->getTours()[i]->id=i;
	}
	sol->setRequestInCluster(v_requests);
	sol->recomputeCost();
}

/*
 * Print Best Solution and Export best solution in .cvs and .gpx
 */
void SolutionManager::printExportBestSolution(std::string path, const std::vector<size_t>& reqInCluster){
	if(cst::genInstanceFile){
		std::string name_inst="c"+std::to_string(reqInCluster.size());
		sol->getInst()->write_instance_gihp(name_inst, reqInCluster);
	}
	param->toString(path);
	cst::toString(path);
	sol->writeSolution(path+".csv");
	sol->to_gpx(path+".gpx");
	sol->getInst()->to_gpx(path+"_inst.gpx", reqInCluster);
	stats->generateStatsFile(path+"_gstat.csv",path+"_op.csv");
}







