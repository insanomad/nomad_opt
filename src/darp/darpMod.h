
/*
 * darpMod.h
 *
 *  Created on: May 26, 2015
 *      Author: oscarts
 */

#ifndef SRC_DARP_DARPMOD_H_
#define SRC_DARP_DARPMOD_H_

#include "instanceManager/Instance.h"
#include "operators/operators.h"
#include "solutionManager/DARPSolution.h"
#include "../darp/clustering/H_clustering.h"
#include "../alns/SetCovering.h"
#include "../alns/ALNS_Parameters.h"
#include "../alns/OperatorManager.h"
#include "../statistics/Statistics.h"
#include "solutionManager/SolutionManager.h"

#include <vector>
#include <string>


class darpMod {
public:

	darpMod(std::string _name, std::string path_instance, std::string path_distances, std::string path_times, std::string path_vehicles, std::string path_output, int nb_reconfigurations, int type_inst, int nb_replica);
	virtual ~darpMod();
	double solveDARP();

	ALNS_Parameters* getAlnsParameters() const {
		return alnsParam;
	}

	Instance* getInstance() const {  //TODO sam : j'ai enlevé le const à cause du checker qui n'en a pas
		return instance;
	}

	int getNbClusters() const {
		return nbClusters;
	}

	void setNbClusters(int nbClusters = 1) {
		this->nbClusters = nbClusters;
	}

	const Neighborhood* getNeighbourhood() const {
		return neigh;
	}

	int getReplica() const {
		return replica;
	}

	void setReplica(int replica = 1) {
		this->replica = replica;
	}

	const SolutionManager* getSolManager() const {
		return solMan;
	}

	DARPSolution* getSol() {
		return sol;
	}

	/**
	 * usefull for unit test
	 * load a solution with an operator and comute the performance
	 */
	double computeOnlyInitSol(ARepairOperator * ope, std::string output);

	const string& getName() const {
		return name;
	}

private:
	Instance* instance;
	Neighborhood* neigh=nullptr;
	ALNS_Parameters* alnsParam;
	SolutionManager* solMan=nullptr;
	DARPSolution* sol=nullptr;
	std::string directory ="";
	string name;
	int replica=1;
	int nbClusters=1;
	string inst_name; // path of the solution proposed by cordeau
};


#endif /* SRC_DARP_DARPMOD_H_ */
