
/*
 * Checker.h
 *
 *  Created on: May 3, 2015
 *      Author: sam
 */

#ifndef CHECKER_H_
#define CHECKER_H_

#include "../instanceManager/Instance.h"
#include "../solutionManager/DARPSolution.h"

class Checker {
public:
	Checker(DARPSolution* s);
	Checker(Instance* p); //TODO passer l'instance en const
	Checker(Instance* p, DARPSolution* s); //TODO passer la solution en const

	bool solCheck();
	bool instCheck();

	bool problem_data();
	bool checkPositionsCoherence();
	bool solution_completness();
	bool solution_vs_problem_data();
	bool p_vehicle_data();
	bool p_triangular_inequalities();

	/**
	 * chacke fesibility of all requests
	 * @return true if all request are feasible
	 */
	bool p_tw_pickup_deliv();

	/**
	 * instance check: check a request
	 * @param i id of the request to check
	 * @return true if the request is feasible
	 */
	bool check_request(size_t i);


	//bool rides();
	bool times();
	bool vehicles_load();
	bool tours_data_id();
	bool tours_load();
	bool capacityTest();
	bool capacityTest2();
	bool nbVehiclesTest(); //-> This method test vehicle capacity as well as max number of reconfiguraions constraints
	bool tours_tij();
	bool distances();
	bool cost();
	bool tour_id_duplicate();

	Instance * ins=NULL;
	DARPSolution * sol;

};


#endif /* CHECKER_H_ */
