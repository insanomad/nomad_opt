/*
 * Checker.cpp
 *
 *  Created on: May 3, 2015
 *      Author: sam
 */

#include <iostream>
#include <cmath>
#include "Checker.h"

#include "../structure/Node.h"
#include "../structure/NodeSol.h"
#include "../structure/Tour.h"
#include "../../constant.h"

using namespace std;


//default constructor
Checker::Checker(DARPSolution* s) {
	sol=s;
	ins=sol->getInstance();
}
Checker::Checker(Instance* p) {
	ins=p;
	sol=nullptr;
}

Checker::Checker(Instance* p, DARPSolution* s) {
	ins=p;
	sol=s;
}

bool Checker::solCheck() {
	bool test=true;
	cout<<" >>Checker: ";
	//check
	if(!solution_completness()){
		cout<<"[WARNING] Solution completness: No"<<endl;
	}
	if(!p_vehicle_data()){
		test=false;
		cout<<"Vehicle data: No"<<endl;
	}
	if(!tours_tij()){
		test=false;
		cout<<"T_ij: No"<<endl;
	}
	if(!capacityTest()){
		test=false;
		cout<<"Tour load: No"<<endl;
	}
	if(!tours_load()){
		test=false;
		cout<<"Tour load: No"<<endl;
	}
	if(!nbVehiclesTest()){
		test=false;
		cout<<"Tour load: No"<<endl;
	}
	if(!times()){
		test=false;
		cout<<"Times: No"<<endl;
	}
	if(!distances()){
		test=false;
		cout<<"Distance: No"<<endl;
	}
	if(!cost()){
		test=false;
		cout<<"Cost: No"<<endl;
	}
	if(!tour_id_duplicate()){
		test=false;
		cout<<"Tour duplicate: No"<<endl;
	}
	if(!solution_vs_problem_data()){
		test=false;
		cout<<"Solution vs. Problem data: No"<<endl;
	}
	if(!checkPositionsCoherence()){
		test=false;
		cout<<"Nodes positions in tour data: No"<<endl;
	}
	if(!tours_data_id()){
		test=false;
		cout<<"Tour id in nodes: No"<<endl;
	}

	if(!test){
		cout<<"[ERROR] Checker solution NOT ok"<<endl;
		if(cst::checkerAbort)abort();
	}
	else cout<<"ok"<<endl;
	return test;
}

bool Checker::p_vehicle_data(){
	bool test=true;
	auto v_ins= sol->getTours()[0]->getInst()->getVehicleTypes().begin();
	int z=1;
	int cont=0;
	for(auto& t: sol->getTours()){
		if(t->getVehicle()== nullptr){
			cout<<"There isn't vehicle in tour"<< t->id<<endl;
			test=false;
		}
		if(t->getVehicles().size() ==0){
			cout<<"The list of vehicles is empty in tour"<< t->id<<endl;
			test=false;
		}
		if(!cst::QuAndBard){
			//tester tout les vehicules en capacité
			t->getCeval()->setNodes(t->getNodes());
			std::vector<int> dominantNodes=t->getCeval()->getDominantNodes();
			for(VehicleType * vt : ins->vehicleTypes){
				bool test2=false;
				if(t->getCeval()->isFesibleVehicle(vt,dominantNodes)){ 				//si faisable verifier qu'il est dans vehicles
					for(auto& vehicInVehicles:t->getVehicles()){
						if(vehicInVehicles.second->getId()==vt->getId()){
							test2=true;
							break;
						}
					}
					if(!test2){
						test=false;
						cout<<"le vehicule faisable : "<< vt->getId() <<" n'est pas dans la liste des faisables : (";
						for(auto& v : t->getVehicles()){
							cout << v.second->getId()<<",";
						}
						cout<<")"<<endl;
					}
				} else{				//sinon verifier qu'il n'y est pas
					for(auto& vehicInVehicles:t->getVehicles()){
						if(vehicInVehicles.second->getId()==vt->getId()){
							test=false;
							cout<<"le vehicule non faisable : "<< vt->getId() <<" est dans la liste des faisables : (";
							for(auto& v : t->getVehicles()){
								cout << v.second->getId()<<",";
							}
							cout<<")"<<endl;
							break;
						}
					}
				}
			}
			//pour chaque vehicule dans vehicules verifier que le cout marginal est bon

//			auto v= t->getVehicles().begin();
//			for(size_t i=0; i<t->getVehicles().size();++i){
//				if(t->getInst()->getVehicleTypes()[i] != (*v).second){
//					cout<<"Vehicle "<< i<< " in tour "<< t->id<<" is different to the instance counterpart"<<endl;
//					test=false;
//				}
//				if((*v).second->getFixedCost()<0 ||(*v).second->getDistanceCost()<0||(*v).second->getTimeCost()<0||(*v).second->getSpeed()< 0){
//					cout<<"Vehicle "<< i<< " in tour "<< t->id<<" hast negative cost or speed"<<endl;
//					test=false;
//				}
//				++v;
//			}
		}//else{  // in qu and bad tours dont change vehicle.

			//			if(t->getVehicle()!= *v_ins){
			//				cout<<*t->getVehicle()<<endl;
			//				cout<<**v_ins<<endl;
			//				cout<<"Vehicle "<< t->getVehicle()->getId()<< " in tour " << t->id <<" is different to the instance counterpart" <<endl;
			//				test=false;
			//			}
			//			if(z >= (t->getInst()->getDepotDepart()[cont]->cap)){
			//				z=1;
			//				++cont;
			//				++v_ins;
			//			}else{
			//				++z;
			//			}
		//}
		if(t->getVehicle()->getFixedCost()<0 ||t->getVehicle()->getDistanceCost()<0||t->getVehicle()->getTimeCost()<0||t->getVehicle()->getSpeed()< 0){
			cout<<"Vehicle in tour "<< t->id<<" hast negative cost or speed"<<endl;
			test=false;
		}
	}
	if(cst::QuAndBard){
		bool ok=false;
		vector<int> insol(sol->getTours()[0]->getInst()->getVehicleTypes().size(),0);
		for(auto& t: sol->getTours()){
			ok=false;
			for(size_t i=0; i<t->getInst()->getVehicleTypes().size();++i){
				VehicleType* vi=t->getInst()->getVehicleTypes()[i];
				//cout<<t->getVehicle()<<" "<<vi<<endl;
				if(t->getVehicle()== vi ){
					insol[i]=insol[i]+1;
					if(insol[i] > (t->getInst()->getDepotDepart()[i]->cap)){
						cout<<"Vehicle "<< t->getVehicle()->getId()<< "is more times than expected "<< insol[i] <<" vs "<< t->getInst()->getDepotDepart()[cont]->cap<<endl;
						test=false;
					}
					ok=true;
					break;
				}
			}
			if(!ok){
				cout<<*t->getVehicle()<<endl;
				cout<<"Vehicle "<< t->getVehicle()->getId()<< " doest not exist in the list of vehicles!!" <<endl;
				test=false;
			}

		}
	}

	return test;
}

bool Checker::instCheck() {
	bool test=true;
	cout<<"**************** Checker ****************"<<endl;
	//check
	if(!problem_data()){
		test=false;
		cout<<"Problem data: No"<<endl;
	}
	if(!p_triangular_inequalities()){
		test=false;
		cout<<"Triangular inquality: No"<<endl;
	}
	if(!p_tw_pickup_deliv()){
		test=false;
		cout<<"The time windows are too short to go from pickup to delivery: No"<<endl;
	}
	if(test) cout<<"Check instance ok"<<endl;
	else {
		cout<<"[ERROR] Check instance NOT ok"<<endl;
		if(cst::checkerAbort) abort();
	}
	return test;
}

/*************************************** Check ******************************************/

bool Checker::problem_data() {
	//les vecteurs et leurs taille supposees correspondent
	bool test=1;
	if(ins->nb_request*2!=ins->dataRequests.size()) {
		test=0;
		cout<< "pro.nb_request*2!=pro.v_node.size()" << endl;
	}
	if(ins->nb_request!=ins->pickup.size()){
		test=0;
		cout<< "pro.nb_riquest!=pro.vp_pickupro.size()" << endl;
	}
	if(ins->nb_request!=ins->delivery.size()){
		test=0;
		cout<< "pro.nb_riquest!=pro.vp_delivery.size()" << endl;
	}
	if(ins->nb_depot!=ins->depot_depart.size()){
		test=0;
		cout<< "pro.nb_depot!=inst.deport_depart.size()" << endl;
	}
	if(ins->nb_depot!=ins->depot_arrival.size()){
		test=0;
		cout<< "pro.nb_vehicle_type!=pro.vp_depot_arrival.size()" << endl;
	}
	for (size_t i = 0; i < ins->nb_request; ++i) {
		//load size
		if(ins->dataRequests[i]->rload.size()!=ins->nb_user_type){
			test=0;
			cout<<"pro.v_node["<<i<<"]->v_delta_load.size()!=pro.nb_user_type"<<endl;
		}
	}
	for (size_t i = 0; i < ins->nb_request; ++i){
		if(ins->pickup[i]->idsym!=ins->delivery[i]->id){
			test=0;
			cout<<"problem id sym pickup "<<ins->pickup[i]->idsym<<" "<<ins->delivery[i]->id<<__FILE__<<" "<<__LINE__<<endl;
		}
		if(ins->delivery[i]->idsym!=ins->pickup[i]->id){
			test=0;
			cout<<"problem id sym delivery "<<__FILE__<<" "<<__LINE__<<endl;
		}
	}
	for (size_t i = 0; i < ins->nb_depot; ++i){
		if(ins->depot_arrival[i]->idsym!=ins->depot_depart[i]->id){
			test=0;
			cout<<"problem id sym depot "<<__FILE__<<" "<<__LINE__<<endl;
		}
		if(ins->depot_depart[i]->idsym!=ins->depot_arrival[i]->id){
			test=0;
			cout<<"problem id sym depot "<<__FILE__<<" "<<__LINE__<<endl;
		}
	}
	//type of node
	for (size_t i = 0; i < ins->nb_request; ++i) {
		if(i<ins->nb_request){
			if(ins->dataRequests[i]->type!=0){
				test=0;
				cout<<"le type du node "<<i<<" n'est pas correct"<<endl;
			}
		}
		else if(i<2*ins->nb_request){
			if(ins->dataRequests[i]->type!=1){
				test=0;
				cout<<"le type du node "<<i<<" n'est pas correct"<<endl;
			}
		}
		else if(i<2*ins->nb_request+ins->nb_depot){
			if(ins->dataRequests[i]->type!=2){
				test=0;
				cout<<"le type du node "<<i<<" n'est pas correct"<<endl;
			}
		}
		else {
			if(ins->dataRequests[i]->type!=3){
				test=0;
				cout<<"le type du node "<<i<<" n'est pas correct"<<endl;
			}
		}
	}
	//idée verifier que le node symetrique est du bon type
	//idée verifier que le node symetrique a un load inverse
	return test;
}

bool Checker::p_triangular_inequalities() {
	bool test=true;
//	bool test2=true;
	size_t n = ins->distance_ij.size();
	for (size_t k = 0; k <n ; ++k){
		for (size_t i = 0; i <n ; ++i){
			for (size_t j = 0; j <n ; ++j){
				if(ins->time_ij[i][j] > ins->time_ij[i][k]+ins->time_ij[k][j]){
					cout<<"(i,j,k):("<<i<<","<<j<<","<<k<<")\tij:"<<ins->time_ij[i][j]<<" ik:"<<ins->time_ij[i][k]<<" kj:"<<ins->time_ij[k][j]<<endl;
					test=false;
				}
			}
		}
	}
	if(!test) {
		cout<< "pro.vv_time!=pro.triangular_inequality(pro.vv_time)" << endl;
	}
//	for (size_t k = 0; k <n ; ++k){
//		//cout <<k<<endl;
//		for (size_t i = 0; i <n ; ++i){
//			for (size_t j = 0; j <n ; ++j){
//				if(ins->distance_ij[i][j]>ins->distance_ij[i][k]+ins->distance_ij[k][j]){
//					test2=false;
//				}
//			}
//		}
//	}
//	if(!test2){
//		cout<< "pro.vv_distance!=pro.triangular_inequality(pro.vv_distance)" << endl;
//	}
	return test; // && test2;
}

bool Checker::p_tw_pickup_deliv() {
	bool test=true;
	for (size_t i = 0; i < (unsigned)ins->nb_request; ++i) {
		if(!check_request(i))
			test=false;
	}
	return test;
}


bool Checker::check_request(size_t i){
	bool test=true;
	//tw
	long left= ins->delivery[i]->tw_end - ins->pickup[i]->tw_start;
	long right = ins->time_ij[i][i+ins->nb_request];
	if( left < right){
		cout<<"TW: Dial ";
		cout <<i ;
		cout <<" (time delivery end - time pickup start < travel time)";
		cout <<endl;
		test=false;
	}
	//max tour duration
	if(ins->dur_max < ins->delivery[i]->tw_start - ins->pickup[i]->tw_end){
		cout<<"Max_ride_time: Dial ";
		cout <<i ;
		cout <<" (time delivery start - time pickup end > max tour duration) ";
		cout    << "("  << ins->delivery[i]->tw_start
				<<" - " << ins->pickup[i]->tw_end
				<<" > " << ins->dur_max
				<<")";
		cout <<endl;
		test=false;
	}
	return test;
}

// todo faire ce test pour les depots
bool Checker::solution_vs_problem_data() {
	size_t id_n;
	Tour * t;
	bool test=1;
	for (size_t i = 0; i < sol->getTours().size(); ++i) {
		t=sol->getTours()[i];
		for (size_t j = 1; j < t->v_node.size()-1; ++j) {  // in dataNode you find only pickups and deliveries..
			id_n=t->v_node[j]->node->id;
			if(ins->dataRequests[id_n]->type!=t->v_node[j]->node->type){
				test=0;
				cout<<"le type du node "<<i<<" n'est pas le meme dans p et s"<<endl;
			}
			if(ins->dataRequests[id_n]->tw_start!=t->v_node[j]->node->tw_start){
				test=0;
				cout<<"le tw_start du node "<<i<<" n'est pas le meme dans p et s"<<endl;
			}
			if(ins->dataRequests[id_n]->tw_end!=t->v_node[j]->node->tw_end){
				test=0;
				cout<<"le tw_end du node "<<i<<" n'est pas le meme dans p et s"<<endl;
			}
			if(ins->dataRequests[id_n]->rload!=t->v_node[j]->node->rload){
				test=0;
				cout<<"le v_delta_load du node "<<i<<" n'est pas le meme dans p et s"<<endl;
			}
			if(ins->dataRequests[id_n]->idsym!=t->v_node[j]->node->idsym){
				test=0;
				cout<<"le node symetrique du node "<<i<<" n'est pas le meme dans p et s"<<endl;
			}
		}
	}
	return test;
}

bool Checker::tours_data_id() {
	Tour *t;
	NodeSol *nj;
	bool test=1;
	/*	for (int i = 0; i < sol->getVrevers().size(); ++i) { // shouldnt work due to clustering constraints
		int idt=*(sol->getVrevers()[i]->id_round);
		t=sol->getTours()[idt];
		if (*(sol->getVrevers()[i]->id_round)!=t->id) {
			cout<<"Problems id tour "<<i<<" : it does not correspond to the position!"<<endl;
		}
	}*/
	for (size_t i = 0; i < sol->getTours().size(); ++i) {
		t=sol->getTours()[i];
		if (t->id!=i) {
			cout<<"Problems tour id "<<i<<" : it does not correspond to position "<<i<<endl;
		}
		for (size_t j = 1; j < t->v_node.size()-1; ++j) {
			nj=t->v_node[j];
			//tout les nodesInSol ont le bon tour associe
			if(*(nj->id_round)!=t->id){
				test=0;
				cout << "sol->getTours()["<<i<<"]->v_node["<<j<<"]->id_round!=sol->getTours()["<<i<<"]->id"<<endl;
			}
			//tout les nodes ont le bon index de position
			if(nj->position!=j){
				test=0;
				cout << "sol->getTours()["<<i<<"]->v_node["<<j<<"]->position_in_round!="<<j<<endl;
			}
		}
	}
	return test;
}
// This method test vehicle capacity as well as max number of reconfiguraions constraints
bool Checker::capacityTest() {
	auto test=true;

	for(auto& tour: sol->getTours()){
		tour->computeNbReconfigurations();
		std::pair<double, VehicleType*>  v(0,tour->getVehicle());
		std::vector<std::pair<double ,VehicleType*> > vehicleList;
		vehicleList.push_back(v);
		VehicleType* feasibleVehicle=sol->getCeval()->performCapacityTest(tour->getNodes(),vehicleList,0);
		if(feasibleVehicle==nullptr){
			cout<<"[Ch.capacityTest] tour "<<tour->getId()<<" is capacity unfeasible! "<<endl;
			test=false;
		}
	}
	capacityTest2(); //-> this test is importat as it's independent to de CapacityEvaluation class
	return test;
}


bool Checker::capacityTest2() {
	auto test=true;
	for(auto& tour: sol->getTours()){
		auto vehicle=tour->getVehicle();
		auto idc =tour->v_node[0]->idConfig;
		for(auto& node: tour->v_node){
			if(!cst::QuAndBard)
				idc =node->idConfig;
			for(size_t u=0; u< node->load.size();++u){
				if( node->load[u] > vehicle->getConfiguration()[idc]->getCapacity()[u]){
					test=false;
					cout<<"[Ch.capacityTest2] tour "<<tour->getId()<<" is capacity unfeasible! vehicle"<< tour->getVehicle()->getId() <<" config "<< idc <<__FILE__<<__LINE__<<endl;
				}
			}
		}
	}
	return test;
}

// This method test vehicle capacity as well as max number of reconfiguraions constraints
bool Checker::nbVehiclesTest() {
	bool test=true;
	for(auto node: ins->dataDepot){
		int cont=0;
		for(auto& tour: sol->getTours()){
			auto vehicle=tour->getVehicle();
				if(node->id==tour->getNode()[0]->node->id){
					cont++;
				}
				if(cont > node->cap){
					cout<<"Nb of vehicles/depots is exceeded : Limit "<< node->cap << " actual: "<<cont<<endl;
					test=false;
				}
		}
	}
	return test;
}


bool Checker::tours_load() {
	Tour *t;
	NodeSol *nj;
	bool test_load,test=1;
	for (size_t i = 0; i < sol->getTours().size(); ++i) {
		t=sol->getTours()[i];
		//les load sont coherents
		for (size_t j = 1; j < t->v_node.size()-1; ++j) {
			nj=t->v_node[j];
			test_load=1;
			if(cst::QuAndBard){
				// seat and wheelchar
				for (size_t k = 0; k < ins->nb_user_type-1; ++k) {
					if(nj->load[k]!=t->v_node[j-1]->load[k]+nj->node->rload[k]){
						test_load=false;
					}
				}
				//Walker
				if(nj->load[2]!=(t->v_node[j-1]->load[2]+nj->node->rload[0]+2*nj->node->rload[1]+nj->node->rload[2])){
					test_load=false;
				}
			}else{
				for (size_t k = 0; k < ins->nb_user_type; ++k) {
					if(nj->load[k]!=t->v_node[j-1]->load[k]+nj->node->rload[k]){
						test_load=false;
					}
				}
			}
			if(!test_load){
				test=0;
				cout << "le load n'est pas coherent pour sol->getTours()["<<i<<"]->v_node["<<j<<"]"<<endl;
			}
		}
		//le dernier load est 0
		nj=t->v_node[t->v_node.size()-2];
		for (size_t k = 0; k < ins->nb_user_type; ++k) {
			if(nj->load[k]!=0){
				test=0;
				cout << "le load n'est pas 0 pour sol->getTours()["<<i<<"]->v_node["<<t->v_node.size()-2<<"]"<<endl;
			}
		}
	}
	return test;
}

bool Checker::checkPositionsCoherence(){
	bool test=true;
	for (auto& t: sol->tours) {
		for (size_t i=0; i < t->v_node.size();++i) {
			if (t->v_node[i]->position!=i) {
				cout<<"position incoherence: node in pos "<<i<<" tour "<<t->id<<endl;
				test=false;
			}
			if (*t->v_node[*t->v_node[i]->posSym]->posSym!= i) {
				cout<<"position sym node: node in pos "<<i<<" tour "<<t->id<< " dont concide with the posSym of the symetric node "<<*t->v_node[i]->posSym<<endl;
				test=false;
			}

		}
	}
	return test;
}

bool Checker::tours_tij() {

	NodeSol *nj,*njp;
	bool test=1;
	for (auto t:sol->getTours()) {
		//t_ij est correct (duree du voyage jusqu'au node suivant)
		for (size_t j = 0; j < t->v_node.size()-1; ++j) {
			nj=t->v_node[j];
			njp=t->v_node[j+1];
			if(nj->getTnext()!=ins->time_ij[nj->node->id][njp->node->id] && t->getNode().size()>2){
				test=false;
				cout << "sol->getTours()["<<t->id<<"]->v_node["<<nj->node->id<<"]->t_ij="<<nj->getTnext()<<" != pro.vv_time["<<nj->node->id<<"]["<<njp->node->id<<"]= "<<ins->time_ij[nj->node->id][njp->node->id]<<endl;
			}
		}
	}
	return test;
}

bool Checker::times() {
	Tour *t;
	NodeSol *nj,*njp;
	bool test=1;
	for (size_t i = 0; i < sol->getTours().size(); ++i) {
		t=sol->getTours()[i];
		if (t->v_node.size()>2) {
			//le dernier
			nj=t->v_node[t->v_node.size()-1];
			if(nj->time > nj->node->tw_end || nj->time < nj->node->tw_start){
				test=0;
				cout << "fenetre de temps non respecte pour getTours()["<<i<<"]->v_node["<< t->v_node.size()<<"]"<<endl;
			}
			//les autres
			for (size_t j = 0; j < t->v_node.size()-1; ++j) {
				nj=t->v_node[j];
				njp=t->v_node[j+1];
				if(nj->time > nj->node->tw_end || nj->time < nj->node->tw_start) {
					test=0;
					cout << "fenetre de temps non respecte pour getTours()["<<i<<"]->v_node["<<j<<"]"<<endl;
				}

				if(nj->time + ins->time_ij[nj->node->id][njp->node->id] > njp->time) {
					test=0;
					cout << "duree de trajet non respecte pour getTours()["<<i<<"]->v_node["<<j<<"->"<<j+1<<"]"<<endl;
				}
			}
			//duration
			double duration=0;
			double distance=0;
			if(cst::QuAndBard){
				for(size_t i=0; i<t->v_node.size()-1;++i){
					distance+=ins->distance_ij[t->v_node[i]->node->id][t->v_node[i+1]->node->id];
				}
				duration= distance*2;
			}else{
				duration=t->v_node[t->v_node.size()-1]->time - t->v_node[0]->time;

			}
			if(duration != t->getDuration()){
				test=0;
				cout << "duration incorrecte pour getTours()["<<i<<"]"<<endl;
			}

			//maximum riding time constraint
			for (size_t j = 0; j < t->v_node.size(); ++j) {
				nj=t->v_node[j];
				if (nj->node->type==0) {
					int posym=-1;
					for (size_t k = 0; k < t->v_node.size(); ++k) {
						if (t->v_node[k]->node->id==nj->node->idsym) {
							posym=k;
						}
					}
					double maxrt=t->v_node[posym]->time - nj->time  -nj->node->stime;
					if (maxrt > nj->node->maxRT) {
						test=false;
						cout<<"violation max ride time " <<maxrt<<" Req : ("<<j<<","<<posym<<")"<<endl;
					}
				}
			}
		}
	}
	//verifier que le temps annoncé est minimal
	for(auto& tour: sol->getTours()){
		if(tour->getNodes().size()){
			if(!sol->getTeval()->performScheduling(tour->getNodes())){
				cout<<"[Ch.time] Time unfaesible "<<endl;
				test=false;
			}
			auto duration=sol->getTeval()->getDuration();
			if(cst::QuAndBard){
				tour->computeDistance();
				duration=sol->getTeval()->getDuration(tour->getDistance());
			}
			//auto duration=sol->getTeval()->getT(last)-sol->getTeval()->getT(0);
			if(tour->getDuration()!=duration){
				cout<<"[Ch.time] Duration is not minimal! A:"<<tour->getDuration()<<" vs Ch:"<< duration<<endl;
				test=false;
			}
		}
	}
	return test;
}

bool Checker::vehicles_load() {
	Tour *t;
	NodeSol *nj;
	bool test_conf,test_vehic,test=1;
	for (size_t i = 0; i < sol->getTours().size(); ++i) {
		t=sol->getTours()[i];
		//un vehicule doit etre affecte
		if(t->getVehicle()==nullptr) {
			cout<< "sol->getTours()["<<i<<"]->vt==NULL" <<endl;
			return 0;
		}
		if(t->v_node[0]==nullptr) {
			cout<< "sol->getTours()["<<i<<"]->v_node[0]==NULL" <<endl;
			return 0;
		}
		if(t->v_node[t->v_node.size()-1]==nullptr) {
			cout<< "sol->getTours()["<<i<<"]->v_node[sol->getTours()["<<i<<"]->nb_node-1]==NULL" <<endl;
			return 0;
		}
		//le vehicule doit etre capable de supporter la charge
		for (size_t j = 0; j < t->v_node.size(); ++j) {
			nj=t->v_node[j];
			test_vehic=0;
			for (size_t k = 0; k < t->getVehicle()->getConfiguration().size(); ++k) {
				test_conf=1;
				for (size_t l = 0; l < ins->nb_user_type; ++l) {
					if(t->getVehicle()->getVConfiguration()[k]->getCapacity()[l] < nj->load[l]){
						test_conf=0;
					}
				}
				if(test_conf){
					test_vehic=1;
				}
			}
			if(!test_vehic){
				test=0;
				cout << "le vehicule sol->getTours()["<<i<<"]->vt->id:"<< t->getVehicle()->getId() << " ne supporte pas la charge"<<endl;
			}
		}
	}
	return test;
}

bool Checker::solution_completness() {
	size_t id_n;
	Tour *t;
	bool test=1,test_presence;
	for (size_t id = 0; id < sol->getRequestInSolution().size(); ++id) {
		id_n=ins->dataRequests[sol->getRequestInSolution()[id]]->id;
		test_presence=0;
		for (size_t i = 0; i < sol->getTours().size(); ++i) {
			t=sol->getTours()[i];
			for (size_t j = 0; j < t->v_node.size(); ++j) {
				if(t->v_node[j]->node->id==id_n) {
					test_presence=1;
					break;
				}
			}
			if(test_presence) break;
		}
		if(!test_presence){
			cout << "le node "<<sol->getRequestInSolution()[id]<<" n'est pas dans la solution" << endl;
			test=0;
		}
	}
	//delete &is_in;
	return test;
}

bool Checker::distances() {
	double dist;
	Tour *t;
	bool test=1;
	//verifier les distances pour les tournees
	for (size_t i = 0; i < sol->getTours().size(); ++i) {
		t=sol->getTours()[i];
		dist=0;
		for (size_t j = 0; j < t->v_node.size()-1; ++j) {
			dist+=ins->distance_ij[t->v_node[j]->node->id][t->v_node[j+1]->node->id];
		}
		if(t->getDistance()!=dist){
			test=0;
			cout<<"distance mauvaise pour sol->getTours()["<< i<<"]:distance="<<t->getDistance()<<" dist="<<dist<<"=";
			for (size_t j = 1; j < t->v_node.size()-2; ++j) {
				cout<<ins->distance_ij[t->v_node[j]->node->id][t->v_node[j+1]->node->id]<<"+";
			}
			cout<<endl;
		}
	}
	return test;
}
bool Checker::cost() {
	double cost=0,total_cost=0;
	Tour *t;
	bool test=true;
	//verifier les distances pour les tournées
	for (size_t i = 0; i < sol->getTours().size(); ++i) {
		t=sol->getTours()[i];
		if (t->v_node.size()>2) {
			cost=t->getVehicle()->getFixedCost();
			cost+=t->getVehicle()->getTimeCost()*(t->getDuration());
			cost+=t->getVehicle()->getDistanceCost()*(t->getDistance());
			cost+=t->getSumRidetimes()*t->getCostRidetime();
			if(abs(t->getCost()-cost) > 0.01){
				test=false;
				cout<<"cost mauvais pour sol->getTours()["<< i<<"]"<<t->getCost()<<" with " << cost<<endl;
			}
			total_cost+=cost;
		}
	}

	return test;
}

bool Checker::tour_id_duplicate() {
	bool test=1;
	//verifier qu'il n'y a pas deux tour avec le meme id
	for (size_t i = 0; i < sol->getTours().size(); ++i) {
		for (size_t j = i+1; j < sol->getTours().size(); ++j) {
			if(sol->getTours()[i]->id==sol->getTours()[j]->id){
				cout<<"id du tour "<< i<<" = id du tour "<<j<< " = " << sol->getTours()[i]->id <<endl;
				test=0;
			}
		}
	}
	for (size_t i = 0; i < sol->getTours().size(); ++i) {
		if(i!=sol->getTours()[i]->id){
			cout<<"id du tour "<< i<<" = " << sol->getTours()[i]->id <<endl;
			test=0;
		}
	}
	return test;





}


