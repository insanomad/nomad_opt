/*
 * darpMod.cpp
 *
 *  Created on: May 26, 2015
 *      Author: oscarts
 */

#include "darpMod.h"

using namespace std;

// constructor to run different type of instances!
//file_instance, file_distances, file_vehicles, nb_reconfigurations, inst_type, nb_replica
darpMod::darpMod(string _name, string path_instance, string path_distances, string path_times, string path_vehicles, string path_output, int nb_reconfigurations, int type_inst, int nb_replica) {
	replica=nb_replica;
	name=_name+"|"+to_string(nb_replica)+"|"+to_string(nb_reconfigurations);
	nbClusters=1;
	instance= new Instance(path_instance, path_distances, path_times, path_vehicles, nb_reconfigurations, type_inst);
	cout<<"Instance: "<<name;
	cout<<std::endl;
	if(instance==nullptr){
		cout<<"Invalid instance type in input args"<<__FILE__<<__LINE__<<endl;
		abort();
	}
	neigh= new NeighbourMinRoute(instance);
	//neigh->init(); //TODO uncomment!
	alnsParam=new ALNS_Parameters(path_output,instance->nb_request*instance->getMult());
}

double darpMod::solveDARP() {

	NeighbourShaw nshaw(instance);
	nshaw.init();
	DistanceRemoval DistanceRemoval("Distance_Removal", 0.1, 0.40, &nshaw);
	WorseRemoval Worse_Removal("Worse_Removal", 0.1, 0.40);
	TimeRelatedRemoval Time_Removal("Time_Removal", 0.1, 0.40);

	RandomRemoval Rand_Removal("Random_Removal", 0.1, 0.45);
	HistoryRemoval History_removal("History_Removal", 0.1, 0.45, (int)instance->nb_node);
	Insert_KRegret k1regret("1_regret", 1 ,instance, false);
	Insert_KRegret k2regret("2_regret", 2 ,instance, false);
	Insert_KRegret k3regret("3_regret", 3 ,instance, false);
	Insert_KRegret k4regret("4_regret", 4 ,instance, false);

	OperatorManager opMan(*alnsParam);
	opMan.addDestroyOperator(dynamic_cast<ADestroyOperator&>(Rand_Removal));
	opMan.addDestroyOperator(dynamic_cast<ADestroyOperator&>(History_removal));
	opMan.addRepairOperator(dynamic_cast<ARepairOperator&>(k1regret));
	opMan.addRepairOperator(dynamic_cast<ARepairOperator&>(k2regret));
	opMan.addRepairOperator(dynamic_cast<ARepairOperator&>(k3regret));
	opMan.addRepairOperator(dynamic_cast<ARepairOperator&>(k4regret));


	if(cst::allOperators){
		opMan.addDestroyOperator(dynamic_cast<ADestroyOperator&>(Worse_Removal));
		opMan.addDestroyOperator(dynamic_cast<ADestroyOperator&>(Time_Removal));
		opMan.addDestroyOperator(dynamic_cast<ADestroyOperator&>(DistanceRemoval));
	}


	sol=new DARPSolution("Solution", instance);

#ifndef CPLEXNOTINSTLED
	ClusterMIPCplex clust(instance,neigh);
#else
	ClusterMIPCoin clust(instance,neigh);
#endif

	Statistics stats(alnsParam->getPath());

	SolutionManager SolMan(name,&clust,instance,sol,
			&dynamic_cast<AOperatorManager&>(opMan),
			alnsParam,
			&stats);

	SolMan.solve(nbClusters); // number of clusters
	return sol->getPenalizedObjectiveValue();
}

double darpMod::computeOnlyInitSol(ARepairOperator * ope, std::string output) {
	DARPSolution sol("Solution", instance);
	OperatorManager opMan(*alnsParam);
	opMan.addRepairOperator(*ope);
	ClusterMIPCoin clust(instance,neigh);
	alnsParam->setMaxNbIterations(0);
	Statistics stat(alnsParam->getPath());
	solMan=new SolutionManager(name,&clust,instance,&sol,
			&dynamic_cast<AOperatorManager&>(opMan),
			alnsParam,
			&stat);
	opMan.selectRepairOperator().repairSolution(dynamic_cast<ISolution&>(sol));
	solMan->printExportBestSolution(output, sol.getRequestInSolution());
	//solMan->solve(1); // number of clusters
	return solMan->getSol()->getObjectiveValue();
}

darpMod::~darpMod() {
	delete instance;
	//if(cst::Clustering!=cst::single)
	delete neigh;
	delete alnsParam;
	if(solMan!=nullptr) delete solMan;
	if(sol!=nullptr) delete sol;
}
