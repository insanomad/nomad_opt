/*
 * Cluster.h
 *
 *  Created on: Jan 14, 2016
 *      Author: sam
 */

#ifndef INSTANCEMANAGER_CLUSTER_H_
#define INSTANCEMANAGER_CLUSTER_H_

#include "../instanceManager/Instance.h"
#include "vector"
#include "Neighborhood.h"

class Cluster {
public:
	/**
	 * Constructor
	 * @param inst		the initial instance
	 * @param _neigh  neighborhood class
	 */
	Cluster(Instance * inst, Neighborhood * _neigh);
	virtual ~Cluster();

	/**
	 * split the instance into nb clusters
	 * nc number of clusters
	 * minNbr min number of request per cluster
	 * maxNbr max number of request per cluster
	 */
	virtual void createClusters(int nbclust, int minNbR, int maxNbR)=0;


	void kmedoid_swap(size_t nb);
	double computeKmedroidCost() const;

	// no clusters are considered
	void setSingleCluster();
	/**
	 * get groups in vv_groups
	 */
	void setGroups();

	/**
	 * get size of the cluster
	 */
	int getSize() const {return (int)v_med.size();}

	int getIdNodeInCluster(int i, int j) const{
		return vv_groups[i][j];
	}


	const std::vector<bool>& getIsMedroid() const {
		return v_is_medroid;
	}

	const std::vector<size_t>& getMedAsso() const {
		return v_med_asso;
	}

	const std::vector<size_t>& getMed() const {
		return v_med;
	}

	double getClusteringCost() const {
		return clusteringCost;
	}

	void setClusteringCost(double clusteringCost) {
		this->clusteringCost = clusteringCost;
	}
	bool checkSolution();

	const std::vector<std::vector<size_t> >& getVvGroups() const {
		return vv_groups;
	}

	void clearGroups(){
		vv_groups.clear();
	}

protected:
	/**
	 * initial instance
	 */
	Instance* inst;

	/**
	 * set of instances
	 */
	std::vector<Instance *> instances;


	/**
	 * kmedroid vector
	 */
	std::vector< bool > v_is_medroid;
	std::vector< size_t > v_med_asso;
	std::vector< size_t > v_med;
	// matrix with clusters. each sub vector corresponds to a different cluster
	std::vector< std::vector< size_t > > vv_groups;
	/**
	 * neighboring nodes
	 */
	Neighborhood * neigh;

	double clusteringCost;
};

std::ostream& operator<<(std::ostream&, const Cluster&); //permet d'utiliser cout


#endif /* INSTANCEMANAGER_CLUSTER_H_ */
