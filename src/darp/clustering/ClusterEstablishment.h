/*
 * ClusterEstablishment.h
 *
 *  Created on: Feb 25, 2017
 *      Author: oscar
 */

#ifndef SRC_DARP_INSTANCEMANAGER_ClusterEstablishment_H_
#define SRC_DARP_INSTANCEMANAGER_ClusterEstablishment_H_
#include "Cluster.h"
class ClusterEstablishment : public Cluster {
public:
	ClusterEstablishment(Instance* ins, Neighborhood* neigh) : Cluster(ins,neigh){}
	virtual ~ClusterEstablishment();
	void createClusters(int nbclust=1, int minNbR=0, int maxNbR=INT_MAX);
};

#endif /* SRC_DARP_INSTANCEMANAGER_ClusterEstablishment_H_ */
