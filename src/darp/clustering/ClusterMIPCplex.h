/*
 * ClusterMIPCplex.h
 *
 *  Created on: Feb 25, 2017
 *      Author: oscar
 */

#ifndef SRC_DARP_INSTANCEMANAGER_CLUSTERMIPCPLEX_H_
#define SRC_DARP_INSTANCEMANAGER_CLUSTERMIPCPLEX_H_
#include "Cluster.h"
class ClusterMIPCplex : public Cluster {
public:
	ClusterMIPCplex(Instance* ins, Neighborhood* neigh) : Cluster(ins,neigh){}
	virtual ~ClusterMIPCplex();
	void createClusters(int nbclust, int minNbR, int maxNbR);
};

#endif /* SRC_DARP_INSTANCEMANAGER_CLUSTERMIPCPLEX_H_ */
