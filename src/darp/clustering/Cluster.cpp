/*
 * Cluster.cpp
 *
 *  Created on: Jan 14, 2016
 *      Author: sam
 */

#include "Cluster.h"
//#include "../darp/structure/Node.h"
#include <algorithm>
#include <limits>

#include <iostream>


//// $Id$
#include "flopc.hpp"
#include <OsiCbcSolverInterface.hpp>

using namespace flopc;


Cluster::Cluster(Instance* _inst, Neighborhood * _neigh) {
	inst=_inst;
	neigh=_neigh;
	clusteringCost=0;
}

Cluster::~Cluster() {}

void Cluster::setSingleCluster(){
	vv_groups.resize(1);// one cluster
	for (Node* pickup : inst->pickup) {
		vv_groups[0].push_back((int)pickup->id);
	}
}

void Cluster::kmedoid_swap(size_t nb) {
	v_is_medroid.resize(inst->nb_request,false);
	v_med_asso.resize(inst->nb_request);
	v_med.resize(nb);
	double dist_min;
	size_t idmin;
	size_t pas=inst->nb_request/nb;
	//init des premier elements des vecteurs comme les medroids (arbitrairement)
	for (size_t i = 0; i < nb; ++i) {
		v_is_medroid[i*pas]=true;
		v_med_asso[i*pas]=i;
		v_med[i]=i*pas;
	}
	//associations des autres au plus proche des medroids
	for (size_t o = 0; o < (unsigned)inst->nb_request; ++o) {
		//verifier que ce n'est pas deja un medroid
		if(!v_is_medroid[o]){
			//associer au plus proche
			dist_min=neigh->getVvDist()[v_med[0]][o];
			idmin=0;
			for (size_t p = 1; p < nb; ++p) {
				if(neigh->getVvDist()[v_med[p]][o]<dist_min){
					dist_min=neigh->getVvDist()[v_med[p]][o];
					idmin=p;
				}
			}
			v_med_asso[o]=idmin;
		}
	}
	//debut de l'algo
	double cost=computeKmedroidCost();
	double cost_old=cost+1;
	size_t m,sauve_asso_o;
	bool test=false;
	while(cost<cost_old){
		test=false;
		cost_old=cost;
		for(size_t o = 0; o < (unsigned)inst->nb_request; ++o) {
			if(!v_is_medroid[o]){
				for (size_t p = 1; p < nb; ++p) {
					m=v_med[p];
					sauve_asso_o=v_med_asso[o];
					v_med[p]=o;
					v_is_medroid[m]=false;
					v_is_medroid[o]=true;
					v_med_asso[o]=p;
					v_med_asso[m]=p;
					if(computeKmedroidCost()>cost){
						//restoration
						v_med[p]=m;
						v_is_medroid[o]=false;
						v_is_medroid[m]=true;
						v_med_asso[o]=sauve_asso_o;
						v_med_asso[m]=p;
					}
					else {
						cost=computeKmedroidCost();
						test=true;
						continue;
					}
				}
				if(test) continue;
			}
		}
	}
	setGroups();
	setClusteringCost(computeKmedroidCost());
}

double Cluster::computeKmedroidCost() const{
	double cost=0;
	for (int i = 0; i < (int)inst->nb_request; ++i) {
		cost+=neigh->getVvDist()[i][v_med[v_med_asso[i]]];
	}
	return cost;
}

void Cluster::setGroups() {
	vv_groups.resize(v_med.size());
	for (int i = 0; i < (int)v_med.size(); ++i) {
		size_t m=v_med[i];
		vv_groups[i].clear();
		vv_groups[i].push_back(m);
		for (int j = 0; j < (int)v_med_asso.size(); ++j) {
			if(v_med_asso[j]==i && j!=m){
				vv_groups[i].push_back(j);
			}
		}
	}

}

bool Cluster::checkSolution(){
	for(int i=0; i< (int)inst->getNbRequest();++i){
		int times=0;
		for(auto c : vv_groups){
			for(auto id: c){
				if(id==i){
					times++;
				}
			}
		}
		if(times==1) break;
		else if(times>1) cout<<" node.id "<<i<<" is more than once in clusters!"<<endl;
		else cout<<" node.id "<<i<<" is not in any cluster"<<endl;
		abort();
	}
	cout<<"test solutoin ok"<<endl;
	return true;
}
std::ostream& operator <<(std::ostream& out, const Cluster& clust) {
	out<<"**************** Cluster ****************"<<std::endl;
	out<<"Cluster cost:" << clust.getClusteringCost() << std::endl;
	for (int i = 0; i < (int)clust.getVvGroups().size(); ++i) {
		out<<clust.getIdNodeInCluster(i,0);
		for (size_t j = 1; j < clust.getVvGroups()[i].size(); ++j) {
			out<<"\t"<<clust.getIdNodeInCluster(i,j);
		}
		out<< std::endl;
	}
	//	out<<"raw datas"<<std::endl;
	//	out<<"i\tis med\tasso\tf(asso)\tdist"<<std::endl;
	//	for (size_t i = 0; i < clust.getIsMedroid().size(); ++i) {
	//		out<<i<<"\t"<<clust.getIsMedroid()[i];
	//		out<<"\t"<<clust.getMedAsso()[i];
	//		out<<"\t"<<clust.getMed()[clust.getMedAsso()[i]];
	//		out<<"\t"<<clust.getDist(i,clust.getMed()[clust.getMedAsso()[i]]);
	//		out<<std::endl;
	//	}
	return out;
}

