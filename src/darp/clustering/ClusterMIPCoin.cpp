/*
 * ClusterMIPCoin.cpp
 *
 *  Created on: Feb 25, 2017
 *      Author: oscar
 */

#include "ClusterMIPCoin.h"

#include "flopc.hpp"
#include <OsiCbcSolverInterface.hpp>

using namespace flopc;

ClusterMIPCoin::~ClusterMIPCoin() {
	// TODO Auto-generated destructor stub
}


void ClusterMIPCoin::createClusters(int nbclust, int minNbR, int maxNbR){

	int nbreq = (int)inst->getNbRequest();


	// distance matrix, vector to array
	double distances[inst->getNbRequest()][inst->getNbRequest()];
	for (size_t i = 0; i < inst->getNbRequest(); ++i) {
		for (size_t j = 0; j < inst->getNbRequest(); ++j) {
			distances[i][j]=(double) neigh->getVvDist()[i][j];
		}
	}

	MP_set p {nbreq};
	MP_set k {nbreq};

	//	double distances[5][5]
	//	  { { 0,  1, 2.24, 3.61, 4.12},
	//		{ 1,  0, 1.41, 3.16, 4},
	//		{2.24, 1.41,  0, 4, 5.10},
	//		{3.61, 3.16, 4,  0, 1.41},
	//		{4.12, 4, 5.10, 1.41, 0}};
	//	MP_data Nummern(&distances);

	MP_data C(&distances[0][0],p,k); // transform the array into a vector

	MP_variable x(p,k),y(k);
	x.binary();  //assignement var= 1 if point p is assigned in cluster k
	y.binary(); //cluster var=1  if the cluster k is centered on location k

	MP_constraint unity(p), cluster(p,k), nclust, minR(k),maxR(k);

	unity(p) =   sum(k, x(p,k)) == 1;
	maxR(k) =   sum(p, x(p,k)) <= maxNbR*y(k);
	minR(k) =   sum(p, x(p,k)) >= minNbR*y(k);
	cluster(p,k)= x(p,k) <= y(k);
	nclust= sum(k, y(k)) == nbclust;

	MP_model m1(new OsiCbcSolverInterface);
	m1.add(unity);
	m1.add(cluster);
	m1.add(nclust);

	MP_expression F;
	F=sum(p*k , C(p,k)*x(p,k));

	m1.minimize(F);
	setClusteringCost(m1.getCurrentModel()->operator ->()->getObjValue());
	cout<<clusteringCost<<endl;
	//y.display();
	//cout<<"Assignment"<<endl;
	vv_groups.clear();
	vv_groups.resize(nbclust);
	int c=0;
	bool newgroup=false;
	for (int j = 0; j < nbreq; ++j) {
		newgroup=false;
		for (int i = 0; i < nbreq; ++i) {
			if (x.level(i,j)==1) {
				int id=(int)inst->pickup[i]->getId();
				vv_groups[c].push_back(id);
				newgroup=true;
			}
		}
		if (newgroup)c++;
	}
	checkSolution();
}
