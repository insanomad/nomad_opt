/*
 * NeighbourMinRoute.cpp
 *
 *  Created on: Feb 25, 2017
 *      Author: oscar
 */

#include "NeighbourMinRoute.h"
#include "../../toolbox/TimeEvaluation.h"

NeighbourMinRoute::NeighbourMinRoute(Instance* ins): Neighborhood(ins) {}

NeighbourMinRoute::~NeighbourMinRoute() {
	// TODO Auto-generated destructor stub
}

long NeighbourMinRoute::distanceFunction(size_t a, size_t b) {
	NodeSol p1(inst->pickup[a]);
	NodeSol d1(inst->delivery[a]);
	NodeSol p2(inst->pickup[b]);
	NodeSol d2(inst->delivery[b]);
	p1.posSym=&d1.position;
	p2.posSym=&d2.position;
	d1.posSym=&p1.position;
	d2.posSym=&p1.position;

	TimeEvaluation te(inst);
	std::vector<NodeSol*> route;
	route.push_back(&p2);
	route.push_back(&d2);
	te.setNodes(route);

	long minCost=100000000;
	int dim=4;
	for(int i=0; i< dim-1;++i){
		for(int j=i+1; j < dim;++j){
			route.insert(route.begin()+i,&p1);
			route.insert(route.begin()+j,&d1);
			te.setNodes(route);
			//printRoute(route);
				if(te.performScheduling(route))
				{
					long cost=te.computeCostEstimation();
					if(cost<minCost)
					{
						minCost=cost;
					}
				}
			route.erase(route.begin()+j);
			route.erase(route.begin()+i);
		}
	}
	//std::cout<<minCost<<std::endl;
	return minCost;
}


