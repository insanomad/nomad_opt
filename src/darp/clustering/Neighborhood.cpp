/*
 * Neighborhood.cpp
 *
 *  Created on: Feb 14, 2016
 *      Author: sam
 */

#include <iostream>
#include "Neighborhood.h"
#include "../structure/NodeSol.h"
#include "../structure/Tour.h"
#include "../structure/RequestInfo.h"
#include <limits>

Neighborhood::Neighborhood(Instance * _inst) {
	inst= _inst;
}

Neighborhood::~Neighborhood() {
}

void Neighborhood::init(){
	computeRequestDistances();
	//setTriangularInequality();
	createNeighborMatrix();
	//display();
}

void Neighborhood::computeRequestDistances() {
	vv_distance.resize(inst->getNbRequest());
	for (size_t i = 0; i < inst->nb_request; ++i) {
		vv_distance[i].resize(inst->nb_request);
		for (size_t j = i+1; j < inst->nb_request; ++j) {
			vv_distance[i][j]=distanceFunction(i,j);
		}
	}
	for (size_t i = 0; i < inst->getNbRequest(); ++i) {
		for (size_t j = 0; j < i; ++j) {
			vv_distance[i][j]=vv_distance[j][i];
		}
	}
	for (size_t j = 0; j < inst->getNbRequest(); ++j) {
		vv_distance[j][j]=0;
	}
}



void Neighborhood::createNeighborMatrix() {
	std::vector<bool> visited;
	vv_neighborhood.resize(inst->nb_request);
	size_t min_id;
	double min_dist;
	for (size_t i = 0; i < inst->getNbRequest(); ++i) {
		vv_neighborhood[i].resize(inst->getNbRequest());
		visited.clear();
		visited.resize(inst->getNbRequest(),false);
		for (size_t j = 0; j < inst->getNbRequest(); ++j) {
			min_dist=std::numeric_limits<double>::max();
			for (size_t k = 0; k < inst->getNbRequest(); ++k) {
				if(vv_distance[i][k]<min_dist && !visited[k]){
					min_dist=(double)vv_distance[i][k];
					min_id=k;
				}
			}
			visited[min_id]=true;
			vv_neighborhood[i][j] = min_id;
		}
	}
}

void Neighborhood::setTriangularInequality() {
	for (size_t k = 0; k < inst->nb_request; ++k) {
		for (size_t i = 0; i < inst->nb_request; ++i) {
			for (size_t j = 0; j < inst->nb_request; ++j) {
				if(vv_distance[i][j]>vv_distance[i][k]+vv_distance[k][j]){
					vv_distance[i][j]=vv_distance[i][k]+vv_distance[k][j];
				}
			}
		}
	}
}

void Neighborhood::display() {
	//distance
	std::cout<<"*********DISTANCE*********"<<std::endl;
	for (size_t i = 0; i < inst->nb_request; ++i){
		std::cout<<"\t"<<i;
	}
	std::cout<<std::endl;
	for (size_t i = 0; i < inst->nb_request; ++i) {
		std::cout<<i;
		for (size_t j = 0; j < inst->nb_request; ++j) {
			std::cout<<"\t"<<vv_distance[i][j];
		}
		std::cout<<std::endl;
	}
	//neighborhood
	std::cout<<"*********NEIGHBORHOOD*********"<<std::endl;
	for (size_t i = 0; i < inst->nb_request; ++i){
		std::cout<<"\t"<<i;
	}
	std::cout<<std::endl;
	for (size_t i = 0; i < inst->nb_request; ++i) {
		std::cout<<i;
		for (size_t j = 0; j < inst->nb_request; ++j) {
			std::cout<<"\t"<<vv_neighborhood[i][j];
		}
		std::cout<<std::endl;
	}
}
void Neighborhood::printRoute(std::vector<NodeSol*>& route){
	for(auto& n: route) std::cout<<"\t"<<n->node->id;
	std::cout<<std::endl;

}
