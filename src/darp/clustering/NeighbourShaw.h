/*
 * NeighbourShaw.h
 *
 *  Created on: Feb 25, 2017
 *      Author: oscar
 */

#ifndef SRC_DARP_INSTANCEMANAGER_NEIGHBOURSHAW_H_
#define SRC_DARP_INSTANCEMANAGER_NEIGHBOURSHAW_H_
#include "Neighborhood.h"
class NeighbourShaw : public Neighborhood {
public:
	NeighbourShaw(Instance* ins);
	virtual ~NeighbourShaw();
	long distanceFunction(size_t a, size_t b);
};

#endif /* SRC_DARP_INSTANCEMANAGER_NEIGHBOURSHAW_H_ */
