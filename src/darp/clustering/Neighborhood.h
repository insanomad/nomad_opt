/*
 * Neighborhood.h
 *
 *  Created on: Feb 14, 2016
 *      Author: sam
 */

#ifndef DARP_INSTANCEMANAGER_NEIGHBORHOOD_H_
#define DARP_INSTANCEMANAGER_NEIGHBORHOOD_H_

#include "../instanceManager/Instance.h"
#include "vector"
#include "../structure/NodeSol.h"

class Neighborhood {
public:
	Neighborhood(Instance* _inst);
	virtual ~Neighborhood();


	/**
	 * distance between the point a and the point b
	 * Based on the possibility to create a tour with both points and return the optimized cost associated to this association
	 * @param a		id of dial a
	 * @param b		id of dial b
	 * @return distance
	 */
	virtual long distanceFunction(size_t a, size_t b)=0;


	const std::vector<std::vector<long> >& getVvDist() const {
		return vv_distance;
	}

	const std::vector<std::vector<size_t> >& getNeighborhood() const {
		return vv_neighborhood;
	}

	/**
	 * display distance and neighborhood
	 */
	void display();
	/**
	 * print route id's
	 */
	void printRoute(std::vector<NodeSol*>& route);
	/**
	 * Initialized the neightbour matrix and compute distances amomng request
	 */
	void init();

protected:
	/**
	 * matrix of distance
	 */
	void computeRequestDistances();

	/**
	 * matrix of neighborhood
	 */
	void createNeighborMatrix();

	/**
	 * triangular inequality
	 */
	void setTriangularInequality();

	/**
	 * *********************************************
	 */

	/**
	 * matrix of distance
	 */
	std::vector< std::vector< long > > vv_distance;
	//double distances[][];


	/**
	 * matrix of neighborhood
	 */
	std::vector< std::vector< size_t > > vv_neighborhood;

	/**
	 * penality when the association in a tour is not feasible
	 */
	double penality=10000;

	/**
	 * instance
	 */
	Instance *inst;

};

#endif /* DARP_INSTANCEMANAGER_NEIGHBORHOOD_H_ */
