/*
 * ClusterEstablishment.cpp
 *
 *  Created on: Feb 25, 2017
 *      Author: oscar
 */

#include "ClusterEstablishment.h"

ClusterEstablishment::~ClusterEstablishment() {
	// TODO Auto-generated destructor stub
}

void ClusterEstablishment::createClusters(int nbclust, int minNbR, int maxNbR){
	vv_groups.clear();
	size_t c=0;
	vv_groups.resize(c+1);
	size_t id=inst->pickup[0]->getId();
	vv_groups[c].push_back(id);

	for (size_t j = 1; j < inst->getPickup().size(); ++j) {
		if (inst->getDelivery()[j]->name == inst->getDelivery()[j-1]->name) {
				size_t id=inst->pickup[j]->getId();
				vv_groups[c].push_back(id);
		}else{
			c++;
			vv_groups.resize(c+1);
			size_t id=inst->pickup[j]->getId();
			vv_groups[c].push_back(id);
		}
	}
	checkSolution();

}
