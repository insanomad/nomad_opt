/*
 * ClusterMIPCoin.h
 *
 *  Created on: Feb 25, 2017
 *      Author: oscar
 */

#ifndef SRC_DARP_INSTANCEMANAGER_CLUSTERMIPCOIN_H_
#define SRC_DARP_INSTANCEMANAGER_CLUSTERMIPCOIN_H_
#include "Cluster.h"
class ClusterMIPCoin : public Cluster {
public:

	ClusterMIPCoin(Instance* ins, Neighborhood* neigh) : Cluster(ins,neigh){}

	virtual ~ClusterMIPCoin();

	void createClusters(int nbclust, int minNbR, int maxNbR);

};

#endif /* SRC_DARP_INSTANCEMANAGER_CLUSTERMIPCOIN_H_ */
