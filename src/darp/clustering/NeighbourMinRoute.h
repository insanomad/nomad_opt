/*
 * NeighbourMinRoute.h
 *
 *  Created on: Feb 25, 2017
 *      Author: oscar
 */

#ifndef SRC_DARP_INSTANCEMANAGER_NEIGHBOURMINROUTE_H_
#define SRC_DARP_INSTANCEMANAGER_NEIGHBOURMINROUTE_H_
#include "Neighborhood.h"
class NeighbourMinRoute : public Neighborhood {
public:
	NeighbourMinRoute(Instance* ins);
	virtual ~NeighbourMinRoute();
	long distanceFunction(size_t a, size_t b);
};

#endif /* SRC_DARP_INSTANCEMANAGER_NEIGHBOURMINROUTE_H_ */
