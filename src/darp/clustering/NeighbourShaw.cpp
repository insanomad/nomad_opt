/*
 * NeighbourShaw.cpp
 *
 *  Created on: Feb 25, 2017
 *      Author: oscar
 */

#include "NeighbourShaw.h"

NeighbourShaw::NeighbourShaw(Instance* ins): Neighborhood(ins) {}

NeighbourShaw::~NeighbourShaw() {
	// TODO Auto-generated destructor stub
}

long NeighbourShaw::distanceFunction(size_t i, size_t j){
	int i_sym=(int)inst->dataRequests[i]->idsym;
	int j_sym=(int)inst->dataRequests[j]->idsym;
	long dist =(inst->distance_ij[i][j]+inst->distance_ij[i_sym][j]+
			inst->distance_ij[i][j_sym]+inst->distance_ij[i_sym][j_sym]);
	return dist;
}
