/*
 * ClusterMIPCplex.cpp
 *
 *  Created on: Feb 25, 2017
 *      Author: oscar
 */

#include "ClusterMIPCplex.h"
#include <ilcplex/ilocplex.h>

typedef IloArray<IloNumArray> NumMatrix;
typedef IloArray<IloBoolVarArray> boolVarMatrix;

ClusterMIPCplex::~ClusterMIPCplex() {
	// TODO Auto-generated destructor stub
}

void ClusterMIPCplex::createClusters(int nbclust, int minNbR, int maxNbR){
	bool isFeasible=false;
	bool isOptimal=false;
	int nbr = (int)inst->getNbRequest();

	IloEnv env;
	try {

		IloInt minR=minNbR;
		IloInt maxR=maxNbR;

		IloInt  one=1;
		IloInt  nbReq=nbr;
		IloInt  nbc=nbclust;
		NumMatrix c(env,nbReq);  //cost
		boolVarMatrix x(env,nbReq);  // decision variables
		IloBoolVarArray y(env, nbReq);  // decision variables
		IloModel model(env); // creating the modeling object

		//init variable x and costs c and objective
		IloExpr obj(env);
		for(int i=0; i<nbReq; i++){
			x[i]=IloBoolVarArray(env,nbReq);
			c[i]=IloNumArray(env,nbReq);
			for(int j=0; j<nbReq; j++) {
				x[i][j] = IloBoolVar(env);
				c[i][j] = neigh->getVvDist()[i][j];
				obj+=c[i][j]*x[i][j];
			}
		}

		// constraint: number of clusters
		IloExpr v(env);
		for(int i=0;i<nbReq;++i) {
			v += y[i];
		}
		model.add(v == nbc);
		v.end();

		// constraint: one request belong to one single cluster
		for(int i=0;i<nbReq;++i) {
			IloExpr v(env);
			for(int j=0;j<nbReq;++j) {
				v += x[i][j];
			}
			model.add(v == one);
			v.end();
		}

		// constraint: nb requests per cluster
		for(int j=0;j<nbReq;++j) {
			IloExpr v(env);
			for(int i=0;i<nbReq;++i) {
				v += x[i][j];
			}
			model.add(minR*y[j] <= v );
			model.add(v <= maxR*y[j] );
			v.end();
		}


		//constraints: x and y
		for(int i=0;i<nbReq;++i) {
			for(int j=0;j<nbReq;++j) {
				model.add( x[i][j] <= y[j]);
			}
		}

		//Objective function
		model.add(IloMinimize(env, obj));
		obj.end();

		IloCplex cplex(env);
		cplex.clear();
		cplex.extract(model);
		//cplex.setOut(env.getNullStream()); // on/off output
		//cplex.setWarning(env.getNullStream()); // on/off warning streams

		cplex.setParam(IloCplex::MIPEmphasis, IloCplex::MIPEmphasisOptimality);
		//cplex.setParam(IloCplex::TiLim, timeLimit);
		//cplex.setParam(IloCplex::Threads, 1);

		isFeasible=cplex.solve();
		//cplex.exportModel("cluster.lp");   // writes the model into a text file
		//cplex.writeSolution("cl_out");    // writes the solution in html format

		isOptimal= (cplex.getStatus()==IloAlgorithm::Status::Optimal);

		if (cplex.getStatus() == IloAlgorithm::Infeasible)
			env.out() << "Unfeasible solution!!!!" << std::endl;

		if(isFeasible){
			vv_groups.clear();
			vv_groups.resize(nbclust);
			int c=0;
			bool newgroup=false;
			for (int j = 0; j < nbr; ++j) {
				newgroup=false;
				for (int i = 0; i < nbr; ++i) {
					if (abs(cplex.getValue(x[i][j])-1.0)<0.01) {
						int id=(int)inst->pickup[i]->getId();
						vv_groups[c].push_back(id);
						newgroup=true;
					}
				}
				if (newgroup)c++;
			}
			checkSolution();
		}else{
			abort();
		}


	}catch(IloException& e) {
		std::cerr << " ERROR: " << e << std::endl;
	}
	catch(...) {
		std::cerr << " ERROR" << std::endl;
	}
//	std::cout<<std::endl;
	env.end();
}
