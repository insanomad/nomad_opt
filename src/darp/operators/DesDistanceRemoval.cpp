#include "DesDistanceRemoval.h"

#include <stdlib.h>
#include <time.h>
#include <vector>
#include <cmath>

#include <algorithm>

//#include "ALNS_inc.h"


using namespace std;

DistanceRemoval::DistanceRemoval(std::string s, double mini_percent, double maxi_percent, Neighborhood * _neigh): ADestroyOperator(s,mini_percent,maxi_percent) {
	neigh=_neigh;
	p=6;
}

DistanceRemoval::~DistanceRemoval() {}

void DistanceRemoval::destroySolution(ISolution& sol) {
	darpsol = &dynamic_cast<DARPSolution&>(sol);
	int nbRequest= (int)(darpsol->getRequestInSolution().size()-darpsol->getNonInserted().size());
	int nb_destroy=getNbRemoval(nbRequest);

	size_t centerRequest=0;
	if (nbRequest!=0)
		centerRequest=darpsol->getRequestInSolution()[rand()%nbRequest];

	vector<size_t> request(neigh->getNeighborhood()[centerRequest]);
	vector<size_t> requestToRemove;
	if(cst::Clustering==cst::single) // b/o there is an extra condition for clustering
	{
		// roulette wheel
		for (int i = 0; i < nb_destroy; ++i) {
			size_t id= size_t(std::pow((double(rand()))/double(RAND_MAX),p)*double(request.size()));
			requestToRemove.push_back(request[id]);
			request.erase(request.begin()+id);
		}
	}

	if(cst::clusterRemoval)
		darpsol->removeSetOfRequestById(requestToRemove);  // it does the same as the previous for however is slower! (no reasonable explanation)
	else{
		for (auto& it : requestToRemove) {
			darpsol->removeRequestById(it);
		}
	}




}

