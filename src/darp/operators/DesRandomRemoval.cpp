
#include "DesRandomRemoval.h"

#include "../../constant.h"
#include <stdlib.h>
#include <time.h>
#include <vector>
//#include <cstdlib>
//#include <cmath>
//#include <math.h>
#include <algorithm>


//#include "ALNS_inc.h"


using namespace std;

RandomRemoval::RandomRemoval(std::string s, double mini_frac, double maxi_frac )
								:ADestroyOperator(s,mini_frac,maxi_frac) {
}

RandomRemoval::~RandomRemoval() {
}

void RandomRemoval::destroySolution(ISolution& sol) {
	darpsol = &dynamic_cast<DARPSolution&>(sol);
	int nbRequest=(int)darpsol->getRequestInSolution().size()-(int)darpsol->getNonInserted().size();
	int mini= (int)max(1.0,nbRequest*this->minimunDestroyFrac);
	int maxi= (int)max(1.0,nbRequest*this->maximumDestroyFrac);
	auto nbRequestToDestroy=rand()%(maxi - mini+1) + mini;

	vector<size_t> requestToRemove = darpsol->getRequestInSolution();
	random_shuffle(requestToRemove.begin(), requestToRemove.end());
	requestToRemove.resize(nbRequestToDestroy);
	if(cst::clusterRemoval)
		darpsol->removeSetOfRequestById(requestToRemove);  // it does the same as the previous for however is slower! (no reasonable explanation)
	else{
		for (int p = 0; p < nbRequestToDestroy; ++p) {
				darpsol->removeRequestById(requestToRemove[p]);
		}
	}
}
