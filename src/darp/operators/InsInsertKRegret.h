
/* ALNS_Framework - a framework to develop ALNS based solvers
 *
 * Copyright (C) 2012 Renaud Masson
 *
 * This library is free software; you can redistribute it and/or
 * modify it either under the terms of the GNU Lesser General Public
 * License version 3 as published by the Free Software Foundation
 * (the "LGPL"). If you do not alter this notice, a recipient may use
 * your version of this file under the LGPL.
 *
 * You should have received a copy of the LGPL along with this library
 * in the file COPYING-LGPL-3; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Suite 500, Boston, MA 02110-1335, USA
 *
 * This software is distributed on an "AS IS" basis, WITHOUT WARRANTY
 * OF ANY KIND, either express or implied. See the LGPL for
 * the specific language governing rights and limitations.
 *
 * The Original Code is the ALNS_Framework library.
 *
 *
 * Contributor(s):
 *	Renaud Masson
 */

#ifndef INSERT_KREGRET_H_
#define INSERT_KREGRET_H_

#include "../checker/Checker.h"
#include "../solutionManager/DARPSolution.h"
#include "../instanceManager/Instance.h"
#include "../../alns/ARepairOperator.h"
#include "MatrixCostInsertions.h"
#include <vector>

/*
 * Parametric best insertion
 */
class Insert_KRegret: public ARepairOperator {
public:

	Insert_KRegret(std::string s, int kregret, Instance * ins, bool mnoise);
	virtual ~Insert_KRegret();

	void repairSolution(ISolution& sol);
	void updateMatrixTourInfo(size_t tourid);

	RequestInfo* getMaxRegret(); //never change to size_t!
	RequestInfo* getMinCost(); //never change to size_t!

	RequestInfo* Kregret(int k_reg);

	void createRequestForNewTour(size_t tourid);

	bool insertFeasibleRequest(RequestInfo& req);
    void evaluateInsertionRequest(size_t posreq);
    void computeInsertionMatrix();

    int getKregret() const { return kregret;}
	void setKregret(int kregret) { this->kregret = kregret;}

private:
	int kregret;
    DARPSolution* darpSol;
    Tour * new_tour;
    MatrixCostInsertions* insMatrix;
    std::vector< std::pair<long, RequestInfo*> > regret;
};

#endif /* INSERT_KREGRET_H_ */
