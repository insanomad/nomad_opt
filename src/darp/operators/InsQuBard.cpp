
#include "InsQuBard.h"

#include "../structure/Tour.h"

//#include "ALNS_inc.h"

#include <time.h>
#include <iostream>
#include <algorithm>    // std::random_shuffle
#include <cstdlib>      // std::rand, std::srand
#include <ctime>        // std::time
#include "../instanceManager/readSolQuBard.h"

using namespace std;

Insert_QuAndBard::Insert_QuAndBard(std::string s): ARepairOperator(s){
	name=s;
	darpSol=nullptr;
}

Insert_QuAndBard::~Insert_QuAndBard() {

}

void Insert_QuAndBard::repairSolution(ISolution& sol){
	darpSol = &dynamic_cast<DARPSolution&>(sol);
	SolutionQuAndBard SC(name,darpSol->getInstance()->nb_request);
	Checker Check(darpSol);
	/* for every tour*/
	for (size_t i = 0; i < SC.tours.size(); ++i) {
		Tour* round=darpSol->getTours()[SC.tours[i][0]];
		/* for every node in the tour*/
		for (size_t j = 1; j < SC.tours[i].size()-1; ++j) {
			NodeSol* node=darpSol->getVrevers()[SC.tours[i][j]];
			node->id_round=&round->id;
			round->v_node.insert(round->v_node.begin()+j,node);
			if (node->node->type==0) {
				darpSol->set_as_inserted(node);
			}
			round->v_node[j]->posSym=&darpSol->getVrevers()[round->v_node[j]->node->idsym]->position;
		}
		/*Scheduling nodes tour by tour*/
		if(!round->updateFeasibleTourParameters(round->getVehicle())){ //remise en question du vehicule
			cout<<"Creatin newRoute of Qu AND Bard!"<<__FILE__<<__LINE__<<endl;
			abort();
		}
	}
	darpSol->recomputeCost();
	double time=0;
	long distance=0;
	double time2=0;
	double fixedCost=0;
	double penalty=0;
	double cost=0;
	for(auto& t : darpSol->getTours()){
		if(t->v_node.size()>2){
			time2+=t->getDuration();
			distance+=t->getDistance();
			fixedCost+=t->getVehicle()->getFixedCost();
			penalty+=t->getSumRidetimes();
			cost+=t->getCost();
			//cout<<t->getCost()<<" "<<t->getDuration()<<" "<<t->getSumRidetimes()<<" "<<t->getVehicle()->getFixedCost()<<endl;
		}
	}
	//cout<<darpSol->getPenalizedObjectiveValue()<<" ct "<<cost<<std::endl;
	time=darpSol->getInstance()->S2Dd(distance)/0.5;
	cout<<" DrivT:"<< time<<" tp:"<<darpSol->getInstance()->S2Dd(penalty)<<" dist:"<<darpSol->getInstance()->S2Dd(distance)<<endl;
	cout<< time*0.377<<" "<<darpSol->getInstance()->S2Dd(penalty)*0.0001<<" "<<fixedCost<<" "<<time*0.377+darpSol->getInstance()->S2Dd(penalty)*0.0001+fixedCost<<endl;
	//std::cout<<darpSol->getInstance()->S2Dd(darpSol->getPenalizedObjectiveValue())<<" ct "<<darpSol->getInstance()->S2Dd(cost)<<std::endl;
}
