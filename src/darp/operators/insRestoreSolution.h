
/* ALNS_Framework - a framework to develop ALNS based solvers
 *
 * Copyright (C) 2012 Renaud Masson
 *
 * This library is free software; you can redistribute it and/or
 * modify it either under the terms of the GNU Lesser General Public
 * License version 3 as published by the Free Software Foundation
 * (the "LGPL"). If you do not alter this notice, a recipient may use
 * your version of this file under the LGPL.
 *
 * You should have received a copy of the LGPL along with this library
 * in the file COPYING-LGPL-3; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Suite 500, Boston, MA 02110-1335, USA
 *
 * This software is distributed on an "AS IS" basis, WITHOUT WARRANTY
 * OF ANY KIND, either express or implied. See the LGPL for
 * the specific language governing rights and limitations.
 *
 * The Original Code is the ALNS_Framework library.
 *
 *
 * Contributor(s):
 *	Renaud Masson
 */

#ifndef Restore_Solution_H_
#define Restore_Solution_H_

//#include "ALNS_inc.h"
#include "../../alns/RouteForSetCovering.h"
#include "../structure/Tour.h"
#include "../../alns/ARepairOperator.h"
#include "../solutionManager/DARPSolution.h"
#include <vector>

using namespace std;


class Restore_Solution: public ARepairOperator {

public:
	Restore_Solution(std::string s);
	virtual ~Restore_Solution();
	void repairSolution(ISolution& sol){};  // empty method
	void rebuiltSolution(ISolution& sol, const vector<RouteForSetCovering*>& tours);
	Tour* makeRoute(DARPSolution& sol, const RouteForSetCovering& tour);

private:
	string name;
    DARPSolution* darpSol=nullptr;
    Tour * new_tour=nullptr;
};

#endif /* Restore_Solution_H_ */
