
/* ALNS_Framework - a framework to develop ALNS based solvers
 *
 * Copyright (C) 2012 Renaud Masson
 *
 * This library is free software; you can redistribute it and/or
 * modify it either under the terms of the GNU Lesser General Public
 * License version 3 as published by the Free Software Foundation
 * (the "LGPL"). If you do not alter this notice, a recipient may use
 * your version of this file under the LGPL.
 *
 * You should have received a copy of the LGPL along with this library
 * in the file COPYING-LGPL-3; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Suite 500, Boston, MA 02110-1335, USA
 *
 * This software is distributed on an "AS IS" basis, WITHOUT WARRANTY
 * OF ANY KIND, either express or implied. See the LGPL for
 * the specific language governing rights and limitations.
 *
 * The Original Code is the ALNS_Framework library.
 *
 *
 * Contributor(s):
 *	Renaud Masson
 */

#ifndef DISTANCE_REMOVAL2_H_
#define DISTANCE_REMOVAL2_H_

//#include "ALNS_inc.h"
#include "../solutionManager/DARPSolution.h"
#include "../structure/Node.h"
#include "../../alns/ADestroyOperator.h"
//#include "MatrixCostInsertions2.h"
#include "../clustering/Neighborhood.h"


/*
 * Sort of cluster removal
 */

class DistanceRemoval: public ADestroyOperator {
public:
	DistanceRemoval(std::string,  double mini_percent, double maxi_percent, Neighborhood * _neigh );
	virtual ~DistanceRemoval();
	void destroySolution(ISolution& sol);

    DARPSolution * darpsol=nullptr;
    size_t sizev=0;
    size_t getsize(){ return sizev;}

    Neighborhood * neigh;
    double p;

};

#endif /* DISTANCE_REMOVAL_H_ */
