
#include <iostream>
#include <algorithm>    // std::random_shuffle and sort
#include <cstdlib>      // std::rand, std::srand
#include <ctime>        // std::time
#include <climits>
#include <cmath>


#include "../structure/Tour.h"
#include "../structure/Node.h"
#include "../structure/RequestInfo.h"
#include "InsInsertKRegret.h"


using namespace std;

Insert_KRegret::Insert_KRegret(std::string s, int kregret, Instance *ins, bool mnoise): ARepairOperator(s){
	setKregret(kregret);
}

Insert_KRegret::~Insert_KRegret() {
}

/**
 * rebuild the solution by adding couple of node one by one
 * @param sol solution a modifier
 */
void Insert_KRegret::repairSolution(ISolution& sol){
	darpSol = &dynamic_cast<DARPSolution&>(sol);
	insMatrix= new MatrixCostInsertions();
	computeInsertionMatrix();

	RequestInfo* request=nullptr;
	size_t nonInsted=0, tourid=0;
	while (nonInsted<darpSol->getNonInserted().size())
	{
		request=Kregret(kregret);
		if (regret.size()>0){
			tourid=request->tourid;  	//vital
			insertFeasibleRequest(*request);
			insMatrix->setRequestAsInserted(request->p->id);
			updateMatrixTourInfo(tourid);
		}else{
			nonInsted++;
		}
	}
	//Checker check(darpSol);
	//check.solCheck();
	//cout<<"check "<<kregret<<"-regret, # Tours "<< darpSol->vp_tours.size()<<endl;
	delete insMatrix;
}

void Insert_KRegret::updateMatrixTourInfo(size_t tourid){
	if (insMatrix->icost.size() > 0 ){
		int tourpos=insMatrix->findTourPosition(tourid); // todo once everything ok, try removing this line
		if ( tourpos >= 0 ){
			Tour* tour=darpSol->getTours()[tourid];
			for (size_t r = 0; r < insMatrix->icost.size(); ++r){
				if (insMatrix->icost[r][tourpos]->isfeasible){	// assumption: adding a new requests wont make a previous infeasible insertion likely to be feasible after.
					insMatrix->reinitInforRequest(*insMatrix->icost[r][tourpos]);
					tour->evalBestInsertion(*insMatrix->icost[r][tourpos]);
				}
			}
		}else {
			cout<<"tour does not exist!"<<endl;
		}
	}
}

RequestInfo* Insert_KRegret::getMinCost(){
	long minCostRegret=LONG_MAX;
	RequestInfo* minRequest=nullptr;
	for (auto& req: regret) {
		if (req.first < minCostRegret) {
			minCostRegret=req.first;
			minRequest=req.second;
		}
	}
	return minRequest;
}

RequestInfo* Insert_KRegret::getMaxRegret(){
	long maxCostRegret=-1;
	RequestInfo* maxRequest=nullptr;
	for (size_t i = 0; i < regret.size(); ++i) {
		if (regret[i].first> maxCostRegret) {
			maxRequest=regret[i].second;
			maxCostRegret=regret[i].first;
		}else if (regret[i].first == maxCostRegret) { // breaking ties
			double noise_percentage=0.95+(rand()%10)/100.0;// noise [+-5%]
			regret[i].second->deltacost*=noise_percentage;
			if (regret[i].second->deltacost < regret[i-1].second->deltacost) {
				maxRequest=regret[i].second;
				maxCostRegret=regret[i].first;
			}
		}
	}
	return maxRequest;
}

RequestInfo* Insert_KRegret::Kregret(int k_reg){
	regret.clear();
	double infinit=1000000000;
	RequestInfo* request;
	// for every request
	for (int r = 0; r < (int)insMatrix->icost.size(); ++r)
	{
		std::vector<int> bestReq(k_reg,-1);
		std::vector<int> bestTour(k_reg,-1);
		std::vector<double> bestCost(k_reg,infinit);
		// For every route
		for (int t = 0; t < (int)insMatrix->icost[r].size(); ++t)
		{
			request =insMatrix->icost[r][t];
			bool notRanked=true;

			for (int k= 0; k< k_reg; ++k)
			{
				if (request->deltacost < bestCost[k] && request->isfeasible && notRanked)
				{
					for (int w = k_reg-1; w > k; --w)
					{
						bestReq[w]=bestReq[w-1];
						bestTour[w]=bestTour[w-1];
						bestCost[w]=bestCost[w-1];
					}
					bestReq[k]=r;
					bestTour[k]=t;
					bestCost[k]=request->deltacost;
					notRanked=false;
				}
			}
		}
		if(bestCost[0] < infinit-1 ){ // there is a feasible insertion
			double sum_difference=0;
			assert2(insMatrix->icost[bestReq[0]][bestTour[0]]->isfeasible);
			if (k_reg==1) {
				sum_difference+=bestCost[0];
			} else {
				for(int k=k_reg-1; k>=0;--k){
					sum_difference+=bestCost[k]-bestCost[0];
				}
			}
			regret.push_back(std::make_pair(sum_difference,insMatrix->icost[bestReq[0]][bestTour[0]]));
		}
	}
	if(k_reg==1)  request=getMinCost();
	else request=getMaxRegret();
	return request;
}


/**
 * Inserts a  FEASIBLE request in the current solution
 * req  holds the information required to perform the insertion.
 */
bool Insert_KRegret::insertFeasibleRequest(RequestInfo& req){
	if(!darpSol->tours[req.tourid]->insertFeasibleRequest(req)){
		cout<<"Inserting a feasible req is not feasible! "<<__FILE__<<__LINE__<<endl;
		cout<<req<<endl;
		abort();
	}
	darpSol->recomputeCost();
	darpSol->set_as_inserted(req.p);
	return true;
}

/**
 * This method is executed when a request insertion
 * results more cost effective in a new tour. In this case
 * the insertion matrix will be updated to include the calculation
 * of non inserted request.
 */
void Insert_KRegret::createRequestForNewTour(size_t tourid){
	NodeSol *p,*d;
	Tour* t=darpSol->getTours()[tourid];
	int postour=insMatrix->createNewTourInfo(tourid);
	for (size_t i = 0; i < darpSol->getNonInserted().size(); ++i) {
		p=darpSol->getVrevers()[darpSol->getNonInserted()[i]];
		d=darpSol->getVrevers()[p->node->idsym];
		if(p->id > d->id) swap(p,d);
		int posreq=insMatrix->findRequestPosition(p->id);
		t->evalBestInsertion(*insMatrix->icost[posreq][postour]);
	}
	t=nullptr;
}
void Insert_KRegret::computeInsertionMatrix() {
	NodeSol *p, *d;
	insMatrix->icost.resize(darpSol->getNonInserted().size());
	for (size_t r = 0; r < darpSol->getNonInserted().size(); ++r){
		p=darpSol->getVrevers()[darpSol->getNonInserted()[r]];
		d=darpSol->getVrevers()[p->node->idsym];
		if(p->id > d->id) swap(p,d);
		size_t ntours=darpSol->getNbTours();
		insMatrix->icost[r].resize(ntours); // +1 b/o last item will be the cost of new tour
		for (size_t i = 0; i < insMatrix->icost[r].size(); ++i) {
			insMatrix->icost[r][i]= new RequestInfo(p,d);
		}
		evaluateInsertionRequest(r);
	}
}

void Insert_KRegret::evaluateInsertionRequest(size_t posreq) {

	double hash=0;
	for (size_t t=0; t<darpSol->getNbTours(); ++t){  // calculate minimum insertion cost per tour
		RequestInfo* request=insMatrix->icost[posreq][t];
		request->tourid=t;
		auto new_hash=darpSol->tours[t]->getHashkey();
		if(hash!=new_hash){  // if it is not another empty route with same departure and arrival nodes
			hash=new_hash;
			darpSol->tours[t]->evalBestInsertion(*request);
		}else{
			auto vehicle=request->vehicle;
			*request=*insMatrix->icost[posreq][t-1];   // never delete * b/o is a deep copy
			request->tourid=t; //key
			request->isfeasible=false;
			if(cst::QuAndBard && vehicle!=nullptr) request->vehicle=vehicle;
		}
	}
}
