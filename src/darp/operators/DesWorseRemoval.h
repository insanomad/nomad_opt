/*
 * DestroyWorseRemoval.h
 *
 *  Created on: Jan 17, 2016
 *      Author: oscar
 */

#ifndef SRC_DARP_DESTROY_MWORSEREMOVAL_H_
#define SRC_DARP_DESTROY_MWORSEREMOVAL_H_

#include "../structure/RequestInfo.h"
#include "../solutionManager/DARPSolution.h"
#include "../checker/Checker.h"
#include "../../alns/ADestroyOperator.h"

#include <vector>
#include <iostream>

class WorseRemoval: public ADestroyOperator{

public:
	WorseRemoval(std::string s, double minprobability, double maxprobability );
	virtual ~WorseRemoval();
	void destroySolution(ISolution& sol);

	DARPSolution* darpsol;
	std::string name;
	double p=1;

	void generateWorseInsertionList(std::vector<RequestInfo>& list);
	void getWorseReqInTour(Tour* tour, RequestInfo& req, std::vector<bool>& taboo);
};



#endif /* SRC_DARP_DESTROY_MWORSEREMOVAL_H_ */
