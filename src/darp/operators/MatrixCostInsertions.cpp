/*
 * MatrixCostInsertions.cpp
 *
 *  Created on: Oct 28, 2015
 *      Author: oscarts
 */
#include "MatrixCostInsertions.h"
#include <vector>
#include <algorithm>
#include <limits>


MatrixCostInsertions::MatrixCostInsertions() {
}

MatrixCostInsertions::~MatrixCostInsertions() {
	for (size_t i = 0; i < icost.size(); ++i) {
		for (size_t j = 0; j < icost[i].size(); ++j) {
			delete icost[i][j];
		}
	}
}

int MatrixCostInsertions::findTourPosition(size_t tourid){

	for (size_t i = 0; i < icost[0].size(); ++i) {
		if (icost[0][i]->tourid==tourid) {
			return i;
		}
	}
	return -1; //if the node it isn't there
}

int MatrixCostInsertions::findRequestPosition(size_t id){
	for (size_t i = 0; i < icost.size(); ++i) {
		if (icost[i].back()->p->id==id) {
			return i;
		}
	}
	return -1;
}

bool MatrixCostInsertions::setRequestAsInserted(size_t p_id){
	int posreq=findRequestPosition(p_id);
	for(auto request: icost[posreq] ){
		delete request;
	}
	icost.erase(icost.begin()+posreq);
	return true;
}

int MatrixCostInsertions::createNewTourInfo(size_t tourid){
	int pos=icost[0].size()-1;
	for (size_t i = 0; i < icost.size(); ++i) {
		icost[i].insert(icost[i].begin()+pos,new RequestInfo(icost[i][0]->p,icost[i][0]->d, tourid));
	}
	return pos;
}

/*
 * The only thing does not reinit are the pickup and delivery pointers
 */
void MatrixCostInsertions::reinitInforRequest(RequestInfo& req){
	req.deltacost=-1;
	req.isfeasible=false;
	req.i=-1;
	req.j=-1;
	req.vehicle=nullptr;
}


std::ostream& operator<< (std::ostream& out, const MatrixCostInsertions& MC){
	out<<"Insertion relative costs \n";
	for (size_t i = 0; i < MC.icost.size() ; ++i) {
		for (size_t j = 0; j < MC.icost[i].size(); ++j) {
			out<<MC.icost[i][j]<<"\t";
		}
			out<<"\n";
	}
	return out;
}


