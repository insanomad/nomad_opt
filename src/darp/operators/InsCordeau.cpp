
#include "InsCordeau.h"

#include "../structure/Tour.h"

//#include "ALNS_inc.h"

#include <time.h>
#include <iostream>
#include <algorithm>    // std::random_shuffle
#include <cstdlib>      // std::rand, std::srand
#include <ctime>        // std::time
#include <iomanip>

#include "../instanceManager/readSolCordeau.h"
using namespace std;

Insert_Cordeau::Insert_Cordeau(std::string s): ARepairOperator(s){
	name=s;
}

Insert_Cordeau::~Insert_Cordeau() {
}

void Insert_Cordeau::repairSolution(ISolution& sol){
	darpSol = &dynamic_cast<DARPSolution&>(sol);
	Instance * inst=darpSol->getInstance();
	SolutionCordeau SC(name,inst->nb_request);
	cost=floor(SC.cost*100)/100; // this values never change.
	/* for every tour*/
	int contR=0;
	for (size_t i = 0; i < SC.tours.size(); ++i) {
		if(SC.tours[i].size()>1){ // compulsory. its one when there only a depot and no nodes in it.
			Tour* round=darpSol->getTours()[contR]; //new Tour(inst,darpSol->getDepotDepart()[0],darpSol->getDepotArrive()[0],inst->getVehicleTypes()[0],darpSol->getTeval(),darpSol->getCeval());
			round->v_node.reserve(SC.tours[i].size());
			/* for every node in the tour*/
			for (size_t j = 1; j < SC.tours[i].size()-1; ++j) {
				NodeSol* node=darpSol->getVrevers()[SC.tours[i][j]];
				node->id_round=&round->id;
				round->v_node.insert(round->v_node.begin()+j,node);
				if (node->node->type==0) {
					darpSol->set_as_inserted(node);
				}
				round->v_node[j]->posSym=&darpSol->getVrevers()[round->v_node[j]->node->idsym]->position;
			}
			/*Scheduling nodes tour by tour*/
			if(!round->updateFeasibleTourParameters(round->getVehicle())){ //remise en question du vehicule
				cout<<"Creating newRoute in insert cordeau!"<<__FILE__<<__LINE__<<endl;
				abort();
			}
			++contR;
		}
	}
	darpSol->recomputeCost();
}
