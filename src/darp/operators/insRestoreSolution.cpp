#include "insRestoreSolution.h"

#include "../structure/Tour.h"
#include "../structure/Node.h"
#include "../checker/Checker.h"
#include "../instanceManager/Instance.h"

#include <time.h>
#include <iostream>
#include <vector>
#include <algorithm>    // std::random_shuffle
#include <cstdlib>      // std::rand, std::srand
#include <ctime>        // std::time

using namespace std;

Restore_Solution::Restore_Solution(std::string s): ARepairOperator(s){
	name=s;
}

Restore_Solution::~Restore_Solution() {
}

void Restore_Solution::rebuiltSolution(ISolution& sol, const vector<RouteForSetCovering*>& tours){
	darpSol = &dynamic_cast<DARPSolution&>(sol);
	Checker Check(darpSol);
	//darpSol->tours.reserve(tours.size());   //key!
	//std::cout<<darpSol->getPenalizedObjectiveValue()/darpSol->getInstance()->multiplier<<std::endl;
	vector<bool> usedRoute(darpSol->tours.size(),false);
	for (auto it = tours.begin();it!=tours.end();++it)
	{
		if((*it)->route.size()>2){
			bool newTour=false;
			Tour* round=nullptr;//new Tour(darpSol->getInstance(), ndepart ,narrive, (*it)->vehicle,darpSol->getTeval() , darpSol->getCeval());
			for(auto& r: darpSol->tours ){
				if(r->v_node[0]->node->id == *(*it)->route.begin() && r->v_node[1]->node->id == (*it)->route.back() && r->v_node.size()==2 && !usedRoute[r->id] ){
					if(cst::QuAndBard && r->getVehicle()->getId()==(*it)->vehicle->getId()){
						round=r;
						usedRoute[r->id]=true;
						break;
					}
					if(!cst::QuAndBard){
						round=r;
						usedRoute[r->id]=true;
						break;
					}
				}
			}
			if(round==nullptr) {
				newTour=true;
				cout<<"Set Covering Problem select more routes than allowed! check constraints"<<endl;
				abort();
			}

			round->v_node.reserve((*it)->route.size());
			auto itn=(*it)->route.begin();
			for (size_t j = 1; j < (*it)->route.size()-1; ++j) { //the first is depot
				++itn;
				NodeSol* n=darpSol->getVrevers()[(*itn)];
				n->id_round=&round->id;
				n->position=j;
				round->v_node.insert(round->v_node.begin()+(round->v_node.size()-1),n); // insert nodes without recalculating tour parameters
				if (n->node->type==0) {
					darpSol->set_as_inserted(n);
				}
				round->v_node[j]->posSym=&darpSol->getVrevers()[round->v_node[j]->node->idsym]->position;
			}
			round->v_node[round->v_node.size()-1]->position=round->v_node.size()-1;
			///dont change to nullptr b/o QuAndBards test fails
			round->recomputePositions();
			if(!round->updateFeasibleTourParameters(nullptr)){ // ((*it)->vehicle)) remise en question du vehicule
				cout<<"Creatin new scp route wasnt feasible!"<<__FILE__<<__LINE__<<endl;
				abort();
			}
		}
	}
	darpSol->recomputeCost();
	if(cst::checkAll) Check.solCheck();
}

Tour* Restore_Solution::makeRoute(DARPSolution& sol, const RouteForSetCovering& rscp){

	NodeSol* ndepart=nullptr;
	NodeSol* narrive=nullptr;
	std::vector<NodeSol*>::const_iterator na=sol.getDepotArrive().begin();
	for(auto& nd : sol.getDepotDepart()){
		if(nd->node->id==*rscp.route.begin()){
			ndepart=nd;
			narrive=(*na);
			break;
		}
		++na;
	}

	Tour* newRoute=new Tour(sol.getInstance(), ndepart ,narrive, rscp.vehicle, sol.getTeval() , sol.getCeval());

	newRoute->v_node.reserve(rscp.route.size());
	auto itn=rscp.route.begin();
	for (size_t j = 1; j < rscp.route.size()-1; ++j) {
		++itn; //the first is depot
		NodeSol* n=sol.getVrevers()[(*itn)];
		n->id_round=&newRoute->id;
		n->position=j;
		newRoute->v_node.insert(newRoute->v_node.begin()+(newRoute->v_node.size()-1),n); // insert nodes without recalculating tour parameters
		if (n->node->type==0) {
			sol.set_as_inserted(n);
		}
		newRoute->v_node[j]->posSym=&sol.getVrevers()[newRoute->v_node[j]->node->idsym]->position;
	}
	newRoute->v_node[newRoute->v_node.size()-1]->position=newRoute->v_node.size()-1;
	//dont change to nullptr b/o QuAndBards test fails
	if(!newRoute->updateFeasibleTourParameters(rscp.vehicle)){ //remise en question du vehicule
		cout<<"Creatin newRoute to remove duplicates!"<<__FILE__<<__LINE__<<endl;
		abort();
	}
	return newRoute;
}
