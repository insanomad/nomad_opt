/*
 * DestroyTimeRelatedRemoval.h
 *
 *  Created on: May 6, 2016
 *      Author: oscar
 */

#ifndef SRC_DARP_DESTROY_TIMEREMOVAL2_H_
#define SRC_DARP_DESTROY_TIMEREMOVAL2_H_

#include "../solutionManager/DARPSolution.h"
#include "../checker/Checker.h"
#include "../../alns/ADestroyOperator.h"
#include "../structure/RequestInfo.h"


#include <vector>
#include <iostream>

class TimeRelatedRemoval: public ADestroyOperator{

public:
	TimeRelatedRemoval( std::string s, double minprobability, double maxprobability );
	virtual ~TimeRelatedRemoval();
	void destroySolution(ISolution& sol);

	void generateTimeRelatedList(std::vector<std::pair<double,size_t>>& request);
	long calcRelatedness(const long ts, const size_t id);

	DARPSolution* darpsol;

	std::string name;
	double p=1;


};


#endif /* SRC_DARP_DESTROY_TIMEREMOVAL_H_ */
