/*
 * DestroyWorseRemoval.cpp
 *
 *  Created on: Jan xx, 2016
 *      Author: oscar
 */

#include "DesWorseRemoval.h"

#include <limits.h>
#include <cmath>
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

WorseRemoval::WorseRemoval(std::string s, double minprobability, double maxprobability )
:ADestroyOperator(s,minprobability,maxprobability) {
	p=6;
	darpsol=nullptr;
}

WorseRemoval::~WorseRemoval() {
	// TODO Auto-generated destructor stub
}

struct SortRequests
{
	inline bool operator() (const RequestInfo& i, const RequestInfo& j) {
		return (i.deltacost>j.deltacost);
	}
};

void WorseRemoval::destroySolution(ISolution& sol) {

	darpsol= &dynamic_cast<DARPSolution&>(sol);
	int nbRequest= (int)(darpsol->getRequestInSolution().size()-darpsol->getNonInserted().size());
	int nb_destroy=getNbRemoval(nbRequest);

	vector<RequestInfo> request;
	generateWorseInsertionList(request);
	sort(request.begin(), request.end(), SortRequests());  //-> sort from highest to smallest delta cost giving priority to the more expensive request
	//cout<<(*requests.begin()).deltacost<<" "<<requests.back().deltacost<<endl;

	vector<size_t> requesToRemove;
	requesToRemove.reserve(nb_destroy);
	for (int i = 0; i < nb_destroy; ++i) {
		size_t id= size_t(std::pow((double(rand()))/double(RAND_MAX),p)*double(request.size()));
		requesToRemove.push_back(request[id].p->id);
		request.erase(request.begin()+id);
	}

	if(cst::clusterRemoval)
		darpsol->removeSetOfRequestById(requesToRemove);  // it does the same as the previous for however is slower! (no reasonable explanation)
	else{
		for (auto& it : requesToRemove) {
			darpsol->removeRequestById(it);
		}
	}
}

void WorseRemoval::generateWorseInsertionList(std::vector<RequestInfo>& liste){

	size_t posNodeSym=0;
	for (auto t: darpsol->tours) {
		for (size_t i=1;i < t->v_node.size()-1;++i)
		{ //excluding depots
			if (t->v_node[i]->node->type==0)
			{
				posNodeSym= *t->v_node[i]->posSym;
				RequestInfo temp(t->v_node[i], t->v_node[posNodeSym]);
				temp.i=i;temp.j=posNodeSym;temp.tourid=t->id;
				temp.deltacost=t->getCost();
				if (t->evalRemoval(temp)) {  // this delta is really total cost
					liste.push_back(temp);
				}else{
					cout<<"removing a request was infeasible!"<<__FILE__<<__LINE__<<endl;
					cout<<temp<<endl;
					abort();
				}
			}
		}
	}
}

