/*
 * MatrixCostInsertions.h
 *
 *  Created on: Oct 28, 2015
 *      Author: oscarts
 */

#ifndef SRC_DARP_STRUCTURE_MATRIXCOSTINSERTIONS_H_
#define SRC_DARP_STRUCTURE_MATRIXCOSTINSERTIONS_H_
#include <cstdlib>
#include <vector>
#include <iostream>
#include <limits>
#include "../structure/RequestInfo.h"
//#include "ALNS_inc.h"

class MatrixCostInsertions {
public:
	MatrixCostInsertions();

	virtual ~MatrixCostInsertions();
	void deleteMatrix();

	std::vector<std::vector<RequestInfo*> > icost;

	bool addInsertionData(RequestInfo* c);

	int findRequestPosition(size_t id);

	int findTourPosition(size_t tourid);

	bool setRequestAsInserted(size_t p_id);

	int createNewTourInfo(size_t tourid);

	void reinitInforRequest(RequestInfo& req);

	//void equalsTo(RequestInfo& req); // used in k-regret



};
std::ostream& operator<< (std::ostream& out, const MatrixCostInsertions& MC);

#endif /* SRC_DARP_STRUCTURE_MATRIXCOSTINSERTIONS_H_ */
