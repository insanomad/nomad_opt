/*
 * DestroyTimeRemoval.cpp
 *
 *  Created on: Jan xx, 2016
 *      Author: oscar
 */

#include "DesTimeRelatedRemoval.h"

#include <limits.h>
#include <cmath>
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

struct SortPairs
{
	inline bool operator() (const pair<double, int>& i,const pair<double, int>& j) {
		return (i.first<j.first);
	}
};

TimeRelatedRemoval::TimeRelatedRemoval( std::string s, double minprobability, double maxprobability )
:ADestroyOperator(s,minprobability,maxprobability) {
	p=6;
	darpsol=nullptr;
}

TimeRelatedRemoval::~TimeRelatedRemoval() {
}


void TimeRelatedRemoval::destroySolution(ISolution& sol) {
	darpsol= &dynamic_cast<DARPSolution&>(sol);
	int nbRequest= (int)(darpsol->getRequestInSolution().size()-darpsol->getNonInserted().size());
	int nb_destroy=getNbRemoval(nbRequest);


	vector<pair<double,size_t>> request;
	generateTimeRelatedList(request);
	sort(request.begin(), request.end(),SortPairs());  // sort from smaller to highest
	//cout<<(*requests.begin()).first<<" "<<requests.back().first<<endl;


	vector<size_t> requestToRemove;
	for (int i = 0; i < nb_destroy; ++i) {
		size_t id= size_t(std::pow((double(rand()))/double(RAND_MAX),p)*double(request.size()));
		requestToRemove.push_back(request[i].second);
		request.erase(request.begin()+id);
	}

	if(cst::clusterRemoval)
		darpsol->removeSetOfRequestById(requestToRemove);  // it does the same as the previous for however is slower! (no reasonable explanation)
	else{
		for (auto& it : requestToRemove) {
			darpsol->removeRequestById(it);
		}
	}

}

void TimeRelatedRemoval::generateTimeRelatedList(vector<pair<double,size_t>>& requests){
	long  ts=rand()%(int)darpsol->getInst()->getMaxtime();
	// calc time distance
	for (size_t  id :darpsol->getRequestInSolution()) {
		if (darpsol->isInserted(id)) {
			long vs=calcRelatedness(ts,id);
			requests.push_back(make_pair(vs,id));
		}
	}

}
// the smaller relateness the closer the node
long TimeRelatedRemoval::calcRelatedness(const long ts, const  size_t id){
	long relatedness=0;
	NodeSol* n=darpsol->getVrevers()[id];
	if (ts <= n->node->tw_start) {
		relatedness=n->node->tw_start-ts;
	} else if (ts<=n->node->tw_end) {
		relatedness=0;
	}else{
		relatedness=ts-n->node->tw_end;
	}
	return relatedness;
}
