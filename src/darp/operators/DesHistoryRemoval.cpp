/*
 * HistoryRemoval.cpp
 *
 *  Created on: Jan xx, 2016
 *      Author: oscar
 */

#include "DesHistoryRemoval.h"

#include "../../constant.h"
#include <limits.h>
#include <cmath>
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

HistoryRemoval::HistoryRemoval(std::string s, double minprobability, double maxprobability,int nb_nodes):ADestroyOperator(s,minprobability,maxprobability) {
	//probability=0.85;
	p=6;
	/* Initialize the array with the students*/
	nbnodes=nb_nodes;
	scores = new double*[nb_nodes];
	for (int i = 0; i < nb_nodes; i++) {
		scores[i]= new double[nb_nodes];
	}
	for (int i = 0; i < nb_nodes; ++i) {
		for (int j = 0; j < nb_nodes; ++j) {
			scores[i][j]=INT_MAX;
		}
	}
}

HistoryRemoval::~HistoryRemoval() {
	/* Delete the array of scores*/
	for (int i = 0; i < nbnodes; i++){
		delete[] scores[i];
	}
	delete [] scores;
}

void HistoryRemoval::destroySolution(ISolution& sol) {
	DARPSolution* darpsol= &dynamic_cast<DARPSolution&>(sol);
	int nbRequest= (int)(darpsol->getRequestInSolution().size()-darpsol->getNonInserted().size());
	int nb_destroy=getNbRemoval(nbRequest);

	updateScores(sol);

	//sort weights by score
	sort(weights.rbegin(),weights.rend());

	//init vector of node to remove
	vector<size_t> requesToRemove;
	requesToRemove.reserve(nb_destroy);

	for (int i = 0; i < nb_destroy; ++i) {
		size_t id= size_t(std::pow((double(rand()))/double(RAND_MAX),p)*double(weights.size()));
		requesToRemove.push_back(weights[id].second);
		weights.erase(weights.begin()+id);
	}

	if(cst::clusterRemoval)
		darpsol->removeSetOfRequestById(requesToRemove);  // it does the same as the previous for however is slower! (no reasonable explanation)
	else{
		for (auto& it : requesToRemove) {
			darpsol->removeRequestById(it);
		}
	}
}

void HistoryRemoval::updateScores(ISolution& sol){
	DARPSolution* darpsol= &dynamic_cast<DARPSolution&>(sol);
	weights.clear();
	//weights.resize(0);
	weights.reserve(darpsol->getAllReqInClust().size()*2+darpsol->getNbDepots()*2);
	double score=darpsol->getPenalizedObjectiveValue();
	for (const auto& tour:darpsol->getTours()) {
		for (auto node=(++tour->v_node.begin()), end=(--tour->v_node.end()); node!=end; ++node ) {
			auto previous=node;
			--previous;
			scores[(*previous)->node->id][(*node)->id]= min( scores[(*previous)->node->id][(*node)->node->id] ,
					score );
			if ((*node)->node->type==0) { // if pickup
				weights.push_back(make_pair(scores[(*previous)->node->id][(*node)->node->id],
						(*node)->id));
			}
		}
	}
}
