/*
 * DestroyTimeRelatedRemoval.h
 *
 *  Created on: May 6, 2016
 *      Author: oscar
 */

#ifndef SRC_DARP_HISTORY_REMOVAL_H_
#define SRC_DARP_HISTORY_REMOVAL_H_

#include "../../alns/ADestroyOperator.h"
#include "../solutionManager/DARPSolution.h"
#include "../checker/Checker.h"


#include <vector>
#include <iostream>

class HistoryRemoval: public ADestroyOperator{

public:
	HistoryRemoval(std::string s, double minprobability, double maxprobability, int nb_nodes);
	virtual ~HistoryRemoval();
	void destroySolution(ISolution& sol);


	std::string name;
	std::vector<std::pair<double, int> > weights;
	double** scores;


	//double probability=1;
	double p=1;
	int nbnodes=0;
	void updateScores(ISolution& sol);
};


#endif /* SRC_DARP_HISTORY_REMOVAL_H_ */
