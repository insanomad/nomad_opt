/*
 * DestroyTimeRelatedRemoval.h
 *
 *  Created on: May 6, 2016
 *      Author: oscar
 */

#ifndef SRC_DARP_DISTANCE_REMOVAL_H_
#define SRC_DARP_DISTANCE_REMOVAL_H_

#include "../structure/RequestInfo.h"
#include "../solutionManager/DARPSolution.h"
#include "../checker/Checker.h"
#include "../../alns/ADestroyOperator.h"

#include <vector>
#include <iostream>


class Distance_Removal: public ADestroyOperator{

public:
	Distance_Removal(std::string s, double minprobability, double maxprobability );
	virtual ~Distance_Removal();
	void destroySolution(ISolution& sol);
	double calculateTotalWeight(std::vector<std::pair<double,size_t>>& relatedness);

	DARPSolution* darpsol;

	std::string name;
	double probability;
	double maxWeight;
    size_t nbRequestToDestroy=0;
    size_t getsize(){ return nbRequestToDestroy;}
	double calcRelatedness(size_t node_id, size_t id_request);
	double getDistanceRelated (size_t id1, size_t id2);

};


#endif /* SRC_DARP_DISTANCE_REMOVAL_H_ */
