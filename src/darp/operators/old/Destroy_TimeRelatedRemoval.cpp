/*
 * DestroyTimeRemoval.cpp
 *
 *  Created on: Jan xx, 2016
 *      Author: oscar
 */

#include "Destroy_TimeRelatedRemoval.h"

#include <limits.h>
#include <cmath>
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

Time_Removal::Time_Removal(std::string s, double minprobability, double maxprobability )
:ADestroyOperator(s,minprobability,maxprobability) {
	probability=0.85;
	maxWeight=0;
}

Time_Removal::~Time_Removal() {
	// TODO Auto-generated destructor stub
}

struct SortPairs
{
	inline bool operator() (const pair<double, size_t>& i,const pair<double, size_t>& j) {
		return (i.first<j.first);
	}
};

void Time_Removal::destroySolution(ISolution& sol) {
	darpsol = &dynamic_cast<DARPSolution&>(sol);
	//Checker check(darpsol);

	int nbRequest=darpsol->getAllReqClust().size()-darpsol->getNonInserted().size();
	size_t ndestroy=ceil(nbRequest*this->maximumDestroyFrac);  // max number of request to destroy

	vector<pair<double,size_t>> time_distance; // the higher relatedness the closest to ts
	double  ts=rand()%(size_t)darpsol->getInstance()->maxtime;

	// calculate time distance
	for (size_t  id :darpsol->getAllReqClust()) {
		if (darpsol->isInserted(id)) {
			double vs=calcRelatedness(ts,id);
			pair<double,size_t> req(vs,id);
			maxWeight=max(maxWeight,req.first);
			time_distance.push_back(req);
		}
	}
	//sort by time distance
	sort(time_distance.begin(), time_distance.end());  // not sure i need to do it but just in case..
	double totalWeight=0;
	size_t cont=0, cumWeight=0;
	while(cont <= ndestroy)
	{
		totalWeight = calculateTotalWeight(time_distance);
		double randNumber= totalWeight*rand()/(double)RAND_MAX;

		for(auto it = time_distance.begin(); it != time_distance.end(); ++it)
		{
			cumWeight+=maxWeight-(*it).first+1;
			if( randNumber < cumWeight ){ //roulette wheel
				darpsol->removeRequestById((*it).second);
				it=time_distance.erase(it);
				cumWeight=0;
				cont++;
				break;
			}
		}

	}
}
double Time_Removal::calculateTotalWeight(std::vector<std::pair<double,size_t>>& relatedness){
	double totalWeight=0;
	for (auto it: relatedness) {
		totalWeight+=maxWeight-it.first+1;
	}
	return totalWeight;
}

double Time_Removal::calcRelatedness(double ts, size_t id){
	double relatedness=0;
	Node_in_Sol* n=darpsol->getVrevers()[id];
	if (ts <= n->node->tw_start) {
		relatedness=n->node->tw_start-ts;
	} else if (ts<=n->node->tw_end) {
		relatedness=0;
	}else{
		relatedness=ts-n->node->tw_end;
	}
	return relatedness;
}

