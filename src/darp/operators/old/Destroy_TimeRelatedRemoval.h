/*
 * DestroyTimeRelatedRemoval.h
 *
 *  Created on: May 6, 2016
 *      Author: oscar
 */

#ifndef SRC_DARP_DESTROY_TIMEREMOVAL_H_
#define SRC_DARP_DESTROY_TIMEREMOVAL_H_

#include "../structure/RequestInfo.h"
#include "../solutionManager/DARPSolution.h"
#include "../checker/Checker.h"
#include "../../alns/ADestroyOperator.h"

#include <vector>
#include <iostream>


class Time_Removal: public ADestroyOperator{

public:
	Time_Removal(std::string s, double minprobability, double maxprobability );
	virtual ~Time_Removal();
	void destroySolution(ISolution& sol);
	double calculateTotalWeight(std::vector<std::pair<double,size_t>>& relatedness);

	DARPSolution* darpsol;

	std::string name;
	double probability;
	double maxWeight;
	double calcRelatedness(double ts, size_t id);
};


#endif /* SRC_DARP_DESTROY_TIMEREMOVAL_H_ */
