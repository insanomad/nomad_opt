
/* ALNS_Framework - a framework to develop ALNS based solvers
 *
 * Copyright (C) 2012 Renaud Masson
 *
 * This library is free software; you can redistribute it and/or
 * modify it either under the terms of the GNU Lesser General Public
 * License version 3 as published by the Free Software Foundation
 * (the "LGPL"). If you do not alter this notice, a recipient may use
 * your version of this file under the LGPL.
 *
 * You should have received a copy of the LGPL along with this library
 * in the file COPYING-LGPL-3; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Suite 500, Boston, MA 02110-1335, USA
 *
 * This software is distributed on an "AS IS" basis, WITHOUT WARRANTY
 * OF ANY KIND, either express or implied. See the LGPL for
 * the specific language governing rights and limitations.
 *
 * The Original Code is the ALNS_Framework library.
 *
 *
 * Contributor(s):
 *	Renaud Masson
 */

#ifndef Tour_RANDOM_REMOVAL_H_
#define Tour_RANDOM_REMOVAL_H_

#include "../structure/Tour.h"
#include "../structure/Node.h"
#include "../../alns/ADestroyOperator.h"
#include "../solutionManager/DARPSolution.h"
#include "../checker/Checker.h"

#include <vector>

/*
 * Sort of cluster removal
 */

class Tour_Removal: public ADestroyOperator {
public:
	Tour_Removal(std::string);
	virtual ~Tour_Removal();
	void destroySolution(ISolution& sol);

	//void select_nodes(std::vector<size_t> &v);

    //void destroy(std::vector<size_t> &v_node_selected);

    DARPSolution * darpsol=NULL;
    //MatrixCostInsertions * InsMatrix;
    //size_t sizev;
    //size_t getsize(){ return sizev;}

};

#endif /* Tour_RANDOM_REMOVAL_H_ */
