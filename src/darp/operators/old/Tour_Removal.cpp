
#include "Tour_Removal.h"

#include <stdlib.h>
#include <time.h>
#include <algorithm>

using namespace std;

Tour_Removal::Tour_Removal(std::string s): ADestroyOperator(s,0,0) {
	// TODO Auto-generated constructor stub
}

Tour_Removal::~Tour_Removal() {
	// TODO Auto-generated destructor stub
}

void Tour_Removal::destroySolution(ISolution& sol) {
	darpsol = &dynamic_cast<DARPSolution&>(sol);
	size_t id_t=rand()%darpsol->tours.size();
	//cout<<"Tour Removal id: "<< darpsol->vp_tours[id_t]->id <<endl;
	darpsol->remove_tour(id_t);
	darpsol->recomputeCost();
}
