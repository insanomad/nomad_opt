
#include "Related_Removal.h"

#include <stdlib.h>
#include <time.h>
#include <vector>

#include <algorithm>

//#include "ALNS_inc.h"


using namespace std;

Related_Removal::Related_Removal(std::string s, double mini_percent, double maxi_percent, Neighborhood * _neigh): ADestroyOperator(s,mini_percent,maxi_percent) {
	neigh=_neigh;
	proba=0.9;
}

Related_Removal::~Related_Removal() {}

void Related_Removal::destroySolution(ISolution& sol) {
	darpsol = &dynamic_cast<DARPSolution&>(sol);
	size_t nbRequest=darpsol->getAllReqClust().size()-darpsol->getNonInserted().size();
	size_t mini= max(1.0,nbRequest*this->minimunDestroyFrac);
	size_t maxi= max(1.0,nbRequest*this->maximumDestroyFrac);
	nbRequestToDestroy=mini+rand()%min( maxi - mini + 1, nbRequest);

	vector<size_t> idRequestToDestroy;

	size_t Request=darpsol->getAllReqClust()[rand()%nbRequest];
	size_t neighbour=0;
	while(idRequestToDestroy.size()<nbRequestToDestroy && neighbour<darpsol->getInstance()->nb_request){
		if( darpsol->isInCluster(neigh->getVvNeighborhood()[Request][neighbour]) &&
				((double)(rand()%100))/100 < proba){
			idRequestToDestroy.push_back(neigh->getVvNeighborhood()[Request][neighbour]);
		}
		neighbour++;
	}

	//random_shuffle(v.begin(), v.end());
	//cout<<" Distance Removal element(s) deplacé(s): ";
	//for (size_t j = 0; j < v.size(); ++j) cout<<" "<<v[j]; cout<<endl;
	//destroy one by one
	for (size_t p = 0; p < idRequestToDestroy.size(); ++p) {
		darpsol->removeRequestById(idRequestToDestroy[p]);
	}
}

