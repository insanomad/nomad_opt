
#include "Insert_CheapInsertion.h"

//#include "ALNS_inc.h"

#include <time.h>
#include <iostream>
#include <algorithm>    // std::random_shuffle
#include <cstdlib>      // std::rand, std::srand
#include <ctime>        // std::time

using namespace std;

Insert_CheapInsertion::Insert_CheapInsertion(std::string s, double min_nt,
		double max_nt, Instance* ins,
		Neighborhood * _neigh, int _mode): ARepairOperator(s, min_nt, max_nt){
	mode=_mode;
	neigh=_neigh;
	proba=0.8;
}

Insert_CheapInsertion::~Insert_CheapInsertion() {

}

/**
 * rebuild the solution by adding couple of node one by one
 * @param sol solution a modifier
 */
void Insert_CheapInsertion::repairSolution(ISolution& sol){
	darpSol = &dynamic_cast<DARPSolution&>(sol);
    Node_in_Sol *p,*d;
    size_t idx_nonInsted=0;
    //Checker check(darpSol);
    while (idx_nonInsted<darpSol->getNonInserted().size())
    {
    	p=darpSol->getVrevers()[darpSol->getNonInserted()[idx_nonInsted]];
    	d=darpSol->getVrevers()[p->node->idsym];
    	if(p->id > d->id) swap(p,d);
    	RequestInfo best_e(p,d);
    	if (!calcBestInsertion(best_e)){
      		idx_nonInsted++;
    	}
    	//check.times();
    }
    //cout<<"ckeck best insertion, # Tours "<< darpSol->vp_tours.size()<<endl;
}

/**
 * Insert a silgle request in the best current position.
 */
bool Insert_CheapInsertion::calcBestInsertion(RequestInfo& best_e) {
	//parcourir tout les tours existants
	size_t size=darpSol->getTours().size();
	vector<Tour*> subset_tours(size);
	if(mode==1)      select_random_tours(subset_tours);
	else if(mode==2) select_neighbour_tours(subset_tours,best_e.p->id);
	else             select_all_tours(subset_tours);

	//recherche du cout relatif induit le plus faible
	long min_t=-1;

	RequestInfo e(best_e.p,best_e.d);

	//	parmis les existants
	for (size_t t = 0; t < subset_tours.size(); ++t) {
		subset_tours[t]->evalBestInsertion(e);
		if(e.isfeasible){
			if(e.deltacost < best_e.deltacost){
				best_e = e;
				min_t=t;
			}
		}
	}
	//pour une nouvelle tournée
	newTour= new Tour(99999 , darpSol->getInstance());
	if (darpSol->getNumberOfTours() >= darpSol->getInstance()->nb_vehicle_types) {
		e.isfeasible=false;
	}else{
		e.i=1;e.j=2;e.vehicle=darpSol->getInstance()->vehicleTypes[0];
		newTour->insertFeasibleRequest(e);  //todo  false for classic darp
	}

	if (best_e.isfeasible || e.isfeasible)
	{
		if( best_e.isfeasible && (newTour->getCost()>= best_e.deltacost))
		{											//on ajoute les point à la tournée min_Tour
			best_e.tourid=subset_tours[min_t]->id;   // compulsory step
			if (!subset_tours[min_t]->insertFeasibleRequest(best_e) ) cout<<"do not delete conditional"<<endl;
			darpSol->recomputeCost();
			delete newTour;
		} else if(e.isfeasible){						//on ajoute une nouvelle tournée
			best_e=e;
			darpSol->insertTour(newTour);
		}
		darpSol->set_as_inserted(best_e.p);
	} else {
		delete newTour;
	}
	return best_e.isfeasible;
}

/**
 * Select all tours
 */
void Insert_CheapInsertion::select_all_tours(vector<Tour*> &subset_tours) {
	for (size_t i = 0; i < darpSol->getTours().size(); ++i) {
		subset_tours[i]=darpSol->getTours()[i];
	}
}

void Insert_CheapInsertion::select_random_tours(vector<Tour*> &subset_tours) {
	//attention en l'état nous n'ajoutons pas automatiquement le tour d'origine
	//il faut etre concient que cela favorise les tournées plus courte
	//car la faisabilité du tour n'est pas du tout assurée
	//or dans ce cas là c'est une nouvelle tournée qui est insérée
	inserted.resize(darpSol->getNumberOfTours());
	for (size_t i = 0;  i < darpSol->getNumberOfToursUsed(); ++i) {
		inserted[i]=i;
	}
	std::random_shuffle(inserted.begin(), inserted.end());
	int pos_t;
	for (size_t i = 0; i < subset_tours.size(); ++i) {
		pos_t=inserted[i];
		subset_tours[i]=darpSol->getTours()[pos_t];
	}
}

void Insert_CheapInsertion::select_neighbour_tours(vector<Tour*> &subset_tours,size_t id) {
	size_t nb_tour=darpSol->getNumberOfTours();
	if(nb_tour>0){
		size_t mini= nb_tour * numtours_min_percent;
		size_t maxi= nb_tour * numtours_max_percent;
		Tour *t;
		size_t col=1,id_n;
		int cont=0;
		size_t qt_t=(rand()%min( maxi - mini + 1, nb_tour)) + mini;
		subset_tours.resize(qt_t);
		bool test=true;
		while (cont< subset_tours.size() && col<darpSol->getInstance()->nb_request){
			if( darpSol->isInCluster(neigh->getVvNeighborhood()[id][col]) &&
					((double)(rand()%100))/100 < proba){
				id_n=neigh->getVvNeighborhood()[id][col];
				if (darpSol->isInserted(id_n)) {
					if(darpSol->getVrevers()[id_n]->id_round!=NULL){
						t=darpSol->getTours()[*(darpSol->getVrevers()[id_n]->id_round)];
						test=true;
						for (size_t k = 0; k < cont; ++k) {
							if(t->id==subset_tours[k]->id){
								test=false;
								break;
							}
						}
						if (test) {
							subset_tours[cont]=t;
							cont++;
						}
					}
				}

			}
			col++;
		}
	}
}
