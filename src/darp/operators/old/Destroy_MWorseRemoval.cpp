/*
 * DestroyWorseRemoval.cpp
 *
 *  Created on: Jan xx, 2016
 *      Author: oscar
 */

#include "Destroy_MWorseRemoval.h"
#include <limits.h>
#include <cmath>
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

Destroy_MWorseRemoval::Destroy_MWorseRemoval(std::string s, double minprobability, double maxprobability )
						:ADestroyOperator(s,minprobability,maxprobability) {
	probability=0.85;
}

Destroy_MWorseRemoval::~Destroy_MWorseRemoval() {
	// TODO Auto-generated destructor stub
}

struct SortRequests
{
	inline bool operator() (const RequestInfo& i, const RequestInfo& j) {
		return (i.deltacost>j.deltacost);
	}
};

void Destroy_MWorseRemoval::destroySolution(ISolution& sol) {

	darpsol = &dynamic_cast<DARPSolution&>(sol);
	Checker check(darpsol);
	vector<RequestInfo> requests;
	int nbreq=darpsol->getAllReqClust().size()-darpsol->getNonInserted().size();
	size_t ndestroy=ceil(nbreq*this->maximumDestroyFrac);  // max number of request to destroy
	size_t cont=1;
	size_t idx=-1;
	//cout<<"worse Removal element(s) deplacé(s):";
	while(cont <= ndestroy && idx<requests.size())
	{
		idx++;
		generateWorseInsertionList(requests);
		sort(requests.begin(), requests.end(), SortRequests());
		double  a=rand()/(double)RAND_MAX;
		if(a < std::pow(probability,cont) ){
			darpsol->removeRequestById(requests[idx].p->id);
			//cout<<" "<<worse.p->id;
			cont++;
		}
	}
	//cout<<endl;
}

void Destroy_MWorseRemoval::generateWorseInsertionList(std::vector<RequestInfo>& liste){

	int idsym=0;
	size_t posNode=0;

	for (auto t: darpsol->tours) {
		for (size_t i=1;i < t->v_node.size()-1;++i)
		{ //excluding depots
			if (t->v_node[i]->node->type==0) {
				idsym=t->v_node[i]->node->idsym;
				posNode= darpsol->getVrevers()[idsym]->position;
				RequestInfo temp(t->v_node[i], t->v_node[posNode]);
				temp.i=i;temp.j=posNode;temp.tourid=t->id;
				if (posNode<i) temp.i=posNode;temp.j=i;
				temp.deltacost=t->getCost();
				bool succes=t->evalRemoval(temp);
				if (succes) {  // this delta is really total cost
					liste.push_back(temp);
				}
			}
		}
	}
}

