/*
 * Distance_Removal.cpp
 *
 *  Created on: Jan xx, 2016
 *      Author: oscar
 */

#include "Distance_Removal.h"

#include <limits.h>
#include <cmath>
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

Distance_Removal::Distance_Removal(std::string s, double minprobability, double maxprobability )
:ADestroyOperator(s,minprobability,maxprobability) {
	probability=0.85;
	maxWeight=0;
}

Distance_Removal::~Distance_Removal() {
	// TODO Auto-generated destructor stub
}

struct SortPairs
{
	inline bool operator() (const pair<double, size_t>& i,const pair<double, size_t>& j) {
		return (i.first<j.first);
	}
};

void Distance_Removal::destroySolution(ISolution& sol) {
	darpsol = &dynamic_cast<DARPSolution&>(sol);
	size_t nbRequest=darpsol->getAllReqClust().size()-darpsol->getNonInserted().size();
	size_t mini= max(1.0,nbRequest*this->minimunDestroyFrac);
	size_t maxi= max(1.0,nbRequest*this->maximumDestroyFrac);
	nbRequestToDestroy=mini+rand()%min( maxi - mini + 1, nbRequest);

	size_t node=darpsol->getAllReqClust()[rand()%nbRequest];

	vector<pair<double,size_t>> distance; // the higher relatedness the closest to ts

	// calculate distance relatedness
	for (size_t  id :darpsol->getAllReqClust()) {
		if (darpsol->isInserted(id)) {
			double dist=calcRelatedness(node, id);
			pair<double,size_t> req(dist,id);
			maxWeight=max(maxWeight,req.first);
			distance.push_back(req);
		}
	}
	//sort by distance
	sort(distance.begin(), distance.end());  // not sure i need to do it but just in case..
	double totalWeight=0;
	size_t cont=0, cumWeight=0;
	while(cont <= nbRequestToDestroy)
	{
		totalWeight = calculateTotalWeight(distance);
		double randNumber= totalWeight*rand()/(double)RAND_MAX;
		for(auto it = distance.begin(); it != distance.end(); ++it)
		{
			cumWeight+=maxWeight-(*it).first+1; // the smallest the distance the higher the probability of being selected
			if( randNumber < cumWeight ){ //roulette wheel
				darpsol->removeRequestById((*it).second);
				it=distance.erase(it);
				cumWeight=0;
				cont++;
				break;
			}
		}

	}
}
double Distance_Removal::calculateTotalWeight(std::vector<std::pair<double,size_t>>& relatedness){
	double totalWeight=0;
	for (auto it: relatedness) {
		totalWeight+=maxWeight-it.first+1;
	}
	return totalWeight;
}

double Distance_Removal::calcRelatedness(size_t id1, size_t id2){
	double relDistance=0;
	Node* N1=darpsol->getVrevers()[id1]->node;
	Node* N2=darpsol->getVrevers()[id2]->node;
	int D=0;
	if(getDistanceRelated(id1,id2)>0) D++;
	if(getDistanceRelated(id1,N2->idsym)>0) D++;
	if(getDistanceRelated(N1->idsym,id2)>0) D++;
	if(getDistanceRelated(N2->idsym,N2->idsym)>0) D++;

	if(D>0){
		relDistance=(getDistanceRelated(id1,id2)+
		         getDistanceRelated(id1,N2->idsym)+
			     getDistanceRelated(N1->idsym,id2)+
			     getDistanceRelated(N1->idsym,N2->idsym))/D;
	}else{
		relDistance=0;
	}
	return relDistance;
}
// calculated the distance from to nodes considering cases presented in ropke.
// TODO modify when multi-depot case
double Distance_Removal::getDistanceRelated(size_t id1, size_t id2){
	size_t idDepot=darpsol->getInstance()->depot_depart[0]->id;
	double distN1ToDepot=darpsol->getInstance()->distance_ij[idDepot][id1];
	double distN2ToDepot=darpsol->getInstance()->distance_ij[idDepot][id2];
	double distance=0;
	if(distN1ToDepot==0 || distN2ToDepot==0){
		distance=0;
	}else{
		distance=darpsol->getInstance()->distance_ij[id1][id2];
	}
	return distance;
}

