
/*
 * Instance.h
 *
 *  Created on: Apr 21, 2015
 *      Author: sam
 */

#ifndef INSTANCE_H_
#define INSTANCE_H_
#include <sstream>
#include <stdlib.h>
#include <string>
#include <limits.h>
#include <vector>

#include "../../constant.h"
#include "../structure/Node.h"
#include "../structure/VehicleType.h"


class Instance {

public:
	//Default constructor
	Instance() {
		dur_max=LONG_MAX;
		dur_min=0;
		maxwait=INT_MAX;
		nb_depot=0;
		nb_node=0;
		nb_request=0;
		nb_user_type=0;
		nb_vehicle_types=0;
		maxNbRoutes=0;
		multDist=1;
		refTime=0;
		penaltyRideTimeCost=0;
		maxNbReconfigurations=0;
	}

	// constructor instances: 0 Cordeau et Laporte 2003, 3 GIHP 2 QuAndBard
	Instance(std::string instance_csv, std::string distances_csv, std::string times_csv, std::string vehicle_csv, int nb_reconfigrations, int type_inst) {
		dur_max=LONG_MAX;
		dur_min=0;
		maxwait=INT_MAX;
		maxNbRoutes=0;
		if (type_inst==0){
			multDist=100000;
			readCourdeauOriginal(instance_csv);
			maxNbReconfigurations=0;
		} else if (type_inst==2){ //QuAndBard
			multDist=10000;
			refTime=7;  // 7h. is the 0 time.
			maxNbReconfigurations=nb_reconfigrations;
			readQuAndBard(instance_csv);
			readVehicleQuAndBard(vehicle_csv);
			penaltyRideTimeCost=0.0001;
			//to_gpx(instance_csv+".gpx");
		}else if (type_inst==3){ //GIHP new
			multDist=1; //critical dont remove even if default value is 1
			maxNbReconfigurations=nb_reconfigrations;
			readVehiclesGIHP(vehicle_csv);
			readNodesGIHP(instance_csv);
			readDistancesGIHP(distances_csv);
			readTimesGIHP(times_csv);
			//to_gpx(instance_csv+".gpx");
		}else{
			std::cout<<"invalid instance type"<<type_inst<<std::endl;
			abort();
		}
		calcMaxTime();  //-> computes maxtime var

		for(auto& v:vehicleTypes ){ //needed in bitset test
			v->generateBitSet();
		}

		if(cst::tightenTW) {
			int count=0;
			while(tightenTimeWindows()){
				if(cst::verbosite>0) std::cout<<"tightening TW:"<<count++<<std::endl;
			}
		}
	}


	//Destructor
	~Instance();

	/**
	 * affiche l'instance
	 * @param loop_limite limite d'affichage pour les boucles
	 */
	std::string serialize(size_t  loop_limite=1000, bool display_matrix=false) const;

	/**
	 *
	 * @param path where to write the instance
	 * @return string writed to file
	 */
	std::string write_instance(std::string path) const;


	/**
	 * << operator
	 */
	friend std::ostream& operator<<(std::ostream&, Instance&);



	//method to transform double values into size_t
	long D2Sd(double dist){
		return (long)ceil(dist*multDist);
	}

	double S2Dd(double dist){
		return (dist/(double)multDist);
	}
	std::string TimeString(long atime);

private:
	long absTime(std::string time);

public:

	/*****************************EXPORT*******************************/
	/**
	 * export to gpx format
	 * @param _file_export
	 * @return string
	 */
	std::string to_gpx(std::string _file_export,  std::vector<size_t> group);

	// getters and setters
	const std::vector<Node*>& getDataRequests() const {
		return dataRequests;
	}

	const std::vector<Node*>& getDelivery() const {
		return delivery;
	}

	const std::vector<Node*>& getDepotDepart() const {
		return depot_depart;
	}
	const std::vector<Node*>& getDepotArrival() const {
		return depot_arrival;
	}

	const std::vector<std::vector< long> >& getDistanceIj() const {
		return distance_ij;
	}

	long getDurMax() const {
		return dur_max;
	}

	long getDurMin() const {
		return dur_min;
	}

	const std::vector<int>& getMaxcap() const {
		return maxcap;
	}

	long getMaxNbRoutes() const {
		return maxNbRoutes;
	}

	long getMaxridetime() const {
		return maxridetime;
	}

	double getMaxtime() const {
		return maxtime;
	}

	long getMaxwait() const {
		return maxwait;
	}

	long getMult() const {
		return multDist;
	}

	void setMult(long m) {
		multDist=m;
	}

	size_t getNbDepot() const {
		return nb_depot;
	}

	size_t getNbRequest() const {
		return nb_request;
	}

	size_t getNbUserType() const {
		return nb_user_type;
	}::

	size_t getNbVehicleTypes() const {
		return nb_vehicle_types;
	}

	const std::vector<Node*>& getPickup() const {
		return pickup;
	}

	const std::vector<std::vector< long> >& getTime() const {
		return time_ij;
	}

	const std::vector<VehicleType*>& getVehicleTypes() const {
		return vehicleTypes;
	}

	double getPenaltyCost() const {
		return penaltyRideTimeCost;
	}

	int getMaxNbReconfigurations() const {
		return maxNbReconfigurations;
	}

	/****************************Attributs************************************/
	//sets
	std::vector<Node*> pickup;
	std::vector<Node*> delivery;
	std::vector<Node*> dataRequests;
	std::vector<Node*> dataNodes;
	std::vector<Node*> dataDepot;
	std::vector<VehicleType*> vehicleTypes;

	//outdated
	std::vector<Node*> depot_depart;
	std::vector<Node*> depot_arrival;

	//parameters
	size_t nb_user_type;
	size_t nb_depot;
	size_t nb_vehicle_types;
	size_t nb_request;
	size_t nb_node;

	long dur_max;
	long dur_min;
	std::vector<int> maxcap;
	long maxridetime=0;
	long maxwait=0;
	long maxtime=0;
	long maxNbRoutes;
	int maxNbReconfigurations;


	std::vector<std::vector<long> > time_ij;
	std::vector<std::vector<long> > time_ij2; // test matrix
	std::vector<std::vector<long> > distance_ij;


	void write_instance_gihp(std::string name, const std::vector<size_t>& getAllReqClust);
	std::stringstream write_nodes_gihp(std::string name, std::vector<size_t>& depots_arrive, const std::vector<size_t>& getAllReqClust);
	std::stringstream write_distance_gihp(std::string name, std::vector<size_t>& nodesCluster) const;
	std::stringstream write_times_gihp(std::string name, std::vector<size_t>& nodesCluster) const;
	std::stringstream write_vehicles_gihp(std::string name) const;

	const std::vector<Node*>& getDataNodes() const {
		return dataNodes;
	}

	/**
	 * arrange time windows
	 */
	void randomizeTWdel();
	void randomizeTWpic();
	void arrangeTWpic();
	void setMaxRT();
	void setServiceTime();

	/**
	 * set the triangular inequality on time
	 * the distance matrix is also updated to respect the new path
	 */
	void forceTriangularInequality();

	// reading test times and distance matrix
	void readTimesGIHPtest(std::string data_dist);

	// expand the current tw "withtw" seconds
	void modifyDeliveryTW(long withtw);

private:
	long multDist=100000; // distance multiplier
	int refTime=0;            //reference time in hours USEFULL ONLY FOR QU AND BARD
	double penaltyRideTimeCost=0; //USEFULL ONLY FOR QU AND BARD

	//methods to read input data
	void readNodesGIHP(std::string); 		//lit l'instance
	void readVehiclesGIHP(std::string);
	//void readGIHPOld(std::string); 		//lit l'instance
	void readDistancesGIHP(std::string distances_csv);
	void readTimesGIHP(std::string times_csv);



	void readQuAndBard(std::string); 		//lit l'instance

	void generateMatrixQuAndBard();
	void readVehicleQuAndBard(std::string instance_csv);
	void fullGIHP(std::string instance_csv);
	void readCourdeauOriginal(std::string instance_csv);
	// calculate the maximum time, useful in time removal operator
	void calcMaxTime();



	/***************************pres traitement******************************/

	//void set_triangular_inequality();
	void triangular_inequality(std::vector<std::vector<long> > &vv);

	//met des zero sur la diagonales
	void set_zero_diag(std::vector<std::vector<long> > &vv);

	//symetrise pour avoir moins d'un facteur "margin" entre les deux sens
	void near_sym_matrix(std::vector<std::vector<long> > &vv, double margin);

	/**
	 * manipulation instance generée automatiquement
	 * - triangularisation
	 * - 0 sur les diagonales
	 * - symetrique à +1/-0
	 * - non inclusion des configurations
	 */
	void arange_matrix();

	/**
	 * move pickup time windows if not feasible (or not enought margin)
	 * @param coef_marge
	 */
	void arange_tw_pickup(double coef_marge);

	/**
	 * arange vt
	 * arange matrix
	 * arange time windows of the pickup
	 */
	void arange();

	/**
	 * tighting time windows
	 * see 5.1.1 in http://www.iro.umontreal.ca/~marcotte/PLU6000/PLU6000_H04/Cordeau1.pdf
	 * @return true if timewindows are modified;
	 */
	bool tightenTimeWindows();



};



#endif /* INSTANCE_H_ */
