
/*
 * Instance_construct.cpp
 *
 *  Created on: Apr 21, 2015
 *      Author: sam
 */
#include "../checker/Checker.h" //TODO attention ! références croisées !
#include "../structure/Configuration.h"
#include "../../constant.h"

#include <fstream>
#include <iostream>
#include <limits>
#include <cstdlib>
#include "Instance.h"
#include <sstream>

#include <cmath>

using namespace std;


// minutes. 7:00 corresponds to 0min. i.e. 7h30 = 30mins
long Instance::absTime(string time){
	auto x = time.find(":");
	string h = time.substr(0,x);
	string m = time.substr(x+1,2);
	//auto temp=(stoi(h)-7+stoi(m)/60.0);
	long abstime=ceil(60*(stoi(h)-refTime)+stoi(m));
	return abstime;
}

string Instance::TimeString(long atime){
	if(cst::QuAndBard){
		int h=floor(atime/60.0);
		double m=(atime/60.0)-h;
		int min=ceil(m*60.0);
		string newTime=to_string(h+refTime)+"h"+to_string(min);
		return newTime;
	}
	long h=floor(atime/3600.0);
	long m=floor((atime - h*3600)/60);
	long s=atime-3600*h-60*m;
	string newTime=to_string(h)+":"+to_string(m)+":"+to_string(s);
	return newTime;

}

// Lecture de l'instance
void Instance::readQuAndBard(std::string instance_csv) {
	ifstream myfile(instance_csv.c_str());
	string times;
	string name;
	if (myfile.is_open())
	{
		nb_user_type=3;
		int data=0;
		double stime=0;
		bool appointment=false;
		string input="";
		for (size_t i = 0; i < 2; ++i) getline(myfile,times);
		int i=0;
		myfile >> name;
		while(!myfile.eof())
		{
			Node* newPickup=new Node(i, 0, i, nb_user_type);
			pickup.push_back(newPickup);
			pickup[i]->type=0;
			pickup[i]->name=name;
			myfile >> pickup[i]->lat;
			myfile >> pickup[i]->lon;
			myfile >> input;
			if(input.compare("appointment")==0){
				pickup[i]->adress=input;
				appointment=true;
			}else{
				pickup[i]->adress=input;
				myfile >> input;
				pickup[i]->adress+=input;
			}
			myfile >> times;
			pickup[i]->tw_start=D2Sd(absTime(times));
			myfile >> times;
			pickup[i]->tw_end=D2Sd(absTime(times));
			myfile >> stime;
			pickup[i]->stime=D2Sd(stime);
			pickup[i]->cap=1;

			Node* newDelivery=new Node(i, 0, i, nb_user_type);
			delivery.push_back(newDelivery);

			delivery[i]->type=1;
			myfile >> delivery[i]->lat;
			myfile >> delivery[i]->lon;
			myfile >> input;
			if(appointment){
				appointment=false;
				pickup[i]->adress=input;
				myfile >> input;
				pickup[i]->adress+=input;
			}else{
				pickup[i]->adress=input;
			}
			delivery[i]->name=delivery[i]->adress;
			myfile >> times;
			delivery[i]->tw_start=D2Sd(absTime(times));
			myfile >> times;
			delivery[i]->tw_end=D2Sd(absTime(times));
			myfile >> stime;
			delivery[i]->stime = D2Sd(stime);
			delivery[i]->cap=1;

			for(int u=0; u< (int)nb_user_type ;++u){
				myfile >> data;
				pickup[i]->rload[u]=data;
				delivery[i]->rload[u]=-data;
			}
			++i;
			myfile >> name;
		}

		//setup id and id sym
		nb_request=pickup.size();
		for(int i=0 ; i< (int)pickup.size(); ++i){
			pickup[i]->idsym=i+nb_request;
			delivery[i]->idsym=i;
			delivery[i]->id=i+nb_request;
			dataRequests.push_back(pickup[i]);

		}
		//fill up delivery vector
		for(auto node: delivery){
			dataRequests.push_back(node);
		}

		myfile.close();
	}
	else{
		cout << "Unable to open"<<instance_csv<<" "<<__FILE__<<__LINE__<<endl;
		abort();
	}
}

void Instance::readVehicleQuAndBard(std::string data_dist){
	ifstream myfile(data_dist.c_str());
	string line,times;
	if (myfile.is_open()){
		int i=0;
		double speed=0;
		string data="";
		for (size_t i = 0; i < 2; ++i) getline(myfile,line); 	//jump 2 lines
		maxNbRoutes=0;
		myfile >> data;
		while(!myfile.eof()){
			VehicleType* vehicle= new VehicleType(i);
			vehicleTypes.push_back(vehicle);

			Node* newDepot=new Node(i, 0, i, nb_user_type);
			dataDepot.push_back(newDepot);

			dataDepot[i]->id=nb_request*2+i;
			dataDepot[i]->idsym=dataDepot[i]->id;
			dataDepot[i]->type=4;

			vehicleTypes[i]->setName(data);
			myfile >> dataDepot[i]->cap;
			maxNbRoutes+=dataDepot[i]->cap;
			myfile >> speed;
			vehicleTypes[i]->setSpeed(speed);
			myfile >> dataDepot[i]->name;
			myfile >> dataDepot[i]->lat;
			myfile >> dataDepot[i]->lon;
			myfile >> times;
			dataDepot[i]->tw_start=D2Sd(absTime(times));
			myfile >> times;
			dataDepot[i]->tw_end=D2Sd(absTime(times));

			//Configurations: u_1 seat u_2 wheelchair u_3 walker
			string next="";
			int id=0;
			int walker=0;
			myfile >> data;
			vehicleTypes[i]->getVConfiguration().clear();
			auto found=data.find("VAN");
			while(found==string::npos && !myfile.eof()){
				Configuration* configuration=new Configuration(id,nb_user_type);
				for(size_t u=0; u< nb_user_type-1 ;++u){
					walker+=(u+1)*stoi(data);
					configuration->setCapacity(u,stoi(data));
					myfile >> data;
				}
				configuration->setCapacity(nb_user_type-1,walker);
				walker=0;
				vehicleTypes[i]->getVConfiguration().push_back(configuration);
				found=data.find("VAN");
				++id;
			}
			++i;
		}
		nb_vehicle_types=vehicleTypes.size();

		// departure and arrival depot
		nb_depot=dataDepot.size();
		for (size_t i = 0; i < nb_depot; ++i) {
			if(dataDepot[i]->type==2){ //departs
				depot_depart.push_back(dataDepot[i]);
			} else if(dataDepot[i]->type==3){ // arrival
				depot_depart.push_back(dataDepot[i]);
			} else{
				depot_depart.push_back(dataDepot[i]);
				depot_arrival.push_back(dataDepot[i]);
			}
		}

		for(auto vehicle : vehicleTypes){
			vehicle->setTimeCost(0.377);  // $/min
			vehicle->setFixedCost(0.01);
			vehicle->setDistanceCost(0);
		}

		maxcap.resize(nb_user_type);
		for(auto vehicle : vehicleTypes){
			for(auto configuration : vehicle->getVConfiguration()){
				for(size_t u=0;u<nb_user_type; ++u){
					maxcap[u]=max(maxcap[u],configuration->getCapacity()[u]);
				}
			}
		}
		generateMatrixQuAndBard();
		if(cst::checkInstance) Checker(this).instCheck();
		for(auto node:dataRequests){
			dataNodes.push_back(node);
		}
		for(auto node: dataDepot){
			dataNodes.push_back(node);
		}

		myfile.close();
	}else{
		cout << "Unable to open"<<__FILE__<<__LINE__<<endl;
		abort();
	}
}


void Instance::generateMatrixQuAndBard(){
	nb_depot=dataDepot.size();
	nb_node=nb_request*2+nb_depot;

	vector<Node*> dataNodes;

	for(auto node: dataRequests){
		dataNodes.push_back(node);
	}
	for(auto depot: dataDepot){
		dataNodes.push_back(depot);
	}
	double distance=0;
	long duration=0;
	distance_ij.resize(nb_node); 								//init_vector
	time_ij.resize(nb_node);
	for (size_t i = 0; i < nb_node; ++i) {
		distance_ij[i].resize(nb_node);
		time_ij[i].resize(nb_node);//init_vector
		for (size_t j = 0; j < nb_node; ++j){
			distance = sqrt(69.1*(dataNodes[i]->getLat()-dataNodes[j]->getLat())*(dataNodes[i]->getLat()-dataNodes[j]->getLat())
					+ 53*(dataNodes[i]->getLon()-dataNodes[j]->getLon())*(dataNodes[i]->getLon()-dataNodes[j]->getLon()));

			double distQB = distance*1000.;
			distQB = distQB/1000;

			distance_ij[i][j]=D2Sd(distQB);

			duration=D2Sd(floor(distQB/vehicleTypes[0]->getSpeed())); //mins

			time_ij[i][j]=duration;
		}
	}
	calcMaxTime();
}

