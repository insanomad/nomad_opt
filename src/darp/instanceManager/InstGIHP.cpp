
/*
 * Instance_construct.cpp
 *
 *  Created on: Apr 21, 2015
 *      Author: sam
 */
#include "../checker/Checker.h" //TODO attention ! références croisées !
#include "../structure/Configuration.h"
#include "../../constant.h"


#include <fstream>
#include <iostream>
#include <limits>
#include <cstdlib>
#include "Instance.h"
#include <sstream>

#include <cmath>

using namespace std;


void Instance::calcMaxTime(){
	maxtime=0;
	for (size_t i = 0; i < time_ij.size(); ++i)
	{
		for (size_t j = 0; j < time_ij[i].size(); ++j)
		{
			if(time_ij[i][j] > maxtime) maxtime=time_ij[i][j];
		}
	}
}

void Instance::modifyDeliveryTW(long withtw){
	for(auto& d: delivery){
//		if((d->tw_end-d->tw_start)< withtw){
//			long mean =d->tw_start+ (d->tw_end-d->tw_start)/2;
//			long start = mean - withtw/2;
//			long smod=(start/60)%(10);
//			start =start + 60*(10-smod); // arondi sup
//			if((start/60)%10!=0) abort();
//			d->tw_start=start;
			d->tw_end=d->tw_start + withtw;
			cout<<withtw<<" : ["<<d->tw_start<<" "<<d->tw_end<<"]"<<endl;
//		}
	}
}

void Instance::readTimesGIHP(std::string data_dist){
	ifstream myfile(data_dist.c_str());
	string line;
	if (myfile.is_open()){
		// time matrix tij
		for (size_t i = 0; i < 2; ++i) getline(myfile,line); 	//jump 2 lines
		time_ij.resize(nb_node); 								//init_vector
		for (size_t i = 0; i <nb_node; ++i) {
			time_ij[i].resize(nb_node); 						//init_vector
			myfile >> line; 					//sauter une mot
			for (size_t j = 0; j < nb_node; ++j) {
				myfile >> time_ij[i][j]; 					//sauter deux mots
			}
		}
		myfile.close();
		if(cst::checkInstance) Checker(this).instCheck();
		calcMaxTime();
	}
	else{
		cout << "Unable to open file";
		abort();
	}
}

void Instance::readDistancesGIHP(std::string data_dist){
	ifstream myfile(data_dist.c_str());
	string line;
	if (myfile.is_open()){
		// distance matrix tij
		for (size_t i = 0; i < 2; ++i) getline(myfile,line); 	//jump 2 lines
		distance_ij.resize(nb_node); 								//init_vector
		for (size_t i = 0; i <nb_node; ++i) {
			distance_ij[i].resize(nb_node); 						//init_vector
			myfile >> line; 					//sauter une mot
			for (size_t j = 0; j < nb_node; ++j) {
				myfile >> distance_ij[i][j]; 					//sauter deux mots
			}
		}
		myfile.close();
	}
	else{
		cout << "Unable to open file "<< data_dist;
		abort();
	}
}


void Instance::readTimesGIHPtest(std::string data_dist){
	ifstream myfile(data_dist.c_str());
	string line;
	if (myfile.is_open())
	{
		// time matrix tij
		for (size_t i = 0; i < 2; ++i) getline(myfile,line); 	//jump 2 lines
		time_ij2.resize(nb_node); 								//init_vector
		for (size_t i = 0; i <nb_node; ++i)
		{
			time_ij2[i].resize(nb_node); 						//init_vector
			myfile >> line; 					//sauter une mot
			for (size_t j = 0; j < nb_node; ++j)
			{
				myfile >> time_ij2[i][j]; 					//sauter deux mots
			}
		}
		myfile.close();
		calcMaxTime();
	}
	else{
		cout << "Unable to open file";
		abort();
	}
	for (size_t i = 0; i <nb_node; ++i)
	{
		for (size_t j = 0; j < nb_node; ++j)
		{
			if(time_ij[i][j]!=time_ij2[i][j])
			{
				cout<<time_ij[i][j]<<" "<<time_ij2[i][j]<<endl;
			}
		}
	}
}

void Instance::readVehiclesGIHP(std::string instance_csv){
	ifstream myfile(instance_csv.c_str());
	string line;
	if (myfile.is_open())
	{
		myfile >> nb_user_type;getline(myfile,line);
		for (size_t i = 0; i < 1; ++i) getline(myfile,line);
		size_t id;
		string name="";
		myfile >> name;
		while(!myfile.eof())
		{
			myfile >> id;
			auto vehicle =new VehicleType(id);
			auto nbc=0;
			double costs=0;
			myfile >> nbc;
			myfile >> costs;
			vehicle->setFixedCost(costs);
			myfile >> costs;
			vehicle->setTimeCost(costs);
			myfile >> costs;
			vehicle->setDistanceCost(costs);
			vehicle->getVConfiguration().resize(nbc);
			for (int j = 0; j < nbc; ++j)
			{
				Configuration* conf=new Configuration(j,nb_user_type);
				int capa=0;
				for (size_t k = 0; k < nb_user_type; ++k)
				{
					myfile >> capa;
					conf->setCapacity(k,capa);
				}
				vehicle->setConfiguration(conf,j);
			}
			vehicleTypes.push_back(vehicle);
			name="";
			myfile >> name;
		}
		nb_vehicle_types=vehicleTypes.size();
		// used for necessary conditions for feasibility test
		maxcap.resize(nb_user_type);
		for(auto vehicle : vehicleTypes)
		{
			for(auto configuration : vehicle->getVConfiguration())
			{
				for(size_t u=0;u<nb_user_type; ++u)
				{
					maxcap[u]=max(maxcap[u],configuration->getCapacity()[u]);
				}
			}
		}

		myfile.close();
	}
	else{
		cout << "Unable to open file: "<<instance_csv <<" "<<__FILE__<<__LINE__<<endl;
		abort();
	}
}
/**
 *  Lecture de l'instance
 */
void Instance::readNodesGIHP(std::string instance_csv) {
	ifstream myfile(instance_csv.c_str());
	string line;

	if (myfile.is_open()){
		myfile >> nb_request;      	myfile.ignore(); getline(myfile,line);
		myfile >> nb_user_type;		myfile.ignore(); getline(myfile,line);

		//Creating objects
		dataRequests.resize(2*nb_request); // allocate memory
		//pickups
		for (size_t i = 0; i <nb_request; ++i) 	dataRequests[i]=new Node(i, 0,  nb_request+i,nb_user_type);
		//deliveries
		for (size_t i = nb_request; i <2*nb_request; ++i) 	dataRequests[i]=new Node(i, 1, i-nb_request, nb_user_type);
		//fill up dataRequest
		pickup.resize(nb_request);
		for (size_t i = 0; i <nb_request; ++i) 		pickup[i]= dataRequests[i];
		delivery.resize(nb_request);
		for (size_t i = 0; i <nb_request; ++i) 		delivery[i]=dataRequests[nb_request+i];

		//Time window : node, a, b && Boarding & landing : node, temps
		for (size_t i = 0; i < 1; ++i) getline(myfile,line);
		for (size_t i = 0; i < 2*nb_request; ++i) {
			myfile >> dataRequests[i]->id;
			myfile >> dataRequests[i]->idsym;
			myfile >> dataRequests[i]->type;
			myfile >> dataRequests[i]->tw_start;
			myfile >> dataRequests[i]->tw_end;
			myfile >> dataRequests[i]->stime;
			myfile >> dataRequests[i]->maxRT;
			myfile >> dataRequests[i]->cap;
			for (size_t u = 0; u < nb_user_type; ++u) {
				myfile >> dataRequests[i]->rload[u];
			}
			myfile >> dataRequests[i]->lon;
			myfile >> dataRequests[i]->lat;
			myfile >> dataRequests[i]->name;
			getline(myfile,dataRequests[i]->adress);
			dataRequests[i]->adress.erase(0,1);
		}

		//depots data
		auto id=0;
		auto idsym=0;
		auto type=0;
		myfile >> id;
		Node * depot;
		while(!myfile.eof()){
			myfile >> idsym;
			myfile >> type;
			depot = new Node(id, type, idsym, nb_user_type);
			myfile >> depot->tw_start;
			myfile >> depot->tw_end;
			myfile >> depot->stime;
			myfile >> depot->maxRT;
			myfile >> depot->cap;
			for (size_t u = 0; u < nb_user_type; ++u) {
				myfile >> depot->rload[u];
			}
			myfile >> depot->lon;
			myfile >> depot->lat;
			myfile >> depot->name;
			dataDepot.push_back(depot);
			getline(myfile,depot->adress);
			depot->adress.erase(0,1);
			maxNbRoutes+=depot->cap;
			myfile >> id;
			if(depot->type==4 || depot->type==2) nb_depot++;
		}

		// departure and arrival depot
		maxNbRoutes=0;
		for (size_t i = 0; i < dataDepot.size(); ++i) {
			if(dataDepot[i]->type==2){ //departs
				depot_depart.push_back(dataDepot[i]);
			} else if(dataDepot[i]->type==3){ // arrival
				depot_arrival.push_back(dataDepot[i]);
			} else{ //Both
				depot_depart.push_back(dataDepot[i]);
				depot_arrival.push_back(dataDepot[i]);
			}
			maxNbRoutes+=dataDepot[i]->cap;
		}

		dataNodes=dataRequests;
		for(auto n: dataDepot){
			dataNodes.push_back(n);
		}
		nb_depot=depot_depart.size();
		nb_node=dataDepot.size()+dataRequests.size();
	}else{
		cout << "Unable to open file: "<<instance_csv <<__FILE__<<__LINE__<<endl;
		abort();
	}
}


/*
 * Ecrire une instance Gihp dans un fichier
 */

void Instance::write_instance_gihp(std::string name, const std::vector<size_t>& getAllReqClust) {
	//
	std::vector<size_t> depots_arrive;
	std::vector<size_t> nodesCluster;
	for(auto& i : getAllReqClust){
		nodesCluster.push_back(i);
	}
	for(auto& j:getAllReqClust){
		nodesCluster.push_back(dataRequests[j]->idsym);
	}
	for(auto& d : depot_depart){
		depots_arrive.push_back(d->id);
		nodesCluster.push_back(d->id);
	}
	for(auto& d : depot_arrival){
		if(d->type==3){
			depots_arrive.push_back(d->id);
			nodesCluster.push_back(d->id);
		}
	}
	write_nodes_gihp(name+".node", depots_arrive, getAllReqClust);
	write_distance_gihp(name+".dist", nodesCluster);
	write_times_gihp(name+".time", nodesCluster);
	write_vehicles_gihp(name+".vehi");
}

//TODO sam je ne comprend pas pourquoi cette methode est si compliquée...
stringstream Instance::write_nodes_gihp(string path, vector<size_t>& depots_arrive, const  std::vector<size_t>& getAllReqClust){
	ofstream fichier;
	fichier.open(path.c_str(), ios::out | ios::trunc);  // ouverture en écriture avec effacement du fichier ouvert
	//if(fichier){
	stringstream out;
	out << getAllReqClust.size() << "\trequests" <<endl;
	out << nb_user_type << "\tuser_types" <<endl;
	//Time window : noeud, a, b && Boarding & landing : node, temps

	out << "node\tsym_n\ttype\ttw_a\ttw_b\tstime\tmaxRT\tcap\tseat\twheelc\tlongitude\tlatitude\tname    \tadress\t"<<endl;
	int nb_depots=depots_arrive.size();
	int nb_request_in_cl=getAllReqClust.size();
	//int nb_request=nb_request;
	//pickups
	int id=0;
	for (auto& i : getAllReqClust) {
		out <<id<<"\t";
		out<< id + nb_request_in_cl << "\t"; //sol->getInst()->v_node[i]->idsym
		out<<dataNodes[i]->type << "\t";
		out << dataNodes[i]->tw_start<< "\t";
		out << dataNodes[i]->tw_end<< "\t";
		out << dataNodes[i]->stime<< "\t";
		out << fixed <<dataNodes[i]->maxRT<< "\t";
		out << dataNodes[i]->cap<< "\t";
		for (size_t j = 0; j < nb_user_type ; ++j) {
			out << dataNodes[i]->rload[j] << "\t";
		}
		out << dataNodes[i]->lon<< "\t";
		out << dataNodes[i]->lat<< "\t";
		out << dataNodes[i]->name<< "\t";
		out << dataNodes[i]->adress<<endl;
		++id;
	}
	//delivies
	for (auto& i : getAllReqClust) {
		out << id<<"\t";
		out<<id-nb_request_in_cl << "\t";
		out<<dataNodes[i+nb_request]->type << "\t";
		out << dataNodes[i+nb_request]->tw_start << "\t";
		out << dataNodes[i+nb_request]->tw_end << "\t";
		out << dataNodes[i+nb_request]->stime<< "\t";
		//out << 10000000 << "\t";
		out << dataNodes[i+nb_request]->maxRT << "\t";
		out << dataNodes[i+nb_request]->cap << "\t";
		for (size_t j = 0; j < nb_user_type ; ++j) {
			out << dataNodes[i+nb_request]->rload[j] << "\t";
		}
		out << dataNodes[i+nb_request]->lon << "\t";
		out << dataNodes[i+nb_request]->lat << "\t";
		out << dataNodes[i+nb_request]->name << "\t";
		out << dataNodes[i+nb_request]->adress << endl;
		++id;
	}
	//depots_depart depart
	for (auto& i : depots_arrive) {
		if(dataNodes[i]->type==2){
			out << id<<"\t";
			out<<id + nb_depots<< "\t";
			out<< dataNodes[i- nb_depots]->type << "\t";
			out << dataNodes[i- nb_depots]->tw_start<< "\t";
			out << dataNodes[i- nb_depots]->tw_end<< "\t";
			out << dataNodes[i- nb_depots]->stime<< "\t";
			out << dataNodes[i- nb_depots]->maxRT<< "\t";
			out << dataNodes[i- nb_depots]->cap<< "\t";
			for (size_t j = 0; j < nb_user_type ; ++j) {
				out << dataNodes[i- nb_depots]->rload[j] << "\t";
			}
			out << dataNodes[i- nb_depots]->lon<< "\t";
			out << dataNodes[i- nb_depots]->lat<< "\t";
			out << dataNodes[i- nb_depots]->name<< "\t";
			out << dataNodes[i- nb_depots]->adress<< endl;
			++id;
		}
	}
	//depots_arrive
	for (auto& i : depots_arrive) {
		out << id<<"\t";
		if(dataNodes[i]->type==3)
			out<<id - nb_depots<< "\t";
		else
			out<<id<< "\t";
		out<<dataNodes[i]->type << "\t";
		out << dataNodes[i]->tw_start<< "\t";
		out << dataNodes[i]->tw_end<< "\t";
		out << dataNodes[i]->stime<< "\t";
		out << dataNodes[i]->maxRT<< "\t";
		out << dataNodes[i]->cap<< "\t";
		for (size_t j = 0; j < nb_user_type ; ++j) {
			out << dataNodes[i]->rload[j] << "\t";
		}
		out << dataNodes[i]->lon<< "\t";
		out << dataNodes[i]->lat<< "\t";
		out << dataNodes[i]->name<< "\t";
		out << dataNodes[i]->adress<< endl;
		++id;
	}
	fichier << out.str();
	fichier.close();
	return out;
}

stringstream Instance::write_distance_gihp(std::string path, std::vector<size_t>& nodesCluster) const{
	ofstream fichier(path.c_str(), ios::out | ios::trunc);  // ouverture en écriture avec effacement du fichier ouvert
	stringstream out;
	out << "d_ij"<<endl<<"\t";
	int id=0;
	for (size_t i = 0; i < nodesCluster.size(); ++i) out << i<<"\t";  out <<endl;
	for (auto& i : nodesCluster) {
		out << id<<"\t"; 					//sauter deux mots
		for (auto& j : nodesCluster ) {
			out << distance_ij[i][j] << "\t";
		}
		out<<endl;
		++id;
	}
	fichier << out.str();
	fichier.close();
	return out;
}

stringstream Instance::write_times_gihp(std::string path, std::vector<size_t>& nodesCluster) const{
	ofstream fichier(path.c_str(), ios::out | ios::trunc);  // ouverture en écriture avec effacement du fichier ouvert
	stringstream out;
	out << "t_ij"<<endl<<"\t";
	int id=0;
	for (size_t i = 0; i < nodesCluster.size(); ++i) out << i<<"\t";  out <<endl;
	for (auto& i : nodesCluster) {
		out << id<<"\t"; 					//sauter deux mots
		for (auto& j : nodesCluster ) {
			out <<  time_ij[i][j] << "\t";
		}
		out << endl;
		++id;
	}
	fichier << out.str();
	fichier.close();
	return out;
}

stringstream Instance::write_vehicles_gihp(std::string path) const{
	ofstream fichier(path.c_str(), ios::out | ios::trunc);  // ouverture en écriture avec effacement du fichier ouvert
	stringstream out;
	out << nb_user_type <<"\tuser_types"<<endl;
	out <<"Name\tidVehi\tNb_Conf\tc_fixe\tc_time\tc_dist\tseat\twheelc"<<endl;
	for (size_t i = 0; i < vehicleTypes.size(); ++i) {
		out <<"VAN"<<i<<"\t";
		out<<vehicleTypes[i]->getId()<<"\t";
		out<<vehicleTypes[i]->getVConfiguration().size()<<"\t";
		out<<vehicleTypes[i]->getFixedCost()<<"\t";
		out<<vehicleTypes[i]->getTimeCost()<<"\t";
		out<<vehicleTypes[i]->getDistanceCost()<<"\t";
		for(auto& c : vehicleTypes[i]->getVConfiguration()){
			for (size_t j = 0; j <nb_user_type; ++j) {
				out << c->getCapacity()[j] << "\t";
			}
		}
		out << endl;
	}
	fichier << out.str();
	fichier.close();
	return out;
}
