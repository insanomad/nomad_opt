#ifndef INSTANC_H_
#define INSTANC_H_

#include <sstream>
#include <fstream>
#include <iostream>
#include <limits>
#include <stdlib.h>
#include <cstdlib>
#include <vector>

using namespace std;
class SolutionQuAndBard{
public:
	vector<vector<int>> tours;
	size_t nbride=0;
	string type=""; // instance type A or B
	size_t cta=0;  //constant to defined according to the first time a customer ca_1 is found in the istance file
	SolutionQuAndBard(string path, size_t numride){
		nbride=numride;
		cout<<path<<endl;
		size_t from=path.find("_");
		type=path.substr(from+1,1);
		if(type =="A") cta=95;  // this value works for A05X inst..if it's A10X you change it to 100-10=90
		else if(type=="B") cta=90;
		else abort();
		readSolution(path);
	}

	void readSolution(std::string instance_csv){
		ifstream myfile(instance_csv.c_str());
		string line="";
		string line2="";
		int data=-1;
		int nodeid=0;
		int cont=1;
		tours.resize(cont);
		vector<bool> found(nbride,false);
		vector<int> depot;
		bool test=true;
		int i=0;
		if (myfile.is_open())
		{
			for(auto i=0; i<21;++i) {myfile >> line;}
			line=""; //key
			while(myfile >> line2)
			{
				int place=line2.find("c");
				int place2=line2.find("ca");
				int place3=line2.find("cb");
				if (place==0 || place2==0 || place3==0){
					if(place2==0) {															// type A   type b
						nodeid=stoi(line2.substr(place+2)) + stoi(line2.substr(place+2))-cta;//95         90
					}else if(place3==0){
						nodeid=stoi(line2.substr(place+2)) + stoi(line2.substr(place+2))-(cta-1);//94        89
					}
					else if(place==0) nodeid=stoi(line2.substr(place+1));

					if (line.compare("c1000")==0 && line2.compare("c1000")==0) {
						cont++;
						tours.resize(cont);
						line="";
						line2="";
						test=true;
					}else if(found[nodeid] && nodeid<1000 ) {
						tours[cont-1].push_back(nodeid+nbride);
					}else {
						if(nodeid!=1000)found[nodeid]=true;
						tours[cont-1].push_back(nodeid);
					}
					//line=line2;
				}else if (test ){
					//myfile >> line2;
					//if (line2.compare("c1000")!=0)
					depot.push_back(stoi(line2));
					test=false;
				}else test=false;

				if (line2.compare("c1000")==0 &&line.compare("")==0) line=line2;
			}
		}else{
			cout<<"problems opening: "<<instance_csv<<endl;
		}

		tours.resize(tours.size()-1);
		for (size_t j = 0; j < tours.size(); ++j) {
			tours[j][0]=depot[j];
			tours[j].push_back(depot[j]);
			for (size_t i = 0; i < tours[j].size(); ++i) {
				cout<<tours[j][i]<<" ";
			}
			cout<<endl;
		}
	}

};
#endif /* INSTANC_H_ */
