
#ifndef INSTANC_H_
#define INSTANC_H_

#include <sstream>
#include <fstream>
#include <iostream>
#include <limits>
#include <stdlib.h>
#include <cstdlib>
#include <vector>

using namespace std;
class SolutionCordeau{
public:
	vector<vector<int> > tours;
	size_t nbride=0;

	SolutionCordeau(string path, size_t numride){
		nbride=numride;
		readSolution(path);
	}

	void readSolution(std::string instance_csv){
		ifstream myfile(instance_csv.c_str());
		string line;
		string line2;
		int data=-1;
		int data2=0;
		int cont=1;
		tours.resize(cont);
		if (myfile.is_open())
		{
			myfile >> cost;	myfile.ignore();
			while(myfile >> line2){ //line.find("Total")>line.length()
				//myfile.ignore();
				if (line2.find("(")<line2.length()){
					data2=stoi(line);
					if (data2==0 && data==0) {
						cont++;
						tours.resize(cont);
					}
					tours[cont-1].push_back(data2-1);
					data=data2;
				}
				line=line2;
			}
		}else{
			cout<<"problems opening: "<<instance_csv<<endl;
		}


		for (size_t j = 0; j < tours.size(); ++j) {
			tours[j][0]=(int)nbride*2;
			tours[j][tours[j].size()-1]=(int)nbride*2+1;
			for (size_t i = 0; i < tours[j].size(); ++i) {
				cout<<tours[j][i]<<" ";
			}
			cout<<endl;
		}
	}

	double cost=-1;

};
#endif /* INSTANC_H_ */
