
/*
 * Instance_construct.cpp
 *
 *  Created on: Apr 21, 2015
 *      Author: sam
 */

#include "../structure/Configuration.h"
#include "../../constant.h"
#include "../checker/Checker.h" //TODO attention ! références croisées !

#include <fstream>
#include <iostream>
#include <limits>
#include <cstdlib>
#include "Instance.h"
#include <sstream>

#include <cmath>

using namespace std;



/*
 * Read instance courdeau Laporte
 */
void Instance::readCourdeauOriginal(std::string instance_csv) {
	vector<double> x;
	vector<double> y;
	ifstream myfile(instance_csv.c_str());
	string line;
	double data;
	if (myfile.is_open())
	{
		nb_user_type=1;

		//readGIHP n, m, and umax
		myfile >> nb_vehicle_types;	myfile.ignore();
		myfile >> nb_request;      	myfile.ignore();
		nb_request=nb_request/2; //compulsory
		myfile >> data;      myfile.ignore();
		dur_max=D2Sd(data);
		myfile >> data;      		myfile.ignore();
		maxcap.resize(1);
		maxcap[0]=(int)data;
		maxNbRoutes=nb_vehicle_types;
		myfile >> data;		//myfile.ignore(); getline(myfile,line);
		maxridetime=D2Sd(data);

		nb_user_type=1;
		nb_depot=1;
		nb_node=2*nb_request +nb_depot;
		//creating objects
		dataRequests.reserve(2*nb_request);
		for (size_t i = 0 ; i < nb_request; ++i){ // pickups
			dataRequests.push_back(new Node(i, 0, nb_request+i, nb_user_type));
		}
		for (size_t i = nb_request; i <nb_request*2; ++i){ // deliveries
			dataRequests.push_back(new Node(i, 1,  i-nb_request, nb_user_type));
		}
		dataDepot.push_back(new Node(nb_request*2, 4, nb_request*2, nb_user_type));

		dataNodes.reserve(nb_node);
		dataNodes=dataRequests;
		dataNodes.push_back(dataDepot[0]); //b/o there is only one depot
		//dataNodes.push_back(new Node(nb_request*2, 2, nb_request*2+1, nb_user_type)); // depot in
		//dataNodes.push_back(new Node(nb_request*2+1, 3, nb_request*2, nb_user_type));// depot out
		//depot_depart.push_back(dataNodes[nb_node-2]);
		//depot_arrival.push_back(dataNodes[nb_node-1]);//idx last element
		depot_depart=dataDepot;
		depot_arrival=dataDepot;//single depot


		pickup.reserve(nb_request);
		for (size_t i = 0; i <nb_request; ++i){pickup.push_back(dataRequests[i]);
		}
		delivery.reserve(nb_request);
		for (size_t i = nb_request; i <nb_request*2; ++i){delivery.push_back(dataRequests[i]);
		}
		// creating vehicles
		vehicleTypes.push_back(new  VehicleType(0));
		vehicleTypes[0]->setDistanceCost(1); //needed
		vehicleTypes[0]->setTimeCost(0); //needed
		Configuration* conf=new Configuration(0,nb_user_type);
		conf->setCapacity(0, maxcap[0]);
		vehicleTypes[0]->getVConfiguration()[0]=conf;

		string n;

		//filling depot info
		myfile >> n;
		myfile >> depot_depart[0]->lat;		myfile.ignore();
		myfile >> depot_depart[0]->lon;		myfile.ignore();
		myfile >> data;      myfile.ignore();
		depot_depart[0]->stime=D2Sd(data);
		myfile >> depot_depart[0]->rload[0];   myfile.ignore();
		myfile >> data;     myfile.ignore();
		depot_depart[0]->tw_start=D2Sd(data);
		myfile >> data; myfile.ignore();
		depot_depart[0]->tw_end=D2Sd(data);
		depot_depart[0]->maxRT=dur_max;
		depot_depart[0]->cap=nb_vehicle_types;


		//		depot_arrival[0]=depot_depart[0]; fait avant!
		//		dataDepot.push_back(depot_depart[0]); fait avant!
		//		dataDepot.push_back(depot_arrival[0]); fait avant!
		double cc=0;
		//creating pickup info
		for (size_t i = 0; i < nb_request; ++i)
		{
			myfile >> n;
			myfile >> cc;		myfile.ignore();
			x.push_back(cc);
			myfile >> cc;		myfile.ignore();
			y.push_back(cc);
			myfile >> data;    myfile.ignore();
			pickup[i]->stime=D2Sd(data);
			myfile >> pickup[i]->rload[0];    myfile.ignore();
			myfile >> data;    myfile.ignore();
			pickup[i]->tw_start=D2Sd(data);
			myfile >> data;     myfile.ignore(); //getline(myfile,line);
			pickup[i]->tw_end=D2Sd(data);
			pickup[i]->maxRT=maxridetime;
		}
		//creating delivery infor
		for (size_t i = 0; i < nb_request; ++i) {
			myfile >> n;
			myfile >> cc;		myfile.ignore();
			x.push_back(cc);
			myfile >> cc;		myfile.ignore();
			y.push_back(cc);
			myfile >> data;      myfile.ignore();
			delivery[i]->stime=D2Sd(data);
			myfile >> delivery[i]->rload[0];      myfile.ignore();
			myfile >> data;      myfile.ignore();
			delivery[i]->tw_start=D2Sd(data);
			myfile >> data;      myfile.ignore(); //getline(myfile,line);
			delivery[i]->tw_end=D2Sd(data);
			delivery[i]->maxRT=maxridetime;
		}
		x.push_back(depot_depart[0]->lat); // depot in
		y.push_back(depot_depart[0]->lon);
		//		x.push_back(depot_depart[0]->lat); // depot out
		//		y.push_back(depot_depart[0]->lon);
		time_ij.resize(nb_node);
		distance_ij.resize(nb_node);
		for(int i=0;i<(int)time_ij.size();++i){
			time_ij[i].resize(nb_node);
			distance_ij[i].resize(nb_node);
			for(int j=0;j<(int)time_ij.size();++j){
				time_ij[i][j]=D2Sd(sqrt((x[i]-x[j])*(x[i]-x[j])+(y[i]-y[j])*(y[i]-y[j])));
				distance_ij[i][j]=time_ij[i][j];
			}
		}

		myfile.close();//close file
		if(cst::checkInstance) Checker(this).instCheck();

	}
	else{
		cout << "Unable to open "<<instance_csv<<" "<<__FILE__<<__LINE__<<endl;
		abort();
	}
}
