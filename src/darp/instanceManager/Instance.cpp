
/*
 * Instance.cpp
 *
 *  Created on: Apr 21, 2015
 *      Author: sam
 */

#include <fstream>
#include <iostream>
#include <sstream>
#include <limits>
#include <stdlib.h>
#include <cstdlib>
#include <cmath>
#include <math.h>
#include <algorithm>

#include "Instance.h"
#include "../structure/Configuration.h"


using namespace std;



void Instance::triangular_inequality(std::vector<std::vector<long> > &vv) {
	size_t n=vv.size();
	for (size_t k = 0; k <n ; ++k){
		//cout <<k<<endl;
		for (size_t i = 0; i <n ; ++i){
			for (size_t j = 0; j <n ; ++j){
				if(vv[i][j]>vv[i][k]+vv[k][j]){
					vv[i][j]=vv[i][k]+vv[k][j];
				}
			}
		}
	}
}

void Instance::forceTriangularInequality() {
	size_t n=time_ij.size();
	for (size_t k = 0; k <n ; ++k){
		for (size_t i = 0; i <n ; ++i){
			for (size_t j = 0; j <n ; ++j){
				if(time_ij[i][j] > time_ij[i][k]+time_ij[k][j]){
					time_ij[i][j] = time_ij[i][k]+time_ij[k][j];
					distance_ij[i][j]=distance_ij[i][k]+distance_ij[k][j];
				}
			}
		}
	}
}


void Instance::set_zero_diag(std::vector<std::vector< long> > &vv) {
	for (size_t j = 0; j <vv.size() ; ++j)
		vv[j][j]=0;
}

void Instance::near_sym_matrix(std::vector<std::vector<long> > &vv, double margin) {
	for (size_t j = 0; j <vv.size() ; ++j)
		for (size_t i = 0; i <vv.size() ; ++i)
			if(double(abs(vv[i][j]-vv[j][i]))/vv[i][j]>margin)
				vv[i][j]=min(vv[i][j],vv[j][i]);
}

void Instance::arange_matrix() {
	set_zero_diag(time_ij);
	set_zero_diag(distance_ij);
	near_sym_matrix(time_ij,2);
	near_sym_matrix(distance_ij,2);
	triangular_inequality(time_ij);
	triangular_inequality(distance_ij);
}

string Instance::to_gpx(std::string _file_export, std::vector<size_t> group) {
	stringstream out;
	out<<"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
	out<<"<gpx xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"http://www.topografix.com/GPX/1/0\" xsi:schemaLocation=\"http://www.topografix.com/GPX/1/0 http://www.topografix.com/GPX/1/0/gpx.xsd\">\n";
	int i;
	for (int id = 0; id < (int)group.size(); ++id) {
		i=(int)group[id];
		out<<"\t<rte>\n";
		out<<"\t\t<name>Dial:"<<i<<"\n"<<pickup[i]->getName()<<" to "<<delivery[i]->getName()<<"\nDistance:"<<distance_ij[i][i+nb_request]<<"</name>\n";
		out<<pickup[i]->get_gpx();
		out<<delivery[i]->get_gpx();
		out<<"\t</rte>";
	}
	for (int  i = 0; i < (int)nb_depot; ++i) {
		out<<"\t<rte>\n";
		out<<"\t\t<name>Vehicule Type:"<<i<<"\n"<<depot_depart[i]->getName()<<" to "<<depot_arrival[i]->getName()<<"\nDistance:"<<distance_ij[i+nb_request*2][i+nb_request*2+nb_depot]<<"</name>\n";
		out<<depot_depart[i]->get_gpx();
		out<<depot_arrival[i]->get_gpx();
		out<<"\t</rte>";
	}
	out<<"</gpx>";
	ofstream fichier(_file_export.c_str(), ios::out | ios::trunc);  // ouverture en écriture avec effacement du fichier ouvert
	if(fichier){
		fichier << out.str();
		fichier.close();
	}
	else cout << "Impossible d'ouvrir le fichier !" << endl;
	return out.str();
}

void Instance::arange_tw_pickup(double coef_marge) {
	for (size_t i = 0; i < (unsigned) nb_request; ++i) {
		//cout<<(unsigned long)((double)vv_time[i][i+nb_ride]*coef_marge)<<"\t"<<vp_delivery[i]->tw_end<< "\t"<< vp_pickup[i]->tw_start<<endl;
		if((unsigned long)((double)time_ij[i][i+nb_request]*coef_marge) > delivery[i]->tw_end - pickup[i]->tw_start){
			pickup[i]->tw_start = delivery[i]->tw_end - (unsigned long)(time_ij[i][i+nb_request]*coef_marge);
		}
	}
}

void Instance::arange() {
	arange_matrix();
	arange_tw_pickup(1);
}

Instance::~Instance() {
	for (auto  node :dataNodes) {
		delete node;
	}
	for (auto vehicle: vehicleTypes) {
		delete vehicle;
	}
}


bool Instance::tightenTimeWindows() {
	bool test=false;
	long lMax=depot_arrival[0]->tw_end;
	long eMin=depot_arrival[0]->tw_start;
	size_t lMaxDep=0;
	size_t eMinDep=0;
	for (Node * dep:depot_arrival) {
		if(lMax<dep->tw_end){
			lMax=dep->tw_end;
			lMaxDep=dep->id;
		}
	}
	for (Node * dep:depot_depart) {
		if(eMin<dep->tw_start){
			eMin=dep->tw_start;
			eMinDep=dep->id;
		}
	}

	//TODO pour tout les depots ://
	//modification e0 (ouverture depot depart) : min_i(e_i-t0i)
	//modification ln0 (fermeture depot arrive) : min_i(e_i-t0i)
	//modification l0 temps de trajet min entre les depots
	//modification en0 temps de trajet min entre les depots

	for(Node * p: pickup){
		Node * d=delivery[p->id];
		long ei=p->tw_start;
		long ein=d->tw_start;
		long lin=d->tw_end;
		long Li=std::min(p->maxRT,lin-ei);
		long di=p->stime;
		long t0i=time_ij[eMinDep][p->id];
		ei=std::max(ein-Li-di,ei); //maxridetime
		ei=std::max(ei,eMin+t0i); //travel time from erliest depot

		long li=p->tw_end;
		long tini=time_ij[p->id][d->id];
		li=std::min(lin-tini-di,li);

		ein=std::max(ein,ei+di+tini);
		lin=std::min(li+di+Li,lin);

		long tin0=time_ij[d->id][lMaxDep];
		long din=d->stime;
		lin=std::min(lin,lMax-din-tin0); //travel time to the latest depot

		if(d->tw_start!=ein || d->tw_end!=lin || p->tw_end!=li || p->tw_start!=ei){
			test=true;
			d->tw_start=ein;
			d->tw_end=lin;
			p->tw_end=li;
			p->tw_start=ei;
			p->maxRT=min(Li,lin-ei);
		}
	}
	return test;
}

std::ostream& operator<<(std::ostream& out, Instance& inst){
	out<<inst.serialize(10,false);
	return out;
}

/**
 * affiche l'instance
 * @param loop_limite limite d'affichage pour les boucles
 */
string Instance::serialize(size_t loop_limite, bool display_matrix) const {
	stringstream ss;
	ss<<"**************** Instance ****************"<<endl;
	ss << "n: " << nb_request <<endl;
	ss << "mt: " << nb_depot <<endl;
	ss << "u: " << nb_user_type <<endl;
	ss << endl << "Requests:" << endl;
	ss << dataRequests[0]->Title_cout();
	for (size_t i = 0; i < nb_request; ++i) {
		if (i==loop_limite){
			ss << "\t...->"<<nb_request<<endl;
			break;
		}
		ss << *dataRequests[i] <<endl;
	}
	for (size_t i = 0; i < nb_depot; ++i) {
		if (i==loop_limite){
			ss << "\t...->"<<nb_depot<<endl;
			break;
		}
		ss << *dataDepot[i] <<endl;
	}


	ss << endl << "Vehicle:" << endl;
	for (size_t i = 0; i < nb_depot; ++i) {
		if (i==loop_limite){
			ss << "\t...->"<<nb_depot<<endl;
			break;
		}
		ss << *vehicleTypes[i] <<endl;
	}

	if(display_matrix){
		ss << endl << "t_ij:" << endl;
		for (size_t i = 0; i < nb_node; ++i) {
			if (i==loop_limite){
				ss << "\t...->"<<nb_node<<endl;
				break;
			}
			for (size_t j = 0; j < nb_node; ++j) {
				if (j==loop_limite){
					ss << "...->"<<nb_node<<endl;
					break;
				}
				ss << time_ij[i][j]<< "\t" ;
			}
			ss << endl;
		}

		ss << endl << "d_ij:" << endl;
		for (int i = 0; i < nb_node; ++i) {
			if (i==loop_limite){
				ss << "\t...->"<<nb_node<<endl;
				break;
			}
			for (int j = 0; j < min(loop_limite,nb_node); ++j) {
				if (j==loop_limite){
					ss << "...->"<<nb_node<<endl;
					break;
				}
				//out << vv_distance[i][j]<< "\t" ;
			}
			ss << endl;
		}
		ss <<endl;
		//	ss << endl << "neighborhood:" << endl;
		//	for (int i = 0; i < vv_neighborhood.size(); ++i) {
		//		if (i==loop_limite){
		//			out << "\t...->"<<vv_neighborhood.size()<<endl;
		//			break;
		//		}
		//		out <<i<<":\t";
		//		for (int j = 0; j < min(loop_limite,(int)vv_neighborhood[i].size()); ++j) {
		//			if (i==loop_limite){
		//				out << "...->"<<vv_neighborhood[i].size()<<endl;
		//				break;
		//			}
		//			out << vv_neighborhood[i][j]<< "\t" ;
		//		}
		//		out << endl;
		//	}
		//	out <<endl;
	}
	ss << dur_min <<" min"<<endl;
	ss << dur_max <<" max"<<endl;
	ss << endl;
	return ss.str();
}

void Instance::randomizeTWpic() {
	//	Node * picPrev=pickup.back();
	auto d=delivery.begin();
	for (auto& p : pickup) {
//		size_t pos=pic->adress.find("\t");
//		long tw=atol(pic->adress.substr(pos).c_str());
//		long s=(tw/60)%10;
//		tw =tw+ 60*(10-s);  // round up to 10 mins multiple
//		pic->adress.resize(pos);
		//cout<<tw<<"\t"<<pic->adress<<"|"<<endl;
		//pour synchroniser les departs groupés : pas nécéssaire !
		//		if(picPrev->name==pic->name){
		//			pic->tw_start=picPrev->tw_start;
		//			p->tw_end=picPrev->tw_end;
		//		}else{


		p->tw_start=(*d)->tw_start - p->maxRT;
		p->tw_end=(*d)->tw_end - p->maxRT;
		cout<<"["<<p->tw_start<<","<<p->tw_end<<"]"<<" "<<"["<<(*d)->tw_start<<","<<(*d)->tw_end<<"]"<<endl;
		d++;
//		int rd= std::rand()%100;
//		if(rd<50) pic->tw_start=0; //pas de fenetre de temps
//		else if(rd<70) pic->tw_start=tw-45*60;
//		else if(rd<80) pic->tw_start=tw-30*60;
//		else if(rd<90) pic->tw_start=tw-20*60;
//		else if(rd<95) pic->tw_start=tw-10*60;
//		else pic->tw_start=tw-5*60;
//		rd= std::rand()%100;
//		if(rd<50) pic->tw_end=21*3600; //pas de fenetre de temps
//		else if(rd<70) pic->tw_end=tw+45*60;
//		else if(rd<80) pic->tw_end=tw+30*60;
//		else if(rd<90) pic->tw_end=tw+20*60;
//		else if(rd<95) pic->tw_end=tw+10*60;
//		else p->tw_end=tw+5*60;

	}
}

void Instance::randomizeTWdel() {
	Node * delPrev=delivery.back();
	for (auto& d : delivery){
		size_t pos=d->adress.find("\t");

		long tw=atol(d->adress.substr(pos).c_str());
//		long smod=(tw/60)%10; // arondi sup
//		tw =tw+60*(10-smod);
//		if((tw/60)%10!=0) abort();

		d->adress.resize(pos);

		if(delPrev->name==d->name){
			d->tw_start=delPrev->tw_start;
			d->tw_end=delPrev->tw_end;
		}else{
			/*
			int rd= std::rand()%100;
			if(rd<50) {
				d->tw_start=tw-mTw*5*60;
				d->tw_end=tw+mTw*5*60;
			}
			else if(rd<90) {
				d->tw_start=tw-mTw*10*60;
				d->tw_end=tw+mTw*20*60;
			}
			else{
				d->tw_start=max((long)6*3600,tw-45*60); //pas de fenetre de temps
				d->tw_end=min(tw+45*60,(long)12*3601);
			}
			 */
			// 15 mins TW
			d->tw_start=tw;
			d->tw_end=d->tw_start+15*60;
			cout<<"twD["<<d->tw_start<<"\t"<<d->tw_end<<"]"<<endl;
		}
		delPrev=d;
	}
}

void Instance::arrangeTWpic() {
//	for (auto& p : pickup) {
//		auto del=delivery[p->id];
//		// trajet direct
//		if(((long)time_ij[p->id][del->id]+p->stime + 15*60 ) > (del->tw_end - p->tw_start))
//			p->tw_start = del->tw_end - (long)time_ij[p->id][del->id]-p->stime - 15*60;
//		if((del->tw_start - p->tw_end) > (long)time_ij[p->id][p->idsym]+p->stime)
//			p->tw_end = del->tw_start - (long)time_ij[p->id][p->idsym]+p->stime;  // this is a largely enough tw_b
//		//ouverture minimale
//		if(p->tw_end-p->tw_start<20*60)
//			p->tw_start-=15*60;
//	}

	//tightening pickups
	tightenTimeWindows();

}

void Instance::setServiceTime(){
	for (auto pic : pickup) {
		auto del=delivery[pic->id];
		if(pic->rload[0]>0)
		{ 	//seat
			pic->stime=2*60;
			del->stime=1*60;
		}
		else//weelchair
		{
			pic->stime=5*60;
			del->stime=2*60;
		}
	}
}

void Instance::setMaxRT() {
	double delta=15*60; //let as double!
	for (auto& p : pickup) {
		auto& d=delivery[p->id];
		//RT=∆*[(t+∆)/∆]
		auto t=time_ij[p->id][p->idsym] ;
		p->maxRT=delta*ceil((t+delta)/delta);
		d->maxRT=p->maxRT;
		std::cout<<t<<" , "<<(t+delta)/delta<<" , RT "<<p->maxRT<<endl;

//		if(time_ij[p->id][p->idsym] + p->stime  < 15*60){
//			p->maxRT=30*60;
//			d->maxRT=30*60;
//		} else if(time_ij[p->id][p->idsym]+p->stime  < 30*60){
//			p->maxRT=45*60;
//			d->maxRT=45*60;
//		} else if(time_ij[p->id][p->idsym]+p->stime  < 45*60){
//			p->maxRT=75*60;
//			d->maxRT=75*60;
//		} else{
//			p->maxRT=std::max(time_ij[p->id][p->idsym]+p->stime + 15*60,(long)120*60);
//			d->maxRT=std::max(time_ij[p->id][p->idsym]+p->stime + 15*60,(long)120*60);
//		}
	}
}
