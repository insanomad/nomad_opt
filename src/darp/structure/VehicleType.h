
/*
 * Vehicle.h
 *
 *  Created on: Apr 22, 2015
 *      Author: sam
 */

#ifndef VEHICLETYPE_H_
#define VEHICLETYPE_H_

#include <ostream>
#include <vector>
#include <bitset>
#include "Configuration.h"
#include "Node.h"


class VehicleType {
public:
	VehicleType();
	VehicleType(int id);
	virtual ~VehicleType();

const std::vector<int>& getCapBounds() const {
		return cap_bounds;
	}

	void setCapBounds(const std::vector<int>& capBounds) {
		cap_bounds = capBounds;
	}

	double getDistanceCost() const {
		return distance_cost;
	}

	void setDistanceCost(double distanceCost = 0) {
		distance_cost = distanceCost;
	}

	double getFixedCost() const {
		return fixed_cost;
	}

	void setFixedCost(double fixedCost = 0) {
		fixed_cost = fixedCost;
	}

	int getId() const {
		return id;
	}

	void setId(int id = 0) {
		this->id = id;
	}

	const std::string& getName() const {
		return name;
	}

	void setName(const std::string& name = "") {
		this->name = name;
	}

	double getSpeed() const {
		return speed;
	}

	void setSpeed(double speed = 0) {
		this->speed = speed;
	}

	double getTimeCost() const {
		return time_cost;
	}

	void setTimeCost(double timeCost = 0) {
		time_cost = timeCost;
	}

	double getTotalCost() const {
		return total_cost;
	}

	void setTotalCost(double totalCost = 0) {
		total_cost = totalCost;
	}

	const std::vector<Configuration*>& getConfiguration() const {
		return v_configuration;
	}

	std::vector<Configuration*>& getVConfiguration() {
		return v_configuration;
	}

	Configuration*& configuration(int i){
		return v_configuration[i];
	}

	void setConfiguration(Configuration* c, int type){
		v_configuration[type]=c;
	}

	void setConfigurations(const std::vector<Configuration*>& configuration) {
		v_configuration = configuration;
	}

	std::bitset<64> getBitSet(std::vector<int>& l);
	void generateBitSet();
	void printBitSet();

	/**
	 * @param a load to test
	 * @return -1 when the required position is out of range
	 */
	int getBitSetPosition(std::vector<int>& a);
	bool isDominated (const std::vector<int>& a, const std::vector<int>& b);
	void computeCapacityBounds();

private:
	//parametres
	int id=0;
	std::vector<Configuration*> v_configuration;
	double fixed_cost=0;
	double time_cost=0;
	double distance_cost=0;
	double total_cost=0;
	double speed=0;

	std::vector<std::bitset<64> > cap_set;
	std::vector<int> cap_bounds;
	int dim=0; // max(cap_bounds)
	std::string name="";

    /*******************Redefines the operator (=) *****************/
    std::vector<Configuration> v_conf_temp;
    VehicleType& operator=(const VehicleType& a);

};

std::ostream& operator<<(std::ostream&, const VehicleType&); //permet d'utiliser cout

#endif /* VEHICLETYPE_H_ */
