
/*
 * Configuration.cpp
 *
 *  Created on: Apr 21, 2015
 *      Author: sam
 */

#include "Configuration.h"

Configuration::Configuration() {
	id=0;
	nb_user_type=1;
	v_capacity.resize(1);
}

/*
 * Constructor
 * !i 	id
 * !nb_user_type	number of user types
 */
Configuration::Configuration(int _i, int _nb_user_type) {
	id=_i;
	nb_user_type=_nb_user_type;
	v_capacity.resize(nb_user_type);
}


Configuration::~Configuration() {
	v_capacity.clear();
}

Configuration& Configuration::operator=(const Configuration& a){
	this->v_capacity.clear();
	this->v_capacity=a.v_capacity;
	this->id=a.id;
	return *this;
}

/*
 * Affichage avec cout<<
 */
std::ostream& operator<<(std::ostream& out, const Configuration& f){
   out <<"cf"<< f.getId()<<"[";
   for (size_t i = 0; i < f.getCapacity().size(); ++i) {
	   out << f.getCapacity()[i];
	   if (i < f.getCapacity().size()-1) out<<",";
   }
   out <<"]";
   return out;
}
