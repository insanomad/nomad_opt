/*
 * CostInsertions.cpp
 *
 *  Created on: Oct 28, 2015
 *      Author: oscarts
 */

#include "RequestInfo.h"
#include <limits.h>


/**
 * minimun constructor.
 * @param pickup pointer
 * @param delivery pointer
 * @idtour route id, by default is set to -1
 */
RequestInfo::RequestInfo(NodeSol* pickup, NodeSol* delivery, int idtour){
	deltacost=INT_MAX;
	tourid= idtour;
	p=pickup;
	i=-1;
	d=delivery;
	j=-1;
	vehicle=nullptr;
	isfeasible=false;
}

/*
 * Destructor..
 */
RequestInfo::~RequestInfo() {
}
/*
 * Overloading operator for sorting
 */
bool operator<(const RequestInfo &s1, const RequestInfo &s2){
    if(s1.deltacost < s2.deltacost)
        return true;
    else
        return false;
}
/*
 * overloading print
 */

std::ostream& operator<< (std::ostream& out, const RequestInfo& req){
	out<<"(F:"<<req.isfeasible<<", CT "<< req.deltacost<<", Tid"<<req.tourid <<", Pid "<<req.p->id<<", Did "<<req.d->id<<", P_i "<<req.i<<" "<<", p_j "<<req.j<<")";
	return out;
}
