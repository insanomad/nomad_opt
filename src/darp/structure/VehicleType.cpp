
/*
 * Vehicle.cpp
 *
 *  Created on: Apr 22, 2015
 *      Author: sam
 */

#include "VehicleType.h"
#include <iostream>
#include <cmath>

VehicleType::VehicleType() {
	id=0;
	fixed_cost=0;
	time_cost=0;
	distance_cost=0;
	total_cost=0;
	speed=0;
	name="";
}

/*
 * Constructeur
 */
VehicleType::VehicleType(int _id) {
	id=_id;
	fixed_cost=0;
	time_cost=0;
	distance_cost=0;
	total_cost=0;
	speed=0;
	v_configuration.resize(1);
	dim=0;
	name="";
}

VehicleType::~VehicleType() {
	for (size_t i = 0; i < v_configuration.size(); ++i) {
		delete v_configuration[i];
	}
	v_configuration.clear();
	v_conf_temp.clear();
	dim=0;
}

std::bitset<64> VehicleType::getBitSet(std::vector<int>& load) {
	std::bitset<64> temp(0); // all bits to 0, b/0 it's 0 in binary
	int pos=getBitSetPosition(load);
	//std::cout<<">>>>>>>>>>>> pos: "<<pos<< std::endl;
	if(pos>=0){
		temp=cap_set[pos];
		//std::cout<<"pos"<<std::endl;
		//printBitSet();
	}
	return temp;
}

int VehicleType::getBitSetPosition(std::vector<int>& a){
	int pos=0;
	for(size_t i=0;i < a.size();++i){
		pos+=a[i]*pow(dim,i);  // S is allways squared matrix
		if(a[i] > cap_bounds[i]) {
			pos=-1;
			break;
		}
	}
	return pos;
}

void VehicleType::generateBitSet() {
	computeCapacityBounds();
	cap_set.resize(pow(dim, v_configuration[0]->getNbUserType()));
	bool dominated=true;
	int count=1;
	std::vector<int> point(v_configuration[0]->getNbUserType(),0);
	//std::cout<<conf[0]<<" "<<conf[1]<<" "<<conf[2]<<std::endl;
	for(auto& s : cap_set){
		//std::cout<<point[0]<<" "<<point[1]<<std::endl;
		for(auto& c : v_configuration){
			dominated=isDominated(point,c->getCapacity());
			if(dominated){
				s.set(c->getId());
				//std::cout<<s<<std::endl;
			}
		}
		//counter
		for(int u=0;u< v_configuration[0]->getNbUserType();++u){
			if((count % (int)pow(dim,u)) == 0){
				point[u]++;
				if(point[u]>=dim) {
					point[u]=0;
				}
			}
		}
		count++;
	}
	//printBitSet();
}

void VehicleType::printBitSet(){
	for(auto& s: cap_set){
		std::cout<<s<<" "<<std::endl;
	}
}
// true if a_i <= b_i for all i
bool VehicleType::isDominated(const std::vector<int>& a, const std::vector<int>& b){
	bool dominated=true;
	if(a.size()==b.size()){
		for(size_t i=0; i<a.size();++i){
			if(a[i]>b[i]){
				dominated=false;
				break;
			}

		}
	}else{
		std::cout<<"ERROR points doesn't have same dimension "<<__FILE__<<__LINE__<<std::endl;
	}
	return dominated;
}

// TODO test
void VehicleType::computeCapacityBounds() {
	dim=0;
	cap_bounds.resize(v_configuration[0]->getNbUserType(),0);
	for(auto& c : v_configuration){
		int i=0;
		for(int Q :c->getCapacity()){
			if(Q > dim){
				dim=Q;
			}
			if(cap_bounds[i] < Q) {
				cap_bounds[i]=Q;
			}
			++i;
		}
	}
	++dim; //b/o 0 counts as another row if the bit set
}






















/*******************Redefines the operator = *****************/
std::vector<Configuration> v_conf_temp;

VehicleType& VehicleType::operator=(const VehicleType& a){

	this->id=a.id;

	this->fixed_cost=a.fixed_cost;
	this->time_cost=a.time_cost;
	this->distance_cost=a.distance_cost;

	//	this->n_start=a.n_start;
	//	this->n_end=a.n_end;

	//part to clone config class
	this->v_configuration=a.v_configuration;
	unsigned long nc=a.v_configuration.size();
	this->v_configuration.resize(nc);
	this->v_conf_temp.resize(nc);
	for (size_t i=0; i<nc; i++) {
		v_conf_temp[i]=*a.v_configuration[i];
		this->v_configuration[i]=&v_conf_temp[i];
	}
	// v_conf_temp.clear();
	return *this;
}
/*
 * affichage avec cout<<
 */
std::ostream& operator<<(std::ostream& out, const VehicleType& f){
	out << "Vehicle type\t" << f.getId();
	for (size_t i = 0; i < f.getConfiguration().size(); ++i) {
		out<< "\t" <<*f.getConfiguration()[i];
	}
	out << "\tfc:"<<f.getFixedCost()<< "\tdc:"<<f.getDistanceCost()<< "\ttc:"<<f.getTimeCost();
	return out;

}


