
/*
 * Configuration.h
 *
 *  Created on: Apr 21, 2015
 *      Author: sam
 */

#ifndef CONFIGURATION_H_
#define CONFIGURATION_H_

#include <vector>
#include <ostream>

class Configuration {
public:
	Configuration();
	Configuration(int _id , int _nb_user_type);
	virtual ~Configuration();

	int getId() const {
		return id;
	}
	int getNbUserType() const {
		return nb_user_type;
	}
	const std::vector<int>& getCapacity() const {
		return v_capacity;
	}
	void setCapacity(int type, int capacity) {
		v_capacity[type] = capacity;
	}



	 /***************** this is used to clone Configuration objects***********************/
	 Configuration& operator=(const Configuration& a);

private:
	int id;
	int nb_user_type;
	std::vector<int> v_capacity;		//size : nb_user_type
    

};

std::ostream& operator<<(std::ostream&, const Configuration&); //permet d'utiliser cout

#endif /* CONFIGURATION_H_ */
