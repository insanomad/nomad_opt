
/*
 * Node.cpp

 *
 *  Created on: Apr 21, 2015
 *      Author: sam
 */

#include <sstream>
#include "Node.h"

using namespace std;

Node::Node() {
	id=0;
	type=0;
	rload.resize(0);
	idsym=0;
	maxRT=INT64_MAX;
}

Node::Node(int _id, int _type, int id_sym, int nbUseType) {
	id=_id;
	type=_type;
	rload.resize(nbUseType);
	idsym=id_sym;
	maxRT=INT64_MAX;
}

Node::~Node() {
	rload.clear();
}

string Node::Title_cout() const{
	return "Reecrire\tid\ttype\tn_sym\ttw_a\ttw_b\ttime\tid_to\tpo_to\n";
}

std::ostream& operator<<(std::ostream& out, const Node& f){
   return out << "Node:\t" << f.id <<
		   "\t" << f.type <<
		   "\t" << f.idsym <<
		   "\t" << f.tw_start <<
		   "\t" << f.tw_end ; }

std::string Node::get_gpx() {
	stringstream out;
	out<<"\t\t<rtept lat=\""<<lat<<"\" lon=\""<<lon<<"\">\n";
		out<<"\t\t\t<desc>Point:"<<id<<"\n"<<name<<"\n"<<adress<<"\nTW:["<<tw_start<<","<<tw_end<<"]\nL:["<< rload[0];
		for (size_t i = 1; i < rload.size(); ++i) {
			out<<","<< rload[i];
		}
		out<<"]</desc>\n";
		out<<"<name>"<<name<<"</name>\n";
		if(type==0) out<<"\t\t\t<sym>Triangle</sym>\n"; //http://raymarine.ning.com/forum/topics/gpx-files?xg_source=activity
		else if(type==1) out<<"\t\t\t<sym>Flag, Blue</sym>\n";
		else if(type==3) out<<"\t\t\t<sym>Car</sym>\n";
	out<<"\t\t</rtept>\n";
	return out.str();
}
