
/*
 * NodeinSol.h
 *
 *  Created on: Jun 9, 2015
 *      Author: sam
 */

#ifndef NODESOL_H_
#define NODESOL_H_

#include "Node.h"

class NodeSol {
public:
	NodeSol();
	NodeSol(Node* n);
	NodeSol(NodeSol *n);
	NodeSol(size_t nid, Node* n, int ntype);

	virtual ~NodeSol();

	//void update(Node_in_Sol *n);

	//variables
	Node * node;
	std::vector<int> load;		/*<cumulated load of the vehicle at departure of the node (size: nb_user_type) */
	long time=0;					/*<time at departure of the node*/
	long FTS;   					/*<forward time slack*/
	//computation variables
	long tnext=0;					/*<duree du voyage jusqu'au node suivant*/
	//node info
	size_t* id_round;				/*<id of the round of the current node*/
	size_t position=0;				/*<index of the current node in its round*/
	size_t* posSym=nullptr;
	int id;							/*< check this id is used...*/
	//unused but referenced in svg class
	long BTS=0;
	long tTot=0;
	long tTard=0;
	//depot id
	int typeDepot; 					/*<2 departure, 3 arrival depot. */

	int idConfig; 				/*< id of the configuration to use */

	/**
	 * export to gpx format
	 * @return string
	 */
	std::string to_gpx();

	int getDepotType() const { return typeDepot;}

	void setDepotType(int depotType) { this->typeDepot = depotType;}

	long getBts() const {
		return BTS;
	}

	void setBts(long bts = 0) {
		BTS = bts;
	}

	long getFts() const {
		return FTS;
	}

	void setFts(long fts) {
		FTS = fts;
	}

	int getId() const {
		return id;
	}

	void setId(int id) {
		this->id = id;
	}

	size_t* getIdRound() const {
		return id_round;
	}

	void setIdRound(size_t* idRound) {
		id_round = idRound;
	}

	const Node* getNode() const {
		return node;
	}

	void setNode(Node* node) {
		this->node = node;
	}

	size_t getPosition() const {
		return position;
	}

	void setPosition(size_t position = 0) {
		this->position = position;
	}

	long getTime() const {
		return time;
	}

	void setTime(long time = 0) {
		this->time = time;
	}

	long getTnext() const {
		return tnext;
	}

	void setTnext(long tnext = 0) {
		this->tnext = tnext;
	}

	long getTard() const {
		return tTard;
	}

	void setTard(long tard = 0) {
		tTard = tard;
	}

	long getTot() const {
		return tTot;
	}

	void setTot(long tot = 0) {
		tTot = tot;
	}

	const std::vector<int>& getLoad() const {
		return load;
	}

	void setLoad(const std::vector<int>& load) {
		this->load = load;
	}

	int getIdConfig() const {
		return idConfig;
	}

	void setIdConfig(int idConfig) {
		this->idConfig = idConfig;
	}
};

#endif /* NODESOL_H_ */
