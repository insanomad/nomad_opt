
/*
 * Tour.cpp
 *
 *  Created on: Apr 21, 2015
 *      Author: sam
 */

#include "Tour.h"
#include <iostream>
#include <sstream>
#include <algorithm> //std::sort

#include <iomanip>
#include "../../toolbox/TimeEvaluation.h"
#include "../../toolbox/CapacityEvaluation.h"
#include "../../constant.h"
#include "../../toolbox/InsertList.h"

//#include "../../toolbox/svg/Svg.h"

using namespace std;


struct SortVehicles{
	inline bool operator() (const std::pair<double ,VehicleType*>& v1, const std::pair<double ,VehicleType*>& v2) {
		return (v1.first < v2.first);
	}
};

// Constructor
Tour::Tour(Instance* ins, NodeSol* depart ,NodeSol* arrive, VehicleType* vehicle, TimeEvaluation* Te, CapacityEvaluation* Ce){
	id=0;
	inst=ins;
	tij=0;
	teval=Te;
	ceval=Ce;
	vehicleUtilization=0;
	//depot nodes
	v_node.push_back(depart);
	v_node.push_back(arrive);
	depart->id_round=&this->id;
	arrive->id_round=&this->id;
	vt=vehicle;
	initTour();
	reinitVehicleList();
}

Tour::~Tour() {
}

void Tour::initTour(){
	if(cst::costRideTimes)cost_ridetime=0.0001;
	else cost_ridetime=0;
	sumridetimes=0;
	duration=0;  // initialize always to 0 ..to avoid bias in the final solution cost TODO sam : c'est faux dans le cas general ou un dépot d'arrivée serait diférent d'un depot de départ, OT: non car un tournee à 0 veut dire pas útilisé de tout!
	distance=0;  // initialize always to 0 ..to avoid bias in the final solution cost
	cost=0;
	v_node[0]->position=0;
	v_node[1]->position=1;
	v_node[1]->posSym= &v_node[0]->position;
	v_node[0]->posSym=&v_node[1]->position;
	v_node[0]->time=v_node[0]->node->getTwStart();
	v_node[1]->time=v_node[0]->time+ v_node[0]->node->stime+inst->getTime()[v_node[0]->node->id][v_node[1]->node->id];
	reinitVehicleList();
}

void Tour::reinitVehicleList(){
	vehicles.clear();
	if(cst::QuAndBard){ // in Qu and Bard the tour doesnot change vehicle.
		vehicles.push_back(make_pair(vt->getFixedCost(),vt));
	}else{
		for(VehicleType* v : inst->vehicleTypes){
			vehicles.push_back(make_pair(v->getFixedCost(),v));
		}
	}
}

bool Tour::evalBestInsertion(RequestInfo& cbest){
	RequestInfo ctemp(cbest.p, cbest.d);
	cbest.deltacost=INT_MAX;
	//tester toutes les positions possibles pour p et d
	//TODO certaines info sont possiblement utilisable pour ne pas tester toutes les positions...
	for (int j = 2; j <= (int)v_node.size(); ++j) { 		//position pour d ; le premier et le dernier elements du vecteur sont le véhicule
		for (int i = 1; i < j; ++i) { 				//position pour p
			ctemp.i=i;
			ctemp.j=j;
			if(evalInsertion(ctemp)){                   //find best position and vehicle
				assert2(ctemp.isfeasible);
				if(ctemp.deltacost < cbest.deltacost){
					cbest=ctemp;
				}
			}
		}
	}
	//TODO set tour id une seulle fois ici
	return cbest.isfeasible;
}

// Performs a feasible insertion!
bool Tour::insertFeasibleRequest(RequestInfo& req){
	double costbefore=cost;
	addRequest(req);
	if(!updateFeasibleTourParameters(req.vehicle)){ //remise en question du vehicule
		cout<<"Inserting a feasible req is not feasible! "<<__FILE__<<__LINE__<<endl;
		cout<<req<<endl;
		cout<<this->str_tour()<<endl;
		return false;//the abort() is catched afterwars (debouging is easier)
	}
	req.deltacost=cost-costbefore; //TODO useless ?
	req.tourid=(int)this->id;
	return true;
}

// in this method we do look to find the cheapest vehicle
bool Tour::evalRemoval(RequestInfo& req){
	req.vehicle=vt;
	req.deltacost=cost;
	req.isfeasible=true;
	if (this->v_node.size()>4) { //SAM pourquoi 4 ?? resp: b/o cela corresponds à enlever la tournee complete
		if(removeRequest(req)){
			if(insertFeasibleRequest(req))
				return true;
		}
		cout<<"Remove a request results in infeasibility! "<<__FILE__<<__LINE__<<endl;
		cout<<this->str_tour()<<endl;
		cout<<req<<endl;
		return false;
	}
	return req.isfeasible;
}

void Tour::estimateVehiclesCost(const long & dist){
	for (auto vehicle:vehicles) {
		vehicle.first=vehicle.second->getFixedCost()+vehicle.second->getDistanceCost()*double(dist);
	}
	std::sort(vehicles.begin(), vehicles.end(), SortVehicles());
}

bool Tour::evalInsertion(RequestInfo& req){ //TODO remove Explain
	if (testNecessaryConditions(req)) // constant time necessary conditions
	{
		addRequest(req);
		if(teval->performScheduling(v_node)) // time testing
		{
			long newDist=computeDistance();  //getDistanceIncremental(req);
			if(cst::SortVehicle)
				estimateVehiclesCost(newDist); // optimized version
			req.vehicle=ceval->performCapacityTest(v_node, vehicles,newDist);
			if(req.vehicle != nullptr) // capacity testing
			{
				req.isfeasible=true;
				double newCost=req.vehicle->getFixedCost()
														+ (req.vehicle->getDistanceCost()*(newDist))
														+ req.vehicle->getTimeCost()*teval->getDuration(newDist) //TODO séparer proprement le calcul pour qu et bard
														+ cost_ridetime*teval->getSumridetimes();
				req.deltacost=newCost-cost;
				req.tourid=(int)this->id;
#ifdef DEBUG
				if (req.deltacost<-0.001){
					cout<<"***************************************************"<<endl;
					cout<<"[WARNING] : deltaCost negatif " <<__FILE__<<__LINE__<<endl;
					cout<<"\tdeltaCost="<<req.deltacost<<endl;
					cout<<"\tdeltaDist="<<newDist-distance <<endl;
					cout<<"\tdeltaTime="<<teval->getDuration(newDist)-duration<<endl;
					cout<<"\tdeltaFiCo="<<req.vehicle->getFixedCost()-vt->getFixedCost()<<endl;
					cout<<"\tvehicle_bf="<<*req.vehicle<<endl;
					cout<<"\tvehicle_bf="<<*vt<<endl;
					cout<<*this;
				}
#endif
				assert2(*req.p->posSym == req.d->position && *req.d->posSym== req.p->position);
				eraseRequest(req.i,req.j);
				return true;
			}
		}
		eraseRequest(req.i,req.j);  // time infeasible, positions are recomputed inside this method
	}
	req.isfeasible=false;  //compulsory
	return false;
}



// remove request and optimize times and vehicle type
bool Tour::removeRequest(RequestInfo& r){
	eraseRequest(r.i,r.j);
	if(!updateFeasibleTourParameters(nullptr)){ //remise en question du vehicule
		cout<<"Remove results in infeasibility!"<<__FILE__<<__LINE__<<endl;
		return false;  //abort();  // the abort is done in the calling methods. better to identify problems.
	}
	return true;
}

long Tour::getDistanceIncremental(RequestInfo& e){
	long dist;
	auto itr=v_node.begin()+e.i-1;
	if(e.i +1 < (e.j)){
		size_t idIm1=(*itr)->node->id;
		size_t idI=(*++itr)->node->id;
		size_t idIp1=(*++itr)->node->id;
		itr=v_node.begin()+e.j-1;
		size_t idJm1=(*itr)->node->id;
		size_t idJ=(*++itr)->node->id;
		size_t idJp1=(*++itr)->node->id;
		dist=distance
				+inst->distance_ij[idIm1][idI]+inst->distance_ij[idI][idIp1]
			    -inst->distance_ij[idIm1][idIp1]+inst->distance_ij[idJm1][idJ]
																																+inst->distance_ij[idJ][idJp1]  -inst->distance_ij[idJm1][idJp1];
		assert2(dist==computeDistance()); // remove
	}else{
		size_t idIm1=(*itr)->node->id;
		size_t idI=(*++itr)->node->id;
		size_t idJ=(*++itr)->node->id;
		size_t idJp1=(*++itr)->node->id;
		dist=distance +inst->distance_ij[idIm1][idI]+inst->distance_ij[idI][idJ]
				+inst->distance_ij[idJ][idJp1]-inst->distance_ij[idIm1][idJp1];
		assert2(dist==computeDistance());// remove
	}
	return dist;
}

// i pickup and j delivery
void Tour::eraseRequest(int i, int j){
	v_node.erase(v_node.begin()+j);
	v_node.erase(v_node.begin()+i);
	recomputePositions();
}

void Tour::addRequest(RequestInfo& req){
	v_node.insert(v_node.begin()+req.i,req.p);
	v_node.insert(v_node.begin()+req.j,req.d);
	req.p->id_round = &this->id;
	req.d->id_round = &this->id;
	req.p->id=req.p->node->id;
	req.d->id=req.d->node->id;
	recomputePositions();
	req.p->posSym=&v_node[req.j]->position;
	req.d->posSym=&v_node[req.i]->position;
}

void Tour::recomputePositions(int i){
	for (size_t k = i; k <v_node.size(); ++k)
		v_node[k]->position=k;
}

bool Tour::updateFeasibleTourParameters(VehicleType* newVt){
	bool res=true;
	if(v_node.size()>2){
		res=teval->performScheduling(v_node);
		teval->updateParameters();
		if(res==false){   // catching a possible bug!
			cout<<"update an infeasible tour ! "<<__FILE__<<__LINE__<<endl;
			cout<<this->str_tour()<<endl;
			return false;
		}
		distance=computeDistance();
		duration=teval->getDuration(distance);
		sumridetimes=teval->getSumridetimes();
		if(newVt==nullptr){ 							//remise en question du vehicule (après un removal)
			reinitVehicleList();
			ceval->updateVehicleListWithFeasibleVehicles(v_node, vehicles); //constituer la liste
			estimateVehiclesCost(distance);
			vt=ceval->performCapacityTest(v_node, vehicles, distance); //normalement le moins cher est trouvé tout de suite
			nbReconfigurations=ceval->getNbReconfigurations();
			assert2(vt!=nullptr);
		}else{
			vt=newVt;
			ceval->updateVehicleListWithFeasibleVehicles(v_node, vehicles); //constituer la liste

#ifdef DEBUG //tester si le vehicule propose est bien le moins cher
			if(!cst::QuAndBard){
				std::vector<std::pair<double ,VehicleType*> > vehiclesTmp;
				for(VehicleType* v : inst->vehicleTypes)
					vehiclesTmp.push_back(make_pair(v->getFixedCost(),v));
				ceval->updateVehicleListWithFeasibleVehicles(v_node, vehiclesTmp);
				bool oldSortVehic=cst::SortVehicle;
				cst::SortVehicle=false;
				VehicleType * cheapestVt=ceval->performCapacityTest(v_node, vehiclesTmp, distance);
				assert2(vt==cheapestVt);
				cst::SortVehicle=oldSortVehic;
			}
#endif

		}
		assert2(vehicles.size()>0);
		ceval->updateLoadParameters();  //USELESS ? //updateTourLoad(0, v_node.size()-1);

#ifdef DEBUG
		if(cst::BitSet) ceval->setIdConfigParameters(v_node, vt, &nbReconfigurations);
#endif
		computeCost();
	}else{
		initTour();
	}
	return res;
}

bool Tour::testNecessaryConditions(RequestInfo& e){
	auto nodeBeforeI=v_node[e.i-1];
	auto nodeAfterI=v_node[e.i];

	//necessary condition a_ins<=b_2
	if(nodeBeforeI->node->tw_start > e.p->node->tw_end){
		return false;
	}

	//necessary condition b_ins>=a_1
	if(nodeBeforeI->node->tw_start > e.p->node->tw_end){
		return false;
	}


	auto nodeAfterJ=v_node[e.j-1];
	auto nodeBeforeJ=v_node[e.j-2];

	//necessary condition a_ins<=b_2
	if(nodeBeforeJ->node->tw_start > e.d->node->tw_end){
		return false;
	}

	//necessary condition b_ins>=a_1
	if(nodeBeforeJ->node->tw_start > e.d->node->tw_end){
		return false;
	}

	// necessary condition 1: load
	for(size_t u=0;u < inst->nb_user_type; ++u){
		auto newLoad= nodeBeforeI->load[u]+e.p->node->rload[u];
		if(newLoad > inst->maxcap[u]) {
			return false;
		}
	}

	// necessary condition 2 pickup: Tp_a
	auto time_arrive = nodeBeforeI->node->tw_start + (long)inst->time_ij[nodeBeforeI->node->id][e.p->node->id] + nodeBeforeI->node->getStime();
	if( time_arrive > (long)e.p->node->tw_end) {
		return false;
	}

	// necessary condition 2 pickup: Tp_b
	auto time_arrive2= max(time_arrive,(long)e.p->node->tw_start) + (long)inst->time_ij[e.p->node->id][nodeAfterI->node->id] + e.p->node->getStime();
	if( time_arrive2 > (long)nodeAfterI->node->tw_end) {
		return false;
	}

	// necessary condition 3 delivery: Td_a
	time_arrive = nodeBeforeJ->node->tw_start + (long)inst->time_ij[nodeBeforeJ->node->id][e.d->node->id] + nodeBeforeJ->node->getStime();
	if( time_arrive > (long)e.d->node->tw_end) {
		return false;
	}

	// necessary condition 4 delivery: Ta_b
	time_arrive2= time_arrive + (long)inst->time_ij[e.d->node->id][nodeAfterJ->node->id] + e.d->node->getStime();
	if( time_arrive2 > (long)nodeAfterJ->node->tw_end) {
		return false;
	}

	return true;
}

long Tour::computeDistance(){
	long dist=0;
	auto nodeBefore=v_node.begin();
	for(auto node= (++v_node.begin()); node != v_node.end(); ++node){
		dist += inst->distance_ij[(*nodeBefore)->node->id][(*node)->node->id];
		++nodeBefore;
	}
	return dist;
}

void Tour::computeCost() {
	cost = vt->getFixedCost()
					+ vt->getDistanceCost()*double(distance)
					+ vt->getTimeCost()*double(duration)
					+ cost_ridetime*double(sumridetimes);
}

double Tour::computeVehicleUtilization(){
	vehicleUtilization=0;
	long totalCap=0;
	for(size_t n=1 ; n < v_node.size()-2; ++n ){
		if(v_node[n]->node->name != v_node[n+1]->node->name){
			for(size_t u=0 ; u < inst->getNbUserType(); ++u){
				totalCap+=vt->getConfiguration()[v_node[n]->getIdConfig()]->getCapacity()[u];
				vehicleUtilization += double(v_node[n]->load[u]);
			}
		}
	}
	vehicleUtilization/=double(totalCap);
	return vehicleUtilization;
}

void Tour::computeNbReconfigurations(){
	ceval->setIdConfigParameters(v_node, vt, &nbReconfigurations);

}
/******************************* Copy object methods************************/



void Tour::copyNodes(Tour * t, std::vector<NodeSol*> &v_revers){
	v_node.clear();
	v_node.resize(0);
	for (size_t i=0; i < t->v_node.size(); i++) {
		v_node.push_back(v_revers[t->v_node[i]->id]);
	}
	//remet tout les id_roud des nodes à l'ID du tour courant
	for (size_t i=0; i< t->v_node.size(); i++) {// importat
		v_node[i]->id_round=&this->id;
		v_node[i]->posSym=&v_revers[t->v_node[i]->node->idsym]->position;
	}
	v_node[0]->posSym=&v_node.back()->position;//importat..b/o in depots idSym may point to itself
	v_node.back()->posSym=&v_node[0]->position;
}


std::ostream& operator<<(std::ostream& out, const Tour& f)
{
	out<<"************* Tournée "<<f.getId()<<" ***************\n";
	out << f.str_tour();
	return out;
}


/*******************************Printing methods****************************************/

/**
 * afficher la liste des temps de la tournée point par point
 */
string Tour::str_tour() const{
	stringstream s;
	size_t totalwait=0;
	s<<"\n";
	s<<"Tour "<<getId();
	s<<"\n"<<"id_node";
	for (size_t j = 0; j < v_node.size(); ++j) {
		s<<"\t"<<v_node[j]->id;
	}
	s<<"\n"<<"times";
	for (size_t j = 0; j < v_node.size(); ++j) {
		s<<"\t"<<inst->S2Dd(v_node[j]->time);
	}
	s<<"\n"<<"type";
	for (size_t j = 0; j < v_node.size(); ++j) {
		s<<"\t"<<v_node[j]->node->type;
	}

	s<<"\n"<<"t(i;i+1)";
	for (size_t j = 0; j < v_node.size()-1; ++j) {
		s<<"\t"<<inst->S2Dd(inst->time_ij[v_node[j]->node->id][v_node[j+1]->node->id]);
	}
	s<<"\n"<<"wait";
	for (size_t j =0 ; j < v_node.size()-1; ++j) {
		size_t wait=v_node[j+1]->time - v_node[j]->time - v_node[j]->node->getStime()-inst->time_ij[v_node[j]->node->id][v_node[j+1]->node->id];
		s<<"\t"<<inst->S2Dd(wait);
		totalwait+=wait;
	}
	s<<"\n"<<"tw_a";
	for (size_t j = 0; j < v_node.size(); ++j) {
		s<<"\t"<<inst->S2Dd(v_node[j]->node->getTwStart());
	}
	s<<"\n"<<"tw_b";
	for (size_t j = 0; j < v_node.size(); ++j) {
		s<<"\t"<<inst->S2Dd(v_node[j]->node->getTwEnd());
	}
	s<<"\n"<<"stime";
	for (size_t j = 0; j < v_node.size(); ++j) {
		s<<"\t"<<inst->S2Dd(v_node[j]->node->getStime());
	}
	s<<"\n"<<"typeNode";
	for (size_t j = 0; j < v_node.size(); ++j) {
		s<<"\t"<<v_node[j]->node->getType();
	}
	s<<"\n"<<"Time";
	for (size_t j = 0; j < v_node.size(); ++j) {
		s<<"\t"<<inst->TimeString(inst->S2Dd(v_node[j]->time));
	}
	for(size_t u =0;u<inst->getNbUserType();++u){
		s<<"\n"<<"Load_u="<<u;
		for (size_t j = 0; j < v_node.size(); ++j) {
			s<<"\t"<<v_node[j]->load[u];
		}
	}
	s<<"\n"<<"ConfigId";
	for (size_t j = 0; j < v_node.size(); ++j) {
		s<<"\t"<<v_node[j]->idConfig;
	}

	if(vt!=nullptr)
	{
		s<<"\n"<< *getVehicle()<<"\n";
		//	out<<"NbReconf\t"<<f.getNbReconfigurations()<<"("<<f.getInst()->getMaxNbReconfigurations()<<")"<<"\n"; pas mis à jour

		if(cst::QuAndBard){
			s<<"Sum_ride_times\t"<<inst->S2Dd(sumridetimes)<<"\n";
			s<<"Sum_Distances\t"<<inst->S2Dd(distance)<<"\n";
			s<<"Route_Duration\t"<<inst->S2Dd(duration)<<"\n";
			s<<"Route_cost\t"<<inst->S2Dd(cost)<<"\n";

		}else{
			s<<"Sum_Distances\t"<<distance<<"\n";
			s<<"Route_Duration\t"<<duration<<"\n";
			s<<"Route_cost\t"<<cost<<"\n";
		}
	}

	s<<"\n"<<"Vehicle utilization:\t"<<vehicleUtilization;
	s<<"\n";
	return s.str();
}

std::string Tour::to_gpx() {
	stringstream out;
	out<<"\t\t<name>";
	out<<"Tour:"<<id<<"\n";
	out<<"VT:"<<vt->getId()<<"\n";
	out<<"TW:"<<v_node[0]->time<<"->"<<v_node.back()->time<<"\n";
	out<<"Cost"<<cost;
	out<<"\t\t</name>\n";
	for (auto& n:v_node) {
		out<<n->to_gpx();
	}
	return out.str();
}

