
/*
 * NodeSol.cpp
 *
 *  Created on: Jun 9, 2015
 *      Author: sam
 */

#include "NodeSol.h"

#include "sstream"

NodeSol::NodeSol() {
	id_round=nullptr;						//id of the round of the current node
	position=-1;				//index of the current node in its round
	id=-1;
	time=0;					//time at departure of the node
	tnext=0;
	node=nullptr;
	typeDepot=4;
	FTS=0;
	idConfig=0;
}

NodeSol::NodeSol(Node * n) {
	id=n->id;
	node=n;
	load.resize(n->rload.size());
	id=n->id;
	time=0;					//time at departure of the node
	tnext=0;
	id_round=nullptr;
	typeDepot=0;
	FTS=0;
	idConfig=0;
}


NodeSol::NodeSol( size_t nid, Node * n, int _typeDepot) {
	id=nid;  // it differs with n.id if n is a depot
	node=n;
	load.resize(n->rload.size());
	time=0;					//time at departure of the node
	tnext=0;
	id_round=nullptr;
	typeDepot=_typeDepot;
	FTS=0;
	idConfig=0;
}

NodeSol::~NodeSol() {
	load.clear();
}



NodeSol::NodeSol(NodeSol * n){ //TODO REMOVE
	abort();
	node=n->node;
	id=n->id;
	load=n->load;
	time=n->time;
	tnext=n->tnext;
	FTS=n->FTS;
	id_round=n->id_round;						//id of the round of the current node
	position=n->position;				//index of the current node in its round
	typeDepot=n->typeDepot;
	posSym=nullptr;
	BTS=n->BTS;
	tTot=n->tTot;
	tTard=n->tTard;
	idConfig=0;
}

std::string NodeSol::to_gpx() {
	std::stringstream out;
	out<<"\t\t<rtept lat=\""<<node->getLat()<<"\" lon=\""<<node->getLon()<<"\">\n";
		out<<"\t\t\t<desc>";
		out<<"Point:"<<id<<"\n";
		out<<node->getName()<<"\n";
		out<<node->getAdress()<<"\n";
		out<<"TW:["<<node->tw_start<<","<<node->tw_end<<"]\n";
		out<<"DL:["<< node->rload[0];
		for (size_t i = 1; i < node->rload.size(); ++i) {
			out<<","<< node->rload[i];
		}
		out<<"]\n";
		out<<"time:"<<time<<"\n";
		out<<"t_ij:"<<tnext<<"\n";
		out<<"L:["<< load[0];
		for (size_t i = 1; i < load.size(); ++i) {
			out<<","<< load[i];
		}
		out<<"]\n";
		out<<"\t\t\t</desc>\n";
		out<<"\t\t\t<name>"<<node->getName()<<"</name>\n";
		if(node->type==0) out<<"\t\t\t<sym>Triangle</sym>\n"; //http://raymarine.ning.com/forum/topics/gpx-files?xg_source=activity
		else if(node->type==1) out<<"\t\t\t<sym>Flag, Blue</sym>\n";
		else if(node->type==3) out<<"\t\t\t<sym>Car</sym>\n";
	out<<"\t\t</rtept>\n";
	return out.str();
}
