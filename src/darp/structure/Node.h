
/*
 * Node.h
 *
 *  Created on: Apr 21, 2015
 *      Author: sam
 */

#ifndef NODE_H_
#define NODE_H_

#include <vector>
#include "Configuration.h"
#include <ostream>
#include <climits>

/**
 * la classe des noeuds
 * @brief Noeud
 */

class Node{
public:
    /*
     * Constructeur simple
     */
    Node();
    
    /**
     * Constructeur du node
     * \param id node 0 pickup, 1 delivery, 2 depart depot, 3 arrival depot, 4 depot depart=arrive
     * \param type type de node
     * \param sym node symetrique
     * @param nb_user_type nombre de type d'utilisateur différent
     */
    Node(int idt, int type, int sym, int nb_user_type);
    
    /**
     * destructeur
     */
    virtual ~Node();

    /**
     * renvoie le point au format gpx
     * @return le point au format gpx
     */
    std::string get_gpx();

    /**
     * affichage des titres pour le print
     * @return les titres pour le print
     */
    std::string Title_cout() const;

	const std::string& getAdress() const {
		return adress;
	}

	long getCap() const {
		return cap;
	}

	size_t getId() const {
		return id;
	}

	size_t getIdsym() const {
		return idsym;
	}

	double getLat() const {
		return lat;
	}

	double getLon() const {
		return lon;
	}

	long getMaxRt() const {
		return maxRT;
	}

	const std::string& getName() const {
		return name;
	}

	const std::vector<int>& getRload() const {
		return rload;
	}

	long getStime() const {
		return stime;
	}

	long getTwEnd() const {
		return tw_end;
	}

	long getTwStart() const {
		return tw_start;
	}

	size_t getType() const {
		return type;
	}

    //parameters
    size_t id;						//identifiant
    size_t type;						//2:depot+, 3:depot-, 0:pickup, 1:delivery
    long tw_start=0;			//time window start
    long tw_end=0;			//time window end
    std::vector<int> rload;			//loading in or out (size: nb_user_type)
    size_t idsym;					//pickup<->delivery or depot+<->depot-

    long maxRT=LONG_MAX;			// Todo include on instance info
    long stime=0;
    long cap=1;//depot capacity

    /**
     * longitude & latitude
     */
    double lon=0;
    double lat=0;

    /**
     * adresse and name
     */
    std::string name="";
    std::string adress="";
};

/**
 * affichage avec cout <<
 */
std::ostream& operator<<(std::ostream&, const Node&); //permet d'utiliser cout


#endif /* NODE_H_ */
