/*
 * CostInsertions.h
 *
 *  Created on: Oct 28, 2015
 *      Author: oscarts
 */

#ifndef SRC_DARP_STRUCTURE_REQUESTINFO_H_
#define SRC_DARP_STRUCTURE_REQUESTINFO_H_
#include <cstdlib>

#include "NodeSol.h"
#include "VehicleType.h"

class RequestInfo {

public:

	//minimum constructor
	RequestInfo(NodeSol* pickup, NodeSol* delivery, int idtour=-1);

	virtual ~RequestInfo();


	//parameters
	double deltacost;
	int tourid;
	NodeSol* p;
	int i;
	NodeSol* d;
	int j;
	VehicleType* vehicle;
	bool isfeasible;
};

bool operator<(const RequestInfo &s1,const RequestInfo &s2);

std::ostream& operator<< (std::ostream& out, const RequestInfo& req);

#endif /* SRC_DARP_STRUCTURE_REQUESTINFO_H_ */
