
/*
 * Tour.h
 *
 *  Created on: Apr 21, 2015
 *      Author: sam
 */

#ifndef TOUR_H_
#define TOUR_H_

#include <vector>
#include <ostream>
#include <climits>
#include <sstream>
#include <list>

#include "../../toolbox/CapacityEvaluation.h"

#include "Node.h"
#include "VehicleType.h"
#include "../instanceManager/Instance.h"
#include "RequestInfo.h"
#include "../../toolbox/TimeEvaluation.h"
//#include "../../constant.h"
#include "../../toolbox/NonFeasibExplain.h"
#include "NodeSol.h"

/*
 * c'est la classe qui représente une tournée pour un vehicule
 */

//class TimeEvaluation;
class CapacityEvaluation;

class Tour {
public:

	/**
	 * @param ins : instance
	 * @param depart : dépot
	 * @param arrive : dépot
	 * @param vehicle
	 * @param Te : methode pour le scheduling
	 * @param Ce : methode pour le test de capacite
	 */
	Tour( Instance* ins, NodeSol* depart ,NodeSol* arrive, VehicleType* vehicle, TimeEvaluation* Te, CapacityEvaluation* Ce);
	void initTour();
	virtual ~Tour();

	/******************************** Core Methods ************************************/

	/**
	 * compute and sort the cost for all vehicles in vehicles
	 * @param the distance of the route
	 */
	void estimateVehiclesCost(const long & dist); 


	/**
	 *
	 */
	bool evalInsertion(RequestInfo& req);
	bool evalRemoval(RequestInfo& req);
	bool insertFeasibleRequest(RequestInfo& req);  // converted into boolean to catch the bug
	bool removeRequest(RequestInfo& req);
	bool evalBestInsertion(RequestInfo& req);
	//void reinitTour();
	//void removeSetOfRequestById(std::vector<int>& request);

	/**
	 * fait tout !
	 * teste le scheduling et la capacité sur une tournée
	 * calcule la performance
	 * @param _vehicle : le vehicule à tester
	 */
	bool updateFeasibleTourParameters(VehicleType* _vehicle);

	/**
	 * reinit the vehicle list with all the vehicles for the route
	 * the list is not filtred by feasibility !
	 */
	void reinitVehicleList();


	/************ ************************get/set/print Methods******************************/
	std::string str_tour() const;
	std::string to_gpx();

	/**
	 * This function returns a unique value to differentiate route. In qu&bard the vehhicle type make the difference in routes.
	 * this method is usseful in k-regret to abovid redundant computations
	 */
	double getHashkey() {
		if(cst::QuAndBard) hashkey = (double)(v_node[0]->id*v_node[v_node.size()-1]->id)*(vt->getId()+1)*v_node.size() + getCost();
		else hashkey = (double)(v_node[0]->node->id*v_node[v_node.size()-1]->node->id)*v_node.size() + getCost();
		return hashkey;
	}

	double getCost() const {return cost;}

	long getDistance() const {return distance;}

	long getDuration() const {return duration;}
	//copy
	/**
	 * met à jour v_node en reliant vers une autre slution que le tour t
	 * les parametres des node_in_sol dans v_revers NE sont PAS mis à jour
	 * @param t le tour origine
	 * @param v_revers le vecteur associé à la solution dans laquelle apparait "this"
	 */
	void copyNodes(Tour * t, std::vector<NodeSol*> &v_revers);

	void setCeval( CapacityEvaluation*& ceval) {
		this->ceval = ceval;
	}

	void setTeval( TimeEvaluation*& teval) {
		this->teval = teval;
	}

	double getCostRidetime() const {
		return cost_ridetime;
	}

	long getSumRidetimes() const {
		return sumridetimes;
	}

	size_t getId() const {
		return id;
	}

	void setId(size_t id) {
		this->id = id;
	}

	VehicleType* getVehicle() const{
		return vt;
	}

	CapacityEvaluation* getCeval() {
		return ceval;
	}

	const TimeEvaluation* getTeval() {
		return teval;
	}

	long getTij() const {
		return tij;
	}

	std::vector<NodeSol*>& getNodes() {
		return v_node;
	}


	long computeDistance();

	const Instance* getInst() const {
		return inst;
	}

	const std::vector<NodeSol*>& getNode() const {
		return v_node;
	}

	const std::vector<std::pair<double, VehicleType*> >& getVehicles() const {
		return vehicles;
	}

	/********************Debugging methods:  to remove once all test ok ***************************/
	bool twtest(int type);


	int getNbReconfigurations() const {
		return nbReconfigurations;
	}

	void setNbReconfigurations(int nbReconfigurations = 0) {
		this->nbReconfigurations = nbReconfigurations;
	}

	std::vector<NodeSol*> v_node;

	size_t id; // preferibly dont make it private

	//const size_t getNbNode() const{	return v_node.size();}

	int nbReconfigurations=0; /**<number of reconfigurations performed*/

	/**
	 * update position in nodes starting in node i. by default from the beginning till the end
	 */
	void recomputePositions(int i=0);
	void computeNbReconfigurations();
	double computeVehicleUtilization();

private:
	/**
	 * ajoute p et d en i et j without exaustive testing
	 * @param e request to be inserted
	 * @return false if not possible
	 */

	bool testNecessaryConditions(RequestInfo& e);
	void addRequest(RequestInfo& req);
	void eraseRequest(int i, int j);

	void computeCost();
	//void computeDetourTime(RequestInfo& r);
	//void computeDetourDistance(RequestInfo& r);

	/**
	 * compute distance in O(1)
	 * @param r request
	 * @return detour
	 */
	long getDistanceIncremental(RequestInfo& r);


	//parameters
	double cost=0;
	double cost_ridetime=0;
	double vehicleUtilization=0;
	long distance=0;
	long duration=0;
	long sumridetimes=0; /**<juste pour qu et bard*/


	Instance * inst;
	TimeEvaluation* teval;
	CapacityEvaluation* ceval;

	/**
	 * liste des vehicules disponible pour la tournée
	 */
	std::vector<std::pair<double ,VehicleType*> > vehicles;

	double hashkey;
	//computation variables
	long tij=0;

	VehicleType* vt;

};

/**
 * permetre l'affichage avec cout <<
 */
std::ostream& operator<<(std::ostream&, const Tour&); //permet d'utiliser cout


#endif /* TOUR_H_ */
